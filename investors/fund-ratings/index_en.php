	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>

 <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             Ratings
            </h2>
          </div>
        </div>
      </div>
</section>




    <!-- Ratings S&P -->
    <section class="ratings">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
            <div class="ratings-title mt-5">
				S&P Global Ratings
            </div>
			<div class="ratings-date mt-3">
				30.06.2020 
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="ratings">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">

		      <div class="container">
		        <div class="row">
        		  <div class="col-lg-3">
					<div class="ratings-item">
						<div class="ratings-item-title">
							<span>S&P Global</span>Ratings
		          		</div>
						<div class="ratings-item-text" style="background-color: #097b75;color: #FFF;">
						Long-term and short-term foreign currency issuer credit: ‘BB+’/‘B’; Outlook Stable
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-lg-3">
					<div class="ratings-item">
						<div class="ratings-item-title">
							<span>S&P Global</span>Ratings
		          		</div>
						<div class="ratings-item-text">
Long-term and short-term local currency issuer credit: ‘BB+’/‘B’; Outlook Stable
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-lg-3">
					<div class="ratings-item">
						<div class="ratings-item-title">
							<span>S&P Global</span>Ratings
		          		</div>
						<div class="ratings-item-text">
Long-term national scale «kzAA-»
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-lg-3">
					<div class="ratings-item">
						<div class="ratings-item-title">
							<span>S&P Global</span>Ratings
		          		</div>
						<div class="ratings-item-text" style="background-color: #097b75;color: #FFF;">
Senior unsecured domestic bonds «ВВ+»
		          		</div>
		          	</div>
		          </div>
        		</div>
		      </div>
          </div>
        </div>
      </div>
    </section>




    <!-- Ratings S&P -->
    <section class="ratings">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
            <div class="ratings-title mt-5">
				Fitch Ratings
            </div>
			<div class="ratings-date mt-3">
				30.12.2019
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="ratings">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">

		      <div class="container">
		        <div class="row">
        		  <div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
Long-term local currency IDR: ‘BBB’ Outlook Stable
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
Long-term foreign currency IDR: ‘BBB’ Outlook Stable
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
Short-term foreign currency IDR: ‘F2’ National long-term rating: ‘AAA (kaz)’ Outlook Stable
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
National long-term rating: ‘AAA (kaz)’ Outlook Stable
		          		</div>
		          	</div>
		          </div>
					<div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
 Senior unsecured domestic bonds: «AAA(kaz)»
		          		</div>
		          	</div>
		          </div>
        		</div>
		      </div>
          </div>
        </div>
      </div>
    </section>

<!-- block -->
    <section class="ratings mt-5 mb-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5 mb-5">

		      <div class="container">
		        <div class="row">


        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>28</span>rank
		          		</div>
						<div class="ratings-item2-text">
in “Ease of Doing Business” (2019)
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>1</span><br>rank
		          		</div>
						<div class="ratings-item2-text">
in "Protecting Minority Investors" (Doing Business, 2019)
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>58</span>rank
		          		</div>
						<div class="ratings-item2-text">
in "Paying taxes" (Doing Business, 2019)
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>59</span>rank
		          		</div>
						<div class="ratings-item2-text">
in WEF Global Competitiveness Index 2018-2019
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>59</span>rank
		          		</div>
						<div class="ratings-item2-text">
in The Heritage Foundation Index of Economic Freedom (2019)
		          		</div>
		          	</div>
		          </div>


        		</div>
		      </div>
          </div>
        </div>
      </div>
    </section>


