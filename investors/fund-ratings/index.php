<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/pages.php");

$pagetitle = Loc::getMessage('RATINGS') ." — ". Loc::getMessage('ABOUT_FUND') ." — ". Loc::getMessage("NAME_SITE");
$description = Loc::getMessage("DESCRIPTION_SITE");
$keywords = Loc::getMessage("KEYWORDS_SITE");
$APPLICATION->SetTitle($pagetitle);
$APPLICATION->SetPageProperty("title", $pagetitle);
$APPLICATION->SetPageProperty("description",  $description);
$APPLICATION->SetPageProperty("keywords",  $keywords);
$APPLICATION->SetPageProperty("og:type",  "website");
$APPLICATION->SetPageProperty("og:title",  $pagetitle);
$APPLICATION->SetPageProperty("og:description",  $description);
$APPLICATION->SetPageProperty("og:url",  "https://sk.kz".$APPLICATION->GetCurPage(false));
$APPLICATION->SetPageProperty("og:image",  "https://sk.kz".SITE_TEMPLATE_PATH."/thumb.jpg");
?>
<div class="ratings">
  <div class="container">
	<div class="row">
	  <div class="col-12"> 
        <div class="breadcrumb ratings__breadcumb">
            <div class="container">
            <div class="row">
                <div class="col-12">
                <div class="breadcrumb__content"><a class="breadcrumb__item" href="/">
                    <p class="breadcrumb__item-text"><?=Loc::getMessage('MAIN');?></p></a>
					<a
						class="breadcrumb__item"
					href="/about-fund/"
					>
						<p class="breadcrumb__item-text"><?=Loc::getMessage('ABOUT_FUND');?></p>
					</a>
         			<span class="breadcrumb__item">
                    <p class="breadcrumb__item-text"><?=Loc::getMessage('RATINGS');?></p></span>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="title ratings__titlecomponent">
            <h2 class="title__text"><?=Loc::getMessage('RATINGS');?></h2>
        </div>
		<div class="ratings__cards">
		  <div class="rating-card">
			<div class="rating-card__header">
			  <p class="rating-card__title"><?=Loc::getMessage('S_P_GLOBAL_RATINGS');?></p>
			  <p class="rating-card__date">30.06.2020</p>
			</div>
			<div class="rating-card__content">
			  <ul class="rating-card__list">
				<li><?=Loc::getMessage('S_P_GLOBAL_RATINGS_LIST_ONE');?></li>
				<li><?=Loc::getMessage('S_P_GLOBAL_RATINGS_LIST_TWO');?></li>
				<li><?=Loc::getMessage('S_P_GLOBAL_RATINGS_LIST_THREE');?></li>
				<li><?=Loc::getMessage('S_P_GLOBAL_RATINGS_LIST_FOUR');?></li>
			  </ul>
			</div>
		  </div>
		  <div class="rating-card">
			<div class="rating-card__header">
			  <p class="rating-card__title"><?=Loc::getMessage('FITCH_RATINGS');?></p>
			  <p class="rating-card__date">30.12.2019</p>
			</div>
			<div class="rating-card__content">
			  <ul class="rating-card__list">
				<li><?=Loc::getMessage('FITCH_RATINGS_LIST_ONE');?></li>
				<li><?=Loc::getMessage('FITCH_RATINGS_LIST_TWO');?></li>
				<li><?=Loc::getMessage('FITCH_RATINGS_LIST_THREE');?></li>
				<li><?=Loc::getMessage('FITCH_RATINGS_LIST_FOUR');?></li>
				<li><?=Loc::getMessage('FITCH_RATINGS_LIST_FIVE');?></li>
			  </ul>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="col-12">
		<div class="ratings__map">
		  <div class="ratings__map-list">
			<div class="ratings__map-item" v-for=" (item,index) in ratingsInfo[$i18n.locale] " :key="index">
			  <div class="ratings__map-item-header"><img class="ratings__map-item-icon ratings__map-item-icon_left" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/triumph.svg">
				<div class="ratings__map-item-title"> 
				  <p class="ratings__map-item-title-text" v-html="item.count"></p>
				</div><img class="ratings__map-item-icon ratings__map-item-icon_right" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/triumph.svg">
			  </div>
			  <div class="ratings__map-item-place"> 
				<p class="ratings__map-item-place-text"><?=Loc::getMessage('RATINGS_MAP_PLACE');?></p>
			  </div>
			  <div class="ratings__map-item-line"></div>
			  <div class="ratings__map-item-info">
				<p class="ratings__map-item-info-text" v-html="item.info"></p>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>

<div class="partnerswrap">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="mpartners">
					<div class="mpartners__arrow mpartners__arrow_left">
						<img
							class="mpartners__arrow-icon"
							src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
							alt=""
						>
					</div>

					<div class="swiper-container mpartners__list">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<a
									href="https://hrqyzmet.kz/"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/brands_qyzmet.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://samruk-akparat.kz/"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aqparat.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://www.skc.kz/"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/samruk.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://aifc.kz/"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aifc_cmyk.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://primeminister.kz/ru"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/min.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="http://www.akorda.kz/ru"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-7.jpg"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://elbasy.kz/ru"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-6.jpg"
											alt=""
										>
									</div>
								</a>
							</div>
						</div>
					</div>

					<div class="mpartners__arrow mpartners__arrow_right">
						<img
							class="mpartners__arrow-icon"
							src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
							alt=""
						>
					</div>

					<div class="mpartners__mobile">
						<a
							href="https://hrqyzmet.kz/"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/brands_qyzmet.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://samruk-akparat.kz/"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aqparat.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://www.skc.kz/"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/samruk.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://aifc.kz/"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aifc_cmyk.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://primeminister.kz/ru"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/min.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="http://www.akorda.kz/ru"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-7.jpg"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://elbasy.kz/ru"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-6.jpg"
									alt=""
								>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>