<?$APPLICATION->SetTitle("Рейтинги Фонда");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>

 <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             Рейтинги Фонда
            </h2>
          </div>
        </div>
      </div>
</section>




    <!-- Ratings S&P -->
    <section class="ratings">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
            <div class="ratings-title">
				S&P Global Ratings
            </div>
			<div class="ratings-date mt-3">
				30.06.2020 г.
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="ratings">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">

		      <div class="container">
		        <div class="row">
        		  <div class="col-lg-3">
					<div class="ratings-item">
						<div class="ratings-item-title">
							<span>S&P Global</span>Ratings
		          		</div>
						<div class="ratings-item-text" style="background-color: #097b75;color: #FFF;">
						Долгосрочный и краткосрочный
						кредитные рейтинги по обязательствам
						в иностранной валюте «BB+/B»
						прогноз «Стабильный»
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-lg-3">
					<div class="ratings-item">
						<div class="ratings-item-title">
							<span>S&P Global</span>Ratings
		          		</div>
						<div class="ratings-item-text" style="">
Долгосрочный и краткосрочный
кредитные рейтинги по обязательствам
в национальной валюте «BB+/B» прогноз «Стабильный»
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-lg-3">
					<div class="ratings-item">
						<div class="ratings-item-title">
							<span>S&P Global</span>Ratings
		          		</div>
						<div class="ratings-item-text" style="">
Долгосрочный рейтинг по национальной шкале
«kzAA-»
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-lg-3">
					<div class="ratings-item">
						<div class="ratings-item-title">
							<span>S&P Global</span>Ratings
		          		</div>
						<div class="ratings-item-text" style="background-color: #097b75;color: #FFF;">
Приоритетные необеспеченные рейтинги внутренних облигаций
«ВВ+»
		          		</div>
		          	</div>
		          </div>
        		</div>
		      </div>
          </div>
        </div>
      </div>
    </section>




    <!-- Ratings S&P -->
    <section class="ratings">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-3">
            <div class="ratings-title mt-5">
				Fitch Ratings
            </div>
			<div class="ratings-date mt-3">
				30.12.2019 г.
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="ratings">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">

		      <div class="container">
		        <div class="row">
        		  <div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
Долгосрочный РДЭ в национальной валюте «BBB»
прогноз «Стабильный»
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
Долгосрочный РДЭ в иностранной валюте «BBB»
прогноз «Стабильный»
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
Краткосрочный РДЭ в иностранной валюте
на уровне «F2»
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
Национальный долгосрочный рейтинг «AAA(kaz)»
прогноз «Стабильный»
		          		</div>
		          	</div>
		          </div>
					<div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
Приоритетные необеспеченные рейтинги внутренних облигаций «AAA(kaz)» 
		          		</div>
		          	</div>
		          </div>
        		</div>
		      </div>
          </div>
        </div>
      </div>
    </section>

<!-- block -->
    <section class="ratings mt-5 mb-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5 mb-3">

		      <div class="container">
		        <div class="row">


        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>28</span>место
		          		</div>
						<div class="ratings-item2-text">
Doing Business по легкости ведения бизнеса (2019)
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>1</span>место
		          		</div>
						<div class="ratings-item2-text">
В рейтинге Doing Business по защите миноритарных инвесторов (2019)
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>58</span>место
		          		</div>
						<div class="ratings-item2-text">
В рейтинге Doing Business по налогообло- жению (2019)
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>59</span>место
		          		</div>
						<div class="ratings-item2-text">
По глобальному индексу конкурентоспо- собности ВЭФ (2018-2019)
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>59</span>место
		          		</div>
						<div class="ratings-item2-text">
В рейтинге экономической свободы The Heritage Foundation (2019)
		          		</div>
		          	</div>
		          </div>


        		</div>
		      </div>
          </div>
        </div>
      </div>
    </section>


