<?$APPLICATION->SetTitle("Қордың рейтингтері");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>

 <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('fund-ratings')?>
            </h2>
          </div>
        </div>
      </div>
</section>




    <!-- Ratings S&P -->
    <section class="ratings">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
            <div class="ratings-title mt-5">
				S&P Global Ratings
            </div>
			<div class="ratings-date mt-3">
				30.06.2020 г.
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="ratings">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">

		      <div class="container">
		        <div class="row">
        		  <div class="col-lg-3">
					<div class="ratings-item">
						<div class="ratings-item-title">
							<span>S&P Global</span>Ratings
		          		</div>
						<div class="ratings-item-text" style="background-color: #097b75;color: #FFF;">
						«BB+/B» шетел валютасындағы міндеттемелер бойынша ұзақ мерзімді және қысқа мерзімді несиелік рейтингі, болжам «Тұрақты»
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-lg-3">
					<div class="ratings-item">
						<div class="ratings-item-title">
							<span>S&P Global</span>Ratings
		          		</div>
						<div class="ratings-item-text">
«BB+/B» ұлттық валютадағы міндеттемелер бойынша ұзақ мерзімді және қысқа мерзімді несиелік рейтингі, болжам «Тұрақты»
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-lg-3">
					<div class="ratings-item">
						<div class="ratings-item-title">
							<span>S&P Global</span>Ratings
		          		</div>
						<div class="ratings-item-text">
«kzAA-» ұлттық шкала бойынша ұзақ мерзімді рейтинг

		          		</div>
		          	</div>
		          </div>
        		  <div class="col-lg-3">
					<div class="ratings-item">
						<div class="ratings-item-title">
							<span>S&P Global</span>Ratings
		          		</div>
						<div class="ratings-item-text" style="background-color: #097b75;color: #FFF;">
«ВВ+» ішкі облигациялардың басым қамтамасыз етілмеген рейтингтері
		          		</div>
		          	</div>
		          </div>
        		</div>
		      </div>
          </div>
        </div>
      </div>
    </section>




    <!-- Ratings S&P -->
    <section class="ratings">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
            <div class="ratings-title mt-5">
				Fitch Ratings
            </div>
			<div class="ratings-date mt-3">
				30.12.2019 г.
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="ratings">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">

		      <div class="container">
		        <div class="row">
        		  <div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
«BBB» ұлттық валютадағы ұзақ мерзімді ЭДР, болжам «Тұрақты»

		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
BBB» шетел валютасындағы ұзақ мерзімді ЭДР, болжам «Тұрақты»
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
«F2» деңгейінде шетел валютасындағы қысқа мерзімді ЭДР
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
«AAA(kaz)» ұлттық ұзақ мерзімді рейтинг, болжам «Тұрақты»
		          		</div>
		          	</div>
		          </div>
					<div class="col-xs-5">
					<div class="ratings-item">
						<div class="ratings-item-title ratings-item-title2">
							<span>Fitch</span>Ratings
		          		</div>
						<div class="ratings-item-text">
«AAA(kaz)» ішкі облигациялардың басым қамтамасыз етілмеген рейтингтері
		          		</div>
		          	</div>
		          </div>
        		</div>
		      </div>
          </div>
        </div>
      </div>
    </section>

<!-- block -->
    <section class="ratings mt-5 mb-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5 mb-5">

		      <div class="container">
		        <div class="row">


        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>28</span>орында
		          		</div>
						<div class="ratings-item2-text">
Doing Business Бизнес жүргізу жеңілдігі бойынша рейтингте (2019)
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>1</span>орында
		          		</div>
						<div class="ratings-item2-text">
Doing Business Миноритарлық инвесторларды қорғау бойынша рейтингте (2019)
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>56</span>орында
		          		</div>
						<div class="ratings-item2-text">
Doing Business Салық салу рейтингінде (2019)
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>59</span>орында
		          		</div>
						<div class="ratings-item2-text">
ДЭФ Ғаламдық бәсекелестік индексі 2018-2019
		          		</div>
		          	</div>
		          </div>
        		  <div class="col-xs-5">
					<div class="ratings-item2">
						<div class="ratings-item2-title">
							<span>59</span>орында
		          		</div>
						<div class="ratings-item2-text">
The Heritage Foundation экономикалық еркіндік индексі (2019)
		          		</div>
		          	</div>
		          </div>


        		</div>
		      </div>
          </div>
        </div>
      </div>
    </section>


