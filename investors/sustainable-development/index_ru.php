<?$APPLICATION->SetTitle("Устойчивое развитие");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>

    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('sustainable-development')?>
            </h2>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<p class="t-1 mb-5 mt-3">
	<b>В 2018 году Единственным акционером Фонда</b> была утверждена новая Стратегия развития Фонда до 2028 года, определяющая  корпоративное управление и устойчивое развитие как одну из трех стратегических целей Фонда на ближайшее десятилетие. 
В рамках данной стратегической цели мы планируем продолжать достижение  высокого  уровня  в  области  корпоративного  управления и устойчивого развития, повышать уровень корпоративного управления в Фонде и Портфельных компаниях, наращивать   кадровый   потенциал,   обеспечить внедрение принципов меритократии. В рамках новой стратегии также предусмотрена реализация спонсорских инициатив, а также определенных Фондом инициатив в области устойчивого развития. Фондом планируется осуществление реализации мероприятий и индикаторов в рамках Концепции Республики Казахстан по  переходу к «зеленой экономике».
			  </p>
<p class="t-1 mb-5">
	<b>По результатам реализации данной стратегической цели</b> ожидается создание системы управления, обеспечивающей достижение улучшенных показателей в области устойчивого развития, охраны труда, здоровья и окружающей среды, улучшение экономических показателей и финансовой устойчивости, противодействия коррупции, повышение рейтинга корпоративного управления и рейтинга социальной стабильности.
</p>

          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-3">
				<div class="devel-01">
					<span class="title">&nbsp;
						<div class="block-line-3"></div>
					</span>
					<p>Эффективность компаний</p>
				</div>
          </div>
          <div class="col-lg-6">
				<div class="devel-02">
					<span class="title">Стратегическая цель
						<div class="block-line-3"></div>
					</span>
					<p>Устойчивое развитие</p>
				</div>

          </div>
          <div class="col-lg-3">
				<div class="devel-01">
					<span class="title">&nbsp;
						<div class="block-line-3"></div>
					</span>
					<p>Управление портфелем</p>
				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
				<a class="anchor" href="#Objectives"><div class="devel-03 devel-03-first-child">
					<span class="devel-03-01"></span>
					<p>Цели в области<br>
устойчивого<br>
развития</p>
					</div></a>
				<a class="anchor" href="#Analysis"><div class="devel-03">
					<span class="devel-03-02"></span>
					<p>Анализ<br>
существенности</p>
				</div></a>
				<a class="anchor" href="#Interaction"><div class="devel-03">
					<span class="devel-03-03"></span>
					<p>Взаимодействие<br>
со стейкхолдерами</p>
				</div></a>
				<a class="anchor" href="#Initiatives"><div class="devel-03 devel-03-last-child">
					<span class="devel-03-04"></span>
					<p>Инициативы<br>
в области<br>
устойчивого<br>
развития</p>
				</div></a>
          </div>

        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div id="Objectives" class="col-lg-12 mt-5">
				<div class="devel-04">
					<span>Цели в области устойчивого развития</span>
					<p>Фонд поддерживает цели ООН по устойчивому развитию</p>
					<br><b>1 января 2016 года мир официально приступил к реализации Повестки дня</b> в области устойчивого развития на период до 2030 года — плана активных преобразований, в основе которого лежат 17 целей в области устойчивого развития, - для решения в течение следующих пятнадцати лет неотложных глобальных проблем.
					<br><b>17 целей в области устойчивого развития и 169 задач (ЦУР)</b> направлены на реализацию прав человека для всех, они носят комплексный и неделимый характер и обеспечивают сбалансированность трех компонентов устойчивого развития: экономического, социального и экологического. Эти цели и задачи будут стимулировать действия национальных правительств в течение следующих периодов в областях, имеющих огромное значение для человечества и планеты.
Из них мы выбрали девять стратегически приоритетных для деятельности Группы Фонда целей, достижению которых она будет способствовать в ходе своей основной деятельности. В текущую деятельность также интегрированы задачи, способствующие достижению остальных целей ООН в области устойчивого развития.
					<!--<div class="text-center mt-5">
						<a href="" class="devel-btn">Подробнее</a>
					</div>-->
				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
				<div class="devel-05">
					<div class="devel-06">
						<span></span>
						<p>Хорошее здоровье<br>и благополучие</p>
						<div class="devel-06-01"></div>
					</div>
					<div class="devel-06">
						<span></span><p>Качественное
образование</p>
						<br><div class="devel-06-02"></div>
					</div>
					<div class="devel-06">
						<span></span><p>Гендерное
равенство</p>
						<br><div class="devel-06-03"></div>
					</div>
					<div class="devel-06">
						<span></span><p>Достойная работа и
экономический рост</p>
						<div class="devel-06-04"></div>
					</div>
					<div class="devel-06">
						<span></span><p>Индустриализация,
инновации и
инфрастркутура</p>
						<div class="devel-06-05"></div>
					</div>
					<div class="devel-06 devel-06-00">
						<span></span><p>Уменьшение<br>
неравенства</p>
						<div class="devel-06-06"></div>
					</div>
					<div class="devel-06 devel-06-00">
						<span></span><p>Устойчивые города
и населенные пункты</p>
						<div class="devel-06-07"></div>
					</div>
					<div class="devel-06 devel-06-00">
						<span></span><p>Борьба с изменением
климата</p>
						<div class="devel-06-08"></div>
					</div>
					<div class="devel-06 devel-06-00">
						<span></span><p>Партнерство в интересах 
устойчивого развития</p>
						<div class="devel-06-09"></div>
					</div>

				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container" id="Analysis">
        <div class="row">
          <div class="col-lg-12 mt-5">
				<div class="devel-07">
			        <div class="row">
          				<div class="col-lg-4">
							<div class="devel-07-01">
Анализ<br>
существенности
							</div>
							<p class="devel-07-02">
Для того, чтобы понять потребности заинтересованных сторон и определить, что такое устойчивое развитие Фонда для них, мы провели анализ существенности.

							</p>
						</div>
          				<div class="col-lg-2">
						</div>
          				<div class="col-lg-6">

							<p class="devel-07-02 devel-07-03">
На первом этапе было предложено определить список аспектов устойчивого развития, связанных с деятельностью Фонда и их воздействием
							</p>
							<p class="devel-07-02">
По итогам проведенного опроса и  обсуждений со всеми ключевыми заинтересованными сторонами по определению существенных аспектов, нами определены 17 тем, связанных с устойчивостью, которые имеют значение для Фонда и наших стейкхолдеров.

							</p>
						</div>
					</div>

				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
<p class="centerp">
Всесторонне охватывая экономические, экологические и социальные аспекты на пути устойчивого развития,<br>
Фонд стремится обеспечить интерактивный диалог со всеми заинтересованными сторонами<br>
и создать  дополнительную стоимость
			  </p>
          </div>
        </div>
      </div>
</section>
	<section>
      <div class="container mt-5">
        <div class="row">
          <div class="col-lg-6">
				<div class="devel-08">
					<span class="devel-08-01">Экономические темы:</span><br>
					<div class="block-line-3"></div><br><br>
					<i class="arrow-green-down">Ключевые финансовые показатели<br></i>
					<i class="arrow-green-down">Противодействие коррупции<br></i>
					<i class="arrow-green-down">Финансовая устойчивость<br></i>
					<i class="arrow-green-down">Социально-ответственные закупки<br></i>
				</div>
          </div>
          <div class="col-lg-6">
				<div class="devel-08">
					<span class="devel-08-01 devel-08-02">Экологические темы:
</span><br>
					<div class="block-line-3"></div><br><br>
					<i class="arrow-green-down">Соответствие требованиям в<br>
области  охраны  окружающей<br>
среды<br></i>
				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container mt-5">
        <div class="row">
          <div class="col-lg-12">
				<div class="devel-08">

        			<div class="row">
			          <div class="col-lg-5">
							<span class="devel-08-01 devel-08-03">Социальные темы:</span><br>
							<div class="block-line-3"></div><br><br>
						</div>
					</div>

        			<div class="row">
			          <div class="col-lg-6">

					<i class="arrow-green-down"> Недопущение дискриминации<br></i>
					<i class="arrow-green-down">Права человека<br></i>
					<i class="arrow-green-down">Социальная ответственность<br></i>
					<i class="arrow-green-down">Здоровье и безопасность на рабочем месте<br></i>
					<i class="arrow-green-down">Занятость<br></i>
					<i class="arrow-green-down">Прозрачность и  связи с общественностью<br></i>

						</div>
			          <div class="col-lg-6">

					<i class="arrow-green-down">Отбор и трудоустройство персонала<br></i>
					<i class="arrow-green-down">Взаимодействие работников и руководства<br></i>
					<i class="arrow-green-down">Корпоративная культура<br></i>
					<i class="arrow-green-down">Вознаграждение и оценка<br></i>
					<i class="arrow-green-down">Подготовка и образование<br></i>
					<i class="arrow-green-down">Свобода ассоциаций и ведения<br>
коллективных договоров<br></i>

						</div>
          			</div>
        		</div>
        	</div>
        </div>
      </div>
</section>
	<section>
      <div class="container mt-5" id="Interaction">
        <div class="row">
          <div class="col-lg-12">
<p class="newh3">
Взаимодействие с заинтересованными сторонами
			  </p>
<p class="t-1 mb-1">
	<b>Система взаимодействия Фонда с заинтересованными сторонами</b> сосредоточена на достижении целей устойчивого развития и гармонизации интересов всех заинтересованных сторон. Взаимодействие со стейкхолдерами основано на принципах уважения интересов и сотрудничества 
			  </p>
<p class="t-1 mt-3">
<b>Фонд видит свою социальную ответственность в том,</b> чтобы быть надежным партнером для заинтересованных сторон. Мы ведем диалог и строим отношения со стейкхолдерами на основе принципов взаимного уважения и партнерства, информационной прозрачности, регулярности взаимодействия и добросовестного соблюдения взятых на себя обязательств.
</p>
        	</div>
        </div>
      </div>
</section>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
<h2 class="h2-left pr-20 section-title text-center">
Фонд использует стандартный процесс привлечения<br>
заинтересованных сторон в соответствии с  AA1000 SES:


			  </h2>
			</div>
		</div>
	</div>
    </section>
<section>
		<div class="container">
        <div class="row">
          <div class="col-lg-1">
          </div>
          <div class="col-lg-5">
				<div class="block-01">
					Идентификация и анализ<br>
заинтересованных сторон

				</div>
				<div class="block-01">
					Приоритизация<br>
заинтересованных сторон

				</div>
				<div class="block-01">
					Планирование взаимодействия<br>
со стейкхолдерами

				</div>


          </div>
          <div class="col-lg-5">
				<div class="block-01">
					Взаимодействие<br>
с заинтересованными сторонами


				</div>
				<div class="block-01">
					Отчетность по эффективности<br>
и оценке
				</div>
			</div>

</section>

<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-1">
<h2 class="h2-left pr-20 section-title text-center">
Принципы взаимодействия со стейкхолдерами
			  </h2>
			</div>
		</div>
	</div>
    </section>

<section>
	<div class="container mt-5">
		<div class="row">
			<div class="col-lg-12">
				<div class="orange-block devel-09">

        			<div class="row">
						<div class="col-lg-4">
							<i class="arrow-green-down">Подотчетность<br></i>
							<i class="arrow-green-down">Надлежащие функции<br> 
управления
<br></i>
							<i class="arrow-green-down">Вовлечение <br>
заинтересованных  сторон
<br></i>
						</div>
						<div class="col-lg-4">
							<i class="arrow-green-down">Процедуры по урегулированию 
конфликтов 
<br></i>
							<i class="arrow-green-down">Идентификация и анализа
заинтересованных сторон
<br></i>
							<i class="arrow-green-down">Консультации<br>
со стейкхолдерами
<br></i>
						</div>
						<div class="col-lg-4">
							<i class="arrow-green-down">Раскрытие
информации<br></i>
							<i class="arrow-green-down">Переговоры 
и партнерство
<br></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
      <div id="Initiatives" class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
<h2 class="h2-left pr-20 section-title text-center">
Инициативы в области устойчивого развития
			  </h2>
			</div>
		</div>
	</div>
    </section>
<section>
	<div class="container mt-5 mb-5">
		<div class="row">
			<div class="col-lg-12">
				<div class="box-green devel-10 text-center">

        			<div class="row">
						<div class="col-lg-2">

						</div>
						<div class="col-lg-9 text-left">
							<div class="block-check">
								<span>Эффективное корпоративное управление и улучшенная культура рисков</span>
								Обеспечивается принятием и внедрением нового Кодекса корпоративного управления
								и риск-ориентированного подхода к устойчивому развитию
							</div>
							<div class="block-check">
								<span>Финансовая устойчивость</span>
								Обеспечивается реализацией приоритетов развития, определенных в планах развития
							</div>
							<div class="block-check">
								<span>Развитие человеческого потенциала</span>
								Обеспечивается путем запуска новой системы управления персоналом с тремя основными
компонентами: деловое партнерство в сфере управления персоналом, центр экспертизы
по управлению персоналом, общая служба управления персоналом

							</div>
							<div class="block-check">
								<span>Высокие этические стандарты и противостояние коррупции</span>
								Обеспечивается посредством реализации Кодекса поведения и осуществлением функции
по соблюдению требований

							</div>

<div class="block-down-box" data-id="0">

							<div class="block-check">
								<span>Ответственные закупки </span>
Обеспечивается путем внедрения нового подхода к закупкам, включая категорийное управление закупками и запуск системы предварительной квалификации потенциальных поставщиков, а также повышением прозрачности процесса закупок
							</div>
							<div class="block-check">
								<span>Хорошая репутация и высокий уровень прозрачности </span>
Обеспечивается благодаря реализации Коммуникационного плана и повышению прозрачности и раскрытия информации
							</div>
							<div class="block-check">
								<span>Устойчивые ценности в сфере безопасности и охраны труда </span>
Обеспечивается реализацией мероприятий по безопасности и охраны труда в портфельных компаниях Фонда
							</div>
							<div class="block-check">
								<span>Ответственные инвестиции </span>
Обеспечивается путем внедрения ключевых принципов ответственных инвестиций в инвестиционной политике Фонда и подбора показателей устойчивости в процесс проектирования, внедрения и мониторинга инвестиций
							</div>
							<div class="block-check">
								<span>Экологическая ответственность </span>
Обеспечивается путем соблюдения экологического законодательства, норм и стандартов в области охраны окружающей среды, а также посредством инвестирования в развитие экологической деятельности Фонда и Группы Фонда
							</div>

</div>


						</div>

					</div>
        			<div class="row">
						<div class="col-lg-6 text-center">
							<a href="" class="block-down" data-id="0"></a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

