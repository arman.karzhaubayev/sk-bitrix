<?$APPLICATION->SetTitle("Тұрақты даму");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>

    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('sustainable-development')?>
            </h2>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<p class="t-1 mb-5 mt-3">
	2018 жылы Қордың Жалғыз акционері Қордың 2028 жылға дейінгі жаңа даму стратегиясын бекітті. Ол Корпоративтік басқару мен тұрақты дамуды Қордың алдағы он жылға арналған үш стратегиялық мақсаттарының бірі ретінде айқындайды.
Осы стратегиялық мақсаттың аясында біз корпоративтік басқару және тұрақты даму саласындағы жоғары деңгейге қол жеткізуді, Қор мен қоржындық компаниялардағы корпоративтік басқару деңгейін арттыруды, кадрлық әлеуетті өсіруді және меритократия қағидаттарын енгізуді қамтамасыз етуді жалғастыруды жоспарлап отырмыз. Жаңа стратегия шеңберінде демеушілік бастамалар, сондай-ақ тұрақты даму саласындағы Қор айқындаған бастамаларды жүзеге асыру қарастырылған. Қор «жасыл экономика» қағидаттарын ұстануды қамтамасыз етеді және Қазақстан Республикасының "жасыл экономикаға" өтуі жөніндегі шаралар мен көрсеткіштер Тұжырымдамасын іске асырады.

			  </p>
<p class="t-1 mb-5">
	Осы стратегиялық мақсатты іске асыру нәтижелері бойынша тұрақты даму, еңбекті, денсаулық пен қоршаған ортаны қорғау, экономикалық көрсеткіштер мен қаржылық тұрақтылықты жақсарту, сыбайлас жемқорлыққа қарсы әрекет ету, корпоративтік басқару рейтингін және әлеуметтік тұрақтылық рейтингін жақсарту саласындағы жақсартылған көрсеткіштерге қол жеткізуді қамтамасыз ететін басқару жүйесін құру күтілуде.
</p>

          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-3">
				<div class="devel-01">
					<span class="title">Стратегиялық мақсат
						<div class="block-line-3"></div>
					</span>
					<p>Компания тиімділігі</p>
				</div>
          </div>
          <div class="col-lg-6">
				<div class="devel-02">
					<span class="title">Стратегиялық мақсат
						<div class="block-line-3"></div>
					</span>
					<p>Тұрақты даму</p>
				</div>

          </div>
          <div class="col-lg-3">
				<div class="devel-01">
					<span class="title">Стратегиялық мақсат
						<div class="block-line-3"></div>
					</span>
					<p>Қоржынды басқару</p>
				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
				<a class="anchor" href="#Objectives"><div class="devel-03 devel-03-first-child">
					<span class="devel-03-01"></span>
					<p>Тұрақты даму саласындағы <br>
						мақсаттар</p>
				</div></a>
				<a class="anchor" href="#Analysis"><div class="devel-03">
					<span class="devel-03-02"></span>
					<p>Маңыздылықты<br>
 талдау </p>
				</div></a>
				<a class="anchor" href="#Interaction"><div class="devel-03">
					<span class="devel-03-03"></span>
					<p>Мүдделі тараптармен <br>әрекеттесу</p>
				</div></a>
				<a class="anchor" href="#Initiatives"><div class="devel-03 devel-03-last-child">
					<span class="devel-03-04"></span>
					<p>Тұрақты даму <br>саласындағы бастамалар  </p>
				</div></a>
          </div>

        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div id="Objectives" class="col-lg-12 mt-5">
				<div class="devel-04">
					<span>Тұрақты даму саласындағы мақсаттар</span>
					<p>Қор БҰҰ тұрақты даму жөніндегі мақсаттарын қолдайды</p>
					<br>2016 жылдың 1 қаңтарында әлем 2030 жылға дейінгі кезеңнің тұрақты даму саласындағы 17 мақсатқа негізделген белсенді түрлену жоспарын, келесі он бес жыл ішінде кезек күттірмейтін жаһандық мәселелерді шешу үшін күн тәртібін іске асыруды ресми түрде бастады.
					<br>Тұрақты даму саласындағы 17 мақсат пен 169 тапсырма (ТДМ) барлығына арналған адам құқықтарын іске асыруға бағытталған, олар кешенді және бөлінбейтін сипатқа ие және тұрақты дамудың келесі үш компонентінің теңгерімділігін қамтамасыз етеді: экономикалық, әлеуметтік және экологиялық. Бұл мақсаттар мен тапсырмалар ұлттық алдағы 15 жыл ішінде адамзат пен планета үшін маңызы үлкен салалардағы ұлттық үкіметтердің әрекеттерін ынталандыратын болады.
Олардың ішінен біз Қор Тобының қызметі үшін стратегиялық басымдыққа ие тоғыз мақсатты таңдап алдық, оларға қол жеткізуге ол негізгі қызметі барысында ықпал етеді. Ағымдағы қызметке сонымен қатар БҰҰ тұрақты даму саласындағы басқа да мақсаттарына қол жеткізуге ықпал ететін тапсырмалар интеграцияланған.
					<!--<div class="text-center mt-5">
						<a href="" class="devel-btn">Көбірек білу</a>
					</div>-->
				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
				<div class="devel-05">
					<div class="devel-06">
						<span></span>
						<p>Жақсы денсаулық <br>және амандық</p>
						<div class="devel-06-01"></div>
					</div>
					<div class="devel-06">
						<span></span><p>Сапалы білім беру</p>
						<br><div class="devel-06-02"></div>
					</div>
					<div class="devel-06">
						<span></span><p>Гендерлік теңдік</p>
						<br><div class="devel-06-03"></div>
					</div>
					<div class="devel-06">
						<span></span><p>Лайықты жұмыс пен экономикалық өсім</p>
						<div class="devel-06-04"></div>
					</div>
					<div class="devel-06">
						<span></span><p>Индустрияландыру, инновация және инфрақұрылым</p>
						<div class="devel-06-05"></div>
					</div>
					<div class="devel-06 devel-06-00">
						<span></span><p>Теңсіздікті <br>азайту</p>
						<div class="devel-06-06"></div>
					</div>
					<div class="devel-06 devel-06-00">
						<span></span><p>Климаттың өзгеруімен күресу</p>
						<div class="devel-06-07"></div>
					</div>
					<div class="devel-06 devel-06-00">
						<span></span><p>Тұрақты қалалар мен елді мекендер</p>
						<div class="devel-06-08"></div>
					</div>
					<div class="devel-06 devel-06-00">
						<span></span><p>Тұрақты даму мүддесіндегі серіктестік</p>
						<div class="devel-06-09"></div>
					</div>

				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container" id="Analysis">
        <div class="row">
          <div class="col-lg-12 mt-5">
				<div class="devel-07">
			        <div class="row">
          				<div class="col-lg-5">
							<div class="devel-07-01">
Маңыздылықты <br>талдау
							</div>
							<p class="devel-07-02">
Мүдделі тараптардың қажеттіліктерін түсіну және Қор үшін тұрақты даму нені білдіретінін анықтау үшін біз маңыздылық талдауын жасадық.
Бірінші кезеңде Қор қызметімен байланысты тұрақты дамудың аспектілерінің тізімі және олардың әсерін анықтау ұсынылды. 
							</p>
						</div>
          				<div class="col-lg-1">
						</div>
          				<div class="col-lg-6">

							<p class="devel-07-02 devel-07-03">
<br>
Маңызды аспектілерді анықтау үшін сауалнама нәтижелері мен барлық 
мүдделі тараптармен жүргізілген талқылаулардың нәтижелері бойынша біз 
Қор мен мүдделі тараптар үшін маңызды болып табылатын тұрақтылыққа қатысты 17 тақырыпты анықтадық.
							</p>
							<p class="devel-07-02">


							</p>
						</div>
					</div>

				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
<p class="centerp">
Тұрақты даму жолындағы экономикалық, экологиялық және әлеуметтік аспектілерді жан-жақты қарастыра отырып, Қор барлық мүдделі тараптармен интерактивті диалог құруға және қосымша құндылықты ұлғайтуға тырысады.
			  </p>
          </div>
        </div>
      </div>
</section>
	<section>
      <div class="container mt-5">
        <div class="row">
          <div class="col-lg-6">
				<div class="devel-08">
					<span class="devel-08-01">Экономикалық тақырыптар:</span><br>
					<div class="block-line-3"></div><br><br>
					<i class="arrow-green-down">Негізгі қаржылық көрсеткіштер<br></i>
					<i class="arrow-green-down">Сыбайлас жемқорлыққа қарсы әрекет<br></i>
					<i class="arrow-green-down">Жұмыс орнындағы денсаулық және қауіпсіздік<br></i>
					<i class="arrow-green-down">Әлеуметтік-жауапты сатып алулар<br></i>
				</div>
          </div>
          <div class="col-lg-6">
				<div class="devel-08">
					<span class="devel-08-01 devel-08-02">Экологиялық тақырыптар:
</span><br>
					<div class="block-line-3"></div><br><br>
					<i class="arrow-green-down">Қоршаған ортаны қорғау саласындағы талаптарға сәйкестік<br></i>
				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container mt-5">
        <div class="row">
          <div class="col-lg-12">
				<div class="devel-08">

        			<div class="row">
			          <div class="col-lg-5">
							<span class="devel-08-01 devel-08-03">Әлеуметтік тақырыптар:</span><br>
							<div class="block-line-3"></div><br><br>
						</div>
					</div>

        			<div class="row">
			          <div class="col-lg-6">

					<i class="arrow-green-down">Кемсітуді болдырмау<br></i>
					<i class="arrow-green-down">Адам құқықтары<br></i>
					<i class="arrow-green-down">Әлеуметтік жауапкершілік<br></i>
					<i class="arrow-green-down">Жұмыс орнындағы денсаулық және қауіпсіздік<br></i>
					<i class="arrow-green-down">Жұмыспен қамту<br></i>
					<i class="arrow-green-down">Ашықтық және қоғаммен байланыс<br></i>

						</div>
			          <div class="col-lg-6">

					<i class="arrow-green-down">Персоналды іріктеу және жұмысқа орналастыру<br></i>
					<i class="arrow-green-down">Жұмыскерлер мен басшылықтың өзара әрекеттесуі<br></i>
					<i class="arrow-green-down">Корпоративтік мәдениет<br></i>
					<i class="arrow-green-down">Сыйақы және бағалау<br></i>
					<i class="arrow-green-down">Дайындау және білім <br></i>
					<i class="arrow-green-down">Ассоциация бостандығы <br>және ұжымдық келісімдерді жүргізу<br></i>

						</div>
          			</div>
        		</div>
        	</div>
        </div>
      </div>
</section>
	<section>
      <div class="container mt-5">
        <div class="row">
          <div class="col-lg-12">
<p class="newh3">
Мүдделі тараптармен әрекеттесу
			  </p>
<p class="t-1 mb-1">
	Қордың мүдделі тараптармен өзара іскерлесу жүйесі тұрақты даму мақсаттарына қол жеткізуге және барлық мүдделі тараптардың мүдделерін үйлестіруге бағытталған. Стейкхолдерлермен өзара іскерлесу мүдделерді құрметтеу және ынтымақтастық қағидаттарына  негізделген.      
			  </p>
<p class="t-1 mt-3">
Қор мүдделі тараптар үшін сенімді әріптес болуда өзінің әлеуметтік жауапкершілігін сезінеді. Біз стейкхолдерлермен өзара сыйластық және әріптестік, ақпараттық ашықтық, өзара іскерлесудің жүйелілігі және өзімізге жүктелген міндеттерді адал орындау қағидаларына негізделетін сұхбат жүргіземіз және қарым-қатынас құрамыз.
</p>
        	</div>
        </div>
      </div>
</section>
<section>
      <div class="container" id="Interaction">
        <div class="row">
          <div class="col-lg-12 mt-5">
<h2 class="h2-left pr-20 section-title text-center">
Қор мүдделі тараптарды тартудың АА 1000 SES <br>сәйкес стандартты процессін пайдаланады:


			  </h2>
			</div>
		</div>
	</div>
    </section>
<section>
		<div class="container">
        <div class="row">
          <div class="col-lg-1">
          </div>
          <div class="col-lg-5">
				<div class="block-01">Мүдделі тараптарды тану

				</div>
				<div class="block-01">Мүдделі тараптардың <br>басымдылығын анықтау
					

				</div>
				<div class="block-01">Мүдделі тараптармен <br>іскерлесуді жоспарлау

				</div>


          </div>
          <div class="col-lg-5">
				<div class="block-01">Мүдделі тараптармен <br>іскерлесу 
					
				</div>
				<div class="block-01">Тиімділік және бағалау <br>бойынша есептілік </div>
			</div>

</section>

<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-1">
<h2 class="h2-left pr-20 section-title text-center">
Стейкхолдерлермен
 іскерлесудің қағидаттары 

			  </h2>
			</div>
		</div>
	</div>
    </section>

<section>
	<div class="container mt-5">
		<div class="row">
			<div class="col-lg-12">
				<div class="orange-block devel-09">

        			<div class="row">
						<div class="col-lg-4">
							<i class="arrow-green-down">Есептілік<br></i>
							<i class="arrow-green-down">Басқарудың қажетті функциялары<br></i>
							<i class="arrow-green-down">Мүдделі тараптарды тану және талдау <br></i>
						</div>
						<div class="col-lg-4">
							<i class="arrow-green-down">Ақпаратты ашу <br></i>
							<i class="arrow-green-down">Мүдделі тараптармен кеңесу<br></i>
							<i class="arrow-green-down">Келіссөздер  және  әріптестік<br></i>
						</div>
						<div class="col-lg-4">
							<i class="arrow-green-down">Дауларды  реттеу процедуралары<br></i>
							<i class="arrow-green-down">Мүдделі тараптарды тарту<br></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
      <div class="container" id="Initiatives">
        <div class="row">
          <div class="col-lg-12 mt-5">
<h2 class="h2-left pr-20 section-title text-center">
Тұрақты даму саласындағы бастамаларды іске асыру тәсілдері 
			  </h2>
			</div>
		</div>
	</div>
    </section>
<section>
	<div class="container mt-5 mb-5">
		<div class="row">
			<div class="col-lg-12">
				<div class="box-green devel-10 text-center">

        			<div class="row">
						<div class="col-lg-2">

						</div>
						<div class="col-lg-9 text-left">
							<div class="block-check">
								<span>Тиімді корпоративтік басқару және жетік тәуекел мәдениеті</span>
								Жаңа корпоративтік басқару Кодексін және тұрақты дамудың тәуекелге бағытталған
әдісін қабылдаумен қол жеткізіледі

							</div>
							<div class="block-check">
								<span>Қаржылық тұрақтылық </span>
								Даму жоспарларында белгіленген дамудың басым міндеттерін орындаумен қамтамасыз етіледі
							</div>
							<div class="block-check">
								<span>Адами әлеуетті  дамыту </span>
								ОбҮш негізгі құрам бөлігі: қызметкерлерді басқару саласындағы іскери әріптестік, қызметкерлерді басқару бойынша сараптама орталығы және қызметкерлерді басқарудың жалпы қызметі бар қызметкерлерді басқарудың жаңа жүйесін іске қосумен қамтамасыз етіледі

							</div>
							<div class="block-check">
								<span>Жоғары этикалық стандарттар және сыбайлас жемқорлықпен күрес</span>
								Мінез-құлық Кодексін жүзеге асырумен және талаптарды орындау функциясын жүзеге асырумен қамтамасыз етіледі

							</div>

<div class="block-down-box" data-id="0">

							<div class="block-check">
								<span>Жауапты сатып алулар </span>
Сатып алулардың жаңа әдістерін, соның ішінде сатып алулардың санаттық басқаруын енгізу және әлеуетті жеткізушілерді алдын ала біліктеу жүйесін іске қосумен және сатып алулар процессінің ашықтығын арттыру жолымен қамтамасыз етіледі
							</div>
							<div class="block-check">
								<span>Жақсы бедел
 және ашықтықтың жоғары деңгейі </span>
Коммуникациялық жоспарды жүзеге асыру және ақпаратты ашумен қамтамасыз етіледі
							</div>
							<div class="block-check">
								<span>Еңбекті қорғау саласындағы тұрақты құндылықтар </span>
Қордың портфельдік компанияларында қауіпсіздік пен еңбекті қорғау
саласындағы тұрақты құндылықтарын ынталандырумен қамтамасыз етіледі
							</div>
							<div class="block-check">
								<span>Жауапты инвестициялар </span>
Қордың инвестициялық саясатында жауапты инвестициялардың негізгі ұстанымдарын енгізумен және инвестицияларды жобалау, енгізу және мониторингтеу процессінде ESG тұрақтылық көрсеткіштерін таңдаумен қамтамасыз етіледі
							</div>
							<div class="block-check">
								<span>Экологиялық жауапкершілік </span>
Қоршаған ортаны қорғау саласында экологиялық заңнаманы, нормалар мен стандарттарды сақтау, сондай-ақ Қордың және Қор Тобының экологиялық қызметін дамытуға инвестициялау арқылы қамтамасыз етіледі
							</div>

</div>


						</div>

					</div>
        			<div class="row">
						<div class="col-lg-6 text-center">
							<a href="" class="block-down" data-id="0"></a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

