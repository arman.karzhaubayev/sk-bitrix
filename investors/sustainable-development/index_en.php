<?$APPLICATION->SetTitle("Sustainable development");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>

    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('sustainable-development')?>
            </h2>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<p class="t-1 mb-5 mt-3">
	<b>The Fund’s Development Strategy until 2028</b> which defines corporate governance and sustainable development as one of the three strategic goals of the Fund for the next decade, was approved by the Sole Shareholder of the Fund in 2018.
As part of this strategic goal, we are planning to continue to achieve a high level of corporate governance and sustainable development, to increase the corporate governance in the Fund and Portfolio companies, to increase human resources, and to ensure the implementation of meritocracy principles. The new strategy also provides for the implementation of sponsorship initiatives, as well as sustainable development initiatives identified by the Fund. The Fund will implement measures and indicators within the Concept of the Republic of Kazakhstan for the transition to the green economy.

			  </p>
<p class="t-1 mb-5">
	<b>Based on the results of the implementation of this strategic goal</b>, it is expected to create a management system that ensures the achievement of improved indicators in sustainable development, labor protection, health and environment, improved economic indicators and financial stability, anti-corruption, increased corporate governance rating and social stability rating.
</p>

          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-3">
				<div class="devel-01">
					<span class="title">Strategic goal
						<div class="block-line-3"></div>
					</span>
					<p>Performance of companies</p>
				</div>
          </div>
          <div class="col-lg-6">
				<div class="devel-02">
					<span class="title">Strategic goal
						<div class="block-line-3"></div>
					</span>
					<p>Sustainable development</p>
				</div>

          </div>
          <div class="col-lg-3">
				<div class="devel-01">
					<span class="title">Strategic goal
						<div class="block-line-3"></div>
					</span>
					<p>Portfolio management</p>
				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
				<a class="anchor" href="#Objectives"><div class="devel-03 devel-03-first-child">
					<span class="devel-03-01"></span>
					<p>Sustainable Development Goals</p>
				</div></a>
				<a class="anchor" href="#Analysis"><div class="devel-03">
					<span class="devel-03-02"></span>
					<p>Materiality Analysis</p>
				</div></a>
				<a class="anchor" href="#Interaction"><div class="devel-03">
					<span class="devel-03-03"></span>
					<p>Stakeholder Engagement</p>
				</div></a>
				<a class="anchor" href="#Initiatives"><div class="devel-03 devel-03-last-child">
					<span class="devel-03-04"></span>
					<p>Implementation Methods of Sustainability Initiatives </p>
				</div></a>
          </div>

        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div id="Objectives" class="col-lg-12 mt-5">
				<div class="devel-04">
					<span>Sustainable Development Goals</span>
					<p>The Fund supports UN Sustainable Development Goals</p>
					<br><b>On January 1, 2016 the world has officially launched the 2030 Agenda for Sustainable Development</b> - an active transformation plan based on 17 sustainable development goals - to address pressing global challenges over the next fifteen years.
					<br><b>17 sustainable development goals and 169 tasks (SDGs)</b> are aimed at realizing human rights for all, they are complex and indivisible and ensure the balance of the three components of sustainable development: economic, social and environmental. These tasks and goals will stimulate the actions of national governments over the next 15 years in the areas of great importance to humanity and the planet. 
We have chosen nine goals of these that have strategic priority for the activities of the Fund Group, the achievement of which it will contribute to in the course of its core business. The current activities also integrate tasks that enable the achievement of the remaining UN sustainability goals.
The sustainable development goals and objectives of the Fund Group are consistent with the UN selected sustainable development priorities. 
<!--
					<div class="text-center mt-5">
						<a href="" class="devel-btn">More</a>
					</div>
-->
				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
				<div class="devel-05">
					<div class="devel-06">
						<span></span>
						<p>Good health <br>and well-being</p>
						<div class="devel-06-01"></div>
					</div>
					<div class="devel-06">
						<span></span><p>Quality education</p>
						<br><div class="devel-06-02"></div>
					</div>
					<div class="devel-06">
						<span></span><p>Gender equality</p>
						<br><div class="devel-06-03"></div>
					</div>
					<div class="devel-06">
						<span></span><p>Decent work and economic growth</p>
						<div class="devel-06-04"></div>
					</div>
					<div class="devel-06">
						<span></span><p>Industry, innovation, and infrastructure</p>
						<div class="devel-06-05"></div>
					</div>
					<div class="devel-06 devel-06-00">
						<span></span><p>Reduced <br>inequalities</p>
						<div class="devel-06-06"></div>
					</div>
					<div class="devel-06 devel-06-00">
						<span></span><p>Sustainable cities and communities</p>
						<div class="devel-06-07"></div>
					</div>
					<div class="devel-06 devel-06-00">
						<span></span><p>Climate action</p>
						<div class="devel-06-08"></div>
					</div>
					<div class="devel-06 devel-06-00">
						<span></span><p>Partnerships</p>
						<div class="devel-06-09"></div>
					</div>

				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container" id="Analysis">
        <div class="row">
          <div class="col-lg-12 mt-5">
				<div class="devel-07">
			        <div class="row">
          				<div class="col-lg-4">
							<div class="devel-07-01">
Materiality<br> Analysis
							</div>
							<p class="devel-07-02">
In order to understand the needs of stakeholders and determine what the Fund’s sustainable development is for them, we conducted a materiality analysis.
							</p>
						</div>
          				<div class="col-lg-2">
						</div>
          				<div class="col-lg-6">

							<p class="devel-07-02 devel-07-03">At the first stage, it was proposed to determine a list of aspects of sustainable development related to the activities of the Fund and their impact.</p>
							<p class="devel-07-02">
Based on the results of the survey and discussions with all key stakeholders to identify significant aspects, we identified 17 topics related to sustainability that are important for the Fund and our stakeholders.

							</p>
						</div>
					</div>

				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
<p class="centerp">
Comprehensively covering economic, environmental and social aspects on the path of sustainable development, the Fund seeks to ensure an interactive dialogue with all interested parties and create additional value.
			  </p>
          </div>
        </div>
      </div>
</section>
	<section>
      <div class="container mt-5">
        <div class="row">
          <div class="col-lg-6">
				<div class="devel-08">
					<span class="devel-08-01">Economic topics:</span><br>
					<div class="block-line-3"></div><br><br>
					<i class="arrow-green-down">Key financial indicators<br></i>
					<i class="arrow-green-down">Anti-corruption<br></i>
					<i class="arrow-green-down">Financial stability<br></i>
					<i class="arrow-green-down">Socially Responsible Procurement<br></i>
				</div>
          </div>
          <div class="col-lg-6">
				<div class="devel-08">
					<span class="devel-08-01 devel-08-02">Ecological topics:
</span><br>
					<div class="block-line-3"></div><br><br>
					<i class="arrow-green-down">Environmental <br> Compliance</i>
				</div>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container mt-5">
        <div class="row">
          <div class="col-lg-12">
				<div class="devel-08">

        			<div class="row">
			          <div class="col-lg-5">
							<span class="devel-08-01 devel-08-03">Social topics:</span><br>
							<div class="block-line-3"></div><br><br>
						</div>
					</div>

        			<div class="row">
			          <div class="col-lg-6">

					<i class="arrow-green-down">Non-discrimination<br></i>
					<i class="arrow-green-down">Human rights<br></i>
					<i class="arrow-green-down">Social Responsibility<br></i>
					<i class="arrow-green-down">Health and Safety<br></i>
					<i class="arrow-green-down">Employment<br></i>
					<i class="arrow-green-down">Transparency and Public Relations<br></i>

						</div>
			          <div class="col-lg-6">

					<i class="arrow-green-down">Personnel screening and employment<br></i>
					<i class="arrow-green-down">Employee-management interactions<br></i>
					<i class="arrow-green-down">Corporate Culture<br></i>
					<i class="arrow-green-down">Remuneration and Evaluation<br></i>
					<i class="arrow-green-down">Training and education<br></i>
					<i class="arrow-green-down">Freedom of association <br>and collective bargaining<br></i>

						</div>
          			</div>
        		</div>
        	</div>
        </div>
      </div>
</section>
	<section>
      <div class="container mt-5" id="Interaction">
        <div class="row">
          <div class="col-lg-12">
<p class="newh3">
Stakeholder Engagement
			  </p>
<p class="t-1 mb-1">
	<b>The Fund's interaction with stakeholders is focused</b> on achieving the goals of sustainable development and harmonizing the interests of all stakeholders. Interaction with stakeholders is based on the principles of respect for interests and cooperation 
			  </p>
<p class="t-1 mt-3">

<b>The Fund sees its social responsibility in being</b> a reliable partner for stakeholders. We conduct a dialogue and build relations with stakeholders on the basis of the principles of mutual respect and partnership, information transparency, regularity of interaction and faithful observance of commitments undertaken.

</p>
        	</div>
        </div>
      </div>
</section>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
<h2 class="h2-left pr-20 section-title text-center">
The Fund uses a Standard process of Stakeholder engagement in accordance with AA1000 SES:


			  </h2>
			</div>
		</div>
	</div>
    </section>
<section>
		<div class="container">
        <div class="row">
          <div class="col-lg-1">
          </div>
          <div class="col-lg-5">
				<div class="block-01">
					Identification <br>of Stakeholders

				</div>
				<div class="block-01">
					Prioritization <br>of Stakeholders

				</div>
				<div class="block-01">
					Planning of Stakeholders<br> Interaction

				</div>


          </div>
          <div class="col-lg-5">
				<div class="block-01">
					Interaction<br> with Stakeholders


				</div>
				<div class="block-01">
					Efficiency and Assessment<br> Reporting
				</div>
			</div>

</section>

<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-1">
<h2 class="h2-left pr-20 section-title text-center">
Principles  of interaction with stakeholders
			  </h2>
			</div>
		</div>
	</div>
    </section>

<section>
	<div class="container mt-5">
		<div class="row">
			<div class="col-lg-12">
				<div class="orange-block devel-09">

        			<div class="row">
						<div class="col-lg-4">
							<i class="arrow-green-down">Information disclosure<br></i>
							<i class="arrow-green-down">Proper management <br>functions<br></i>
							<i class="arrow-green-down">Identifying and studying<br> stakeholders<br></i>
						</div>
						<div class="col-lg-4">
							<i class="arrow-green-down">Accountability <br></i>
							<i class="arrow-green-down">Consultations with stakeholders <br></i>
							<i class="arrow-green-down">Negotiations  and partnership  <br></i>
						</div>
						<div class="col-lg-4">
							<i class="arrow-green-down">Conflicts   settlement  procedures<br></i>
							<i class="arrow-green-down">Stakeholder  involvement<br></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
<h2 class="h2-left pr-20 section-title text-center">
Implementation Methods of Sustainability Initiatives 
			  </h2>
			</div>
		</div>
	</div>
    </section>
<section>
	<div id="Initiatives" class="container mt-5 mb-5">
		<div class="row">
			<div class="col-lg-12">
				<div class="box-green devel-10 text-center">

        			<div class="row">
						<div class="col-lg-2">

						</div>
						<div class="col-lg-9 text-left">
							<div class="block-check">
								<span>Strong corporate governance and improved risk culture </span>
								Is provided by adoption and implementation of new Corporate Governance Code and Risk based approach to Sustainability
							</div>
							<div class="block-check">
								<span>Financial sustainability</span>
								Is provided by implementation of development priorities defined in Development plans
							</div>
							<div class="block-check">
								<span>Strong Human Development </span>
								Is provided by launching the new HR system with 3 main components: HR business partnership, HR expertise Center, General HR Service

							</div>
							<div class="block-check">
								<span>High ethical standards and anti-corruption </span>
								Is provided by Implementation of Ethics Code and Compliance function 

							</div>

<div class="block-down-box" data-id="0">

							<div class="block-check">
								<span>Responsible procurement </span>
Is provided by introduction of the new procurement approach, including category procurement management and launching the system of preliminary qualification of potential suppliers, as well as improved transparency of procurement process 
							</div>
							<div class="block-check">
								<span>Good reputation and high level of transparency  </span>
Is provided by Implementation of the Communication Plan and increasing the transparency and information disclosure processes
							</div>
							<div class="block-check">
								<span>Strong H&S culture </span>
Is provided by promotion of strong H&S culture within the Portfolio companies of the Fund
							</div>
							<div class="block-check">
								<span>Responsible investment  </span>
Is provided by implementing key Responsible Investment Principles in Investment Policy of the Fund and fitting sustainability metrics of ESG into the process of design, implementation and monitoring of an investment
							</div>
							<div class="block-check">
								<span>Ecological responsibility </span>
It is provided by observing environmental legislation, norms and standards in the field of environmental protection, and also by investing in the development of the environmental activities of the Fund and the Fund’s Group
							</div>

</div>


						</div>

					</div>
        			<div class="row">
						<div class="col-lg-6 text-center">
							<a href="" class="block-down" data-id="0"></a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

