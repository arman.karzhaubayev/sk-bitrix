<?$APPLICATION->SetTitle("Қоржын компаниялар");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>

 <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('portfolio-companies')?>
            </h2>
          </div>
        </div>
      </div>
</section>


    <section class="birzh-cat">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 desc">
<!--
            <p><b>
				«Қазақстан-2050» стратегиясы әлемнің дамыған 30 елінің қатарына кіру үшін </b>
кономикаға инвестициялар көлемін ұлғайту және еңбек өнімділігін арттыруды 
міндеттейді. Қор бірқатар аса ірі активтерді иелене отырып, 
Стратегия мақсаттарына қол жеткізуді қамтамасыз етуде маңызды рөл атқарады.
			  </p><p>
2018 жылдың қорытындысы бойынша «Самұрық-Қазына» ұлттық әл-ауқат қоры мен оған қарасты портфельдік компаниялардың Қазақстан экономикасындағы үлесі 4 трлн теңгеден асты.
</p><p>
Сонымен қатар, 2017 жылмен салыстырғанда Қор кірісі (ROE) 6,5%-дан 8,0%-ға дейін артып, Темасек Холдинг (Сингапур), Future Fund (Австралия) сынды халықаралық ұлттық әл-ауқат қорларының өсу қарқынына жетті.
</p>
-->
<p><b>
	«Самұрық-Қазына» АҚ</b> компаниялар тобына мұнай-газ және көлік-логистика секторларының, химия және атом өнеркәсіптерінің, тау-кен кешендерінің, энергетика және жылжымайтын мүлік салаларының кәсіпорындары кіреді.
</p><p>Қор активі шамамен $69 млрдты құрайды.</p>
</div>
        </div>
        
      </div>
    </section>
    <!-- PokazatPK -->
    <section class=" pb-75">
      <div class="container">
        
        <div class="row">
          <div class="col-lg-12 ">
            <h2 class="center pb-30 fsize30">
              <b>Қордың портфельді компанияларының көрсеткіштері</b>
            </h2>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="graybr5 pokaz-pk " style="height: 658px;">
            <div class="pokaz-pk-title fsize24">Активтердің таза құны, 2019 (млн. долл.)</div>
            <div class="pokaz-pk-items">

            <div class="pokaz-pk-items-l">
                
                  <ul>
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/001.png" alt="" /></div>
        <div class="pkpitem-title">Мұнай және газ</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 21 503</div>
      </li>
<!--
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/002.png" alt="" /></div>
        <div class="pkpitem-title">Промышленность</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 2 794</div>
      </li>
-->
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/003.png" alt="" /></div>
        <div class="pkpitem-title">Транспорт және логистика</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 3 138</div>
      </li>
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/004.png" alt="" /></div>
        <div class="pkpitem-title">Қаржылық активтер</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 15 130</div>
      </li>
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/005.png" alt="" /></div>
        <div class="pkpitem-title">Тау-кен саласы</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 1 321</div>
      </li>
</ul>
</div>



                </div>
                <div class="pokaz-pk-items-r">


                <ul>
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/006.png" alt="" /></div>
        <div class="pkpitem-title">Горнорудная
отрасль</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 4 841</div>
      </li>
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/007.png" alt="" /></div>
        <div class="pkpitem-title">Энергетика</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 2 536</div>
      </li>
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/008.png" alt="" /></div>
        <div class="pkpitem-title">Химия өнеркәсібі</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 511</div>
      </li>
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/009.png" alt="" /></div>
        <div class="pkpitem-title">
Жылжымайтын мүлік</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 118</div>
      </li>
      
</ul>
                </div>
            
                
            </div>
        </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /PokazatPK -->
    <!-- Slider -->
    <section class="IPO-section pb-75">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <p class="center section-title pb-1 desc">
              2019 жылдың қорытындылары бойынша «Самұрық-Қазына» АҚ
<br> портфолио компанияларының қаржылық көрсеткіштері
            </p>
            
          </div>
        </div>
      </div>
    </section>
    <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"main_slider", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "/investors/portfolio-companies/#ELEMENT_CODE#/",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "DETAIL_PICTURE",
			2 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => constant("PORTFOLIO_COMPANIES_" . LANGUAGE_ID),
		"IBLOCK_TYPE" => "portfolio_companies",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "50",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "ACTIVES",
			1 => "REVENUE",
			2 => "YEAR",
			3 => "OPERATION_PROFIT",
			4 => "EQUITY",
			5 => "FL",
			6 => "NET_PROFIT",
			7 => "WEB",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "main_slider",
		"SHOW_ALL_COMPANY" => "N",
	),
	false
);?>

    <!-- /Slider -->
    
