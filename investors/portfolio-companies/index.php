<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/pages.php");

$pagetitle = Loc::getMessage('PORTFOLIO_COMPANIES') ." — ". Loc::getMessage('INVESTORS_AND_STAKEHOLDERS') ." — ". Loc::getMessage("NAME_SITE");
$description = Loc::getMessage("DESCRIPTION_SITE");
$keywords = Loc::getMessage("KEYWORDS_SITE");
$APPLICATION->SetTitle($pagetitle);
$APPLICATION->SetPageProperty("title", $pagetitle);
$APPLICATION->SetPageProperty("description",  $description);
$APPLICATION->SetPageProperty("keywords",  $keywords);
$APPLICATION->SetPageProperty("og:type",  "website");
$APPLICATION->SetPageProperty("og:title",  $pagetitle);
$APPLICATION->SetPageProperty("og:description",  $description);
$APPLICATION->SetPageProperty("og:url",  "https://sk.kz".$APPLICATION->GetCurPage(false));
$APPLICATION->SetPageProperty("og:image",  "https://sk.kz".SITE_TEMPLATE_PATH."/thumb.jpg");
?>

<div class="companies">
	<div class="companies__header">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="title">
						<h2 class="title__text"><?=Loc::getMessage('PORTFOLIO_COMPANIES');?></h2>
					</div>

					<div class="companies__content">
						<?=Loc::getMessage('PORTFOLIO_COMPANIES_TEXT');?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="companies__list">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="title companies__title">
						<div class="title__line">
							<img
								class="title__line-border"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
								alt=""
							>

							<img
								class="title__line-center"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
								alt=""
							>

							<img
								class="title__line-border"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
								alt=""
							>
						</div>

						<h2 class="title__text">
							<?=Loc::getMessage('INDICATORS_OF_THE_FUNDS_PORTFOLIO_COMPANIES');?>
						</h2>
					</div>
				</div>

				<div class="col-12 col-sm-10 col-sm-start-2">
					<div class="iconsblock">
						<div class="iconsblock__header">
							<p class="iconsblock__title">
								<?=Loc::getMessage('NET_ASSET_VALUE');?>
							</p>
						</div>

						<div class="iconsblock__content">
							<div
								class="iconsblock__item"
								v-for="(item, index) in companiesCost[lang]"
								:key="index"
							>
								<img
									class="iconsblock__item-icon"
									:src="item.icon"
									alt=""
								>

								<p
									class="iconsblock__item-title"
									v-html="item.title"
								></p>

								<hr class="iconsblock__item-line">

								<p
									class="iconsblock__item-text"
									v-html="item.text"
								></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="companies__info">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="title companies__title">
						<div class="title__line">
							<img
								class="title__line-border"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
								alt=""
							>

							<img
								class="title__line-center"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
								alt=""
							>

							<img
								class="title__line-border"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
								alt=""
							>
						</div>

						<h2 class="title__text">
							<?=Loc::getMessage('FINANCIAL_INDICATORS_OF_PORTFOLIO_COMPANIES');?>
						</h2>
					</div>

					<div class="mainpage__companies"></div>
				</div>

				<div class="col-1 col-xs-0">
					<div class="mainpage__companies-arrow mainpage__companies-arrow_left">
						<img
							class="mainpage__companies-arrow-icon"
							src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow.svg"
							alt=""
						>
					</div>
				</div>

				<div class="col-10 col-xs-12">
					<div class="swiper-container maincompany-swiper-container">
						<div class="swiper-wrapper">
							<div
								class="swiper-slide"
								v-for="(item,index) in mainCompanies[lang]"
								:key="index"
							>
								<div class="companycard">
									<a
										:href="item.link"
										target="_blank"
									>
										<div class="companycard__image">
											<img
												class="companycard__image-src"
												:src="item.image"
												alt=""
											>

											<div class="companycard__overlay">
												<img
													class="companycard__overlay-logo"
													:src="item.hoverImage"
													alt=""
												>
											</div>
										</div>
									</a>

									<div class="companycard__content">
										<div class="companycard__title">
											<p
												class="companycard__title-text"
												v-html="item.name"
											></p>
										</div>

										<div class="companycard__indicators">
											<p class="companycard__indicators-text">
												{{ $t('messages.company1') }}

												<span v-html="item.value1"></span>
											</p>

											<p class="companycard__indicators-text">
												{{ $t('messages.company2') }}

												<span v-html="item.value2"></span>
											</p>

											<p class="companycard__indicators-text">
												{{ $t('messages.company3') }}

												<span v-html="item.value3"></span>
											</p>

											<p class="companycard__indicators-text">
												{{ $t('messages.company4') }}

												<span v-html="item.value4"></span>
											</p>

											<p
												class="companycard__indicators-text"
												v-if="item.value5"
											>
												{{ $t('messages.company5') }}

												<span v-html="item.value5"></span>
											</p>
										</div>

										<a
											:href="item.link"
											target="_blank"
										>
											<div class="companycard__btn">
												<p class="companycard__btn-text">{{ $t('messages.company6') }}</p>

												<img
													class="companycard__btn-icon"
													src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.png"
													alt=""
												>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-1 col-xs-0">
					<div class="mainpage__companies-arrow mainpage__companies-arrow_right">
						<img
							class="mainpage__companies-arrow-icon"
							src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow.svg"
							alt=""
						>
					</div>
				</div>

				<div class="col-10 col-start-2 col-xs-12 col-xs-start-1">
					<div class="mainpage__companies-nav">
						<div class="mainpage__companies-dots"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="partnerswrap">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="mpartners">
					<div class="mpartners__arrow mpartners__arrow_left">
						<img
							class="mpartners__arrow-icon"
							src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
							alt=""
						>
					</div>

					<div class="swiper-container mpartners__list">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<a
									href="https://hrqyzmet.kz/"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/brands_qyzmet.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://samruk-akparat.kz/"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aqparat.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://www.skc.kz/"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/samruk.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://aifc.kz/"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aifc_cmyk.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://primeminister.kz/ru"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/min.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="http://www.akorda.kz/ru"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-7.jpg"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://elbasy.kz/ru"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-6.jpg"
											alt=""
										>
									</div>
								</a>
							</div>
						</div>
					</div>

					<div class="mpartners__arrow mpartners__arrow_right">
						<img
							class="mpartners__arrow-icon"
							src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
							alt=""
						>
					</div>

					<div class="mpartners__mobile">
						<a
							href="https://hrqyzmet.kz/"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/brands_qyzmet.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://samruk-akparat.kz/"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aqparat.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://www.skc.kz/"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/samruk.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://aifc.kz/"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aifc_cmyk.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://primeminister.kz/ru"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/min.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="http://www.akorda.kz/ru"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-7.jpg"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://elbasy.kz/ru"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-6.jpg"
									alt=""
								>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>