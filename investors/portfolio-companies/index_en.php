<?$APPLICATION->SetTitle("Portfolio companies");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>

 <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             Portfolio companies
            </h2>
          </div>
        </div>
      </div>
</section>


    <section class="birzh-cat">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 desc">
<!--
    <p><b>Strategy «Kazakhstan-2050» sets an ambitious goal for the Republic </b>
        – to increase the volume of investment in the economy and enhance labour productivity for the purpose of becoming one of the 30 developed countries in the world. The Group owns a number of major assets and plays a pivotal role in achieving objectives specified in the Strategy. 
        Фонд играет важную роль в достижении данной цели, так как управляет крупнейшими национальными активами.
<p>According to the results of 2018, Samruk-Kazyna JSC and its portfolio companies share in the economy of the Republic of Kazakhstan has amounted to KZT 4 trillion.</p>

<p>At the same time, the Fund has shown an increase in profitability (ROE) at the level of 8.0%, comparing to 6.5% in 2017 and reached the growth rates of international peers, such as Temasek Holdings (Singapore), Future Fund (Australia).
</p>
-->
<p><b>
	«Samruk-Kazyna» group</b> includes companies in the oil and gas, transport and logistics sectors and the chemical and nuclear industries, and the metals and mining, energy and real estate sectors.
</p><p>The Fund's assets amount to about USD 69 billion.</p>
</div>
        </div>
        
      </div>
    </section>
    <!-- PokazatPK -->
    <section class=" pb-75">
      <div class="container">
        
        <div class="row">
          <div class="col-lg-12 ">
            <h2 class="center pb-30 fsize30">
              <b>Indicators of the Fund’s portfolio companies</b>
            </h2>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="graybr5 pokaz-pk " style="height: 658px;">
				<div class="pokaz-pk-title fsize24">Net asset value, 2019 (bil. doll.)</div>
            <div class="pokaz-pk-items">

            <div class="pokaz-pk-items-l">
                
                  <ul>
            <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/001.png" alt="" /></div>
        <div class="pkpitem-title">Oil & Gas</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 21 503</div>
      </li>
<!--
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/002.png" alt="" /></div>
        <div class="pkpitem-title">Промышленность</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 2 794</div>
      </li>
-->
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/003.png" alt="" /></div>
        <div class="pkpitem-title">Transport and logistics</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 3 138</div>
      </li>
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/004.png" alt="" /></div>
        <div class="pkpitem-title">Financial assets</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 15 130</div>
      </li>
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/005.png" alt="" /></div>
        <div class="pkpitem-title">Communications</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 1 321</div>
      </li>
</ul>
</div>



                </div>
                <div class="pokaz-pk-items-r">


                <ul>
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/006.png" alt="" /></div>
        <div class="pkpitem-title">Metals and mining</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 4 841</div>
      </li>
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/007.png" alt="" /></div>
        <div class="pkpitem-title">Energy</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 2 536</div>
      </li>
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/008.png" alt="" /></div>
        <div class="pkpitem-title">Chemical industry</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 511</div>
      </li>
      <li class="pokaz-pk-items">
        <div class="pkpitem-icon"><img src="/bitrix/templates/skfinance/img/icons/009.png" alt="" /></div>
        <div class="pkpitem-title">Real Estate</div>
        <div class="pkpitem-cost"><img src="/bitrix/templates/skfinance/img/icons/usd.png" alt="" /> 118</div>
      </li>
      
</ul>
                </div>
            
                
            </div>
        </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /PokazatPK -->
    <!-- Slider -->
    <section class="IPO-section pb-75">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <p class="center section-title pb-1 desc">
              Financial indicators of portfolio companies of<br> «Samruk-Kazyna» JSC at the end of 2019
            </p>
            
          </div>
        </div>
      </div>
    </section>
    <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"main_slider", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "/investors/portfolio-companies/#ELEMENT_CODE#/",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "NAME",
			1 => "DETAIL_PICTURE",
			2 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => constant("PORTFOLIO_COMPANIES_" . LANGUAGE_ID),
		"IBLOCK_TYPE" => "portfolio_companies",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "50",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "ACTIVES",
			1 => "REVENUE",
			2 => "YEAR",
			3 => "OPERATION_PROFIT",
			4 => "EQUITY",
			5 => "FL",
			6 => "NET_PROFIT",
			7 => "WEB",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "main_slider",
		"SHOW_ALL_COMPANY" => "N",
	),
	false
);?>

    <!-- /Slider -->
    
