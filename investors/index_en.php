<?$APPLICATION->SetTitle("Investors and stakeholders");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 page-title">
             Investors and stakeholders
            </h2>
            <h2 class="h2-left pr-20 pb-30 section-title text-left mt-3">
«Samruk-Kazyna»JSC»NWF invests in three areas: 
            </h2>
          </div>
        </div>
      </div>
</section>



<section class="birzh-cat">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 mt-3">
        <a href="#Finance" class="orng-btn anchor">Financial investments</a>
      </div>
      <div class="col-lg-4 text-center mt-3">
        <a href="#Strategy" class="orng-btn anchor">Strategic investment</a>
      </div>
      <div class="col-lg-4 text-right mt-3">
        <a href="#Full" class="orng-btn anchor">Direct investments</a>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container" id="Finance">
    <div class="row">
      <div class="col-lg-12 mt-3">
        <h2 class="h2-left pr-20 pb-30 section-title text-left mt-3">
          Financial investments
        </h2>
        <h2 class="h2-left pr-20 pb-30 page-title text-center mt-3">
          Privatization
        </h2>
      </div>
    </div>
  </div>
</section>


<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 mt-3">
        <h2 class="h2-left pr-20 pb-30 section-title text-center">
			<p class="centerp">
168 assets of the Samruk-Kazyna JSC
<br>group of companies are included 
<br>in the Comprehensive Privatization Plan for 2016-2020
</p>

        </h2>
      </div>
    </div>
  </div>
</section>


<!-- PLAN -->


<?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/plan_priv_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>

<!-- /PLAN -->

<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="hr pb-30"><img src="/bitrix/templates/skfinance/img/hr.png" alt="" /></div>
        <p class="centerp">
          <b>Currently the companies are executing internal<br />
 preparations to improve operational efficiency<br />
 and determine the best outline for probable IPO
</b> 
        </p>
      </div>
    </div>
  </div>
</section>


<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
    <?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/strat_invest_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
    <?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/spo_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>
      </div>
    </div>

  </div>
</section>

<section>
  <div class="container">

    <div class="row">
      <div class="col-lg-12">
        <h2 class="center pb-30 section-title mt-5">Assets</h2>
      </div>
    </div>
    <div class="row orange-blocks pb-15">
      <div class="col-lg-6">
        <div class="actv-01 pb-15">
          <div class="container">
          <div class="row">
<!--
            <div class="col-lg-2">
              <div class="block-06 img-003"></div>

            </div>
-->
            <div class="col-lg-12">
              <a class="list_black2" href="/documents/Assets to IPO_SPO_sale to strategic investors_eng.pdf">
              <p class="priv-09">Assets to IPO/SPO/sale to strategic investor
				<label class="block-line-3"></label>
				</p>
				</a>
            </div>
          </div>

          
          
        </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="actv-01 pb-15">
          <div class="container">
          <div class="row">
<!--
            <div class="col-lg-2">
              <div class="block-06 img-003"></div>

            </div>
-->
            <div class="col-lg-12">
<a class="list_black2" href="/documents/Assets were soled in 2016-2020_eng.pdf">
              <p class="priv-09">Assets sold on comprehensive privatization plan
				<label class="block-line-3"></label></p>
				</a>
            </div>
          </div>


          
        </div>
        </div>
      </div>
    </div>


    <div class="row orange-blocks pb-15 mt-4">
      <div class="col-lg-6">
        <div class="actv-01 pb-15">
          <div class="container">
          <div class="row">
<!--
            <div class="col-lg-2">
              <div class="block-06 img-003"></div>

            </div>
-->
            <div class="col-lg-12">
              <a class="list_black2" href="/documents/Assets in pre_sale preparation_eng.pdf">
              <p class="priv-09">Assets in pre-sale preparation
				<label class="block-line-3"></label>
				</p>
				</a>
            </div>
          </div>

          
          
        </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="actv-01 pb-15">
          <div class="container">
          <div class="row">
<!--
            <div class="col-lg-2">
              <div class="block-06 img-003"></div>

            </div>
-->
            <div class="col-lg-12">
              <a class="list_black2" href="/documents/Assets to reorganization and liquidation_eng.pdf">
              <p class="priv-09">Assets to reorganization and liquidation
				<label class="block-line-3"></label></p>
				</a>
            </div>
          </div>

          
          
        </div>
        </div>
      </div>
    </div>
    
    
    
  </div>
</section>
<section class="mt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
Privatization updates
            </h2>
          </div>
         </div>
        </div>
</section>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<!-- Swiper -->
  <div class="swiper-container swiper-container-news">
    <div class="swiper-wrapper">


		<div class="swiper-slide swiper-slide-news">
				<div class="news-slider-item corp-slider">
					<div class="text-left">

						<span class="news-date">26.09.2019</span>
						<span class="news-cat">Press releases</span>
<a class="news-title">
</a>
<span class="news-anons">

										
 Not for publication, distribution or release directly or indirectly, in whole or in part, in or into the united states, australia, canada, japan or any other jurisdiction in which offers or sales would be prohibited by law


 THE INFORMATION CONTAINED WITHIN THIS ANNOUNCEMENT IS DEEMED TO...																
</span>
					</div>
          		</div>
		</div>
<div class="swiper-slide swiper-slide-news">
				<div class="news-slider-item corp-slider">
					<div class="text-left">

						<span class="news-date">30.01.2019</span>
						<span class="news-cat">Portfolio companies</span>
<a class="news-title">
Notification of trades: KazMunayGas-Service NS JSC
</a>
<span class="news-anons">

										‘KazMunayGas-Service’ LLP announces the sale of 100% stake of in KazMunayGas-Service NS JSC. The electronic auction will be held at the site www.gosreestr.kz on February 27, 2019 at 10:00 am, Astana time. The applications are accepted since the publication of notification. The deadline for...																

</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-news">
				<div class="news-slider-item corp-slider">
					<div class="text-left">

						<span class="news-date">29.11.2018</span>
						<span class="news-cat">Portfolio companies</span>
<a class="news-title">
Bid notification: ‘KazMunayGas-Service NS’ JSC
</a>
<span class="news-anons">

										
	 ‘KazMunayGas-Service’ LLP announces a repeated sale of 100% stake of ‘KazMunayGas-Service NS’ JSC. The electronic auction will be held at the site www.gosreestr.kz on December 7, 2018 at 10:00 am, Astana time.


	 The applications are accepted since the publication of...																
</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-news">
				<div class="news-slider-item corp-slider">
					<div class="text-left">

						<span class="news-date">28.11.2018</span>
						<span class="news-cat">Portfolio companies</span>
<a class="news-title">
Notification of trades: «Aktobe international airport» JSC, «Atyrau international airport» JSC and «Pavlodar airport» JSC
</a>
<span class="news-anons">

										
	 «Airport Management Group» LLP announces the open two-stage tender of selling package of assets that includes: 100% shares of “Aktobe international airport” JSC, 100% shares of “Atyrau international airport” JSC and 100% shares of “Pavlodar airport” JSC. Please follow the link for 
	details....																
</span>
					</div>
          		</div>
		</div>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-news-button-next"></div>
    <div class="swiper-news-button-prev"></div>
  </div>

<!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('.swiper-container-news', {
      slidesPerView: 3,
      spaceBetween: 0,
      slidesPerGroup: 1,
      navigation: {
        nextEl: '.swiper-news-button-next',
        prevEl: '.swiper-news-button-prev',
      },
    });
  </script>
			</div>
		</div>
	</div>
    </section>
<section>
      <div class="container mt-5">
        <div class="row">
          <div class="col-lg-12 text-center mt-5 mb-5">
			  <a href="/investors/privatization/privatization-news/" class="btn-read-more">More news</a>
          </div>
         </div>
        </div>
    </section>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mt-3 mb-5">
				<div class="purchases-05 purchases-05-01 priv-06">
					<span class="title">
The privatization of the assets<br> of the Fund's group<br> of companies is carried out
						<div class="block-line-3"></div>
</span>
					<p>Within the framework of the Comprehensive Privatization Plan for 2016-2020, approved by the Decree of the Government of the Republic of Kazakhstan dated December 30, 2015 No. 1141 </p>



				</div>
          </div>
          <div class="col-lg-6 mt-3 mb-5">
				<div class="purchases-05 purchases-05-02 priv-06">
					<a href=""><span class="title">Rules for the sale of assets of the Fund's group within the framework of the Comprehensive Privatization Plan
											<div class="block-line-3"></div>
						</span></a>
				</div>

          </div>
         </div>
        </div>
    </section>
<section class="">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 text-center">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
Contacts on privatization of assets the group<br>
of the Fund of Companies Samruk-Kazyna JSC 
            </h2>

          </div>
          <div class="col-lg-6 text-center">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
	            Contacts of Commissions on assets divestiture and facilities <br>
				of Samruk-Kazyna JSC:

            </h2>
          </div>
         </div>
        <div class="row">
          <div class="col-lg-12 text-center">

<a href="" class="block-down" data-id="1" onclick="$(this).hide();" style="    float: none;
    background: url(/bitrix/templates/skfinance/img/corp-arrow.png) no-repeat center center #FFF;
    transform: rotate(90deg);
    width: 45px;
    height: 75px;
    padding: 0;
    margin: 0;"></a>
          </div>
         </div>

        </div>
</section>
<section>
      <div class="container priv-07 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-2">

          </div>
          <div class="col-lg-4">
			  <b>Curator:Samruk-Kazyna JSC</b>
          </div>
          <div class="col-lg-4 text-right">
+7 (7172) 55-26-26

          </div>
         </div>
        </div>
      <div class="container priv-07 block-down-box priv-07 " data-id="1">
		<div class="row mt-5">
          <div class="col-lg-2">

          </div>
          <div class="col-lg-4 ">
<b>Single operator:<br>
	Samruk-Kazyna Contract LLP</b>
	


          </div>
          <div class="col-lg-4 text-right">
+7 (7172) 55-90-67,<br>
55-29-68, 55-29-86


          </div>
         </div>
        </div>

</section>
<!--
<section class="mt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
	            Contacts of Commissions on assets divestiture and facilities <br>
				of Samruk-Kazyna JSC:

            </h2>
<a href="" class="block-down" data-id="2" onclick="$(this).hide();" style="    float: none;
    background: url(/bitrix/templates/skfinance/img/corp-arrow.png) no-repeat center center #FFF;
    transform: rotate(90deg);
    width: 45px;
    height: 75px;
    padding: 0;
    margin: 0;"></a>
          </div>
         </div>
        </div>
</section>
-->
<section>

      <div class="container priv-07 mb-5 block-down-box priv-07 mt-5" data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>Samruk-Energy JSC </b>
          </div>
          <div class="col-lg-3">
+7 (7172) 55-31-34
          </div>
          <div class="col-lg-3">
<b>Каzpost JSC</b>
          </div>
          <div class="col-lg-3">
7 (7172) 61-16-99 ext. 1212
          </div>
         </div>
        </div>
      <div class="container priv-07 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>KazMunayGas National Company JSC</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 78-65-80
          </div>
          <div class="col-lg-3">
<b>Kazakhtelecom JSC </b>
          </div>
          <div class="col-lg-3">
+7 (727) 258-32-97
          </div>
         </div>
        </div>

      <div class="container priv-07 mt-5 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>Air Astana JSC</b>
          </div>
          <div class="col-lg-3">
+7 (727) 258-41-36
          </div>
          <div class="col-lg-3">
<b>United Chemical Company LLP</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 27-71-27 ext. 125
          </div>
         </div>
        </div>

      <div class="container priv-07 mt-5 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>Kazakhstan temir zholy National Company JSC</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 60-35-20
          </div>
          <div class="col-lg-3">
<b>Tau-Ken Samruk JSC</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 55-95-23
          </div>
         </div>
        </div>

      <div class="container priv-07 mt-5 mb-5 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>KEGOC JSC</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 69-35-56 <br>
+7 (702) 99-99-640
          </div>
          <div class="col-lg-3">
			  <b>Kazatomprom National Company JSC</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 45-80-15
          </div>
         </div>
        </div>

</section>


<section>
  <div class="container" id="Strategy">

    <div class="row invest-desc mt-5 pb-30">
      <div class="col-lg-12">
		<h2 class="h2-left pr-20 pb-30 section-title text-left mt-3">
          Strategic investment
        </h2>
		<h2 class="h2-left pr-20 pb-30 page-title text-center mt-3">
          Projects
        </h2>

      </div>
    </div>



    <div class="row invest-desc mt-5 pb-30">
      <div class="col-lg-12">
        <p class="pb-30 newh3">
          <b>Construction and expansion projects of the “Burnoye” solar power plant in Zhambyl region</b>
        </p>
        <p class="pb-15"><strong>These projects were implemented by “Samruk-Kazyna Invest”</strong> in partnership with UG Energy Limited (UK) on the basis of project companies "Burnoye Solar-1" LLP and "Burnoye Solar-2" LLP with a 49% participation share in “Samruk-Kazyna Invest”. Also, within the framework of these projects, the EBRD  Bank has attracted debt financing.</p>
        <p class="pb-15"><strong>The first and second stages of the solar power plant </strong>were commissioned in 2015 and 2018, respectively.</p>
      </div>
    </div>
    
        <?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/orange_blocks_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>

    
    <div class="row">
      <div class="col-lg-12">
        <div class="hr pb-30"><img src="/bitrix/templates/skfinance/img/hr.png" alt=""></div>
        <p class="centerp pb-30">
          The entire volume of electricity generated redeems LLP "Settlement and financial center <br>
          to support renewable energy sources" on the basis of the Law "About support of use of renewable sources of energy."
        </p>
      </div>
    </div>
    
    <?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/partners_projects_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>
        </div>

</section>


<section>
  <div class="container"  id="Full">

    <div class="row invest-desc mt-5 pb-30">
      <div class="col-lg-12">
		<h2 class="h2-left pr-20 pb-30 section-title text-left mt-3">
          Direct investments
        </h2>
		<h2 class="h2-left pr-20 pb-30 page-title text-center mt-3">
          Investment projects
        </h2>

      </div>
    </div>
  </div>
</section>


<section class="">
  <div class="container">
    <div class="row invest-desc pb-30">
      <div class="col-lg-12">
        <p>
Under the new Development Strategy, Samruk-Kazyna invests in new initiatives together with strategic investors that have experience and technologies for joint financing and implementation of the project. Establishing integrated businesses in partnership with leading companies enables Samruk-Kazyna to diversify the portfolio and create new companies that will drive the structural transformation of the national economy.

</p>
      </div>
    </div>
  </div>
</section>
<!-- /ACTIVES -->

<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
	      <div class="box-green-big invp-01-p">
        <div class="container pb-75">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="center section-title white mb-5">Investment Sectors:<br /></h2>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-001 va-mdl"></div>
              <p class="invp-01">Chemical  <br />industry</p>
            </div>
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-002 va-mdl"></div>
              <p class="invp-01">
                Manufacturing  <br />
                industry
              </p>
            </div>
            <div class="col-lg-4">
              <div class="block-03 icon-otr-003 va-mdl"></div>
              <p class="invp-01">
                Mining and metallurgic <br />
                complex
              </p>
            </div>
          </div>
        </div>
        <div class="container pb-60">
          <div class="row">
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-004 va-mdl"></div>
              <p class="invp-01">Engineering industry</p>
            </div>
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-005 va-mdl"></div>
              <p class="invp-01">
                Transport and <br />
                logistics
              </p>
            </div>
            <div class="col-lg-4">
              <div class="block-03 icon-otr-006 va-mdl"></div>
              <p class="invp-01">IT and digitalization</p>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-007 va-mdl"></div>
              <p class="invp-01">Infrastructure</p>
            </div>
            <div class="col-lg-8 mw-280">
              <div class="block-03 icon-otr-008 va-mdl"></div>
              <p class="invp-01">RES</p>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section class="mt-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
	      <div class="box-green-big priv-03" style="padding: 60px;">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="center section-title white mb-5">
Investment mechanisms for the cooperation with Samruk-Kazyna 
</h2>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
Samruk-Kazyna is actively engaged in attracting FDI in Kazakhstan through mutually beneficial investment structures. In particular, there are two schemes of the potential partnership: co-investment and investment through Samruk-Kazyna’s private equity funds.

            </div>
          </div>
        </div>

        </div>
      </div>
    </div>
  </div>
</section>

<section class="mt-5 mb-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
	      <div class="block-yellow priv-03" style="padding: 60px;min-height: auto;color: #000000;">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="center section-title white mb-5" style="color: #000000;">
Decision-making Process:
</h2>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
The process of decision-making in Samruk-Kazyna is guided by responsibility to 
the present and future generations of Kazakhstan. 
<br>We adhere to a rigorous phased approach for each investment in conformity with Corporate Standard of investment activity.


            </div>
          </div>
        </div>

        </div>
      </div>
    </div>
  </div>
</section>
<!--
<section class="mt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
				Contacts:
            </h2>
          </div>
         </div>
        </div>
</section>
<section>
      <div class="container priv-07">
        <div class="row">
          <div class="col-lg-2">

          </div>
          <div class="col-lg-4">
			  <b>Director of International Cooperation Department</b>
          </div>
          <div class="col-lg-4 text-right">
+7 7172 55 4036

          </div>
         </div>
        </div>
      <div class="container priv-07 mb-5">
        <div class="row mt-5">
          <div class="col-lg-2">

          </div>
          <div class="col-lg-4 ">
<b>Head of Initiative Search Sector</b>

          </div>
          <div class="col-lg-4 text-right">
+7 7172 55 4072


          </div>
         </div>
        </div>

</section>

<section class="mb-5">
      <div class="container priv-07">
        <div class="row">
          <div class="col-lg-1">

          </div>
          <div class="col-lg-6">
			  <b>Sultanov Aslan:</b><br> 
			  <span style="font-weight: 400;">Director of International Cooperation Department</span>
          </div>
          <div class="col-lg-3 text-right">
+7 7172 55 4036<br>
			  <span style="font-weight: 400;">a.sultanov@sk.kz</span>
          </div>
         </div>
        </div>
      <div class="container priv-07 ">
        <div class="row mt-5">
          <div class="col-lg-1">

          </div>
          <div class="col-lg-6 ">
<b>Abilmazhinov Saken:</b><br>
			  <span style="font-weight: 400;">Head of Initiative Search Sector</span>

          </div>
          <div class="col-lg-3 text-right">
+7 7172 55 4072<br>
			  <span style="font-weight: 400;">s.abilmazhinov@sk.kz</span>


          </div>
         </div>
        </div>

</section>



-->