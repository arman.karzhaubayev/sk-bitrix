<?$APPLICATION->SetTitle("Инвесторам и стейкхолдерам");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 page-title">
             Инвесторам и стейкхолдерам
            </h2>
            <h2 class="h2-left pr-20 pb-30 section-title text-left mt-3">
АО «ФНБ «Самрук-Қазына» осуществляет инвестиции по трем направлениям: 
            </h2>
          </div>
        </div>
      </div>
</section>





<section class="birzh-cat">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 mt-3">
        <a href="#Finance" class="orng-btn anchor">Финансовые инвестиции</a>
      </div>
      <div class="col-lg-4 text-center mt-3">
        <a href="#Strategy" class="orng-btn anchor">Стратегические инвестиции</a>
      </div>
      <div class="col-lg-4 text-right mt-3">
        <a href="#Full" class="orng-btn anchor">Прямые инвестиции</a>
      </div>
    </div>
  </div>
</section>


<section>
  <div class="container" id="Finance">
    <div class="row">
      <div class="col-lg-12 mt-3">
        <h2 class="h2-left pr-20 pb-30 section-title text-left mt-3">
          Финансовые инвестиции
        </h2>
        <h2 class="h2-left pr-20 pb-30 page-title text-center mt-3">
          Приватизация
        </h2>
      </div>
    </div>
  </div>
</section>

<!-- PLAN -->

<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 mt-3">
        <h2 class="h2-left pr-20 pb-30 section-title text-center">
			<p class="centerp">
          В Комплексный план приватизации на 2016-2020 годы 
<br>включены 168 активов группы компаний АО «Самрук-Қазына»  
			</p>

        </h2>
      </div>
    </div>
  </div>
</section>


<?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/plan_priv_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>

<!-- /PLAN -->

<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="hr pb-30"><img src="/bitrix/templates/skfinance/img/hr.png" alt="" /></div>
        <p class="centerp">
          <b>В настоящее время портфельные компании осуществляют</b> <br />
          внутреннюю подготовительную работу по повышению операционной<br />
          и финансовой эффективности и определению оптимального<br />
          периметра для возможного IPO.
        </p>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/strat_invest_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>
  </div>
</section>
<section>
  <div class="container">
    <!-- SPO Block -->
    <?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/spo_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>

	<div class="row">
      <div class="col-lg-12">
        <h2 class="center pb-30 section-title mt-5">Активы</h2>
      </div>
    </div>

<div class="row orange-blocks pb-15">
      <div class="col-lg-6">
        <div class="actv-01 pb-15">
          <div class="container">
          <div class="row">
<!--
            <div class="col-lg-2">
              <div class="block-06 img-003"></div>

            </div>
-->
            <div class="col-lg-12">
              <a target="_blank" class="list_black2" href="/documents/Активы на  IPO_SPO_продажу стратегинвесторам_rus.pdf">
              <span class="priv-09">Активы, подлежащие к выводу на IPO / SPO/ продаже стратегическому инвестору
				<label class="block-line-3"></label>
				</span>
				</a>
            </div>
          </div>
          <!--
          <div class="container ">
          <div class="row actv-02">
            <div class="col-lg-1">
              <div class="pdf"></div>
              
            </div>
            <div class="col-lg-11">
              
				<a href="/documents/Активы на IPO_SPO_продажу стратегинвестору_ru.pdf" class="actv-02-p">Активы на IPO_SPO_продажу стратегинвестору</a>

            </div>
          </div>

          </div>
          -->
          
        </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="actv-01 pb-15">
          <div class="container">
          <div class="row">
<!--
            <div class="col-lg-2">
              <div class="block-06 img-003"></div>

            </div>
-->
            <div class="col-lg-12">
<a target="_blank" class="list_black2" href="/documents/Активы_реализованные в 2016-2020 гг_rus.pdf" >
              <p class="priv-09">Активы, реализованные  в рамках Комплексного плана приватизации<br>&nbsp;
				<label class="block-line-3"></label></p>
				</a>
            </div>
          </div>
          <!--
          <div class="container ">
          <div class="row actv-02">
            <div class="col-lg-1">
              <div class="pdf"></div>
              
            </div>
            <div class="col-lg-11">
              
              <a href="/documents/Активы_реализованные в 2016-2019 годах_ru.pdf" class="actv-02-p">
Активы реализованные в 2016-2019 годах</a>

            </div>
          </div>
          </div>
-->          

        </div>
        </div>
      </div>
    </div>


<div class="row orange-blocks pb-15 mt-4">
      <div class="col-lg-6">
        <div class="actv-01 pb-15">
          <div class="container">
          <div class="row">
<!--
            <div class="col-lg-2">
              <div class="block-06 img-003"></div>

            </div>
-->
            <div class="col-lg-12">
              <a target="_blank" class="list_black2" href="/documents/Активы в предпродажной подготовке_rus.pdf">
              <p class="priv-09">Активы в процессе предпродажной  подготовки
				<label class="block-line-3"></label>
				</p>
				</a>
            </div>
          </div>
          <!--
          <div class="container ">
          <div class="row actv-02">
            <div class="col-lg-1">
              <div class="pdf"></div>
              
            </div>
            <div class="col-lg-11">
              
              <a href="/documents/Активы в предпродажной подготовке_ru.pdf" class="actv-02-p">
Активы в предпродажной подготовке</a>

            </div>
          </div>
          </div>
          
          -->
        </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="actv-01 pb-15">
          <div class="container">
          <div class="row">
<!--
            <div class="col-lg-2">
              <div class="block-06 img-003"></div>

            </div>
-->
            <div class="col-lg-12">
              <a target="_blank" class="list_black2" href="/documents/Активы на реорганизацию и ликвидацию_rus.pdf">
              <p class="priv-09">Активы, подлежащие реорганизации и ликвидации
				<label class="block-line-3"></label></p>
				</a>
            </div>
          </div>
          <!--
          <div class="container ">
          <div class="row actv-02">
            <div class="col-lg-1">
              <div class="pdf"></div>
              
            </div>
            <div class="col-lg-11">
              
              <a href="/documents/Активы на реорганизацию и ликвидацию_ru.pdf" class="actv-02-p">
Активы на реорганизацию и ликвидацию</a>

            </div>
          </div>
          </div>
          -->
          
        </div>
        </div>
      </div>
    </div>

  </div>
</section>



<section class="mt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
Новости приватизации
            </h2>
          </div>
         </div>
        </div>
</section>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<!-- Swiper -->
  <div class="swiper-container swiper-container-news">
    <div class="swiper-wrapper">


		<div class="swiper-slide swiper-slide-news">
				<div class="news-slider-item corp-slider">
					<div class="text-left">

						<span class="news-date">26.09.2019</span>
						<span class="news-cat">Новости Фонда</span>
<a class="news-title">
Самрук-Қазына объявил
результаты дополнительного
размещения
</a>
<span class="news-anons">
На Лондонской фондовой бирже и Бирже «Международного финансового центра «Астана» состоялось доразмещение глобальных депозитарных расписок ведущей компании Фонда – АО «НАК «Казатомпром». Размещение осуществлено в соответствии с правилами и правовыми нормативами бирж путем Accelerated Book-Building – ускоренного размещения акций компании.
</span>
					</div>
          		</div>
		</div>
<div class="swiper-slide swiper-slide-news">
				<div class="news-slider-item corp-slider">
					<div class="text-left">

						<span class="news-date">18.05.2019</span>
						<span class="news-cat">Пресс-Релизы</span>
<a class="news-title">
Казатомпром продал 75% долей участия в уставных капиталах ТОО «Astana Solar», ТОО «Kazakhstan Solar Silicon» и ТОО «МК «KazSilicon»
</a>
<span class="news-anons">

</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-news">
				<div class="news-slider-item corp-slider">
					<div class="text-left">

						<span class="news-date">27.02.2019</span>
						<span class="news-cat">Новости компаний</span>
<a class="news-title">
Извещение о торгах:
АО «КазМунайГаз-
Сервис NS»
</a>
<span class="news-anons">
ТОО «КазМунайГаз-Сервис» объявляет о повторном проведении торгов по реализации 100% пакета акций АО «КазМунайГаз-Сервис NS».
</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-news">
				<div class="news-slider-item corp-slider">
					<div class="text-left">

						<span class="news-date">27.02.2019</span>
						<span class="news-cat">Новости компаний</span>
<a class="news-title">
Извещение о торгах:
АО «КазМунайГаз-
Сервис NS»
</a>
<span class="news-anons">
ТОО «КазМунайГаз-Сервис» объявляет о повторном проведении торгов по реализации 100% пакета акций АО «КазМунайГаз-Сервис NS».
</span>
					</div>
          		</div>
		</div>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-news-button-next"></div>
    <div class="swiper-news-button-prev"></div>
  </div>

<!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('.swiper-container-news', {
      slidesPerView: 3,
      spaceBetween: 0,
      slidesPerGroup: 1,
      navigation: {
        nextEl: '.swiper-news-button-next',
        prevEl: '.swiper-news-button-prev',
      },
    });
  </script>
			</div>
		</div>
	</div>
    </section>
<section>
      <div class="container mt-5">
        <div class="row">
          <div class="col-lg-12 text-center mt-5 mb-5">
			  <a href="/investors/privatization/privatization-news/" class="btn-read-more">Больше новостей</a>
          </div>
         </div>
        </div>
    </section>


<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mt-3 mb-5">
				<div class="purchases-05 purchases-05-01 priv-06">
					<span class="title">Приватизация активов<br>
группы компаний Фонда<br>
проводится:
						<div class="block-line-3"></div>
</span>
					<p>В рамках Комплексного плана приватизации на 2016-2020 гг., утвержденного постановлением Правительства Республики Казахстан от 30.12.2015 г. №1141 </p>



				</div>
          </div>
          <div class="col-lg-6 mt-3 mb-5">
				<div class="purchases-05 purchases-05-02 priv-06">
					<a href="" class=""><span class="title">Правила реализации активов
группы Фонда в рамках
Комплексного Плана
приватизации 
						<div class="block-line-3"></div>
						</span></a>
					<!--<p><a href="" class="pdf"></a>Единые правила, утвержденные решением Совета директоров)</p>-->
				</div>

          </div>
         </div>
        </div>
    </section>
<section class="">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 text-center">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
Контакты по приватизации активов<br>
				<span>группы Фонда компаний АО «Самрук-Қазына»</span>
            </h2>

          </div>
          <div class="col-lg-6 text-center">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
Контактные данные Комиссий по реализации  активов<br>
дочерних компаний АО «Самрук-Қазына»:

            </h2>
			</div>
         </div>
        </div>

        <div class="row">
          <div class="col-lg-12 text-center">
<a href="" class="block-down" data-id="1" onclick="$(this).hide();" style="    float: none;
    background: url(/bitrix/templates/skfinance/img/corp-arrow.png) no-repeat center center #FFF;
    transform: rotate(90deg);
    width: 45px;
    height: 75px;
    padding: 0;
    margin: 0;"></a>
         </div>
        </div>
</section>
<section>
      <div class="container block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-2">

          </div>
          <div class="col-lg-4">
			  <b>Куратор: АО «Самрук-Қазына»</b>
          </div>
          <div class="col-lg-4 text-right">
+7 (7172) 55-26-26

          </div>
         </div>
        </div>
      <div class="container block-down-box priv-07 " data-id="1">
        <div class="row mt-5">
          <div class="col-lg-2">

          </div>
          <div class="col-lg-4 ">
<b>Единый оператор:<br>
	ТОО «Самрук-Қазына Контракт»</b>

          </div>
          <div class="col-lg-4 text-right">
+7 (7172) 55-90-67,<br>
55-29-68, 55-29-86


          </div>
         </div>
        </div>

</section>
<!--
<section class="mt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
Контактные данные Комиссий по реализации  активов<br>
дочерних компаний АО «Самрук-Қазына»:

            </h2>
<a href="" class="block-down" data-id="2" onclick="$(this).hide();" style="    float: none;
    background: url(/bitrix/templates/skfinance/img/corp-arrow.png) no-repeat center center #FFF;
    transform: rotate(90deg);
    width: 45px;
    height: 75px;
    padding: 0;
    margin: 0;"></a>
          </div>
         </div>
        </div>
</section>
-->

<section>

      <div class="container priv-07 mb-5 block-down-box priv-07 mt-5" data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>АО «Самрук-Энерго» </b>
          </div>
          <div class="col-lg-3">
+7 (7172) 55-31-34
          </div>
          <div class="col-lg-3">
<b>АО «Казпочта»</b>
          </div>
          <div class="col-lg-3">
7 (7172) 61-16-99 вн.1212
          </div>
         </div>
        </div>
      <div class="container priv-07 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>АО «НК «КазМунайГаз»</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 78-65-80
          </div>
          <div class="col-lg-3">
<b>АО «Казахтелеком» </b>
          </div>
          <div class="col-lg-3">
+7 (727) 258-32-97
          </div>
         </div>
        </div>

      <div class="container priv-07 mt-5 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>АО «Air Astana»</b>
          </div>
          <div class="col-lg-3">
+7 (727) 258-41-36
          </div>
          <div class="col-lg-3">
<b>ТОО «Объединенная
химическая компания»</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 27-71-27 вн. 125
          </div>
         </div>
        </div>

      <div class="container priv-07 mt-5 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>АО «Қазақстан темір жолы»</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 60-35-20
          </div>
          <div class="col-lg-3">
<b>АО «Тау-Кен Самрук»</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 55-95-23
          </div>
         </div>
        </div>

      <div class="container priv-07 mt-5 mb-5 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>АО «KEGOC»</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 69-35-56 <br>
+7 (702) 99-99-640
          </div>
          <div class="col-lg-3">
			  <b>АО «НАК «Казатомпром»</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 45-80-15
          </div>
         </div>
        </div>

</section>




<!--
<section class="IPO-section mt-5">
  <div class="container">

    <div class="row">
      <div class="col-lg-12">
        <h2 class="center section-title pb-30">
          <b>Фондом путем публичного размещения акций (IPO)</b><br />
          на фондовой бирже планируется вывести крупные активы :
        </h2>
        <p class="centerp">
          Сроки приватизации активов АО «ФНБ Самрук-Қазына» <br />
          в рамках комплексного плана приватизаций
        </p>
      </div>
    </div>

<?//$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/ipo_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>


  </div>
</section>
-->
<!-- /IPO/SPO -->


<!-- STRAT INVEST -->
<section>
  <div class="container" id="Strategy">

    <div class="row invest-desc mt-5 pb-30">
      <div class="col-lg-12">
		<h2 class="h2-left pr-20 pb-30 section-title text-left mt-3">
          Стратегические инвестиции
        </h2>
		<h2 class="h2-left pr-20 pb-30 page-title text-center mt-3">
          Проекты
        </h2>

      </div>
    </div>

<div class="row invest-desc pb-30">
      <div class="col-lg-12">
        <p class="pb-30 newh3">
          <b>Проекты строительства и расширения солнечной электростанции «Бурное» в Жамбылской области</b>
        </p>
        <p class="pb-15"><strong>Данные проекты реализованы «Самрук-Қазына Инвест»</strong> 
в партнерстве с компанией UG Energy Limited (Великобритания) на базе проектных компаний 
ТОО «Burnoye Solar-1» и ТОО «Burnoye Solar-2» с долей участия «Самрук-Қазына Инвест» 49%. 
Также, в рамках указанных проектов привлечено заемное финансирование ЕБРР.</p>
        <p class="pb-15"><strong>Первая и вторая очереди солнечной электростанции </strong>
введены в эксплуатацию в 2015 году и 2018 году соответственно.</p>
      </div>
    </div>
    
        <?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/orange_blocks_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>

    
    <div class="row">
      <div class="col-lg-12">
        <div class="hr pb-30"><img src="/bitrix/templates/skfinance/img/hr.png" alt=""></div>
        <p class="centerp pb-30">
          Весь объем вырабатываемой электроэнергии выкупает<br>
          <b>ТОО «Расчетно-финансовый центр по поддержке возобновляемых источников энергии»</b><br>
          на основании Закона РК «О поддержке использования возобновляемых источников энергии»
        </p>
      </div>
    </div>
    
    <?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/partners_projects_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>

    
  </div>
</section>



<!-- /STRAT INVEST -->
<!--
<section class="mt-70">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 d-flex justify-content-end">
        <a href="#!" class="orng-btn-24">Прямые инвестиции </a>
      </div>
    </div>
  </div>
</section>
-->
<!-- ACTIVES -->

<section>
  <div class="container"  id="Full">

    <div class="row invest-desc mt-5 pb-30">
      <div class="col-lg-12">
		<h2 class="h2-left pr-20 pb-30 section-title text-left mt-3">
          Прямые инвестиции
        </h2>
		<h2 class="h2-left pr-20 pb-30 page-title text-center mt-3">
          Инвестиционные проекты
        </h2>

      </div>
    </div>
  </div>
</section>
<section>
  <div class="container">
<!--
    <div class="row">
      <div class="col-lg-12">
        <h2 class="center section-title">Инвестиционные проекты</h2>
      </div>
    </div>
-->
    <div class="row invest-desc pb-30">
      <div class="col-lg-12">
        <p><b>В соответствии с новой Стратегией развития</b> Самрук-Қазына
 инвестирует в новые инициативы вместе со стратегическими инвесторами, имеющими 
необходимые опыт и технологии для совместного финансирования и реализации проекта. 
Создание интегрированного бизнеса в партнерстве с ведущими компаниями позволит Самрук-Қазына
диверсифицировать портфель и создать новые компании, которые станут движущей силой структурных 
преобразований экономики страны</p>
      </div>
    </div>
  </div>
</section>
<!-- /ACTIVES -->
<!--
<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 ">
	      <div class="box-green-big invp-01-p">
        <div class="container pb-75">
          <div class="row">
            <div class="col-lg-4 mw-330">
              <div class="block-03 icon-v-001 va-top"></div>
              <p class="invp-01">
                Инвестиционный<br />
                мандат<br />
                <span>> $ 150 млн</span>
              </p>
            </div>
            <div class="col-lg-4 mw-330">
              <div class="block-03 icon-v-002 va-top"></div>
              <p class="invp-01">
                Инструмент <br />инвестирования <br />
                <span>Участие в капитале</span>
              </p>
            </div>
            <div class="col-lg-4">
              <div class="block-03 icon-v-003 va-top"></div>
              <p class="invp-01">
                Доля участия <br />в проекте <br />
                <span> < 50% (неконтролирующая доля)</span>
              </p>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-4 mw-330">
              <div class="block-03 icon-v-004 va-top"></div>
              <p class="invp-01">
                Стратегия<br /> выхода<br />
                <span>Продажа доли<br />
                  партнеру/инвестору или<br />
                  IPO через 5-7 лет</span>
              </p>
            </div>
            <div class="col-lg-4 mw-330">
              <div class="block-03 icon-v-005 va-top"></div>
              <p class="invp-01">
                Целевые<br />
                  инвестиционные<br />
                  показатели<span>
                  IRR > CoE, NPV > O</span>
              </p>
            </div>
            <div class="col-lg-4">
              <div class="block-03 icon-v-006 va-top"></div>
              <p class="invp-01">
                Ограничения <br /><span>
                  - Стартап компании<br />
                  - Венчурное финансирование<br />
                  - Нефтегаз в части разведки<br />
                  и добычи</span
                >
              </p>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</section>
-->
<!--
<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="center section-title mt-5">Инвестиционные механизмы сотрудничества с АО «Самрук-Казына»</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 centerp mt-5">
        <p><b>Со-инвестирование <br />
          с Самрук-Казына</b></p>
      </div>
      <div class="col-lg-9 centerp mt-5">
        <p><b>Инвестирование через фонды прямых инвестиций с участием Самрук-Казына</b></p>
      </div>
    </div>
  </div>
</section>

<section class="mt-35">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <div class="graybr invp-grey invp-02">
          <ul>
            <li>Инвестиционные проекты <br />
              существующих компаний
            </li>
            <li> Создание новых компаний</li>
          </ul>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="invp-green">
          <div class="container invp-03-blocks pr-0">
            <div class="row">
              <div class="col-lg-6 invp-03">
                Da Vinci Emerging<br />
                Technologies Fund III
                <br>
                <center><i class="fa fa-angle-down fs-36 invp-03-01-btn" aria-hidden="true"></i></center>
					<div class="invp-03-01">
Партнеры: DEG, КСМ
<br>Размер: $100 млн. (первое закрытие) 
<br>География: страны СНГ (кроме России)
<br>Отрасли: программное обеспечение и IT услуги, IT инфраструктура и финтех, B2C/ B2B технологии
				  </div>
              </div>
              <div class="col-lg-6 invp-03 ml-5 ">
                Eurasian Nurly (Bright)<br />
                Investment Fund <br>
                <center><i class="fa fa-angle-down fs-36 invp-03-02-btn" aria-hidden="true"></i></center>
					<div class="invp-03-02">
Партнеры: CITIC Group, КСМ 
<br>Размер: $160 млн. (первое закрытие)
<br>География: Казахстан и другие страны Евразийского региона
<br>Отрасли: инфраструктура, горнодобывающая промышленность, энергетика, логистика, информационные технологии, обрабатывающая промышленность, сельское хозяйство и другие сектора

				  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
-->



<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
	      <div class="box-green-big invp-01-p">
        <div class="container pb-75">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="center section-title white mb-5">Отрасли инвестирования<br /></h2>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-001 va-mdl"></div>
              <p class="invp-01">Химическая <br />промышленность</p>
            </div>
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-002 va-mdl"></div>
              <p class="invp-01">
                Обрабатывающая <br />
                промышленность
              </p>
            </div>
            <div class="col-lg-4">
              <div class="block-03 icon-otr-003 va-mdl"></div>
              <p class="invp-01">
                Горно-металлургический<br />
                комплекс
              </p>
            </div>
          </div>
        </div>
        <div class="container pb-60">
          <div class="row">
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-004 va-mdl"></div>
              <p class="invp-01">Машиностроение</p>
            </div>
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-005 va-mdl"></div>
              <p class="invp-01">
                Транспорт и<br />
                логистика
              </p>
            </div>
            <div class="col-lg-4">
              <div class="block-03 icon-otr-006 va-mdl"></div>
              <p class="invp-01">IT и цифровизация</p>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-007 va-mdl"></div>
              <p class="invp-01">Инфраструктура</p>
            </div>
            <div class="col-lg-8 mw-280">
              <div class="block-03 icon-otr-008 va-mdl"></div>
              <p class="invp-01">ВИЭ</p>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section class="mt-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
	      <div class="box-green-big priv-03" style="padding: 60px;">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="center section-title white mb-5">Инвестиционные механизмы сотрудничества 
с Самрук-Қазына</h2>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
Самрук-Қазына активно участвует в привлечении прямых иностранных инвестиций в 
Казахстан через взаимовыгодные инвестиционные структуры. В частности, существуют
 две схемы потенциального партнерства: совместное инвестирование в капитал и 
инвестирование через фонды прямых инвестиций с участием Самрук-Қазына.
            </div>
          </div>
        </div>

        </div>
      </div>
    </div>
  </div>
</section>

<section class="mt-5 mb-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
	      <div class="block-yellow priv-03" style="padding: 60px;min-height: auto;color: #000000;">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="center section-title white mb-5" style="color: #000000;">
Процесс принятия решения:</h2>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
При принятии решений Самрук-Қазына руководствуется ответственностью перед нынешним
 и будущим поколениями Казахстана.<br>
Мы придерживаемся строгого поэтапного подхода к каждой инвестиции в соответствии 
с Корпоративным Стандартом по инвестиционной деятельности.

            </div>
          </div>
        </div>

        </div>
      </div>
    </div>
  </div>
</section>
<!--
<section class="">
      <div class="container mt-5">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
Контакты
            </h2>
          </div>
         </div>
        </div>
</section>
<section>
      <div class="container priv-07">
        <div class="row">
          <div class="col-lg-2">

          </div>
          <div class="col-lg-4">
			  <b>Директор департамента международного сотрудничества</b>
          </div>
          <div class="col-lg-4 text-right">
+7 7172 55 4036

          </div>
         </div>
        </div>
      <div class="container priv-07 mb-5">
        <div class="row mt-5">
          <div class="col-lg-2">

          </div>
          <div class="col-lg-4 ">
<b>Руководитель Сектора по поиску инициатив</b>

          </div>
          <div class="col-lg-4 text-right">
+7 7172 55 4072


          </div>
         </div>
        </div>

</section>

<section class="mt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
				Контакты:
            </h2>
          </div>
         </div>
        </div>
</section>
<section class="mb-5">
      <div class="container priv-07">
        <div class="row">
          <div class="col-lg-1">

          </div>
          <div class="col-lg-6">
			  <b>Султанов Аслан:</b><br> 
			  <span style="font-weight: 400;">Директор департамента международного сотрудничества</span>
          </div>
          <div class="col-lg-3 text-right">
+7 7172 55 4036<br>
			  <span style="font-weight: 400;">a.sultanov@sk.kz</span>
          </div>
         </div>
        </div>
      <div class="container priv-07 ">
        <div class="row mt-5">
          <div class="col-lg-1">

          </div>
          <div class="col-lg-6 ">
<b>Абильмажинов Сакен:</b><br>
			  <span style="font-weight: 400;">Руководитель Сектора по поиску инициатив</span>

          </div>
          <div class="col-lg-3 text-right">
+7 7172 55 4072<br>
			  <span style="font-weight: 400;">s.abilmazhinov@sk.kz</span>


          </div>
         </div>
        </div>

</section>
-->