<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/pages.php");

$pagetitle = Loc::getMessage('INVESTORS_AND_STAKEHOLDERS') ." — ". Loc::getMessage("NAME_SITE");
$description = Loc::getMessage("DESCRIPTION_SITE");
$keywords = Loc::getMessage("KEYWORDS_SITE");
$APPLICATION->SetTitle($pagetitle);
$APPLICATION->SetPageProperty("title", $pagetitle);
$APPLICATION->SetPageProperty("description",  $description);
$APPLICATION->SetPageProperty("keywords",  $keywords);
$APPLICATION->SetPageProperty("og:type",  "website");
$APPLICATION->SetPageProperty("og:title",  $pagetitle);
$APPLICATION->SetPageProperty("og:description",  $description);
$APPLICATION->SetPageProperty("og:url",  "https://sk.kz".$APPLICATION->GetCurPage(false));
$APPLICATION->SetPageProperty("og:image",  "https://sk.kz".SITE_TEMPLATE_PATH."/thumb.jpg");

use Bitrix\Main\Loader;
Loader::includeModule("iblock");

function makeSort($blockId, $key = 'default', $lang = 'ru', $params){
	$result = Array();

	if($key == 'main'){
		$result = Array('SHOW_COUNTER' => 'DESC');
	} elseif($key == 'history'){
		$result = Array('SORT' => 'ASC');
	} else {
		$result = Array('ACTIVE_FROM' => 'DESC');
	}

	return $result;
}

function makeFilter($blockId, $key = 'default', $lang = 'ru', $params){
	$additions = Array();

	if($key == 'news') {
		$additions = Array(
			array(
				"LOGIC" => "OR",
				"!NAME" => false,
				"!DETAIL_TEXT" => false,
			),
			"PROPERTY_PRIVATIZATION_FLAG_VALUE" => "Да",
			"<=DATE_ACTIVE_FROM" => date('d.m.Y H:i:s')
		);
	} else {
		$additions = Array(
			"<=DATE_ACTIVE_FROM" => date('d.m.Y H:i:s')
		);
	}

	return Array(
		"ACTIVE" => "Y"
		,"IBLOCK_ID" => $blockId
		,$additions
	);
}
function getNews($blockId, $newsCount, $key, $lang, $params){

	$siteUrl = (CMain::IsHTTPS() ? 'https:' : 'http:').'//'.SITE_SERVER_NAME;

	$arSort  	= Array('ACTIVE_FROM' => 'DESC');
	$arFilter 	= Array(
					"ACTIVE"    			=> "Y", 
					"IBLOCK_ID" 			=> $blockId, 
					"<=DATE_ACTIVE_FROM" 	=> date('d.m.Y H:i:s')
				);
	if (function_exists('makeSort')) {
		$arSort = makeSort($blockId, $key, $lang, $params);
	}
	if (function_exists('makeFilter')) {
		$arFilter = makeFilter($blockId, $key, $lang, $params);
	}

	$arNavStartParams["nTopCount"] = $newsCount;

	// print_r($arFilter);

	$res        = CIBlockElement::GetList(
		$arSort,
		$arFilter,
		false,
		$arNavStartParams,
		Array(
			"ID","IBLOCK_ID",
			"PROPERTY_*",
			"DETAIL_PAGE_URL",
			"NAME","ACTIVE_FROM",
			"CODE","IBLOCK_SECTION_ID",
			"PREVIEW_PICTURE","PREVIEW_TEXT","DETAIL_TEXT",
			"DETAIL_PICTURE",
			"SHOW_COUNTER", "SORT"
		)
	);

	$counter = 0;
	$ndlCDBResult = $res;
	$arResult = [];
	while($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arProps  = $ob->GetProperties();

		//$result[$counterOffset]['ID']             = $arFields['ID'];
		//$result[$counter]['NAME']           = $arFields['NAME'];
		//$result[$counter]['SORT']           = $arFields['SORT'];
		//$result[$counter]['ACTIVE_FROM'] = $arFields['ACTIVE_FROM'];
		//$result[$counter]['SHOW_COUNTER'] = $arFields['SHOW_COUNTER'];
		$result[$counter]['FIELDS'] = $arFields;
		$result[$counter]['PROPS'] = $arProps;

		++$counter;
	}
	return $result;
}

switch (LANGUAGE_ID) {
  case "ru":
    $newsId = 10;
    break;
  case "en":
    $newsId = 41;
    break;
  case "kz":
    $newsId = 42;
    break;
  default:
   	$newsId = 10;
}
$news = getNews($newsId, 3, 'news', 'ru', $params);
global $USER;
if ($USER->IsAdmin()){
	//echo "<pre>";
	//print_r($news);
	//echo "</pre>";
};
?>
<div class="investors" :class=" investorsActiveTab==2?'investors_nobotpad':'' ">
		  <div class="breadcrumb investors__breadcumb">
			<div class="container">
			  <div class="row">
				<div class="col-12">
				  <div class="breadcrumb__content"><a class="breadcrumb__item" href="/">
					  <p class="breadcrumb__item-text"><?=Loc::getMessage('MAIN');?></p></a>
                      <span class="breadcrumb__item">
					  <p class="breadcrumb__item-text"><?=Loc::getMessage('INVESTORS_AND_STAKEHOLDERS');?></p></span>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		  <div class="title investors__titlecomponent">
			<h2 class="title__text"><?=Loc::getMessage('INVESTORS_AND_STAKEHOLDERS');?></h2>
		  </div>
  <div class="container container_xs_nopad">
	<div class="row">
	  <div class="col-12"> 
		<div class="investors-nav">
		  <p class="investors-nav__title"><?=Loc::getMessage('SAMRUK_KAZYNA_JSC_NWF_INVESTS_IN_THREE_AREAS');?></p>
		  <div class="investors-nav__list">
			<div class="investors-nav__arrow investors-nav__arrow_left" @click="setInvestorsPrevTab">
                <img class="investors-nav__arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-min-light.svg" alt="">
            </div>
			<div class="investors-nav__arrow investors-nav__arrow_right" @click="setInvestorsNextTab"><img class="investors-nav__arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-min-light.svg" alt=""></div>
			<div class="investors-nav__item" :class=" investorsActiveTab==0?'investors-nav__item_active':'' " @click="investorsActiveTab=0">
				<img class="investors-nav__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/investors/001-investment.svg" alt="">
			  <p class="investors-nav__item-text"><?=Loc::getMessage('FINANCIAL_INVESTMENTS');?></p>
			</div>
			<div class="investors-nav__item" :class=" investorsActiveTab==1?'investors-nav__item_active':'' " @click="investorsActiveTab=1">
				<img class="investors-nav__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/investors/002-money.svg" alt="">
		  		<p class="investors-nav__item-text"><?=Loc::getMessage('STRATEGIC_INVESTMENT');?></p>
			</div>
			<div class="investors-nav__item" :class=" investorsActiveTab==2?'investors-nav__item_active':'' " @click="investorsActiveTab=2">
				<img class="investors-nav__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/investors/003-strategic.svg" alt="">
			  <p class="investors-nav__item-text"><?=Loc::getMessage('DIRECT_INVESTMENTS');?></p>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
  <transition name="fade" mode="out-in">
	<div class="investors__tab_1" v-if="investorsActiveTab==0" key="tab0">
	  <div class="container">
		<div class="row">
		  <div class="col-12">
			<div class="titlelined"><img class="titlelined__icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/line-title2.svg">
			  <p class="titlelined__text">1. <?=Loc::getMessage('FINANCIAL_INVESTMENTS');?></p>
				<img class="titlelined__icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/line-title2.svg">
			</div>
			<div class="investors__title">
			  <p class="investors__title-text"><?=Loc::getMessage('PRIVATIZATION');?></p>
			</div>
			<p class="investors__text investors__text_bold"><?=Loc::getMessage('PRIVATIZATION_TEXT');?></p>
			<div class="investors__list">
			  <div class="investors-card">
				<div class="investors-card__header">
				  <div class="investors-card__count">
					<p class="investors-card__count-text">143</p>
				  </div>
				  <div class="investors-card__action">
					<p class="investors-card__action-text"><?=Loc::getMessage('INVESTORS_CARD_LEFT_ACTION');?></p>
				  </div>
				</div>
				<div class="investors-card__content">
				  <div class="investors-card__line">
					<div class="investors-card__line-left">
					  <p class="investors-card__line-count">1</p>
					</div>
					<div class="investors-card__line-right">
					  <p class="investors-card__line-text"><?=Loc::getMessage('INVESTORS_CARD_LEFT_TEXT_ONE');?></p>
					</div>
				  </div>
				  <div class="investors-card__line">
					<div class="investors-card__line-left">
					  <p class="investors-card__line-count">89</p>
					</div>
					<div class="investors-card__line-right">
					  <p class="investors-card__line-text"><?=Loc::getMessage('INVESTORS_CARD_LEFT_TEXT_TWO');?></p>
					</div>
				  </div>
				  <div class="investors-card__line">
					<div class="investors-card__line-left">
					  <p class="investors-card__line-count">53</p>
					</div>
					<div class="investors-card__line-right">
					  <p class="investors-card__line-text"><?=Loc::getMessage('INVESTORS_CARD_LEFT_TEXT_THREE');?></p>
					</div>
				  </div>
				</div>
			  </div>
			  <div class="investors-card">
				<div class="investors-card__header">
				  <div class="investors-card__count">
					<p class="investors-card__count-text">25</p>
				  </div>
				  <div class="investors-card__action">
					<p class="investors-card__action-text"><?=Loc::getMessage('INVESTORS_CARD_CENTRE_ACTION');?></p>
				  </div>
				</div>
				<div class="investors-card__content">
				  <div class="investors-card__line">
					<div class="investors-card__line-left">
					  <p class="investors-card__line-count">18</p>
					</div>
					<div class="investors-card__line-right">
					  <p class="investors-card__line-text"><?=Loc::getMessage('INVESTORS_CARD_CENTRE_TEXT_ONE');?></p>
					</div>
				  </div>
				  <div class="investors-card__line">
					<div class="investors-card__line-left">
					  <p class="investors-card__line-count">7</p>
					</div>
					<div class="investors-card__line-right">
					  <p class="investors-card__line-text"><?=Loc::getMessage('INVESTORS_CARD_CENTRE_TEXT_TWO');?></p>
					</div>
				  </div>
				</div>
			  </div>
			  <div class="investors-card">
				<div class="investors-card__header investors-card__header_noborder">
				  <div class="investors-card__count">
					<p class="investors-card__count-text">8</p>
				  </div>
				  <div class="investors-card__action">
					<p class="investors-card__action-text"><?=Loc::getMessage('INVESTORS_CARD_RIGHT_ACTION');?></p>
				  </div>
				</div>
				<div class="investors-card__content">
                <?=Loc::getMessage('INVESTORS_CARD_RIGHT_TEXT_ONE');?>
				</div>
			  </div>
			</div>
			<p class="investors__text"><?=Loc::getMessage('PRIVATIZATION_TEXT_BOTTOM');?></p>
		  </div>
		</div>
	  </div>
	  <div class="container container_xs_nopad">
		<div class="row">
		  <div class="col-12">
			<div class="investors__companies">
			  <div class="investors-companies">
				<div class="investors-companies__nav">
				  <div class="investors-companies__nav-arrow investors-companies__nav-arrow_left">
						<img class="investors-companies__nav-arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-min-light.svg" alt="" @click=" ()=&gt;investorCompaniesActive=0"></div>
				  <div class="investors-companies__nav-arrow investors-companies__nav-arrow_right">
						<img class="investors-companies__nav-arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-min-light.svg" alt="" @click=" ()=&gt;investorCompaniesActive=1"></div>
				  <div class="investors-companies__nav-item" :class=" investorCompaniesActive==0?'investors-companies__nav-item_active':'' " @click=" ()=&gt;investorCompaniesActive=0 ">
					<p class="investors-companies__nav-item-text">IPO</p>
				  </div>
				  <div class="investors-companies__nav-item" :class=" investorCompaniesActive==1?'investors-companies__nav-item_active':'' " @click=" ()=&gt;investorCompaniesActive=1 ">
					<p class="investors-companies__nav-item-text">SPO</p>
				  </div>
				</div>
				<div class="investors-companies__content" v-if="investorCompaniesActive==0">
				  <div class="investors-companies__title">
					<p class="investors-companies__title-text">2022 <?=Loc::getMessage('YEAR');?></p>
				  </div>
				  <div class="investors-companies__list">
						<img class="investors-companies__list-item" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/investors/company-1.svg" alt="">
						<img class="investors-companies__list-item" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/investors/company-2.svg" alt="">
						<img class="investors-companies__list-item" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/investors/company-3.svg" alt="">
						<img class="investors-companies__list-item" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/investors/company-4.svg" alt="">
					</div>
				  <div class="investors-companies__title">
					<p class="investors-companies__title-text">2023 <?=Loc::getMessage('YEAR');?></p>
				  </div>
				  <div class="investors-companies__list">
						<img class="investors-companies__list-item" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/investors/company-5.svg" alt="">
					</div>
				</div>
				<div class="investors-companies__content" v-else-if="investorCompaniesActive==1">
				  <div class="investors-companies__title">
					<p class="investors-companies__title-text">2024 <?=Loc::getMessage('YEAR');?></p>
				  </div>
				  <div class="investors-companies__list">
					<img class="investors-companies__list-item" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/investors/company-6.svg" alt="	«Қазақтелеком» АҚ">
					</div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="container">
		<div class="row">
		  <div class="col-5 col-sm-7 col-xs-12">
			<?if(LANGUAGE_ID == "ru"){
				$link = "/documents/Список некрупных, подлежащих реализации_ru 01 12 21.pdf";
			} elseif(LANGUAGE_ID == "kz"){
				$link = "/documents/Tiзiм_сатылатын шагын активтер_kz 01 01 21.pdf";
			} elseif(LANGUAGE_ID == "en"){
				$link = "/documents/List of assets to be sold_eng 01 01 21.pdf";
			};
			$file = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$link);
			$size = CFile::FormatSize($file['size']);
			?>
			  <a href="<?=$link?>" target="blank"> 
				  <div class="iconbtn investors__download">
					<div class="iconbtn__icon"><img class="iconbtn__icon-src" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/pdf.svg" alt="">
					  <div class="iconbtn__icon-info">
						<p class="iconbtn__icon-text"><?=Loc::getMessage('DOWNLOAD');?> </p>
						<p class="iconbtn__icon-text"><?=$size;?> </p>
					  </div>
					</div>
					<div class="iconbtn__content">
					  <div class="iconbtn__text"><?=Loc::getMessage('LIST_OF_SMALL_COMPANIES');?></div>
					</div>
				  </div>
				</a></div>
		  <div class="col-12">
					<div class="title">
					  <div class="title__line"><img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt=""><img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt=""><img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt=""></div>
					  <h2 class="title__text"><?=Loc::getMessage('ASSETS');?></h2>
					</div>
			<div class="investors__assets">

				<a target="blank" :href=" $i18n.locale=='ru'?'https://sk.kz/documents/%D0%90%D0%BA%D1%82%D0%B8%D0%B2%D1%8B%20%D0%BD%D0%B0%20%20IPO_SPO_%D0%BF%D1%80%D0%BE%D0%B4%D0%B0%D0%B6%D1%83%20%D1%81%D1%82%D1%80%D0%B0%D1%82%D0%B5%D0%B3%D0%B8%D0%BD%D0%B2%D0%B5%D1%81%D1%82%D0%BE%D1%80%D0%B0%D0%BC_rus.pdf':($i18n.locale=='kk'?'https://sk.kz/documents/IPO_SPO_%D1%81%D1%82%D1%80%D0%B0%D1%82%D0%B5%D0%B3%D0%B8%D0%BD%D0%B2%D0%B5%D1%81%D1%82%D0%BE%D1%80%D2%93%D0%B0%20%D1%81%D0%B0%D1%82%D1%8B%D0%BB%D0%B0%D1%82%D1%8B%D0%BD%20%D0%B0%D0%BA%D1%82%D0%B8%D0%B2%D1%82%D0%B5%D1%80_kz.pdf':'https://sk.kz/documents/Assets%20to%20IPO_SPO_sale%20to%20strategic%20investors_eng.pdf')  "> 
					<div class="iconbtn iconbtn_right iconbtn_darktext">
					  <div class="iconbtn__content">
						<div class="iconbtn__text"><?=Loc::getMessage('ASSETS_LIST_ONE');?></div>
					  </div>
					  <div class="iconbtn__icon"><img class="iconbtn__icon-src" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/doc.svg" alt=""></div>
					</div>
				</a>

				<a target="blank" :href="$i18n.locale=='ru'?'https://sk.kz/documents/Активы_реализованные%20в%202016-2020%20годах_ru.pdf':($i18n.locale=='kk'?'https://sk.kz/documents/2016-2020%20жылдары%20сатылған%20активтер_kz.pdf':'https://sk.kz/documents/Assets%20were%20soled%20in%202016-2020_eng.pdf') "> 
					<div class="iconbtn iconbtn_right iconbtn_darktext">
					  <div class="iconbtn__content">
						<div class="iconbtn__text"><?=Loc::getMessage('ASSETS_LIST_TWO');?></div>
					  </div>
					  <div class="iconbtn__icon"><img class="iconbtn__icon-src" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/doc.svg" alt=""></div>
					</div>
				</a>

				<a target="blank" :href="$i18n.locale=='ru'?'https://sk.kz/documents/Активы%20в%20предпродажной%20подготовке_ru.pdf':($i18n.locale=='kk'?'https://sk.kz/documents/Cатуалды%20дайындық%20кезеңіндегі%20активтер_kz.pdf':'https://sk.kz/documents/Assets%20in%20pre_sale%20preparation_eng.pdf') "> 
					<div class="iconbtn iconbtn_right iconbtn_darktext">
					  <div class="iconbtn__content">
						<div class="iconbtn__text"><?=Loc::getMessage('ASSETS_LIST_THREE');?></div>
					  </div>
					  <div class="iconbtn__icon"><img class="iconbtn__icon-src" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/doc.svg" alt=""></div>
					</div>
				</a>

				<a target="blank" :href="$i18n.locale=='ru'?'https://sk.kz/documents/%D0%90%D0%BA%D1%82%D0%B8%D0%B2%D1%8B%20%D0%BD%D0%B0%20%D1%80%D0%B5%D0%BE%D1%80%D0%B3%D0%B0%D0%BD%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8E%20%D0%B8%20%D0%BB%D0%B8%D0%BA%D0%B2%D0%B8%D0%B4%D0%B0%D1%86%D0%B8%D1%8E_rus.pdf':($i18n.locale=='kk'?'https://sk.kz/documents/%D2%9A%D0%B0%D0%B9%D1%82%D0%B0%20%D2%9B%D2%B1%D1%80%D1%8B%D0%BB%D0%B0%D1%82%D1%8B%D0%BD_%D1%82%D0%B0%D1%80%D0%B0%D1%82%D1%8B%D0%BB%D0%B0%D1%82%D1%8B%D0%BD%20%D0%B0%D0%BA%D1%82%D0%B8%D0%B2%D1%82%D0%B5%D1%80_kz.pdf':'https://sk.kz/documents/Assets%20to%20reorganization%20and%20liquidation_eng.pdf') "> 
					<div class="iconbtn iconbtn_right iconbtn_darktext">
					  <div class="iconbtn__content">
						<div class="iconbtn__text"><?=Loc::getMessage('ASSETS_LIST_FOUR');?></div>
					  </div>
					  <div class="iconbtn__icon"><img class="iconbtn__icon-src" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/doc.svg" alt=""></div>
					</div>
				</a></div>
		  </div>
		  <div class="col-12">
				<div class="title">
				  <div class="title__line">
						<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
						<img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt="">
						<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
					</div>
				  <h2 class="title__text"><?=Loc::getMessage('PRIVATIZATION_UPDATES');?></h2>
				</div>
				<div class="investors__news">
					<?foreach($news as $arElements):?>
					<?
					$smtp = MakeTimeStamp($arElements['FIELDS']['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS");
					$arElements['FIELDS']['ACTIVE_FROM'] = date("d.m.Y", $smtp);
					$previewText = HTMLToTxt($arElements['FIELDS']['DETAIL_TEXT']);
					$arElements['FIELDS']['DETAIL_TEXT'] = TruncateText($previewText, 300);
					?>
					<a href="/investors/privatization/news/<?=$arElements['FIELDS']['ID'];?>/" target="_blank">
							<div class="pnewscard">
							  <div class="pnewscard__content">
								<div class="pnewscard__category">
								  <p class="pnewscard__category-text"><?=Loc::getMessage('INVESTORS_NEWS_CATEGORY');?></p>
								</div>
								<div class="pnewscard__title">
								  <p class="pnewscard__title-text"><?=$arElements['FIELDS']['NAME'];?></p>
								</div>
								<div class="pnewscard__des">
								  <p class="pnewscard__des-text"><?=$arElements['FIELDS']['DETAIL_TEXT'];?></p>
								</div>
								<div class="pnewscard__footer">
								  <p class="pnewscard__date"><?=$arElements['FIELDS']['ACTIVE_FROM'];?></p>
								  <div class="pnewscard__views">
									<img class="pnewscard__views-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/views.svg" alt="">
									<p class="pnewscard__views-text"><?=$arElements['FIELDS']['SHOW_COUNTER'];?></p>
								  </div>
								</div>
							  </div>
							</div>
						</a>
<?endforeach?>
			</div>
			<div class="investors__btnwrap">
				<a href="/investors/privatization/privatization-news/" target="_blank">
					<div class="investors__btn">
					  <p class="investors__btn-text"><?=Loc::getMessage('MORE_NEWS');?></p>
					</div>
				</a>
			</div>
		  </div>
		  <div class="col-12">
					<div class="title">
					  <div class="title__line">
						<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
						<img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt="">
						<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
					</div>
					  <h2 class="title__text"><?=Loc::getMessage('CONTACTS');?></h2>
					</div>
			<div class="investors__contacts">
			  <table class="table">
				<tr>
				  <th width="33.33%"><?=Loc::getMessage('CONTACT_INFORMATION_ON_THE_SALE_AND_PRIVATIZATION_OF_ASSETS');?></th>
				  <th width="33.33%"><?=Loc::getMessage('COMPANY_NAMES');?></th>
				  <th width="33.33%"><?=Loc::getMessage('PHONE');?></th>
				</tr>
				<tr>
				  <td rowspan="2"><?=Loc::getMessage('CONTACTS_ON_PRIVATIZATION_OF_ASSETS_THE_GROUP_OF_THE_FUND_OF_COMPANIES_SAMRUK_KAZYNA_JSC');?></td>
				  <td><?=Loc::getMessage('SAMRUK_KAZYNA_JSC');?></td>
				  <td>+7 (7172) 55-26-26</td>
				</tr>
				<tr> 
				  <td><?=Loc::getMessage('SAMRUK_KAZYNA_CONTRACT_LLP');?></td>
				  <td>+7 (7172) 55-90-67, 55-29-68, 55-29-86</td>
				</tr>
				<tr>
				  <td rowspan="10"><?=Loc::getMessage('CONTACTS_OF_COMMISSIONS_ON_ASSETS_DIVESTITURE_AND_FACILITIES_OF_SAMRUK_KAZYNA_JSC');?></td>
				  <td><?=Loc::getMessage('SAMRUK_ENERGY_JSC');?></td>
				  <td>+7 (7172) 55-31-34</td>
				</tr>
				<tr>
				  <td><?=Loc::getMessage('KAZMUNAYGAS_NATIONAL_COMPANY_JSC');?></td>
				  <td>+7 (7172) 78-65-80</td>
				</tr>
				<tr>
				  <td><?=Loc::getMessage('AIR_ASTANA_JSC');?></td>
				  <td>+7 (727) 258-41-36</td>
				</tr>
				<tr>
				  <td><?=Loc::getMessage('KAZAKHSTAN_TEMIR_ZHOLY_NATIONAL_COMPANY_JSC');?></td>
				  <td>+7 (7172) 60-35-20</td>
				</tr>
				<tr>
				  <td><?=Loc::getMessage('KEGOC_JSC');?></td>
				  <td>+7 (7172) 69-35-56 +7 (702) 99-99-640</td>
				</tr>
				<tr>
				  <td><?=Loc::getMessage('КАZPOST_JSC');?></td>
				  <td> 7 (7172) 61-16-99 вн.1212</td>
				</tr>
                <tr>
				  <td><?=Loc::getMessage('KAZAKHTELECOM_JSC');?></td>
				  <td>+7 (727) 258-32-97</td>
				</tr>
				<tr>
				  <td><?=Loc::getMessage('UNITED_CHEMICAL_COMPANY_LLP');?></td>
				  <td>+7 (7172) 27-71-27 вн. 125</td>
				</tr>
				<tr>
				  <td><?=Loc::getMessage('TAU_KEN_SAMRUK_JSC');?></td>
				  <td>+7 (7172) 55-95-23</td>
				</tr>
				<tr>
				  <td><?=Loc::getMessage('KAZATOMPROM_NATIONAL_COMPANY_JSC');?></td>
				  <td>+7 (7172) 45-80-15</td>
				</tr>
			  </table>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="container container_xs_nopad investors__contactsmobile">
		<div class="row">
		  <div class="col-12">
			<div class="investors__contblock investors__contblock_accent">
			  <p class="investors__contblock-text"><?=Loc::getMessage('CONTACTS_ON_PRIVATIZATION_OF_ASSETS_THE_GROUP_OF_THE_FUND_OF_COMPANIES_SAMRUK_KAZYNA_JSC');?></p>
			</div>
			<div class="investors__contblock">
			  <p class="investors__contblock-text"><span><?=Loc::getMessage('CURATOR');?>:</span> <?=Loc::getMessage('SAMRUK_KAZYNA_JSC');?> <br> +7 (7172) 55-26-26</p>
			</div>
			<div class="investors__contblock">
			  <p class="investors__contblock-text"><span><?=Loc::getMessage('SINGLE_OPERATOR');?>:</span> <?=Loc::getMessage('SAMRUK_KAZYNA_CONTRACT_LLP');?> <br> +7 (7172) 55-90-67, 55-29-68, 55-29-86</p>
			</div>
			<div class="investors__contblock investors__contblock_accent">
			  <p class="investors__contblock-text"><?=Loc::getMessage('CONTACTS_OF_COMMISSIONS_ON_ASSETS_DIVESTITURE_AND_FACILITIES_OF_SAMRUK_KAZYNA_JSC');?></p>
			</div>
			<div class="investors__contblock">
			  <p class="investors__contblock-text"><?=Loc::getMessage('SAMRUK_ENERGY_JSC');?> <br> +7 (7172) 55-31-34</p>
			</div>
			<div class="investors__contblock">
			  <p class="investors__contblock-text"><?=Loc::getMessage('KAZMUNAYGAS_NATIONAL_COMPANY_JSC');?> <br> +7 (7172) 78-65-80</p>
			</div>
			<div class="investors__contblock">
			  <p class="investors__contblock-text"><?=Loc::getMessage('AIR_ASTANA_JSC');?> <br> +7 (727) 258-41-36</p>
			</div>
			<div class="investors__contblock">
			  <p class="investors__contblock-text"><?=Loc::getMessage('KAZAKHSTAN_TEMIR_ZHOLY_NATIONAL_COMPANY_JSC');?> <br> +7 (7172) 60-35-20</p>
			</div>
			<div class="investors__contblock">
			  <p class="investors__contblock-text"><?=Loc::getMessage('KEGOC_JSC');?> <br> +7 (7172) 69-35-56 +7 (702) 99-99-640</p>
			</div>
			<div class="investors__contblock">
			  <p class="investors__contblock-text"><?=Loc::getMessage('КАZPOST_JSC');?> <br> +7 (7172) 61-16-99 вн.1212</p>
			</div>
			<div class="investors__contblock">
			  <p class="investors__contblock-text"><?=Loc::getMessage('KAZAKHTELECOM_JSC');?> <br> +7 (727) 258-32-97</p>
			</div>
			<div class="investors__contblock">
			  <p class="investors__contblock-text"><?=Loc::getMessage('UNITED_CHEMICAL_COMPANY_LLP');?> <br> +7 (7172) 27-71-27 вн. 125</p>
			</div>
			<div class="investors__contblock">
			  <p class="investors__contblock-text"><?=Loc::getMessage('TAU_KEN_SAMRUK_JSC');?> <br> +7 (7172) 55-95-23</p>
			</div>
			<div class="investors__contblock">
			  <p class="investors__contblock-text"><?=Loc::getMessage('KAZATOMPROM_NATIONAL_COMPANY_JSC');?> <br> +7 (7172) 45-80-15</p>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="investors__tab_2" v-else-if="investorsActiveTab==1" key="tab1">
	  <div class="container">
		<div class="row">
		  <div class="col-12">
					<div class="titlelined">
						<img class="titlelined__icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/line-title2.svg">
					  <p class="titlelined__text">2. <?=Loc::getMessage('STRATEGIC_INVESTMENT');?></p><img class="titlelined__icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/line-title2.svg">
					</div>
			<div class="investors__title">
			  <p class="investors__title-text"><?=Loc::getMessage('PROJECTS');?></p>
			</div>
			<?=Loc::getMessage('PROJECTS_TEXT');?>
		  </div>
		</div>
	  </div>
	  <div class="investors__facts">
		<div class="container">
		  <div class="row">
			<div class="col-12"> 
			  <div class="investors__facts-list">
				<div class="facts-card">
				  <div class="facts-card__header">
					<p class="facts-card__title"><?=Loc::getMessage('PROJECTS_FACTS_LEFT_TITLE');?></p>
					<div class="facts-card__value">
					  <p class="facts-card__value-text"><?=Loc::getMessage('PROJECTS_FACTS_LEFT_VALUE');?></p>
					</div>
				  </div>
				  <div class="facts-card__content">
					<p class="facts-card__text"><?=Loc::getMessage('PROJECTS_FACTS_LEFT_TEXT');?></p>
				  </div>
				</div>
				<div class="facts-card">
				  <div class="facts-card__header">
					<p class="facts-card__title"><?=Loc::getMessage('PROJECTS_FACTS_CENTRE_TITLE');?></p>
					<div class="facts-card__value">
					  <p class="facts-card__value-text"><?=Loc::getMessage('PROJECTS_FACTS_CENTRE_VALUE');?></p>
					</div>
				  </div>
				  <div class="facts-card__content">
					<p class="facts-card__text"><?=Loc::getMessage('PROJECTS_FACTS_CENTRE_TEXT');?></p>
				  </div>
				</div>
				<div class="facts-card">
				  <div class="facts-card__header">
					<p class="facts-card__title"><?=Loc::getMessage('PROJECTS_FACTS_RIGHT_TITLE');?></p>
					<div class="facts-card__value">
					  <p class="facts-card__value-text"><?=Loc::getMessage('PROJECTS_FACTS_RIGHT_VALUE');?></p>
					</div>
				  </div>
				  <div class="facts-card__content">
					<p class="facts-card__text"><?=Loc::getMessage('PROJECTS_FACTS_RIGHT_TEXT');?></p>
				  </div>
				</div>
			  </div>
			  <div class="investors__facts-content">
				<p class="investors__facts-text"><?=Loc::getMessage('PROJECTS_TEXT_BOTTOM');?></p>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="container container_xs_nopad">
		<div class="row">
		  <div class="col-12"> 
					<div class="title">
					  <div class="title__line">
						<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
						<img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt="">
						<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt=""></div>
					  <h2 class="title__text"><?=Loc::getMessage('PROJECT_PARTNER');?></h2>
					</div>
			<div class="investors__partners">
			  <div class="partner-block">
				<div class="partner-block__left">
					<img class="partner-block__left-logo" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/investors/partner-1.svg" alt="">
				  <p class="partner-block__left-text"><?=Loc::getMessage('PROJECT_PARTNER_LIST_ENERGY_LIMITED');?></p>
				</div>
				<div class="partner-block__right">
                <?=Loc::getMessage('PROJECT_PARTNER_LIST_ENERGY_LIMITED_TEXT');?>
				</div>
			  </div>
			  <div class="partner-block">
				<div class="partner-block__left">
					<img class="partner-block__left-logo" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/investors/partner-2.svg" alt="">
				  <p class="partner-block__left-text"><?=Loc::getMessage('PROJECT_PARTNER_LIST_HEVEL');?></p>
				</div>
				<div class="partner-block__right">
                <?=Loc::getMessage('PROJECT_PARTNER_LIST_HEVEL_TEXT');?>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<div class="investors__tab_3" v-else-if="investorsActiveTab==2" key="tab2">
	  <div class="container">
		<div class="row">
		  <div class="col-12">
			<div class="titlelined"><img class="titlelined__icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/line-title2.svg">
			  <p class="titlelined__text">3. <?=Loc::getMessage('DIRECT_INVESTMENTS');?></p>
				<img class="titlelined__icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/line-title2.svg">
			</div>
			<div class="investors__title">
			  <p class="investors__title-text"><?=Loc::getMessage('INVESTMENT_PROJECTS');?></p>
			</div>
			<p class="investors__text investors__text_mb_l"><?=Loc::getMessage('INVESTMENT_PROJECTS_TEXT');?></p>
					<div class="title">
					  <div class="title__line">
						<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt=""><img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt=""><img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt=""></div>
					  <h2 class="title__text"><?=Loc::getMessage('INVESTMENT_SECTORS');?></h2>
					</div>
			<div class="investors__branchlist">
			  <div class="investors__branch" v-for="(branch, index) in investorsBranches[$i18n.locale]" :key="index">
				<div class="investors__branch-icon"><img class="investors__branch-icon-src" :src="branch.icon" alt=""></div>
				<div class="investors__branch-des">
				  <p class="investors__branch-text" v-html="branch.text"></p>
				</div>
				<hr class="investors__branch-line">
			  </div>
			</div>
					<div class="title">
					  <div class="title__line"><img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt=""><img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt=""><img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt=""></div>
					  <h2 class="title__text"><?=Loc::getMessage('INVESTMENT_MECHANISMS');?></h2>
					</div>
			<p class="investors__text"><?=Loc::getMessage('INVESTMENT_MECHANISMS_TEXT');?></p>
		  </div>
		</div>
	  </div>
	  <div class="investors__decisionmobile">
		<div class="investors__title">
		  <p class="investors__title-text"><?=Loc::getMessage('DECISION_MAKING_PROCESS');?></p>
		</div>
		<p class="investors__text"><?=Loc::getMessage('DECISION_MAKING_PROCESS_TEXT');?></p>
	  </div>
	</div>
  </transition>
</div>
<div class="partnerswrap">
  <div class="container investors__decision" v-if="investorsActiveTab==2">
	<div class="row">
	  <div class="col-12">
		<div class="investors__decision-content">
		  <div class="investors__title">
			<p class="investors__title-text"><?=Loc::getMessage('DECISION_MAKING_PROCESS');?></p>
		  </div>
		  <p class="investors__text"><?=Loc::getMessage('DECISION_MAKING_PROCESS_TEXT');?></p>
		</div>
	  </div>
	</div>
  </div>
  <div class="container">
	<div class="row">
	  <div class="col-12"> 
		<div class="mpartners">
		  <div class="mpartners__arrow mpartners__arrow_left"><img class="mpartners__arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg" alt=""></div>
		  <div class="swiper-container mpartners__list">
			<div class="swiper-wrapper">
			  <div class="swiper-slide"><a href="https://hrqyzmet.kz/" target="_blank">
				  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/brands_qyzmet.png" alt=""></div></a></div>
			  <div class="swiper-slide"><a href="https://samruk-akparat.kz/" target="_blank">
				  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aqparat.png" alt=""></div></a></div>
			  <div class="swiper-slide"><a href="https://www.skc.kz/" target="_blank">
				  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/samruk.png" alt=""></div></a></div>
			  <div class="swiper-slide"><a href="https://aifc.kz/" target="_blank">
				  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aifc_cmyk.png" alt=""></div></a></div>
			  <div class="swiper-slide"><a href="https://primeminister.kz/ru" target="_blank">
				  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/min.png" alt=""></div></a></div>
			  <div class="swiper-slide"><a href="http://www.akorda.kz/ru" target="_blank">
				  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-7.jpg" alt=""></div></a></div>
			  <div class="swiper-slide"><a href="https://elbasy.kz/ru" target="_blank">
				  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-6.jpg" alt=""></div></a></div>
			</div>
		  </div>
		  <div class="mpartners__arrow mpartners__arrow_right"><img class="mpartners__arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg" alt=""></div>
		  <div class="mpartners__mobile"><a href="https://hrqyzmet.kz/" target="_blank">
			  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/brands_qyzmet.png" alt=""></div></a><a href="https://samruk-akparat.kz/" target="_blank">
			  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aqparat.png" alt=""></div></a><a href="https://www.skc.kz/" target="_blank">
			  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/samruk.png" alt=""></div></a><a href="https://aifc.kz/" target="_blank">
			  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aifc_cmyk.png" alt=""></div></a><a href="https://primeminister.kz/ru" target="_blank">
			  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/min.png" alt=""></div></a><a href="http://www.akorda.kz/ru" target="_blank">
			  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-7.jpg" alt=""></div></a><a href="https://elbasy.kz/ru" target="_blank">
			  <div class="mpartners__item"><img class="mpartners__item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-6.jpg" alt=""></div></a></div>
		</div>
	  </div>
	</div>
  </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>