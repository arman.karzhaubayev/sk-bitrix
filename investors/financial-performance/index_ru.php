<?$APPLICATION->SetTitle("Финансовые показатели");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             Финансовые показатели
            </h2>
          </div>
        </div>
      </div>
</section>




	<section>
      <div class="container">
        <div class="row finance mt-5">
          <div class="col-lg-4">
			  <div class="finance-title">«Самрук-Қазына»</div>
			</div>
          <div class="col-lg-8">
			  <div class="finance-text">
				  <b>Акционерное общество «Фонд национального благосостояния «Самрук-Қазына»</b> создано в соответствии с Указом Президента Республики Казахстан от 13 октября 2008 года № 669 «О некоторых мерах по конкурентоспособности и устойчивости национальной экономики»
     <br><br>
				  <b>Постановление Правительства Республики Казахстан</b> от 17 октября 2008 года № 962 «О мерах по реализации Указа Президента Республики Казахстан от 13 октября 2008 года № 669» путем слияния акционерных обществ «Фонд устойчивого развития «Қазына» и «Казахстанский холдинг по управлению государственными активами «Самрук»
				</div>
          </div>
        </div>
      </div>
    </section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
			<div class="finance-00 ">
				<div class="container">
        			<div class="row">
          				<div class="col-lg-5">
<span class="title">
Финансовые показатели<br>
за 3 месяца 2020 года<br> 
(цифры указаны в млрд. тенге)
	<div class="block-line-3"></div>
							</span>
          				</div>
          				<div class="col-lg-4">

          				</div>
          				<div class="col-lg-3 text-right">
<span class="title">
<br><br>Март 2020 факт
	<div class="block-line-4"></div>
							</span>
          				</div>
        			</div>
		      </div>


				<div class="container">
        			<div class="row">
          				<div class="col-lg-6 mt-5">
<table class="finance-table">
<thead>
<tr>
	<th></th>
	<th style="width:250px;"></th>
	<th>Выручка<br>млрд</th>
	<th>Чистый<br>
доход</th>
</tr>
</thead>
<tbody>
<tr>
	<td><div class="finance-01" style="background: url(/bitrix/templates/skfinance/img/finance_01.png) no-repeat #FFF center center"></div></td>
	<td>АО «НК «КазМунайГаз»</td><td>978,58</td><td>156,5</td>
</tr>
<tr>
	<td><div class="finance-01" style="background: url(/bitrix/templates/skfinance/img/finance_02.png) no-repeat #FFF center center"></div></td>
	<td>АО «НК «Казахстан
темир жолы»
</td><td>280,72   </td><td>114,9</td>
</tr>
<tr>
	<td><div class="finance-01" style="background: url(/bitrix/templates/skfinance/img/finance_03.png) no-repeat #FFF center center"></div></td>
	<td>АО «Эйр Астана»
</td><td>61,84</td><td>10,3</td>
</tr>
<tr>
	<td><div class="finance-01" style="background: url(/bitrix/templates/skfinance/img/finance_04.png) no-repeat #FFF center center"></div></td>
	<td>АО «Самрук-Энерго»
</td><td>81,67</td><td>7,5</td>
</tr>

<tr>
	<td><div class="finance-01" style="background: url(/bitrix/templates/skfinance/img/finance_10.jpg) no-repeat #FFF center center;    background-size: 75%;"></div></td>
	<td>АО «Казахтелеком»
</td><td>121,84</td><td>20,55</td>
</tr>
<tr>
	<td><div class="finance-01" style="background: url(/bitrix/templates/skfinance/img/finance_11.png) no-repeat #FFF center center;    background-size: 80%;"></div></td>
	<td>АО «KEGOC»
</td><td>83,65</td><td>11,49</td>
</tr>

<tr>
	<td><div class="finance-01" style="background: url(/bitrix/templates/skfinance/img/finance_05.png) no-repeat #FFF center center"></div></td>
	<td>АО «НГРК «Тау-Кен
Самрук»
</td><td>3,40</td><td>4,4</td>
</tr>




</tbody>
</table>

          				</div>
<div class="col-lg-6 mt-5">
<table class="finance-table">
<thead>
<tr>
	<th></th>
	<th style="width:250px;"></th>
	<th>Выручка<br>млрд</th>
	<th>Чистый<br>
доход</th>
</tr>
</thead>
<tbody>
<tr>
	<td><div class="finance-01" style="background: url(/bitrix/templates/skfinance/img/finance_06.png) no-repeat #FFF center center"></div></td>
	<td>АО «Казпочта»
</td><td>11,30</td><td>0,02</td>
</tr>
<tr>
	<td><div class="finance-01" style="background: url(/bitrix/templates/skfinance/img/finance_07.png) no-repeat #FFF center center"></div></td>
	<td>ТОО «Объединенная
химическая компания»
</td><td>4,22</td><td>50,4</td>
</tr>
<tr>
	<td><div class="finance-01" style="background: url(/bitrix/templates/skfinance/img/finance_08.png) no-repeat #FFF center center"></div></td>
	<td>АО «SamrukKazyna
Construction» (ФНСК) 4
</td><td>8,99</td><td>2,9</td>
</tr>
<tr>
	<td><div class="finance-01" style="background: url(/bitrix/templates/skfinance/img/finance_09.png) no-repeat #FFF center center"></div></td>
	<td>АО«Qazaq Air»
</td><td>1,73</td><td>2,6</td>
</tr>
<tr>
	<td><div class="finance-01" style="background: url(/bitrix/templates/skfinance/img/finance_12.png) no-repeat #FFF center center;    background-size: 75%;"></div></td>
	<td>АО «НАК «Казатомпром»
</td><td>26,14</td><td>61,75</td>
</tr>
<tr>
	<td></td><td></td><td></td><td><br><br><br><br><br></td>
</tr>

<tr>
	<td class="last" colspan="2">Сумма Топ-12</td>
	<td class="last"> 1 432,44</td><td class="last">6,9</td>
</tr>



</tbody>
</table>

          				</div>
        			</div>
      			</div>
<!--
				<div class="container">
        			<div class="row">
          				<div class="col-lg-4 mt-5">
						</div>
          				<div class="col-lg-5 mt-5">
<ul class="finance-02">
	<li>Без учета авансирования и трэйдинга</li>
	<li>Без учета доход от реализации продукции и товаров</li>
	<li>Включая доходы от реализации продукции</li>
	<li>аффинажного завода, без учета сырья</li>
	<li>Данные отражены за вычетом амортизации дисконта</li>
							</ul>
						</div>
          				<div class="col-lg-3 mt-5">

          				</div>
        			</div>
      			</div>
-->
			</div>
          </div>
        </div>
      </div>
    </section>

<? 
$id = intval($_REQUEST['id']);
if($id == 0) $id = 103;
$GLOBALS['arrFilter'] = array('SECTION_ID'=>$id);
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "reportsplans",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "N",
        "CHECK_DATES" => "Y",
        "COMPONENT_TEMPLATE" => "reportsplans",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "Y",
        "FIELD_CODE" => array(
			0 => "ID", 
			1 => "CODE", 
			2 => "XML_ID", 
			3 => "NAME", 
			4 => "SORT", 
			5 => "PREVIEW_TEXT", 
			6 => "PREVIEW_PICTURE", 
			7 => "ACTIVE_FROM",
			8 => "",
			9 => "",
			10 => "",
		),
        "FILE_404" => "",
        "FILTER_NAME" => "arrFilter",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => constant("REPORTS_IBLOCK_" . LANGUAGE_ID),
        "IBLOCK_TYPE" => "sliders",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "100",
        "PAGER_BASE_LINK" => "",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "PAGER_DESC_NUMBERING" => "Y",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_PARAMS_NAME" => "arrPager",
        "PAGER_SHOW_ALL" => "Y",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(0 => "", 1 => "BUTTON", 2 => "BUTTON_LINK", 3 => "",),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "SORT",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER1" => "ASC",
        "SORT_ORDER2" => "DESC"
    )
); ?>

	
	
	
<!--
<section class="history finance-02 mt-5 mb-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="history-item">
              <div class="row">
                <div class="col-lg-2">
                  <div class="history-description">
                    Отчеты<br>
и планы
                  </div>
                </div>
                <div class="col-lg-10">
                  <div class="histry-years">
                    <div class="row">
								<div class="col wt-bt-3">
									<a class="history-year finance-report" href="">Годовые отчеты</a>
								</div>
																																										<div class="col ">
									<a class="history-year finance-report" href="">Об устойчивом
развитии</a>
								</div>
																																										<div class="col ">
									<a class="history-year finance-report" href="">Информация
о дивидендах</a>
								</div>
																																										<div class="col ">
									<a class="history-year finance-report" href="">Догосрочные
планы</a>
								</div>
																																										<div class="col ">
									<a class="history-year finance-report" href="">Финансовые
отчеты</a>
								</div>
																																										<div class="col ">
									<a class="history-year finance-report" href="">Консолидированная
финансовая
отчетность</a>
								</div>
                    </div>
                    <div class="bubble">
            <div class="history-swiper-container">
              <div class="history-swiper-wrapper">
					<div class="history-swiper-slide">
                  				<div class="history-swiper-item">
									<span>2019</span>
									Годовой отчет АО «Самрук-Қазына» за 2018 год (Том 1)
									<div class="clear"></div>
									<span>2018</span>
									Годовой отчет АО «Самрук-Қазына» за 2018 год (Том 2)
									<div class="clear"></div>
									<span>2017</span>
									Годовой отчет АО «Самрук-Қазына» за 2017 год (Том 1)
									<div class="clear"></div>
								</div>
					</div>
				</div>
            </div>


                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
-->
<div class="clear mt-5 mb-5"></div>
<section class="mt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
          </div>
        </div>
      </div>
    </section>

<? /*
$id = intval($_REQUEST['id']);
if($id == 0) $id = 571;
$GLOBALS['arrFilter'] = array('SECTION_ID'=>$id);
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "audit",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "N",
        "CHECK_DATES" => "Y",
        "COMPONENT_TEMPLATE" => "audit",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "Y",
        "FIELD_CODE" => array(
			0 => "ID", 
			1 => "CODE", 
			2 => "XML_ID", 
			3 => "NAME", 
			4 => "SORT", 
			5 => "PREVIEW_TEXT", 
			6 => "PREVIEW_PICTURE", 
			7 => "ACTIVE_FROM",
			8 => "",
			9 => "",
			10 => "",
		),
        "FILE_404" => "",
        "FILTER_NAME" => "arrFilter",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => constant("REPORTS_IBLOCK_" . LANGUAGE_ID),
        "IBLOCK_TYPE" => "sliders",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "100",
        "PAGER_BASE_LINK" => "",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "PAGER_DESC_NUMBERING" => "Y",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_PARAMS_NAME" => "arrPager",
        "PAGER_SHOW_ALL" => "Y",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(0 => "", 1 => "BUTTON", 2 => "BUTTON_LINK", 3 => "",),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "SORT",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER1" => "ASC",
        "SORT_ORDER2" => "DESC"
    )
); */?>
<!--
<section class="history mt-5 finance-02 mb-5">
      <div class="container mt-5 mb-5">
        <div class="row">
          <div class="col-lg-12 mt-5 mb-5">
            <div class="history-item mt-5">
              <div class="row">
                <div class="col-lg-3">
                  <div class="history-description">
                   Система<br>
и аудит
                  </div>
                </div>
                <div class="col-lg-9">
                  <div class="histry-years">
                    <div class="row">
								<div class="col-lg-3 wt-bt-3">
									<a class="history-year finance-report" href="">Аудиторские
отчеты</a>
								</div>
                    </div>
                    <div class="bubble">
            <div class="history-swiper-container">
              <div class="history-swiper-wrapper">
					<div class="history-swiper-slide">
                  				<div class="history-swiper-item">
									<span>2019</span>
									Годовой отчет АО «Самрук-Қазына» за 2018 год (Том 1)
									<div class="clear"></div>
									<span>2018</span>
									Годовой отчет АО «Самрук-Қазына» за 2018 год (Том 2)
									<div class="clear"></div>
									<span>2017</span>
									Годовой отчет АО «Самрук-Қазына» за 2017 год (Том 1)
									<div class="clear"></div>
								</div>
					</div>
				</div>
            </div>


                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
-->
<div class="clear mb-5"></div>


