<?$APPLICATION->SetTitle("Инвесторлар мен стейкхолдерлерге");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('investors')?>
            </h2>
            <h2 class="h2-left pr-20 pb-30 section-title text-left mt-3">
«Самұрық-Қазына» ҰӘҚ» АҚ үш бағытта инвестиция құяды: 
            </h2>
          </div>
        </div>
      </div>
</section>




<section class="birzh-cat">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 mt-3">
        <a href="#Finance" class="orng-btn anchor">Қаржылық инвестициялар</a>
      </div>
      <div class="col-lg-4 text-center mt-3">
        <a href="#Strategy" class="orng-btn anchor">Стратегиялық инвестициялар</a>
      </div>
      <div class="col-lg-4 text-right mt-3">
        <a href="#Full" class="orng-btn anchor">Тікелей инвестициялар</a>
      </div>
    </div>
  </div>
</section>


<section>
  <div class="container" id="Finance">
    <div class="row">
      <div class="col-lg-12 mt-3">
        <h2 class="h2-left pr-20 pb-30 section-title text-left mt-3">
          Қаржылық инвестициялар
        </h2>
        <h2 class="h2-left pr-20 pb-30 page-title text-center mt-3">
          Жекешелендіру
        </h2>
      </div>
    </div>
  </div>
</section>



<!-- PLAN -->

<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 mt-3">
        <h2 class="h2-left pr-20 pb-30 section-title text-center">
			<p class="centerp">

Жекешелендірудің 2016-2020 жылдарға арналған кешенді
<br>жоспарына «Самұрық-Қазына» АҚ компаниялар тобының 168 активі енгізілген

</p>

        </h2>
      </div>
    </div>
  </div>
</section>


<?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/plan_priv_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>

<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="hr pb-30"><img src="/bitrix/templates/skfinance/img/hr.png" alt="" /></div>
        <p class="centerp">
			<b>Қазіргі уақытта компаниялар операциялық тиімділікті<br>
арттыру және ықтимал IPO үшін оңтайлы периметрді айқындау<br>
бойынша ішкі дайындық жұмыстарын жүзеге асыруда</b>

        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
<?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/strat_invest_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
    <?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/spo_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="center pb-30 section-title mt-3">Активтер</h2>
      </div>
    </div>
    <div class="row orange-blocks pb-15">
      <div class="col-lg-6">
        <div class="actv-01 pb-15">
          <div class="container">
          <div class="row">
<!--
            <div class="col-lg-2">
              <div class="block-06 img-003"></div>

            </div>
-->
            <div class="col-lg-12">
              <a class="list_black2" href="/documents/IPO_SPO_стратегинвесторға сатылатын активтер_kz.pdf">
              <p class="priv-09">IPO / SPO / стартегиялық инвесторға сатуға жатқызылған активтер
				<label class="block-line-3"></label>
				</p>
				</a>
            </div>
          </div>

          
          
        </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="actv-01 pb-15">
          <div class="container">
          <div class="row">
<!--
            <div class="col-lg-2">
              <div class="block-06 img-003"></div>

            </div>
-->
            <div class="col-lg-12">
<a class="list_black2" href="/documents/2016-2020 жылдары сатылған активтер_kz.pdf">
              <p class="priv-09">Жекешелендірудің кешенді жоспары шеңберінде сатылған активтер
				<label class="block-line-3"></label></p>
				</a>
            </div>
          </div>

          
          
        </div>
        </div>
      </div>
    </div>


    <div class="row orange-blocks pb-15 mt-4">
      <div class="col-lg-6">
        <div class="actv-01 pb-15">
          <div class="container">
          <div class="row">
<!--
            <div class="col-lg-2">
              <div class="block-06 img-003"></div>
              
            </div>
-->
            <div class="col-lg-12">
              <a class="list_black2" href="/documents/Cатуалды дайындық кезеңіндегі активтер_kz.pdf">
              <p class="priv-09">Сату алды дайындық кезеңіндегі активтер
				<label class="block-line-3"></label>
				</p>
				</a>
            </div>
          </div>

          
          
        </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="actv-01 pb-15">
          <div class="container">
          <div class="row">
<!--
            <div class="col-lg-2">
              <div class="block-06 img-003"></div>

            </div>
-->
            <div class="col-lg-12">
              <a class="list_black2" href="/documents/Қайта құрылатын_таратылатын активтер_kz.pdf">
              <p class="priv-09">Қайта құруға және таратылуға жатқызылған активтер
				<label class="block-line-3"></label></p>
			  </a>
            </div>
          </div>

          
          
        </div>
        </div>
      </div>
    </div>
    
    
    
  </div>
</section>
<section class="mt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
Жекешелендіру жаңалықтары
            </h2>
          </div>
         </div>
        </div>
</section>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<!-- Swiper -->
  <div class="swiper-container swiper-container-news">
    <div class="swiper-wrapper">


		<div class="swiper-slide swiper-slide-news">
				<div class="news-slider-item corp-slider">
					<div class="text-left">

						<span class="news-date">26.09.2019</span>
						<span class="news-cat">Баспасөз хабарламалары</span>
<a class="news-title">
Самұрық-Қазына қосымша орналастырудың нәтижелерін хабарлады
</a>
<span class="news-anons">
Лондон қор биржасы мен «Астана» халықаралық қаржы орталығының қор биржасында Қордың жетекші компаниясы – «Қазатомөнеркәсіп» ҰАК» АҚ ғаламдық депозитарлық қолхаттарын қосымша орналастыру өтті.
</span>
					</div>
          		</div>
		</div>
<div class="swiper-slide swiper-slide-news">
				<div class="news-slider-item corp-slider">
					<div class="text-left">

						<span class="news-date">27.02.2019</span>
						<span class="news-cat">Компаниялар жаңалықтары</span>
<a class="news-title">
Сауда туралы хабарлама: «ҚазМұнайГаз-Сервис NS» АҚ
</a>
<span class="news-anons">
«ҚазМұнайГаз-Сервис» ЖШС «ҚазМұнайГаз-Сервис NS» АҚ-ның 100% акциясын қайта саудаға шығарды.
Электрондық конкурс 2019 жылдың 14 наурызында Астана уақыты бойынша сағат 10:00-де www.gosreestr.kz алаңында өтеді.
</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-news">
				<div class="news-slider-item corp-slider">
					<div class="text-left">

						<span class="news-date">30.01.2019</span>
						<span class="news-cat">Компаниялар жаңалықтары</span>
<a class="news-title">
Сауда туралы хабарлама: «ҚазМұнайГаз-Сервис NS» АҚ</a>
<span class="news-anons">
«ҚазМұнайГаз-Сервис» ЖШС «ҚазМұнайГаз-Сервис NS» АҚ-ның 100% акциясын қайта саудаға шығарды. Электрондық конкурс 2019 жылдың 27 ақпанында Астана уақыты бойынша сағат 10:00-де www.gosreestr.kz алаңында өтеді.
</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-news">
				<div class="news-slider-item corp-slider">
					<div class="text-left">

						<span class="news-date">24.01.2019</span>
						<span class="news-cat">Компаниялар жаңалықтары</span>
<a class="news-title">
Сауда туралы хабарлама: «ҚазМұнайГаз-Сервис NS» АҚ
</a>
<span class="news-anons">
«ҚазМұнайГаз-Сервис» ЖШС «ҚазМұнайГаз-Сервис NS» АҚ-ның 100% акциясын қайта саудаға шығарды. 
</span>
					</div>
          		</div>
		</div>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-news-button-next"></div>
    <div class="swiper-news-button-prev"></div>
  </div>

<!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('.swiper-container-news', {
      slidesPerView: 3,
      spaceBetween: 0,
      slidesPerGroup: 1,
      navigation: {
        nextEl: '.swiper-news-button-next',
        prevEl: '.swiper-news-button-prev',
      },
    });
  </script>
			</div>
		</div>
	</div>
    </section>
<section>
      <div class="container mt-5">
        <div class="row">
          <div class="col-lg-12 text-center mt-5 mb-5">
			  <a href="/investors/privatization/privatization-news/" class="btn-read-more">
Көбірек жалықтар</a>
          </div>
         </div>
        </div>
    </section>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mt-3 mb-5">
				<div class="purchases-05 purchases-05-01 priv-06">
					<span class="title">Қордың компаниялар тобының активтерін жекешелендіру:
						<div class="block-line-3"></div>
</span>
					<p>Қазақстан Республикасы Үкіметінің 2015 жылғы 30 желтоқсандағы № 1141 қаулысымен бекітілген 2016-2020 жылдарға арналған жекешелендірудің кешенді жоспары аясында
</p>



				</div>
          </div>
          <div class="col-lg-6 mt-3 mb-5">
				<div class="purchases-05 purchases-05-02 priv-06">
					<a href=""><span class="title">Жекешелендірудің кешенді жоспары аясында Қор тобының активтерін сату ережелері
						<div class="block-line-3"></div>
						</span></a>
				</div>

          </div>
         </div>
        </div>
    </section>
<section class="mt-3">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 text-center">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
«Самұрық-Қазына» АҚ компаниялар тобының <br>
активтерін жекешелендіру бойынша байланыс телефоны
            </h2>
          </div>
          <div class="col-lg-6 text-center">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">


«Самұрық-Қазына» АҚ еншілес ұйымдарының<br> активтерін сату жөніндегі комиссиямен байланыс:
            </h2>
          </div>
         </div>
        <div class="row">
          <div class="col-lg-12 text-center">
<a href="" class="block-down" data-id="1" onclick="$(this).hide();" style="    float: none;
    background: url(/bitrix/templates/skfinance/img/corp-arrow.png) no-repeat center center #FFF;
    transform: rotate(90deg);
    width: 45px;
    height: 75px;
    padding: 0;
    margin: 0;"></a>
          </div>
         </div>
        </div>
</section>
<section>
      <div class="container priv-07 mt-3 mb-3 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-2">

          </div>
          <div class="col-lg-4">
			  <b>Куратор: «Самұрық-Қазына» АҚ</b>
          </div>
          <div class="col-lg-4 text-right">
+7 (7172) 55-26-26

          </div>
         </div>
        </div>
      <div class="container priv-07 mt-3 block-down-box priv-07 " data-id="1">
        <div class="row mt-5">
          <div class="col-lg-2">

          </div>
          <div class="col-lg-4 mt-3">
<b>Бірыңғай оператор:<br>
	«Самұрық-Қазына Контракт» ЖШС</b>

          </div>
          <div class="col-lg-4 mt-3 text-right">
+7 (7172) 55-90-67,<br>
55-29-68, 55-29-86


          </div>
         </div>
        </div>

</section>
<!--
<section class="mt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">


«Самұрық-Қазына» АҚ еншілес ұйымдарының<br> активтерін сату жөніндегі комиссиямен байланыс:
            </h2>
<a href="" class="block-down" data-id="2" onclick="$(this).hide();" style="    float: none;
    background: url(/bitrix/templates/skfinance/img/corp-arrow.png) no-repeat center center #FFF;
    transform: rotate(90deg);
    width: 45px;
    height: 75px;
    padding: 0;
    margin: 0;"></a>
          </div>
         </div>
        </div>
</section>
-->
<section>

      <div class="container priv-07 mt-5 mb-5 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>«Самұрық-Энерго» АҚ  </b>
          </div>
          <div class="col-lg-3">
+7 (7172) 55-31-34
          </div>
          <div class="col-lg-3">
<b>«Қазпочта» АҚ  </b>
          </div>
          <div class="col-lg-3">
7 (7172) 61-16-99 вн.1212
          </div>
         </div>
        </div>
      <div class="container priv-07 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>«КазМунайГаз» ҰК» АҚ </b>
          </div>
          <div class="col-lg-3">
+7 (7172) 78-65-80
          </div>
          <div class="col-lg-3">
<b>«Қазақтелеком» АҚ  </b>
          </div>
          <div class="col-lg-3">
+7 (727) 258-32-97
          </div>
         </div>
        </div>

      <div class="container priv-07 mt-5 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>«Air Astana» АҚ </b>
          </div>
          <div class="col-lg-3">
+7 (727) 258-41-36
          </div>
          <div class="col-lg-3">
<b>«Біріккен химиялық компания» ЖШС</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 27-71-27 вн. 125
          </div>
         </div>
        </div>

      <div class="container priv-07 mt-5 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>«Қазақстан темір жолы» АҚ </b>
          </div>
          <div class="col-lg-3">
+7 (7172) 60-35-20
          </div>
          <div class="col-lg-3">
<b>«Тау-Кен Самұрық» АҚ </b>
          </div>
          <div class="col-lg-3">
+7 (7172) 55-95-23
          </div>
         </div>
        </div>

      <div class="container priv-07 mt-5 mb-5 block-down-box priv-07 " data-id="1">
        <div class="row">
          <div class="col-lg-3">
<b>АО «KEGOC»</b>
          </div>
          <div class="col-lg-3">
+7 (7172) 69-35-56 <br>
+7 (702) 99-99-640
          </div>
          <div class="col-lg-3">
			  <b>«Қазатомөнеркәсіп» ҰАК» АҚ </b>
          </div>
          <div class="col-lg-3">
+7 (7172) 45-80-15
          </div>
         </div>
        </div>

</section>


<!-- /PLAN -->

<section>
  <div class="container" id="Strategy">

    <div class="row invest-desc mt-5 pb-30">
      <div class="col-lg-12">
		<h2 class="h2-left pr-20 pb-30 section-title text-left mt-3">
          Стратегиялық инвестициялар
        </h2>
		<h2 class="h2-left pr-20 pb-30 page-title text-center mt-3">
          Жобалар
        </h2>

      </div>
    </div>


    <div class="row invest-desc mt-5 pb-30">
      <div class="col-lg-12">
        <p class="pb-30 newh3">
          <b>Жамбыл облысындағы «Бурное» күн электр станциясының құрылысы және кеңейту жобалары</b>
        </p>
        <p class="pb-15"><strong>
Бұл жобаларды «Самұрық-Қазына Инвест» «UG Energy Limited» (Ұлыбритания)</strong>
 серіктестігімен «Burnoye Solar-1» және «Burnoye Solar-2» жобалау компаниялары негізінде «Самұрық-Қазына Инвест» 49 % қатысуымен жүзеге асырды. Сондай-ақ, осы жобалар аясында ЕҚҚДБ қарызды қаржыландыру тартылды.

</p>
        <p class="pb-15"><strong>
Күн станциясының бірінші және екінші кезеңдері сәйкесінше
</strong>
 2015 және 2018 жылдары іске қосылды.
</p>
      </div>
    </div>
    
        <?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/orange_blocks_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>

    
    <div class="row">
      <div class="col-lg-12">
        <div class="hr pb-30"><img src="/bitrix/templates/skfinance/img/hr.png" alt=""></div>
        <p class="centerp pb-30">

Өндірілген электр энергиясының барлық көлемін<br>
			<b>«Жаңартылатын энергия көздерін пайдалануды қолдау туралы»</b><br>
Қазақстан Республикасының Заңының негізінде<br>
«Жаңартылатын энергия көздерін қолдау жөніндегі есеп айырысу-қаржы орталығы» ЖШС сатып алады.
        </p>
      </div>
    </div>
    
    <?$APPLICATION->IncludeComponent( "bitrix:main.include", "", array( "AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/inc/partners_projects_".LANGUAGE_ID.".php", 'MODE' => 'html' ), false );?>


  </div>
</section>


<section>
  <div class="container"  id="Full">

    <div class="row invest-desc mt-5">
      <div class="col-lg-12">
		<h2 class="h2-left pr-20 pb-30 section-title text-left mt-3">
          Тікелей инвестициялар
        </h2>
		<h2 class="h2-left pr-20 page-title text-center mt-3">
          Инвестициялық жобалар
        </h2>

      </div>
    </div>
  </div>
</section>



<section class="mt-3">
  <div class="container">
    <div class="row invest-desc pb-3">
      <div class="col-lg-12">
        <p>
Жаңа Даму стратегиясына сәйкес Самұрық-Қазына жобаны бірлесіп қаржыландыру және іске асыру үшін қажетті тәжірибесі мен технологиялары бар стратегиялық инвесторлармен бірге жаңа бастамаларға инвестиция салады. Жетекші компаниялармен әріптестікте интеграцияланған бизнесті құру Самұрық-Қазынаға портфельді әртараптандыруға және ел экономикасының құрылымдық өзгерістерінің қозғаушы күші болатын жаңа компаниялар құруға мүмкіндік береді.

</p>
      </div>
    </div>
  </div>
</section>
<!-- /ACTIVES -->



<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
	      <div class="box-green-big invp-01-p">
        <div class="container pb-75">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="center section-title white mb-5">Инвестициялау салалары:<br /></h2>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-001 va-mdl"></div>
              <p class="invp-01">Химия  <br />өнеркәсібі</p>
            </div>
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-002 va-mdl"></div>
              <p class="invp-01">
                Өңдеуші  <br />
                өнеркәсіп
              </p>
            </div>
            <div class="col-lg-4">
              <div class="block-03 icon-otr-003 va-mdl"></div>
              <p class="invp-01">
                Тау-кен металлургия  <br />
                кешені
              </p>
            </div>
          </div>
        </div>
        <div class="container pb-60">
          <div class="row">
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-004 va-mdl"></div>
              <p class="invp-01">Машина жасау</p>
            </div>
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-005 va-mdl"></div>
              <p class="invp-01">
                Көлік және <br />
                логистика
              </p>
            </div>
            <div class="col-lg-4">
              <div class="block-03 icon-otr-006 va-mdl"></div>
              <p class="invp-01">IT және цифрландыру</p>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-4 mw-280">
              <div class="block-03 icon-otr-007 va-mdl"></div>
              <p class="invp-01">Инфрақұрылым</p>
            </div>
            <div class="col-lg-8 mw-280">
              <div class="block-03 icon-otr-008 va-mdl"></div>
              <p class="invp-01">ЖЭК</p>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section class="mt-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
	      <div class="box-green-big priv-03" style="padding: 60px;">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="center section-title white mb-5">
Самұрық-Қазынамен ынтымақтастықтың инвестициялық тетіктері
</h2>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
Самұрық-Қазына өзара тиімді инвестициялық құрылымдар арқылы Қазақстанға тікелей шетелдік инвестицияларды тартуға белсенді қатысады. Атап айтқанда, әлеуетті әріптестіктің екі схемасы бар: капиталға бірлесіп инвестициялау және Самұрық-Қазынаның қатысуымен құрылған тікелей инвестициялар қорлары арқылы инвестициялау.

            </div>
          </div>
        </div>

        </div>
      </div>
    </div>
  </div>
</section>

<section class="mt-5 mb-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
	      <div class="block-yellow priv-03" style="padding: 60px;min-height: auto;color: #000000;">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="center section-title white mb-5" style="color: #000000;">
Шешім қабылдау процесі:
</h2>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <div class="col-lg-12">
Самұрық-Қазына шешім қабылдау кезінде Қазақстанның қазіргі және болашақ 
ұрпақтары алдындағы жауапкершілікті басшылыққа алады. 
<br>Біз инвестициялық қызметтің Корпоративтік Стандартына сәйкес әр инвестицияға қатаң кезең-кезеңдік тәсілді ұстанамыз.


            </div>
          </div>
        </div>

        </div>
      </div>
    </div>
  </div>
</section>
<!--
<section class="">
      <div class="container mt-5">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
Байланыс
            </h2>
          </div>
         </div>
        </div>
</section>

<section>
      <div class="container priv-07">
        <div class="row">
          <div class="col-lg-2">

          </div>
          <div class="col-lg-4">
			  <b>Халықаралық ынтымақтастық департаментінің директоры

</b>
          </div>
          <div class="col-lg-4 text-right">
+7 7172 55 4036

          </div>
         </div>
        </div>
      <div class="container priv-07 mb-5">
        <div class="row mt-5">
          <div class="col-lg-2">

          </div>
          <div class="col-lg-4 ">
<b>Бастамаларды іздеу секторының басшысы</b>

          </div>
          <div class="col-lg-4 text-right">
+7 7172 55 4072


          </div>
         </div>
        </div>

</section>

<section class="mt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
				Байланыс:
            </h2>
          </div>
         </div>
        </div>
</section>
<section class="mb-5">
      <div class="container priv-07">
        <div class="row">
          <div class="col-lg-1">

          </div>
          <div class="col-lg-6">
			  <b>Султанов Аслан:</b><br> 
			  <span style="font-weight: 400;">
Халықаралық ынтымақтастық департаментінің директоры
</span>
          </div>
          <div class="col-lg-3 text-right">
+7 7172 55 4036<br>
			  <span style="font-weight: 400;">a.sultanov@sk.kz</span>
          </div>
         </div>
        </div>
      <div class="container priv-07 ">
        <div class="row mt-5">
          <div class="col-lg-1">

          </div>
          <div class="col-lg-6 ">
<b>Абильмажинов Сакен:</b><br>
			  <span style="font-weight: 400;">
Бастамаларды іздеу секторының басшысы
</span>

          </div>
          <div class="col-lg-3 text-right">
+7 7172 55 4072<br>
			  <span style="font-weight: 400;">s.abilmazhinov@sk.kz</span>


          </div>
         </div>
        </div>

</section>
-->