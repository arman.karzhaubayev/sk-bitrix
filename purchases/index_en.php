<?$APPLICATION->SetTitle("Procurements");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>

 <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('purchases')?>
            </h2>
          </div>
        </div>
      </div>
</section>



	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 purchases-01">
<p>
	<b>АО «Самрук-Казына» - один из крупнейших заказчиков товаров, работ и услуг в стране.</b> 
Ежегодно объем закупок составляет на сумму не менее 3 трлн тенге, из них закупки товаров у отечественных товаропроизводителей составляет порядка 1 трлн тенге.
План закупок группы компаний в 2020 году составил 3,0 трлн тенге и исполнен за 5 месяцев текущего года на 80% или 2,4 трлн тенге. Экономия по итогам проведенных закупок составила 100 млрд тенге.
Фонд усовершенствовал и внутрихолдинговые закупки. В конкурентную среду передано 819 млрд тенге. К закупкам группы Фонда получили доступ 17 000 субъектов бизнеса.
			  </p>

          </div>
          <div class="col-lg-12 purchases-02 mt-3">
<p>
	2020 год Фондом "Самрук-Қазына" объявлен Годом поддержки казахстанских товаропроизводителей и отечественных предпринимателей
			  </p>

          </div>
        </div>
      </div>
    </section>

	<section class="mt-3">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-3">
				Нормативно-правовые акты
            </h2>
          </div>
         </div>
        </div>
    </section>

<? $APPLICATION->IncludeComponent("bitrix:news", "ga.purchase", Array(
    "FILTER_NAME" => 'activeFilter', //имя переменной, в которой хранится фильтр
    "ADD_ELEMENT_CHAIN" => "Y",    // Включать название элемента в цепочку навигации
    "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
    "AJAX_MODE" => "Y",    // Включить режим AJAX
    "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
    "AJAX_OPTION_HISTORY" => "Y",    // Включить эмуляцию навигации браузера
    "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "N",    // Включить подгрузку стилей
    "BROWSER_TITLE" => "NAME",    // Установить заголовок окна браузера из свойства
    "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
    "CACHE_GROUPS" => "Y",    // Учитывать права доступа
    "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
    "CACHE_TYPE" => "N",    // Тип кеширования
    "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
    "DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",    // Формат показа даты
    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
    "DETAIL_DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
    "DETAIL_FIELD_CODE" => array(    // Поля
        0 => "SHOW_COUNTER",
        1 => "",
    ),
    "DETAIL_PAGER_SHOW_ALL" => "Y",    // Показывать ссылку "Все"
    "DETAIL_PAGER_TEMPLATE" => "",    // Название шаблона
    "DETAIL_PAGER_TITLE" => "Страница",    // Название категорий
    "DETAIL_PROPERTY_CODE" => array(    // Свойства
        0 => "DOP_FILES",
        1 => "",
    ),
    "DETAIL_SET_CANONICAL_URL" => "N",    // Устанавливать канонический URL
    "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
    "DISPLAY_DATE" => "Y",    // Выводить дату элемента
    "DISPLAY_NAME" => "Y",    // Выводить название элемента
    "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
    "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
    "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
    "FILE_404" => "",
    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",    // Скрывать ссылку, если нет детального описания
    "IBLOCK_ID" => constant('PURCHASES_IBLOCK_' . LANGUAGE_ID),    // Инфоблок
    "IBLOCK_TYPE" => "purchases",    // Тип инфоблока
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
    "LIST_FIELD_CODE" => array(    // Поля
        0 => "",
        1 => "",
    ),
    "LIST_PROPERTY_CODE" => array(    // Свойства
        0 => "FILE",
        1 => "PROMOSITE",
    ),
    "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
    "META_DESCRIPTION" => "-",    // Установить описание страницы из свойства
    "META_KEYWORDS" => "-",    // Установить ключевые слова страницы из свойства
    "NEWS_COUNT" => 8,    // Количество новостей на странице
    "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
    "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
    "PAGER_SHOW_ALL" => "Y",    // Показывать ссылку "Все"
    "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
    "PAGER_TEMPLATE" => "",    // Шаблон постраничной навигации
    "PAGER_TITLE" => "Документы",    // Название категорий
    "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
    "SEF_FOLDER" => "/purchases/",    // Каталог ЧПУ (относительно корня сайта)
    "SEF_MODE" => "Y",    // Включить поддержку ЧПУ
    "SEF_URL_TEMPLATES" => array(
        "detail" => "#SECTION_CODE_PATH#/#ELEMENT_ID#/",
        "news" => "",
        "section" => "#SECTION_CODE_PATH#/",
    ),
    "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
    "SET_STATUS_404" => "Y",    // Устанавливать статус 404
    "SET_TITLE" => "N",    // Устанавливать заголовок страницы
    "SHOW_404" => "N",    // Показ специальной страницы
    "SORT_BY1" => "DATE_ACTIVE_FROM",    // Поле для первой сортировки новостей
    "SORT_BY2" => "ID",    // Поле для второй сортировки новостей
    "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
    "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
    "USE_CATEGORIES" => "N",    // Выводить материалы по теме
    "USE_FILTER" => "Y",    // Показывать фильтр
    "USE_PERMISSIONS" => "N",    // Использовать дополнительное ограничение доступа
    "USE_RATING" => "N",    // Разрешить голосование
    "USE_REVIEW" => "N",    // Разрешить отзывы
    "USE_RSS" => "N",    // Разрешить RSS
    "USE_SEARCH" => "N",    // Разрешить поиск
    "USE_SHARE" => "N",    // Отображать панель соц. закладок
    "TEMPLATE_LIST" => "list", //шаблон списка новостей
),
    false
); ?>
<!--
	<section class="mb-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 purchases-03-01">
<ul>
<li class="active"><a href="">Порядок осуществления
	закупок</a></li>
<li><a href="">Стандарт управления
закупочной деятельностью</a></li>
<li><a href="">Акты, детализирующие нормы
Стандарта</a></li>
<li><a href="">Перечни</a></li>
<li><a href="">Архив (Правила закупок)</a></li>
</ul>


          </div>
          <div class="col-lg-2 pl-4 pr-4">
				<div class="purchases-03-02">
<ul>
<li><a href="">2020</a></li>
<li><a href="">2019</a></li>
<li><a href="">2018</a></li>
<li><a href="">2017</a></li>
<li><a href="">2016</a></li>
<li><a href="">2015</a></li>
</ul>
				</div>
          </div>
          <div class="col-lg-6 purchases-03-03">

<table>
	<tr class="pb-5"><td class="pr-5"><a class="date" href="">22.05.2020</a></td>
		<td><a class="doc mb-5" href="">Перечень товаров, работ и услуг,
закупки которых могут осуществляться
в рамках внутрихолдинговой кооперации
	</a></td></tr>
<tr><td class="mr-5"><a class="date" href="">14.04.2020</a></td>
		<td><a class="doc mb-5" href="">Пул товаров для импортозамещения
</a></td></tr>
<tr><td class="mr-5"><a class="date" href="">13.04.2020</a></td>
		<td><a class="doc mb-5" href="">Перечень товаров, производимых
предприятиями машиностроительной,
химической и целлюлозно-бумажной

</a></td></tr>
</table>
          </div>
         </div>
        </div>
    </section>
-->
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<p class="t-1 mb-4 mt-5 text-center">
	<b>Закупки группы компаний АО «Самрук-Қазына»<br>
проводятся на  портале электронных закупок

</b>
			  </p>
          </div>
         </div>
        </div>
    </section>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center mb-1">
			  <a target="_blank" href="https://zakup.sk.kz" class="btn-read-more">zakup.sk.kz</a>
          </div>
         </div>
        </div>
    </section>


	<section class="mt-2 mb-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-3">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-3">
				Архив закупок АО «Самрук-Қазына»
            </h2>
          </div>
         </div>
        </div>
    </section>
<? 

$APPLICATION->IncludeComponent("bitrix:news", "ga.purchase.archive", Array(
    "FILTER_NAME" => 'arrFilter', //имя переменной, в которой хранится фильтр
    "ADD_ELEMENT_CHAIN" => "Y",    // Включать название элемента в цепочку навигации
    "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
    "AJAX_MODE" => "Y",    // Включить режим AJAX
    "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
    "AJAX_OPTION_HISTORY" => "Y",    // Включить эмуляцию навигации браузера
    "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "N",    // Включить подгрузку стилей
    "BROWSER_TITLE" => "NAME",    // Установить заголовок окна браузера из свойства
    "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
    "CACHE_GROUPS" => "Y",    // Учитывать права доступа
    "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
    "CACHE_TYPE" => "N",    // Тип кеширования
    "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
    "DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",    // Формат показа даты
    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
    "DETAIL_DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
    "DETAIL_FIELD_CODE" => array(    // Поля
        0 => "SHOW_COUNTER",
        1 => "",
    ),
    "DETAIL_PAGER_SHOW_ALL" => "Y",    // Показывать ссылку "Все"
    "DETAIL_PAGER_TEMPLATE" => "",    // Название шаблона
    "DETAIL_PAGER_TITLE" => "Страница",    // Название категорий
    "DETAIL_PROPERTY_CODE" => array(    // Свойства
        0 => "DOP_FILES",
        1 => "",
    ),
    "DETAIL_SET_CANONICAL_URL" => "N",    // Устанавливать канонический URL
    "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
    "DISPLAY_DATE" => "Y",    // Выводить дату элемента
    "DISPLAY_NAME" => "Y",    // Выводить название элемента
    "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
    "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
    "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
    "FILE_404" => "",
    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",    // Скрывать ссылку, если нет детального описания
    "IBLOCK_ID" => constant('PURCHASES_IBLOCK_' . LANGUAGE_ID),    // Инфоблок
    "IBLOCK_TYPE" => "purchases",    // Тип инфоблока
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",    // Формат показа даты
    "LIST_FIELD_CODE" => array(    // Поля
        0 => "",
        1 => "",
    ),
    "LIST_PROPERTY_CODE" => array(    // Свойства
        0 => "FILE",
        1 => "PROMOSITE",
    ),
    "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
    "META_DESCRIPTION" => "-",    // Установить описание страницы из свойства
    "META_KEYWORDS" => "-",    // Установить ключевые слова страницы из свойства
    "NEWS_COUNT" => 8,    // Количество новостей на странице
    "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
    "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
    "PAGER_SHOW_ALL" => "Y",    // Показывать ссылку "Все"
    "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
    "PAGER_TEMPLATE" => "",    // Шаблон постраничной навигации
    "PAGER_TITLE" => "Документы",    // Название категорий
    "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
    "SEF_FOLDER" => "/purchases/",    // Каталог ЧПУ (относительно корня сайта)
    "SEF_MODE" => "Y",    // Включить поддержку ЧПУ
    "SEF_URL_TEMPLATES" => array(
        "detail" => "#SECTION_CODE_PATH#/#ELEMENT_ID#/",
        "news" => "",
        "section" => "#SECTION_CODE_PATH#/",
    ),
    "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
    "SET_STATUS_404" => "Y",    // Устанавливать статус 404
    "SET_TITLE" => "N",    // Устанавливать заголовок страницы
    "SHOW_404" => "N",    // Показ специальной страницы
    "SORT_BY1" => "DATE_ACTIVE_FROM",    // Поле для первой сортировки новостей
    "SORT_BY2" => "ID",    // Поле для второй сортировки новостей
    "SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
    "SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
    "USE_CATEGORIES" => "N",    // Выводить материалы по теме
    "USE_FILTER" => "Y",    // Показывать фильтр
    "USE_PERMISSIONS" => "N",    // Использовать дополнительное ограничение доступа
    "USE_RATING" => "N",    // Разрешить голосование
    "USE_REVIEW" => "N",    // Разрешить отзывы
    "USE_RSS" => "N",    // Разрешить RSS
    "USE_SEARCH" => "N",    // Разрешить поиск
    "USE_SHARE" => "N",    // Отображать панель соц. закладок
    "TEMPLATE_LIST" => "list", //шаблон списка новостей
),
    false
); ?>
<!--
	<section class="mt-5 mb-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 purchases-03-01 purchases-04-01">
<ul>
<li class="active"><a href="">План закупок</a></li>
<li><a href="">План долгосрочных закупок</a></li>
<li><a href="">Закупки способом тендера</a></li>
<li><a href="">Закупки способом запроса
ценовых предложений
</a></li>
<li><a href="">Закупки способом
из одного источника</a></li>
</ul>


          </div>
          <div class="col-lg-2 pl-4 pr-4">
				<div class="purchases-03-02">
<ul>
<li><a href="">2020</a></li>
<li><a href="">2019</a></li>
<li><a href="">2018</a></li>
<li><a href="">2017</a></li>
<li><a href="">2016</a></li>
<li><a href="">2015</a></li>
</ul>
				</div>
          </div>
          <div class="col-lg-6 purchases-03-03">

<table>
	<tr class="pb-5"><td class="pr-5"><a class="date" href="">22.05.2020</a></td>
		<td><a class="doc mb-5" href="">Перечень товаров, работ и услуг,
закупки которых могут осуществляться
в рамках внутрихолдинговой кооперации
	</a></td></tr>
<tr><td class="mr-5"><a class="date" href="">14.04.2020</a></td>
		<td><a class="doc mb-5" href="">Пул товаров для импортозамещения
</a></td></tr>
<tr><td class="mr-5"><a class="date" href="">13.04.2020</a></td>
		<td><a class="doc mb-5" href="">Перечень товаров, производимых
предприятиями машиностроительной,
химической и целлюлозно-бумажной

</a></td></tr>
</table>
          </div>
         </div>
        </div>
    </section>
-->
	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
<p class="t-1 mb-4 mt-2 text-center">
	<b>Специальные программы для отечественных товаропроизводителей</b>
			  </p>
          </div>
         </div>
        </div>
    </section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mt-3 mb-5">
				<div class="purchases-05 purchases-05-01">
					<span class="title">Программа<br>
новых производств: 
						<div class="block-line-3"></div>
</span>
заключено 35 договоров на 22 млрд тенге, а объем частных инвестиций за 2018-2020 годы составил почти 220 млрд тенге. На этих проектах создается около 2000 рабочих мест. Формируется Пул товаров, которые Фонд готов закупать у казахстанских производителей на постоянной основе. На сегодня в этом списке более 1200 позиций с общим объемом долгосрочной потребности более 250 млрд тенге.


				</div>
          </div>
          <div class="col-lg-6 mt-3 mb-5">
				<div class="purchases-05 purchases-05-02">
					<span class="title">С 2017 года внедрено категорийное
управление закупками (КУЗ): 
						<div class="block-line-3"></div>
					</span>
для товаров, работ и услуг, имеющих важное значение для бизнес-процессов компаний Фонда, разрабатываются закупочные категорийные стратегии. 
<br><br>
Подтвержденный экономический эффект от реализации закупочных стратегий с момента внедрения на конец 2019 года составил более 35,3 млрд тенге, в том числе за 2019 год в сумме 21,6 млрд тенге.
				</div>

          </div>
         </div>
        </div>
    </section>




