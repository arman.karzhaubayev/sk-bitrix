<?$APPLICATION->SetTitle("Қор туралы");?>	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('about-fund')?>
            </h2>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<p class="t-1 mb-3 mt-3">

«Самұрық-Қазына» Ұлттық әл-ауқат қоры» Акционерлік қоғамы 2008 жылы Қазақстан Республикасы Президентінің Жарлығымен құрылды. Қордың жалғыз акционері - Қазақстан Республикасының Үкіметі. 
			  </p>
<p class="t-1 mb-5">
Қор Қазақстан Республикасының ұлттық әл-ауқатын арттыру және болашақ ұрпақ үшін ұзақ мерзімді тұрақтылықты қамтамасыз ету міндеті жүктелген инвестициялық холдинг.

<!--<?=GetMessage('main_1')?>-->
<!--<b>Акционерное общество «Фонд национального благосостояния «Самрук-Қазына»</b> — Фонд, единственным акционером которого является Правительство Республики Казахстан.-->
<!--<?=GetMessage('main_2')?>-->
	<!--<b>Фонд был основан в 2008 году Указом Президента Республики Казахстан</b> и представляет собой коммерческую структуру — инвестиционный холдинг, миссия которого заключается в повышении национального благосостояния Республики Казахстан и поддержке модернизации национальной экономики.-->
</p>
    <!-- History -->
<? 
if(intval($_REQUEST['year'])==0)
	$_REQUEST['year'] = 2012;
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "aboutfund.history",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPONENT_TEMPLATE" => "aboutfund.history",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "Y",
        "FIELD_CODE" => array(0 => "ID", 1 => "CODE", 2 => "XML_ID", 3 => "NAME", 4 => "SORT", 5 => "PREVIEW_TEXT", 6 => "PREVIEW_PICTURE", 7 => "",),
        "FILE_404" => "",
        "FILTER_NAME" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => constant("HISTORY_SLIDER_IBLOCK_" . LANGUAGE_ID),
        "IBLOCK_TYPE" => "sliders",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "100",
        "PAGER_BASE_LINK" => "",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "PAGER_DESC_NUMBERING" => "Y",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_PARAMS_NAME" => "arrPager",
        "PAGER_SHOW_ALL" => "Y",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(0 => "", 1 => "BUTTON", 2 => "BUTTON_LINK", 3 => "",),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "SORT",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER1" => "ASC",
        "SORT_ORDER2" => "DESC"
    )
); ?>

<!--
	<section class="about-01 mt-5 mb-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-5 mb-3">
				Қорды құрудағы басты мақсат
            </h2>
          </div>
        </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-4 about-01-block">
			  <span class="mr-3">1</span>
				<p>Ұлттық экономиканы
жаңғыртуға және
әртараптандыруға жәрдемдесу</p>
          </div>
          <div class="col-lg-4 about-01-block">
			  <span class="mr-3">2</span>
				<p>Ел экономикасын
тұрақтандыруға қатысу</p>
          </div>
          <div class="col-lg-4 about-01-block">
			  <span class="mr-3">3</span>
				<p>Компания қызметінің
тиімділігін арттыру
					деятельности компании</p>
          </div>
        </div>
		</div>

      </div>
    </section>
	<section class="about-02 mt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-3">
				Қордың міндеттері
            </h2>
			<p class="text-center">2008 жылы құрыла салысымен-ақ, Қор дағдарыстан кейінгі Қазақстан экономикасын қалпына келтіруші катализаторлардың бірі болды. Осы жылдары ол бірнеше міндеттер атқарды:
			</p>
          </div>
         </div>
        </div>
      <div class="container mt-3">
        <div class="row mb-1">
          <div class="col-lg-6">
				<div class="block-01">
					Коммерциялық банктерді
капиталдандыру арқылы қаржы
секторын тұрақтандыру
				</div>
				<div class="block-01">
					Қымбат ипотекалық қарыздарды
қайта қаржыландыру
				</div>
          </div>
          <div class="col-lg-6">
				<div class="block-01">
					Үлескерлік құрылыс нарығындағы
проблемаларды шешу

				</div>
				<div class="block-01">
					ШОБ несиелеу және стратегиялық
инвестициялық жобаларды жүзеге асыру
				</div>
          </div>
         </div>
        </div>

    </section>

-->
	<section class="about-03 mb-5 mt-3">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-3">
				«Самұрық-Қазына» ҰӘҚ» АҚ даму стратегиясы
            </h2>
          </div>
         </div>
        </div>
      <div class="container block-02">
        <div class="row">
          <div class="col-lg-4">

<p class="block-04">
	<b>«Самұрық-Қазына» қорының миссиясы</b> 
Қазақстан Республикасының ұлттық әл-ауқатын арттыру және болашақ ұрпақ үшін ұзақ мерзімді тұрақтылықты қамтамасыз ету.
			  </p>

          </div>
          <div class="col-lg-8">

		      <div class="container">
        			<div class="row">
          				<div class="col-lg-12">

				<p class="block-05">
«Самұрық-Қазына» АҚ –ның 2028 жылға дейінгі даму стратегиясын 2018 жылы Қорды басқару жөніндегі кеңесі және Жалғыз акционер мақұлдады.
			  </p>
						</div>
<!--
          				<div class="col-lg-6">
<div class="block-yellow" style="    min-height: 200px;
    margin-top: 90px;
    font-size: 22px;
    line-height: 28px;">
«Самұрық-Қазына» АҚ –ның 2028 жылға дейінгі даму стратегиясын 2018 жылы Қорды басқару жөніндегі кеңесі және Жалғыз акционер мақұлдады.
							</div>

<!--
  <div class="swiper-container swiper-container-about">
    <div class="swiper-wrapper">
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-01">
						<span>Мұнайгаз</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-02">
						<span>Көлік және логистика салаларындағы кәсіпорындар</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-03">
						<span>Химия өндірісі</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-04">
						<span>Атом өндірісі</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-05">
						<span>Тау-кен, металлургия кешені</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-06">
						<span>Энергетика</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-07">
						<span>Машина жасау </span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-08">
						<span>Жылжымайтын мүлік</span>
					</div>
          		</div>
		</div>
    </div>
    <div class="swiper-about-button-next"></div>
    <div class="swiper-about-button-prev"></div>
  </div>


  <script>
    var swiper = new Swiper('.swiper-container-about', {
      navigation: {
        nextEl: '.swiper-about-button-next',
        prevEl: '.swiper-about-button-prev',
      },
    });
  </script>
-->






						<!--</div>-->
					</div>
				</div>

          </div>
         </div>
        </div>

    </section>
<!--
	<section class="about-04 mb-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-3">
				Қордың стратегиялық мақсаттары
            </h2>
          </div>
         </div>
        </div>
    </section>
	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<p class="t-1 text-center">
	<b>портфельдік компаниялардың табыстылығын арттыру арқылы тиімділікті арттыру;</b>
</p>
<p class="t-1 mt-3 text-center">
	<b>портфель құрылымын оңтайландыру және табысты әртараптандыру арқылы портфельді тиімді басқару;</b><br>
</p>
<p class="t-1 mt-3 text-center">
	<b>әлеуметтік тұрақтылық пен тиімді корпоративті басқару арқылы тұрақты даму</b><br>
</p>
          </div>
         </div>
        </div>
    </section>
-->
<!--
	<section class="about-05 mt-1 mb-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
				<div class="about-05-01">
					<div class="img-01"></div>
					<span>Миссиясы</span>
Қазақстан Республикасының ұлттық әл-ауқатын арттыру және болашақ ұрпақ үшін ұзақ мерзімді тұрақтылықты қамтамасыз ету

				</div>
          </div>
          <div class="col-lg-6">
				<div class="about-05-01">
					<div class="img-02"></div>
					<span>Пайымы</span>
<br>Жоғары қаржылық көрсеткіштер мен инвестициялардан түсетін кірісті қамтамасыз етуші, сондай-ақ портфельді алдыңғы қатарлы әл-ауқат қорлары деңгейінде дамытушы инвестициялық холдинг.

				</div>

          </div>
         </div>
        </div>
      <div class="container mt-4">
        <div class="row">
          <div class="col-lg-12">
				<div class="about-05-01">
					<div class="img-03"><span>№1 стратегиялық мақсат</span></div>

Қордың басты мақсаттары мен мандаттарының бірі операциялық, өндірістік, қаржылық тиімділік, коммерциялық қызмет және салынған капиталдан түсетін кірісті арттыру бөлігінде тиімділік көрсеткіштерін ұқсас әлемдік жетекші компаниялардың көрсеткіштерімен теңестіру есебінен портфельдік компанияларды тиімді басқару болып табылады. Қаржылық орнықтылықты нығайту, операциялық тиімділікті арттыру, өндірістік активтерді пайдалану тиімділігін арттыру, сондай-ақ Трансформациялау бағдарламасын іске асыру осы стратегиялық мақсатты іске асыру үшін бірінші кезектегі міндет болып табылады.
				</div>
          </div>
          </div>
         </div>

    </section>
-->


	<section class="mb-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
				Қордың стратегиялық мақсаттары
            </h2>
          </div>
         </div>
        </div>
    </section>

	<section class="about-06 mt-1 mb-1">
      <div class="container mb-5">
        <div class="row">
          <div class="col-lg-6">
				<div class="about-06-01">
						<span>Компаниялардың
рентабельдігін арттыру</span>
					<a href="" class="block-down" data-id="0"></a>
				</div>
				<div class="block-down-box" data-id="0">
Портфельдік компаниялардың рентабельділігін арттыру Қордың басты міндеттердің бірі болып табылады. Қаржылық орнықтылықты нығайту, операциялық тиімділікті арттыру, бизнес-процестерді оңтайландыру және реинжинирингтеу, Қордың портфельдік компаниялары арасындағы синергияны қамтамасыз ету және портфельдік компаниялардың инвестициялық тартымдылығын арттыру бойынша бірқатар шараларды қамтамасыз ету жоспарланып отыр
				</div>
          </div>
          <div class="col-lg-6">
				<div class="about-06-01">
<span>Портфельдің құрылымын оңтайландыру</span>
					<a href="" class="block-down" data-id="1"></a>
				</div>
				<div class="block-down-box" data-id="1">
Портфельдің құрылымын оңтайландыру Қордың инвестициялар портфелін белсенді басқаруға көшуі үшін маңызды қадам болып табылады. Портфельдің құнын көтеру және оның кірістілігін ұлғайту мақсатында Қордың ағымдағы активтер портфелін қайта құрылымдау, инвестициялық жобаларды қайта қарау, заңды тұлғалар саны мен активтерді басқару деңгейлерін қысқарту және стратегиялық емес активтерден шығу міндеттері Қорға өз ресурстарын қайта бөлуге мүмкіндік беретін болады.
				</div>

          </div>
         </div>
        </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
				<div class="about-06-01">
						<span>Кірістерді әртараптандыру
</span>
					<a href="" class="block-down" data-id="2"></a>
				</div>
				<div class="block-down-box" data-id="2">

Активтерді белсенді басқару Қорға Қазақстанның экономикасы үшін әлеуетті мультипликативтік әсерге ие және кірістілігі жоғары активтерге/жобаларға басымдық бере отырып, портфель кірістілігін арттыруға, оны әртүрлі салаларға әртараптандыруға мүмкіндік береді.
<p>
<br> <b>
Қазақстан Республикасындағы инвестициялар
</b>
				</p>
Қор инвестицияларды Қазақстан экономикасының әртүрлі секторларын дамыту және Қазақстан экономикасы үшін стратегиялық маңызы бар жобаларды (негізгі мемлекеттік бағдарламаларға сәйкес) дамыту мақсатында Қазақстан Республикасының заңнамалық актілерімен белгіленген және жекеменшік сектордың дамуына кедергі келтірмейтін, "сары парақтар" қағидатымен белгіленген шектеулерді ескере отырып, Қазақстан Республикасы шегінде жүзеге асырады.
<p>
<br> <b>
Қазақстан Республикасының шегінен тыс инвестициялар
</b>
				</p>
Орта мерзімді және ұзақ мерзімді перспективада даму стратегиясы әртараптандыру арқылы кірістілікті арттыру, әл-ауқатты сақтау мақсатында ұлттық экономикамен және өнеркәсіппен байланысты емес Қазақстан Республикасынан тыс инвестицияларды бөлуді көздейді. Қор жетекші тәуелсіз қорлармен, сондай-ақ бірлескен тікелей инвестицияларды жүзеге асыру үшін басқа инвестициялық компаниялармен және қорлармен әріптестік қатынастар құрады, нәтижесінде олар Қазақстанның өнеркәсіп салаларымен


				</div>

          </div>
          <div class="col-lg-6">
				<div class="about-06-01">
<span>Корпоративтік басқару
және тұрақты даму
</span>
					<a href="" class="block-down" data-id="3"></a>
				</div>
				<div class="block-down-box" data-id="3">



Орнықты даму бастамалары, корпоративтік басқару және корпоративтік мәдениет орнықты дамудың негізі және портфель құнына ықпал ететін шекті факторлары болып табылады.
<p>
<br> <b>
Корпоративтік басқару
</b>
				</p>
Қор тиімді басқаруды қамтамасыз ету, инвестициялық тартымдылықты арттыру және белсенді инвестор функциясын сәтті орындау мақсатында Қорда және портфельдік компанияларда Экономикалық ынтымақтастық және даму ұйымының (ЭЫДҰ) корпоративтік басқару жөніндегі ұсынымдарына сәйкес корпоративтік басқарудың озық тәжірибесін енгізуді жалғастыруы қажет.
<p>
<br> <b>
Адами капиталды дамыту
</b>
				</p>
Тиімді корпоративтік мәдениетті қалыптастыру, меритократияның негізге алынатын қағидаттарын енгізу, адамдарды трансформациялау және «коммерциялық» ойлау мен өзгерістерге әзір болу үшін жаңа мінез-құлық модельдерін енгізу арқылы Қорда және портфельдік компанияларда персоналды дамыту бойынша үздік тәжірибелерді пайдалануды қамтамасыз етеді.
<p>
<br> <b>
Демеушілік қызмет және әлеуметтік жауапкершілік
</b>
				</p>
Қор демеушілік және қайырымдылық көмек көрсетуді жалғастырады. Көмектің бөлінуі қайта қаралуға және қоғам қажеттілігіне қарай басымдық берілуге тиіс. Ең үздік тәжірибелерге сүйене отырып, Қор денсаулық сақтауға, білім беруге, жастардың кәсіби дамуына, қоғамдастықты, қоршаған ортаны, өнер мен мәдениет мұрасын, білімді дамытуға, зерттеулерге, инновацияларға, ғылым мен технологияларға, бақытсыздыққа ұшыраған тұлғалар мен отбасыларына қолдау көрсетуге бағытталған әлеуметтік жобаларды қаржыландырады.
<p>
<br> <b>
Тұрақты даму бастамалары
</b>
				</p>
Қор қызметтің экономикалық, экологиялық және әлеуметтік аспектілерін тиімді және сәтті басқару мақсатында жоғары этикалық стандарттарды ендіреді және сенімге негізделген корпоративті мәдениетті қалыптастырады, корпоративті басқаруды жетілдіреді, қаржылық орнықтылықты қамтамасыз етеді, адами капиталға инвестициялауды, экологиялық орнықтылықты қамтамасыз ету бойынша барлық шараларды қабылдайды.

				</div>

          </div>
         </div>
        </div>
    </section>


	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5 text-center desc ">
<p class="t-1 mb-3 mt-3">

Қазақстан Республикасының 2025 жылға дейінгі Стратегиялық даму жоспарына сәйкес Қор мемлекеттік бастамалар мен бағдарламаларды Орындаушы рөлінен инвестициялық компанияның рөліне ауысуға ниетті. Бұл мақсат ағымдағы портфельдік компаниялардың қызметін басқарумен қатар, инвестициялардан түсетін кірістерді қамтамасыз ететін Қордың инвестициялар портфелін белсенді басқаруды және жетекші егеменді әл-ауқат қорлары деңгейінде дамытушы қоржынды көздейді. Қор жекешелендіру бағдарламасын, қаражат пен дивидендтерді дивестициялау мен қайта инвестициялау бағдарламасын, Қазақстанда және одан тыс жерлерде жаңа инвестицияларды іске асыру арқылы өз қоржысының сапасын белсенді басқаруды және жақсартуды жалғастыруға ниетті.

			  </p>
          </div>
         </div>
        </div>
    </section>



	<section>
<!--
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<p class="t-1 mb-5 mt-5 text-center">
	2015 жылы қабылданған корпоративтік басқарудың жаңа<br>
	<b>Кодексі Қорды</b> компанияларын директорлар кеңесі арқылы тиімді<br>
басқара білетін стратегиялық холдингке айналдыруды көздейді.
			  </p>
          </div>
         </div>
        </div>
-->
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
			  <div class="block-line"></div>
          </div>
         </div>
        </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
			  <p class="t-1 mb-3 mt-4 text-center">«Самұрық-Қазына» АҚ<br>
Корпоративтік басқару Кодексі
				  <br><a target="_blank" href="/documents/2018-2028 Даму стратегиясы.doc" class="pdf mt-3"></a>
</p>

          </div>
         </div>
        </div>
    </section>
<!--
	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center mt-5 mb-5">
			  <a href="/about-fund/corporate-governance/" class="btn-read-more">Узнать больше</a>
          </div>
         </div>
        </div>
    </section>
-->
<!--
	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center mt-5 mb-1">
			  <div class="block-line-full"></div>
          </div>
         </div>
        </div>
    </section>
-->

			</div>
</div>
</div>
</section>