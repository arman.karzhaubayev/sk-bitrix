<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/pages.php");

$pagetitle = Loc::getMessage('ABOUT_FUND') ." — ". Loc::getMessage("NAME_SITE");
$description = Loc::getMessage("DESCRIPTION_SITE");
$keywords = Loc::getMessage("KEYWORDS_SITE");
$APPLICATION->SetTitle($pagetitle);
$APPLICATION->SetPageProperty("title", $pagetitle);
$APPLICATION->SetPageProperty("description",  $description);
$APPLICATION->SetPageProperty("keywords",  $keywords);
$APPLICATION->SetPageProperty("og:type",  "website");
$APPLICATION->SetPageProperty("og:title",  $pagetitle);
$APPLICATION->SetPageProperty("og:description",  $description);
$APPLICATION->SetPageProperty("og:url",  "https://sk.kz".$APPLICATION->GetCurPage(false));
$APPLICATION->SetPageProperty("og:image",  "https://sk.kz".SITE_TEMPLATE_PATH."/thumb.jpg");
?>
<div class="about">
	<div class="breadcrumb about__breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="breadcrumb__content">
						<a
							class="breadcrumb__item"
							href="/"
						>
							<p class="breadcrumb__item-text"><?=Loc::getMessage('MAIN');?></p>
						</a>

						<span class="breadcrumb__item">
							<p class="breadcrumb__item-text"><?=Loc::getMessage('ABOUT_FUND');?></p>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="about__info">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="title">
						<h2 class="title__text"><?=Loc::getMessage('ABOUT_FUND');?></h2>
					</div>

					<div class="mainpage__about-content">
						<img
							class="mainpage__about-logo"
							src="<?=SITE_TEMPLATE_PATH?>/assets/img/logo.svg"
							alt=""
						>

						<div class="mainpage__about-content-des">
							<p class="mainpage__about-text mainpage__about-text_bold">
								<?=Loc::getMessage('ABOUT_FUND_TEXT_TOP');?>
							</p>

							<p class="mainpage__about-text">
								<?=Loc::getMessage('ABOUT_FUND_TEXT_BOTTOM');?>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="about__mobilehistory">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="title">
						<div class="title__line">
							<img
								class="title__line-border"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
								alt=""
							>

							<img
								class="title__line-center"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
								alt=""
							>

							<img
								class="title__line-border"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
								alt=""
							>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="mainhistory__slider">
			<div class="mainhistory__title">
				<p class="mainhistory__title-text"><?=Loc::getMessage('OUR_HISTORY');?></p>
			</div>

			<div class="swiper-container mainhistory__slider-container">
				<div class="swiper-wrapper">
					<div
						class="swiper-slide"
						v-for="(year,index) in historyMobile"
						:key="index"
					>
						<div class="mainhistory__slider-item">
							<div class="mainhistory__list">
								<div
									class="mainhistory__list-item"
									v-for="(item,index) in year[1]"
									:key="index"
								>
									<p class="mainhistory__list-item-number">0{{index+1}}</p>

									<p
										class="mainhistory__list-item-text"
										v-html="item"
									></p>
								</div>
							</div>
						</div>

						<div class="mainhistory__year">
							<div class="mainhistory__year-arrow mainhistory__year-arrow_left">
								<img
									class="mainhistory__year-arrow-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-long.svg"
									alt=""
								>
							</div>

							<div class="mainhistory__year-arrow mainhistory__year-arrow_right">
								<img
									class="mainhistory__year-arrow-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-long.svg"
									alt=""
								>
							</div>

							<p
								class="mainhistory__year-text"
								v-html="year[0]"
							></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="about__history">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="mainhistory">
						<div class="mainhistory__control">
							<div class="mainhistory__control-item mainhistory__control-item_top">
								<img
									class="mainhistory__control-item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
									alt=""
								>
							</div>

							<div class="mainhistory__control-item mainhistory__control-item_bottom">
								<img
									class="mainhistory__control-item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
									alt=""
								>
							</div>
						</div>

						<div class="mainhistory__years">
							<div class="swiper-container mainhistory__yearsslider">
								<div class="swiper-wrapper">
									<div
										class="swiper-slide"
										v-for="(item, index) in historyMobile"
										:key="index"
									>
										<div
											class="mainhistory__years-item"
											:data-year="item[0]"
											:class="item[0]==historyActiveYear?'mainhistory__years-item_active':''"
											@click="()=&gt;historyActiveYear=item[0]"
										>
											<p
												class="mainhistory__years-item-text"
												v-html="item[0]"
											></p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="mainhistory__content">
							<div class="mainhistory__title">
								<p class="mainhistory__title-text">{{ $t('messages.history1') }}</p>
							</div>

							<div class="mainhistory__list">
								<div
									class="mainhistory__list-item"
									v-for="(item,index) in history[lang][historyActiveYear]"
									:key="index"
								>
									<p class="mainhistory__list-item-number">0{{index+1}}</p>

									<p
										class="mainhistory__list-item-text"
										v-html="item"
									></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="about__strategy">
		<div class="container container_xs_nopad">
			<div class="row">
				<div class="col-12">
					<div class="title about__title">
						<div class="title__line">
							<img
								class="title__line-border"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
								alt=""
							>

							<img
								class="title__line-center"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
								alt=""
							>

							<img
								class="title__line-border"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
								alt=""
							>
						</div>

						<h2 class="title__text">
							<?=Loc::getMessage('DEVELOPMENT_STRATEGY_OF_SAMRUK_KAZYNA_SWF_JSC');?>
						</h2>
					</div>
				</div>

				<div class="col-12">
					<div class="columninfo">
						<div class="columninfo__left">
							<p class="columninfo__title"><?=Loc::getMessage('DEVELOPMENT_STRATEGY_OF_SAMRUK_KAZYNA_SWF_JSC_TEXT_LEFT_TOP');?></p>

							<p class="columninfo__text columninfo__text_light">
								<?=Loc::getMessage('DEVELOPMENT_STRATEGY_OF_SAMRUK_KAZYNA_SWF_JSC_TEXT_LEFT_BOTTOM');?>
							</p>
						</div>

						<div class="columninfo__right">
							<p class="columninfo__text columninfo__text_l">
								<?=Loc::getMessage('DEVELOPMENT_STRATEGY_OF_SAMRUK_KAZYNA_SWF_JSC_TEXT_RIGHT');?>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="about__goals">
		<div class="container container_xs_nopad">
			<div class="row">
				<div class="col-12">
					<div class="title about__title">
						<div class="title__line">
							<img
								class="title__line-border"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
								alt=""
							>

							<img
								class="title__line-center"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
								alt=""
							>

							<img
								class="title__line-border"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
								alt=""
							>
						</div>

						<h2 class="title__text"><?=Loc::getMessage('FUNDS_STRATEGIC_GOALS');?></h2>
					</div>
				</div>

				<div class="col-12">
					<div class="tabinfo">
						<div class="tabinfo__nav">
							<div
								class="tabinfo__nav-item"
								v-for="(item, index) in companyGoals[lang]"
								:key="index"
								:class="companyGoalsActiveIndex==index?'tabinfo__nav-item_active':''"
								@click="companyGoalsActiveIndex=index"
							>
								<p
									class="tabinfo__nav-item-text"
									v-html="item.title"
								></p>
							</div>
						</div>

						<div class="tabinfo__navmobile">
							<div
								class="tabinfo__navmobile-arrow tabinfo__navmobile-arrow_left"
								@click="prevTabinfoElem"
							>
								<img
									class="tabinfo__navmobile-arrow-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow-light.svg"
									alt=""
								>
							</div>

							<div
								class="tabinfo__navmobile-arrow tabinfo__navmobile-arrow_right"
								@click="nextTabinfoElem"
							>
								<img
									class="tabinfo__navmobile-arrow-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow-light.svg"
									alt=""
								>
							</div>

							<div class="tabinfo__navmobile-item">
								<p
									class="tabinfo__navmobile-item-text"
									v-html="companyGoals[lang][companyGoalsActiveIndex].title"
								></p>
							</div>
						</div>

						<div class="tabinfo__content">
							<p
								class="tabinfo__text"
								v-for="(item, index) in companyGoals[lang][companyGoalsActiveIndex].content"
								:key="index"
								v-html="item"
							></p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="about__content">
						<?=Loc::getMessage('FUNDS_STRATEGIC_GOALS_TEXT');?>
					</div>
				</div>
				<?if(LANGUAGE_ID == "ru"):?>
				<div class="col-5 col-sm-7 col-xs-12">
					<a
						class="btnwrap"
						href="/documents/новая стратегия Фонда на 2018-2028 гг..doc"
						target="_blank"
					>
						<div class="btn btn_hasicon about__btn">
							<p class="btn__text">
								<?=Loc::getMessage('NEW_FUND_STRATEGY');?>
							</p>

							<div class="btn__icon">
								<img
									class="btn__icon-src"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
									alt=""
								>
							</div>
						</div>
					</a>
				</div>
				<?elseif(LANGUAGE_ID == "kz"):?>
				<div class="col-5 col-sm-7 col-xs-12">
					<a
						class="btnwrap"
						href="/documents/2018-2028 Даму стратегиясы.doc"
						target="_blank"
					>
						<div class="btn btn_hasicon about__btn">
							<p class="btn__text">
								<?=Loc::getMessage('NEW_FUND_STRATEGY');?>
							</p>

							<div class="btn__icon">
								<img
									class="btn__icon-src"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
									alt=""
								>
							</div>
						</div>
					</a>
				</div>
				<?endif?>
			</div>
		</div>
	</div>
</div>

<div class="partnerswrap">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="mpartners">
					<div class="mpartners__arrow mpartners__arrow_left">
						<img
							class="mpartners__arrow-icon"
							src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
							alt=""
						>
					</div>

					<div class="swiper-container mpartners__list">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<a
									href="https://hrqyzmet.kz/"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/brands_qyzmet.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://samruk-akparat.kz/"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aqparat.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://www.skc.kz/"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/samruk.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://aifc.kz/"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aifc_cmyk.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://primeminister.kz/ru"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/min.png"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="http://www.akorda.kz/ru"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-7.jpg"
											alt=""
										>
									</div>
								</a>
							</div>

							<div class="swiper-slide">
								<a
									href="https://elbasy.kz/ru"
									target="_blank"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-6.jpg"
											alt=""
										>
									</div>
								</a>
							</div>
						</div>
					</div>

					<div class="mpartners__arrow mpartners__arrow_right">
						<img
							class="mpartners__arrow-icon"
							src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
							alt=""
						>
					</div>

					<div class="mpartners__mobile">
						<a
							href="https://hrqyzmet.kz/"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/brands_qyzmet.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://samruk-akparat.kz/"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aqparat.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://www.skc.kz/"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/samruk.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://aifc.kz/"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aifc_cmyk.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://primeminister.kz/ru"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/min.png"
									alt=""
								>
							</div>
						</a>

						<a
							href="http://www.akorda.kz/ru"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-7.jpg"
									alt=""
								>
							</div>
						</a>

						<a
							href="https://elbasy.kz/ru"
							target="_blank"
						>
							<div class="mpartners__item">
								<img
									class="mpartners__item-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-6.jpg"
									alt=""
								>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>