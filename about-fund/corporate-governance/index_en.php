<?$APPLICATION->SetTitle("Corporate Governance");?><!-- Stock --> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"MODE" => "html",
		"PATH" => SITE_TEMPLATE_PATH."/inc/stock.php"
	)
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"MODE" => "html",
		"PATH" => SITE_TEMPLATE_PATH."/inc/top_menu.php"
	)
);?> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-7">
			<h2 class="h2-left pr-20 pb-30 page-title">
			<?=GetMessage('corporate-governance');?> </h2>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p class="t-1 mb-5 mt-3">
				 The corporate governance system of Samruk-Kazyna JSC protects the rights and provides investors with prompt and reliable information about profit, management principles and the state of affairs in the company, minimizes and eliminates possible risks of interests and rights of shareholders.
			</p>
		</div>
	</div>
</div>
 </section>
<section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left pr-20 pb-30 section-title text-center mt-3 t-up"> <span class="fw-4">IN THE PART OF CORPORATE GOVERNANCE</span><br>
			 THE FUND IS OPERATING 6 PROJECTS:</h2>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			 <!-- Swiper -->
			<div class="swiper-container swiper-container-corp">
				<div class="swiper-wrapper">
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-001.png"><br>
								 <b>New Code</b> <br><br> For the first time in Kazakhstan, the Fund, with the assistance of the Organization for Economic Co-operation and Development (OECD), has developed a new corporate governance code that meets the best international standards
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-002.png"><br>
								 <b>New methodology </b><br> <br> With the introduction of the new Corporate Governance Code, the Fund's portfolio companies face new challenges
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-001.png"><br>
								 <b>Effectiveness </b> <br><br> As a result of the transformation, the Fund should turn from an operating holding into a strategic holding
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-002.png"><br>
								<b>Sustainable development</b><br><br> Sustainable development is the coherence of our economic, environmental and social goals with a balance of interests of stakeholders for a sustainable increase in the value of the Fund group in the long term
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-001.png"><br>
								 <b>Transparency</b><br> <br> Transparency is an important area for investors and stakeholders. The introduction of a culture of responsible attitude to information is a necessary step before entering the IPO
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-002.png"><br>
								 <b>Report</b> <br><br> The quality of Annual Accounts reports directly affects transparency, therefore the content and presentation of information in them have been revised. The next step will be the preparation of reports in accordance with the GRI standard
							</div>
						</div>
					</div>
				</div>
				 <!-- Add Arrows -->
				<div class="swiper-corp-button-next">
				</div>
				<div class="swiper-corp-button-prev">
				</div>
			</div>
			 <!-- Initialize Swiper --> <script>
    var swiper = new Swiper('.swiper-container-corp', {
      slidesPerView: 3,
      spaceBetween: 0,
      slidesPerGroup: 1,
      /*loop: true,
		loopFillGroupWithBlank: true,*/
      navigation: {
        nextEl: '.swiper-corp-button-next',
        prevEl: '.swiper-corp-button-prev',
      },
    });
  </script>
		</div>
	</div>
</div>
 </section> <section>
<div class="container mt-5">
	<div class="row">
		<div class="col-lg-12 corp-01 mt-5">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-003.png"> <span class="corp-01-01">
						Management bodies </span> <br>
						 The corporate governance system in Samruk-Kazyna JSC includes management, control and responsibility of management bodies in all companies of the Fund at all levels:
					</div>
					<div class="col-lg-2">
					</div>
					<div class="col-lg-6">
						<div class="corp-01-02">
 <br>
 <br>
 <br>
 <br>
							 The Government of the Republic of Kazakhstan is the Sole Shareholder of Samruk-Kazyna JSC, empowered to approve the Fund's development strategy, appoint directors and general director, approve financial results and receive dividends;
							<div class="block-line-04">
							</div>
							 The Board of Directors provides general management of the company and delegates day-to-day operational tasks to the Management Board, which is headed by its CEO.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <section>
<div class="container mt-5">
	<div class="row">
		<div class="col-lg-12">
			<div class="corp-02">
				<h2 class="h2-left pr-10 pb-10 section-title text-center mt-3">
				Fund Management Board </h2>
				<p class="t-1 mt-3 text-justify">
					 The Council under the management of the Samruk-Kazyna JSC is an advisory body chaired by the First President of the Republic of Kazakhstan - Elbasy. The main task of the Council is to develop proposals to improve the competitiveness and efficiency of the Samruk-Kazyna JSC.
				</p>
				<div class="container corp-02-01 mt-5">
					<div class="row">
						<div class="col-lg-9">
 <img src="/bitrix/templates/skfinance/img/img-corp-005.png">
						</div>
						<div class="col-lg-3">
 <br>
							 Nursultan Nazarbayev
							<p>
 <br>
								 First President of the Republic of Kazakhstan - Elbasy, Chairman of the Fund Management Board
							</p>
						</div>
					</div>
				</div>
				<h2 class="h2-left pr-10 pb-10 section-title text-center mt-3 fs-24">
				Members of the Fund Management Board:</h2>
				<div class="container corp-02-02 mt-5">
					<div class="row">
						<div class="col-lg-4">
 <img src="/bitrix/templates/skfinance/img/img-corp-006.png">
						</div>
						<div class="col-lg-2">
							 Askar Mamin
							<p>
 <br>
 <br>
								 Prime Minister of the Republic of Kazakhstan
							</p>
						</div>
						<div class="col-lg-4">
 <img src="/bitrix/templates/skfinance/img/img-corp-008.png">
						</div>
						<div class="col-lg-2">
							 Erlan Koshanov
							<p>
 <br>
 <br>
								 Head of Administration of President of the Republic of Kazakhstan
							</p>
						</div>
					</div>
				</div>
				<div class="container corp-02-02 mt-5">
					<div class="row">
						<div class="col-lg-3">
						</div>
						<div class="col-lg-4">
 <img width="276" src="/upload/medialibrary/0e2/0e2998d9cffe9b52795766e5bcaefab8.jpg" height="270">
						</div>
						<div class="col-lg-3">
							 Almassadam Satkaliyev
							<p style="text-align: justify;padding-right: 22px;">
 <br>
 <br>
								 Chairman of the Board of the Samruk-Kazyna JSC
							</p>
						</div>
						<div class="col-lg-6">
							 <!--
                <div class="corp-02-03">
                  two representatives of domestic business
                </div>
                <div class="corp-02-03">representative of a foreign business</div>
-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left mt-5 pr-20 pb-30 section-title text-center mt-3">
			BOARD OF DIRECTORS</h2>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="corp-02 corp-03">
				<div class="container">
					 <?/*$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/corp-1_".LANGUAGE_ID.".php",
        'MODE' => 'html'
    ),
    false
);*/?>
					<div class="row">
						<div class="col-lg-12">
							<div class="owl-carousel owl-theme" id="corp-1">
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/john.jpg">
										<p>
 <b>John Dudas</b> <br>
 <br>
											 Chairman of the Board of Directors of Samruk-Kazyna JSC
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/mazhibaeyev.jpg">
										<p>
 <b>Kairat Mazhibayev</b> <br>
 <br>
											 Member of the Board of Directors, independent director of Samruk-Kazyna JSC
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/irgaliev.jpg">
										<p>
 <b>Asset Irgaliyev</b> <br>
 <br>
											 Member of the Board of Directors of Samruk-Kazyna JSC
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/ong.jpg">
										<p>
 <b>Ong Boon Khvi</b> <br>
 <br>
											 Member of the Board of Directors, Independent Director of Samruk-Kazyna JSC
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/bozumbayev.jpg">
										<p>
 <b>Kanat Bozumbayev</b> <br>
 <br>
											 Member of the Board of Directors of Samruk-Kazyna JSC, Assistant to the President of the Republic of Kazakhstan
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/upload/medialibrary/0e2/0e2998d9cffe9b52795766e5bcaefab8.jpg">
										<p>
 <b>Almassadam Satkaliyev</b> <br>
 <br>
											 Member of the Board of Directors Chairman of the Board of Samruk-Kazyna JSC
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/luka.jpg">
										<p>
 <b>Luca Sutera</b> <br>
 <br>
										</p>
										<p>
											 Independent Director of Samruk-Kazyna JSC, Member of the Board of Directors
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					 <script>
	$('#corp-1').owlCarousel({
		items:2,
    nav:false,
    dots:true,
    margin:10,
    loop:false

    
})


	 </script>
				</div>
				<h2 class="h2-left pr-10 pb-10 section-title text-center mt-5 fs-24">
				Governance </h2>
				<div class="container mt-5">
					 <?/*$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/corp-2_".LANGUAGE_ID.".php",
        'MODE' => 'html'
    ),
    false
);*/?>
					<div class="row">
						<div class="col-lg-12">
							<div class="owl-carousel owl-theme" id="corp-2">


								<div class="item">
									<div class="person">
 										<img width="276" src="/upload/medialibrary/0e2/0e2998d9cffe9b52795766e5bcaefab8.jpg" height="270">
										<p><b>Almassadam Satkaliyev</b> <br><br>
											 Chairman of the Board of Samruk-Kazyna JSC
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 										<img src="/bitrix/templates/skfinance/img/governance/nurbaeva.jpg">
										<p>
									 		<b>Nazira Nurbayeva </b> <br><br>
											 Managing Director for Economics and Finance
										</p>
									</div>
								</div>


								<div class="item">
									<div class="person">
										<img src="/bitrix/templates/skfinance/img/governance/zhanadil.jpg">
										<p>
											<b>Yernar Zhanadil</b> <br><br>
											 Managing Director for Investment, Privatization and International Cooperation
										</p>
									</div>
								</div>



								<div class="item">
									<div class="person">
 										<img src="/bitrix/templates/skfinance/img/governance/kravchenko.jpg">
										<p><b>Andrey Kravchenko</b> <br><br>
											 Managing Director for Legal Support and Risks
										</p>
									</div>
								</div>



								<div class="item">
									<div class="person">
 										<img src="/upload/iblock/9f9/9f9b15834bca4d00a0cf7a53c0e3b173.jpg">
										<p>
 											<b>Marat Aitenov</b><br><br>
											 Managing Director of Public and Government Relations
										</p>
									</div>
								</div>


							</div>
						</div>
					</div>
					 <script>
	$('#corp-2').owlCarousel({
		items:2,
    nav:false,
    dots:true,
    margin:10,
    loop:false

    
})


	 </script>
				</div>
			</div>
		</div>
	</div>
</div>
 </section>
<!-- Management --> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left mt-5 pr-20 pb-30 section-title text-center mt-3">
			Management </h2>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="corp-02 corp-03">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="owl-carousel owl-theme" id="corp-5">

								<div class="item">
									<div class="person">
 										<img src="/upload/medialibrary/0e2/0e2998d9cffe9b52795766e5bcaefab8.jpg">
										<p>
 											<b>Almassadam Satkaliyev</b> <br><br>
											 Chairman of the Board of Samruk-Kazyna JSC
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 										<img alt="akuchalakov.jpg" src="/bitrix/templates/skfinance/img/governance/akuchalakov.jpg" >
										<p>
											 <b>Bolat Akchulakov</b>  <br><br>
											 Managing Director for Assets Management
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 										<img src="/bitrix/templates/skfinance/img/governance/zhanadil.jpg">
										<p>
											 <b>Yernar Zhanadil</b><br><br>
											 Managing Director for Investment, Privatization and International Cooperation
										</p>
									</div>
								</div>


								<div class="item">
									<div class="person">
 										<img src="/bitrix/templates/skfinance/img/governance/durmagambetov.jpg">
										<p>
											 <b>Erlan Durmagambetov</b><br><br>
											 Managing Director of Strategy, Sustainability and Digital Transformation
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 										<img src="/bitrix/templates/skfinance/img/governance/tajigaliev.jpg">
										<p>
											 <b>Mukhtar Tazhigaliyev</b><br><br>
											 Managing Director of Corporate Governance and Social and Labor Relations
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 										<img src="/bitrix/templates/skfinance/img/governance/kravchenko.jpg">
										<p>
 											<b>Andrey Kravchenko</b><br><br>
											 Managing Director for Legal Support and Risks
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
										<img src="/upload/iblock/9f9/9f9b15834bca4d00a0cf7a53c0e3b173.jpg">
										<p>
											<b>Marat Aitenov</b><br><br>
											 Managing Director for Public and Government Relations
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 										<img src="/bitrix/templates/skfinance/img/governance/nurbaeva.jpg">
										<p>
									 		<b>Nazira Nurbayeva </b> <br><br>
											 Managing Director for Economics and Finance
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 										<img src="/local/templates/default_template/assets/img/pages/management/sovet4/person-8.jpg">
										<p>
											 <b>Berdigulov Ernat Kudaibergenovich</b>  <br>
 											<br>
											 Co-Managing Director of Strategy, Sustainability and Digital Transformation
<script>
	$('#corp-5').owlCarousel({
		items:2,
    nav:false,
    dots:true,
    margin:10,
    loop:false

    
})
	 </script>
										</p>
									</div>
								</div>



							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section>
<!-- /Management --> <section class="mt-5">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left section-title text-center">
			Committees of the Board of Directors </h2>
		</div>
	</div>
</div>
 </section> <section class="about-06 mt-5 mb-1">
<div class="container mb-5">
	<div class="row">
		<div class="col-lg-6">
			<div class="about-06-01">
				 Audit Committee <a href="" class="block-down" data-id="0"></a>
			</div>
			<div class="block-down-box" data-id="0">
				 Chairman: <span class="corp-04">
				Sutera Luca<br>
				 Independent Director. </span>
				Member: <span class="corp-04">
				Ong Boon Hwee<br>
				 Independent Director. </span>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="about-06-01_1" style="width: 100%;
    height: 134px;
    border-radius: 5px;
    background-color: #08706a;
    font-size: 24px;
    color: #ffffff;
    font-weight: 700;
    font-family: 'Playfair Display';
    display: inline-block;
    line-height: 50px;
    padding: 10px;text-align: center;"><a href="" class="block-down" data-id="1"></a>
				 Nomination and Remuneration Committee 
			</div>
			<div class="block-down-box" data-id="1">
				 Chairman: <span class="corp-04">
				Dudas Jon <br>
				 Independent Director. </span>
				Member: <span class="corp-04">
				Bozumbayev Kanat <br>
				 Aide to the President of the Republic of Kazakhstan;<br>
 </span> <span class="corp-04">
				Mazhibayev Kairat<br>
				 Independent Director;<br>
 </span> <span class="corp-04">
				Sutera Luca<br>
				 Independent Director.<br>
 </span>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-6">
			<div class="about-06-01">
				 Special Committee <a href="" class="block-down" data-id="2"></a>
			</div>
			<div class="block-down-box" data-id="2">
				 Chairman: <span class="corp-04">
				Sultanov Bakhyt </span>
				Member: <span class="corp-04">
				Bergenev Adylgazy<br>
				 Zhanadil Ernar<br>
				 Tuleshov Gabdulkumyt<br>
				 Kiyakbaeva Ardak<br>
				 Bushmuhambetova Ainur<br>
				 Karymsakov Beıbıt<br>
				 Issayev Bakhtiyar<br>
 </span>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="about-06-01_1" style="width: 100%;
    height: 134px;
    border-radius: 5px;
    background-color: #08706a;
    font-size: 24px;
    color: #ffffff;
    font-weight: 700;
    font-family: 'Playfair Display';
    display: inline-block;
    line-height: 50px;
    padding: 10px;text-align: center;"><a href="" class="block-down" data-id="3"></a>
				 Transformation Program Oversight Committee 
			</div>
			<div class="block-down-box" data-id="3">
				 Chairman: <span class="corp-04">
				Dudas Jon <br>
				 Independent Director. </span>
				Member: <span class="corp-04">
				Bozumbayev Kanat<br>
				 Aide to the President of the Republic of Kazakhstan; </span> <span class="corp-04">
				Ong Boon Hwee<br>
				 Independent Director. </span>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-3 mt-5">
		</div>
		<div class="col-lg-6 mt-5">
			<div class="about-06-01">
				 Strategy Committee <a href="" class="block-down" data-id="5"></a>
			</div>
			<div class="block-down-box" data-id="5">
				 Chairman: <span class="corp-04">
				Irgaliyev Asset<br>
				 Member of the Board of Directors; </span>
				Member: <span class="corp-04">
				Dudas Jon<br>
				 Independent Director; </span> <span class="corp-04">
				Mazhibayev Kairat<br>
				 Independent Director </span> <span class="corp-04">
				Ong Boon Hwee <br>
				 Independent Director. </span>
			</div>
		</div>
	</div>
</div>
 </section> <section class="mt-5">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left section-title text-center">
			Organizational structure </h2>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12 mt-5">
			<div class="organization">
				<div class="organization-structure">
					<div class="organization-base organization-item">
						<div class="organization-base-content">
							<div class="organization-main-col">
								<div class="organization-item-main organization-item-directors">
 <span>Board of Directors<i class="organization-base-open-dir"></i></span> <i class="organization-item-open-base-dir"></i>
								</div>
								<div class="organization-item-main organization-item-seo" style="margin-bottom:20px;">
 <span>Chief Executive Officer (CEO)<i class="organization-base-open-dir"></i></span> <i class="organization-item-open-dir arrow-yelow"></i>
								</div>
							</div><br><br><br>
							<div class="organization-main-col">
								<div class="organization-chain-directors">
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Corporate Secretary Service
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Internal Audit Service
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Ombudsman
										</div>
									</div>
								</div>
								<div class="organization-chain-seo">
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Advisor to the CEO
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Directorate for Summary and Analytical Work and Control
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Compliance Service
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Security Department
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 State Secrets Protection and Information Security Department
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="organization-chain">
							<div class="organization-items">
								<div class="organization-item">
									<div class="organization-item-main">
										<br><br><br>Managing Director for Assets Management <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Oil& Gas Directorate
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Power and Mining Assets Directorate
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Transport & Logistics Directorate
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												Communications Directorate
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												Financial and Economic Asset Management Department
											</div>
										</div>  
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										 <br>Managing Director forInvestment, Privatization andInternational Cooperation <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Assets Privatization and Restructuring Department
											</div>
										</div>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Investment Department
											</div>
										</div>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Departmentfor International Cooperation
											</div>
										</div>
									</div>
									
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										 <br>Managing Director for Strategy, Sustainability and Digital Transformation <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Co-Managing Director
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Strategic Planning and Market Analysis Department
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Innovation and Knowledge Management Department
											</div>
										</div>
									<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Digital Transformation Department
											</div>
									</div>
									<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Sustainable Development Directorate
											</div>
									</div>
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										<br>Managing Director for Corporate Governance and Social and Labor Relations <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 HR Department
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Corporate Governance Department
											</div>
										</div>
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										<br><br>Managing Director for Legal Support and Risks <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Legal Support and Methodology Department
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Risk Management and Internal Control Department
											</div>
										</div>
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										<br><br>Managing Director for Public and Government Relations <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Public Relations Department
											</div>
										</div>
									<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Government Relations Department
											</div>
										</div>
									<div class="organization-chain-item">
											<div class="organization-chain-content">
												Procurement Control and Monitoring Department
											</div>
										</div>
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										<br><br><br> Managing Director for Economics and Finance<i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Economics and Planning Department
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Accounting and Financial Reporting Department
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Corporate Finance Department
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Treasury Department
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
 <br>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p class="t-1 mb-5 mt-5 text-center">
				 <!--
	<b>*The Fund has a total authorized staffing of 193 posts.</b>
-->
			</p>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="block-line">
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p class="t-1 mb-3 mt-4 text-center">
				 Samruk-Kazyna JSC<br>
				 Corporate Governance Code <br>
 <a href="/documents/CG Code_<?=LANGUAGE_ID?>.pdf" target="_blank" class="pdf mt-3"></a>
			</p>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12 mt-3">
			<div class="block-line">
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p class="t-1 mb-5 mt-5 text-center">
				 In 2015 the Fund adopted <br>
 <b> a new Corporate Governance Code </b> after extensive and in-depth discussion. <br>
 <b>From January 1, 2017</b> in the annual reports of the Fund and Portfolio Companies compliance <br>
				 with the Code must be disclosed.
			</p>
		</div>
	</div>
</div>
 </section>