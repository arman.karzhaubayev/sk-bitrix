<?$APPLICATION->SetTitle("Корпоративтік басқару");?><!-- Stock --> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"MODE" => "html",
		"PATH" => SITE_TEMPLATE_PATH."/inc/stock.php"
	)
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"MODE" => "html",
		"PATH" => SITE_TEMPLATE_PATH."/inc/top_menu.php"
	)
);?> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-7">
			<h2 class="h2-left pr-20 pb-30 page-title">
			<?=GetMessage('corporate-governance')?> </h2>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p class="t-1 mb-5 mt-3">
				 «Самұрық-Қазына» АҚ корпоративті басқару жүйесі акционерлердің құқықтарын қорғайды және инвесторларға табыстың басқару принциптері және компаниядағы жағдай туралы жедел және сенімді ақпарат ұсынады, басқару қағидалары және қоғамдағы жағдай, акционерлердің мүдделері мен құқықтарының бұзылу ықтимал тәуекелдерін минимумға дейін жеткізеді және жояды.
			</p>
		</div>
	</div>
</div>
 </section>

<section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left pr-20 pb-30 section-title text-center mt-3 t-up"> <span class="fw-4">Корпоративтік басқару бөлімінде</span><br>
			 Қор 6 жобаны жүзеге асырады: </h2>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			 <!-- Swiper -->
			<div class="swiper-container swiper-container-corp">
				<div class="swiper-wrapper">
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-001.png"><br>
								 <b>Жаңа Кодекс</b><br><br>
								  Қор ЭЫДҰ-ның қолдауымен Қазақстанда алғаш рет Корпоративтік басқарудың кодексін жасады. Жаңа кодекс үздік халықаралық стандарттарға сәйкес келеді
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-002.png"><br>
								 <b>Жаңа әдістеме</b><br><br>
								 Корпоративтік басқарудың жаңа Кодексін енгізумен Қордың қоржындық компаниялары алдында жаңа міндеттер пайда болады
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-001.png"><br>
								 <b>Тиімділік </b><br><br>
								 Трансформациялау нәтижесінде Қор операциялық холдингтен стратегиялық холдингке айналуға тиіс
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-002.png"><br>
								 <b>Тұрақты даму  </b><br><br>
								 Тұрақты даму <!--
АО «Самрук-Казына» — это Фонд национального благосостояния. 
Поэтому группа Фонда должна ответственно вести свой бизнес и преумножить ценность 
портфеля как для текущих, так и будущих поколений Казахстана. 
--> Тұрақты даму – бұл ұзақ мерзімді перспективада Қор тобының құнын орнықты ұлғайту үшін тараптар мүдделірінің теңгерімін сақтай отырып, экономикалық, экологиялық және әлеуметтік мақсаттарымыздың келісімділігі
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-001.png"><br>
								 <b>Ашықтық </b><br><br>
								 Ашықтық – инвесторлар мен стейкхолдерлер үшін маңызды бағыт. Ақпаратқа жауапкершілікпен қарау мәдениетін енгізу – IPO-ға шығар алдында қажетті кезең
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-002.png"><br>
								 <b>Есептер </b><br><br>
								 Жылдық есептердің сапасы ашықтыққа тікелей әсер етеді, сондықтан олардағы ақпараттың мазмұны мен тарату тәсілі қайта қаралады. Келесі қадам GRI стандартына сәйкес есептер дайындау болады
							</div>
						</div>
					</div>
				</div>
				 <!-- Add Arrows -->
				<div class="swiper-corp-button-next">
				</div>
				<div class="swiper-corp-button-prev">
				</div>
			</div>
			 <!-- Initialize Swiper --> <script>
    var swiper = new Swiper('.swiper-container-corp', {
      slidesPerView: 3,
      spaceBetween: 0,
      slidesPerGroup: 1,
      /*loop: true,
		loopFillGroupWithBlank: true,*/
      navigation: {
        nextEl: '.swiper-corp-button-next',
        prevEl: '.swiper-corp-button-prev',
      },
    });
  </script>
		</div>
	</div>
</div>
 </section> <section>
<div class="container mt-5">
	<div class="row">
		<div class="col-lg-12 corp-01 mt-5">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-003.png"> <br>
 <span class="corp-01-01">
						Басқару органы </span> <br>
						 «Самұрық-Қазына» АҚ-ның Корпоративтік басқару жүйесіне Қордың Компаниялар тобы бойынша алғашқыдан соңғы деңгейге дейінгі бүкіл басқару, бақылау және басқару органдарының жауапкершілігі кіреді:
					</div>
					<div class="col-lg-2">
					</div>
					<div class="col-lg-6">
						<div class="corp-01-02">
 <br>
 <br>
 <br>
 <br>
							 «Самұрық-Қазына» АҚ-ның жалғыз акционері бұл – Қазақстан Республикасының Үкіметі. Оның құзырына Қордың Даму стратегиясын бекіту, Басқарма Төрағасы мен директорларын тағайындау, қаржылық нәтижелерді бекіту мен дивидендтер алу кіреді;
							<div class="block-line-04">
							</div>
							 Компания қызметіне жалпы жетекшілікті, оның ағымдағы қызметтік міндеттерін шешу өкілеттілігі берілген Төраға бастаған компания Басқармасының Директорлар кеңесі жасайды.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <section>
<div class="container mt-5">
	<div class="row">
		<div class="col-lg-12">
			<div class="corp-02">
				<h2 class="h2-left pr-10 pb-10 section-title text-center mt-3">
				Қорды басқару жөніндегі кеңес </h2>
				<p class="t-1 mt-3 text-justify">
					 Қорды басқару жөніндегі кеңес Қазақстан Республикасының Тұңғыш Президенті - Елбасы басқаратын консультативтік-кеңесші орган болып табылады. Ол Қордың бәсекеге қабілеттілігі мен қызмет тиімділігін арттыру жөнінде ұсыныстар жасайды.
				</p>
				<div class="container corp-02-01 mt-5">
					<div class="row">
						<div class="col-lg-9">
 <img src="/bitrix/templates/skfinance/img/img-corp-005.png">
						</div>
						<div class="col-lg-3">
 <br>
							 Нұрсұлтан Әбішұлы Назарбаев
							<p>
 <br>
								 Қазақстан Республикасының Тұңғыш Президенті - Елбасы, Кеңес төрағасы
							</p>
						</div>
					</div>
				</div>
				<h2 class="h2-left pr-10 pb-10 section-title text-center mt-3 fs-24">
				Кеңес мүшелері </h2>
				<div class="container corp-02-02 mt-5">
					<div class="row">
						<div class="col-lg-4">
 <img src="/bitrix/templates/skfinance/img/img-corp-006.png">
						</div>
						<div class="col-lg-2">
							 Асқар Ұзақбайұлы Мамин
							<p>
 <br>
 <br>
								 Қазақстан Республикасының Премьер-Министрі
							</p>
						</div>
						<div class="col-lg-4">
 <img src="/bitrix/templates/skfinance/img/img-corp-008.png">
						</div>
						<div class="col-lg-2">
							 Ерлан Жаханұлы Қошанов
							<p>
 <br>
 <br>
								 Қазақстан Республикасы Президенті Әкімшілігінің Басшысы
							</p>
						</div>
					</div>
				</div>
				<div class="container corp-02-02 mt-5">
					<div class="row">
						<div class="col-lg-3">
						</div>
						<div class="col-lg-4">
 <img alt="CEO" src="/upload/medialibrary/0e2/0e2998d9cffe9b52795766e5bcaefab8.jpg" title="CEO">
						</div>
						<div class="col-lg-3">
							 Сатқалиев Алмасадам Маиданұлы
							<p style="text-align: justify; padding-right: 28px;">
 <br>
 <br>
								 «Самұрық-Қазына» АҚ Басқарма Төрағасы
							</p>
						</div>
						<div class="col-lg-6">
							 <!--
                <div class="corp-02-03">
                  екі отандық бизнес өкілі
                </div>
                <div class="corp-02-03">шетелдік бизнес өкілі</div>
-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left mt-5 pr-20 pb-30 section-title text-center mt-3">
			Директорлар кеңесі </h2>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="corp-02 corp-03">
				<div class="container">
					 <?/*$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/corp-1_".LANGUAGE_ID.".php",
        'MODE' => 'html'
    ),
    false
);*/?>
					<div class="row">
						<div class="col-lg-12">
							<div class="owl-carousel owl-theme" id="corp-1">
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/john.jpg">
										<p>
											 Джон Дудас <br>
 <br>
											 «Самұрық-Қазына» АҚ Директорлар кеңесінің төрағасы, тәуелсіз директоры
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/mazhibaeyev.jpg">
										<p>
											 Қайрат Қуанышбайұлы Мәжібаев <br>
 <br>
											 «Самұрық-Қазына» АҚ Директорлар кеңесінің мүшесі, тәуелсіз директоры
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/irgaliev.jpg">
										<p>
											 Иргалиев Асет Арманович <br>
 <br>
											 «Самұрық-Қазына» АҚ Директорлар кеңесінің мүшесі
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/ong.jpg">
										<p>
											 Онг Бун Хви <br>
 <br>
											 Директорлар кеңесінің мүшесі, «Самұрық-Қазына» АҚ тәуелсіз директоры
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/bozumbayev.jpg">
										<p>
											 Қанат Алдабергенұлы Бозымбаев <br>
 <br>
											 Қазақстан Республикасы Президентінің көмекшісі
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img width="276" src="/upload/medialibrary/0e2/0e2998d9cffe9b52795766e5bcaefab8.jpg" height="270">
										<p>
											 Сатқалиев Алмасадам Маиданұлы<br>
 <br>
											 Директорлар кеңесінің мүшесі, «Самұрық-Қазына» АҚ Басқарма Төрағасы
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/luka.jpg">
										<p>
											 Лука Сутера <br>
 <br>
										</p>
										<p>
											 «Самұрық-Қазына» АҚ Директорлар кеңесінің мүшесі,<br>
											 Тәуелсіз директор
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					 <script>
	$('#corp-1').owlCarousel({
		items:2,
    nav:false,
    dots:true,
    margin:10,
    loop:false

    
})


	 </script>
				</div>
				<h2 class="h2-left pr-10 pb-10 section-title text-center mt-5 fs-24">
				Басқарма </h2>
				<div class="container mt-5">
					 <?/*$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/corp-2_".LANGUAGE_ID.".php",
        'MODE' => 'html'
    ),
    false
);*/?>
					<div class="row">
						<div class="col-lg-12">
							<div class="owl-carousel owl-theme" id="corp-2">
								<div class="item">
									<div class="person">
 <img width="276" src="/upload/medialibrary/0e2/0e2998d9cffe9b52795766e5bcaefab8.jpg" height="270">
										<p>
											 Сатқалиев Алмасадам Маиданұлы<br>
 <br>
											 «Самұрық-Қазына» АҚ Басқарма Төрағасы
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/nurbaeva.jpg">
										<p>
											 Нұрбаева Назира Нұртүлеуқызы  <br>
 <br>
											 Экономика және қаржы жөніндегі басқарушы директор
										</p>
									</div>
								</div>


								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/zhanadil.jpg">
										<p>
											 Жанәділ Ернар Бейсенұлы <br>
 <br>
											 Инвестициялар, жекешелендіру және халықаралық ынтымақтастық жөніндегі басқарушы директор
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/kravchenko.jpg">
										<p>
											 Кравченко Андрей Николаевич <br>
 <br>
											 Құқықтық сүйемелдеу және тәуекелдер жөніндегі басқарушы директор
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 <img src="/upload/iblock/9f9/9f9b15834bca4d00a0cf7a53c0e3b173.jpg">
										<p>
											 Әйтенов Марат Дүйсенбекұлы<br>
 <br>
											 Қоғаммен және Үкіметпен байланыс жөніндегі басқарушы директор
										</p>
									</div>
								</div>


							</div>
						</div>
					</div>
					 <script>
	$('#corp-2').owlCarousel({
		items:2,
    nav:false,
    dots:true,
    margin:10,
    loop:false

    
})


	 </script>
				</div>
			</div>
		</div>
	</div>
</div>
 </section>
<!-- Management --> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left mt-5 pr-20 pb-30 section-title text-center mt-3">
			Басшылық </h2>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="corp-02 corp-03">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="owl-carousel owl-theme" id="corp-5">
								<div class="item">
									<div class="person">
 <img width="276" src="/upload/medialibrary/0e2/0e2998d9cffe9b52795766e5bcaefab8.jpg" height="270">
										<p>
											 Сатқалиев Алмасадам Маиданұлы <br>
 <br>
											 «Самұрық-Қазына» АҚ Басқарма Төрағасы
										</p>
									</div>
								</div>
								<<div class="item">
									<div class="person">
 <img alt="akuchalakov.jpg" src="/bitrix/templates/skfinance/img/governance/akuchalakov.jpg" >
										<p>
											 Ақшолақов Болат Оралұлы <br>
 <br>
											 Активтерді басқару жөніндегі басқарушы директор 
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/zhanadil.jpg">
										<p>
											 Жанәділ Ернар Бейсенұлы <br>
 <br>
											 Инвестициялар, жекешелендіру және халықаралық ынтымақтастық жөніндегі басқарушы директор
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/durmagambetov.jpg">
										<p>
											 Дұрмағамбетов Ерлан Дмитриевич <br>
 <br>
											 Стратегия, тұрақты даму және  цифрлық трансформациялау жөніндегі басқарушы директор
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/tajigaliev.jpg">
										<p>
											 Тажіғалиев Мұхтар Өтепқалиұлы  <br>
 <br>
											 Корпоративтік басқару және әлеуметтік-еңбек қатынастары жөніндегі басқарушы директор
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/kravchenko.jpg">
										<p>
											 Кравченко Андрей Николаевич <br>
 <br>
											 Құқықтық сүйемелдеу және тәуекелдер жөніндегі басқарушы директор
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 <img src="/upload/iblock/9f9/9f9b15834bca4d00a0cf7a53c0e3b173.jpg">
										<p>
											 Әйтенов Марат Дүйсенбекұлы<br>
 <br>
											 Қоғаммен және Үкіметпен байланыс жөніндегі басқарушы директор
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/nurbaeva.jpg">
										<p>
											 Нұрбаева Назира Нұртүлеуқызы  <br>
 <br>
											 Экономика және қаржы жөніндегі басқарушы директор
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 										<img src="/local/templates/default_template/assets/img/pages/management/sovet4/person-8.jpg">
										<p>
											 Бердіғұлов Ернат Құдайбергенұлы  <br>
 											<br>
											 Стратегия, орнықты даму және цифрлық трансформация жөніндегі тең-басқарушы директоры
										</p>
									</div>
								</div>

							</div>
						</div>
					</div>
					 <script>
	$('#corp-5').owlCarousel({
		items:2,
    nav:false,
    dots:true,
    margin:10,
    loop:false

    
})
	 </script>
				</div>
			</div>
		</div>
	</div>
</div>
 </section>
<!-- /Management --> <section class="mt-5">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left section-title text-center">
			Директорлар кеңесінің комитеттері </h2>
		</div>
	</div>
</div>
 </section> <section class="about-06 mt-5 mb-1">
<div class="container mb-5">
	<div class="row">
		<div class="col-lg-6">
			<div class="about-06-01">
				 Аудит комитеті <a href="" class="block-down" data-id="0"></a>
			</div>
			<div class="block-down-box" data-id="0">
				 Комитет төрағасы: <span class="corp-04">
				Сутера Лука<br>
				 тәуелсіз директор. </span>
				Комитет мүшесі: <span class="corp-04">
				Онг Бун Хви<br>
				 тәуелсіз директор. </span>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="about-06-01_1" style="width: 100%;
    height: 134px;
    border-radius: 5px;
    background-color: #08706a;
    font-size: 24px;
    color: #ffffff;
    font-weight: 700;
    font-family: 'Playfair Display';
    display: inline-block;
    line-height: 50px;
    padding: 10px;text-align: center;"><a href="" class="block-down" data-id="1"></a>
				 Тағайындау және сыйақы комитеті 
			</div>
			<div class="block-down-box" data-id="1">
				 Комитет төрағасы: <span class="corp-04">
				Дудас Джон<br>
				 тәуелсіз директор. </span>
				Комитет мүшесі: <span class="corp-04">
				Бозымбаев Қанат Алдабергенұлы<br>
				 Қазақстан Республикасы Президентінің көмекшісі;<br>
 </span> <span class="corp-04">
				Мәжібаев Қайрат Қуанышбайұлы<br>
				 тәуелсіз директор;<br>
 </span> <span class="corp-04">
				Сутера Лука<br>
				 тәуелсіз директор.<br>
 </span>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-6">
			<div class="about-06-01">
				 Мамандандырылған комитет <a href="" class="block-down" data-id="2"></a>
			</div>
			<div class="block-down-box" data-id="2">
				 Комитет төрағасы: <span class="corp-04">
				Бақыт Тұрлыханұлы Сұлтанов </span>
				Комитет мүшесі: <span class="corp-04">
				Бергенев Адылгазы Садвоқасұлы<br>
				 Ернар Жанәділ Бейсенұлы<br>
				 Түлешов Ғабдұлхамит Мәденұлы<br>
				 Қияқбаева Ардақ Боранқызы<br>
				 Бұшмұхамбетова Айнұр Сансызбайқызы<br>
				 Карымсаков Бейбит Еркинбаевич<br>
				 Исаев Бахтияр Орынбасарович<br>
 </span>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="about-06-01_1" style="width: 100%;
    height: 134px;
    border-radius: 5px;
    background-color: #08706a;
    font-size: 24px;
    color: #ffffff;
    font-weight: 700;
    font-family: 'Playfair Display';
    display: inline-block;
    line-height: 50px;
    padding: 10px;text-align: center;"><a href="" class="block-down" data-id="3"></a>
				 Трансформациялау бағдарламасының іске асырылуын бақылау комитеті
			</div>
			<div class="block-down-box" data-id="3">
				 Комитет төрағасы: <span class="corp-04">
				Дудас Джон<br>
				 тәуелсіз директор. </span>
				Комитет мүшесі: <span class="corp-04">
				Бозымбаев Қанат Алдабергенұлы<br>
				 Қазақстан Республикасы Президентінің көмекшісі; </span> <span class="corp-04">
				Онг Бун Хви<br>
				 тәуелсіз директор. </span>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-3 mt-5">
		</div>
		<div class="col-lg-6 mt-5">
			<div class="about-06-01">
				 Стратегия комитеті <a href="" class="block-down" data-id="5"></a>
			</div>
			<div class="block-down-box" data-id="5">
				 Комитет төрағасы: <span class="corp-04">
				Ерғалиев Әсет Арманұлы<br>
				 Қазақстан Республикасының Ұлттық экономика министрі. </span>
				Комитет мүшесі: <span class="corp-04">
				Дудас Джон<br>
				 тәуелсіз директор; </span> <span class="corp-04">
				Мәжібаев Қайрат Қуанышбайұлы<br>
				 тәуелсіз директор; </span> <span class="corp-04">
				Онг Бун Хви<br>
				 тәуелсіз директор. </span>
			</div>
		</div>
	</div>
</div>
 </section> <section class="mt-5">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left section-title text-center">
			Ұйымдастырушылық құрылым </h2>
		</div>
	</div>
</div>
 </section>
<section>
<div class="container">
	<div class="row">
		<div class="col-lg-12 mt-5">
			<div class="organization">
				<div class="organization-structure">
					<div class="organization-base organization-item">
						<div class="organization-base-content">
							<div class="organization-main-col">
								<div class="organization-item-main organization-item-directors">
 <span>Директорлар кеңесі<i class="organization-base-open-dir"></i></span> <i class="organization-item-open-base-dir"></i>
								</div>
								<div class="organization-item-main organization-item-seo" style="margin-bottom:20px;">
 <span>Басқарма төрағасы<i class="organization-base-open-dir"></i></span> <i class="organization-item-open-dir arrow-yelow"></i>
								</div>
							</div><br><br><br>
							<div class="organization-main-col">
								<div class="organization-chain-directors">
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Корпоративтік хатшы қызметі
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Ішкіаудит қызметі
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Омбудсмен
										</div>
									</div>
								</div>
								<div class="organization-chain-seo">
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Басқарма төрағасының кеңесшілері
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Жиынтық-талдау жұмысы және бақылау дирекциясы
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Комплаенс қызметі
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Қауіпсіздікдепартаменті
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">Мемлекеттік құпияларды қорғау және ақпараттық қауіпсіздік департаменті
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="organization-chain">
							<div class="organization-items">
								<div class="organization-item">
									<div class="organization-item-main">
										<br><br><br>Активтерді басқару жөніндегі басқарушы директор <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 «Мұнай-газ» дирекциясы
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Энергетика және тау-кен активтері дирекциясы
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 «Көлік және логистика» дирекциясы
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 «Коммуникациялар» дирекциясы
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												Активтерді қаржы-экономикалық басқару департаменті
											</div>
										</div>  
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										 <br>Инвестициялар, жекешелендіру және халықаралық ынтымақтастық жөніндегі басқарушы директор <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">Активтерді жекешелендіру және қайта құрылымдау департаменті
											</div>
										</div>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Инвестициялар департаменті
											</div>
										</div>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">Халықаралық ынтымақтастық департаменті
											</div>
										</div>
									</div>
									
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										 <br>Стратегия, тұрақтыдаму және цифрлық трансформациялау жөніндегі басқарушы директор <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Теңбасқарушы директор
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">Стратегиялық жоспарлау және нарықты талдау департаменті
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Инновациялар және білімді басқару департаменті
											</div>
										</div>
									<div class="organization-chain-item">
											<div class="organization-chain-content">Цифрлық трансформациялау департаменті
											</div>
									</div>
									<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Тұрақты даму дирекциясы
											</div>
									</div>
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										<br>Корпоративтік басқару және әлеуметтік-еңбек қатынастары жөніндегі басқарушы директор <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Адам ресурстарын басқару департаменті
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Корпоративтік басқару департаменті
											</div>
										</div>
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										<br><br>Құқықтық сүйемелдеу және тәуекелдер жөніндегі басқарушы директор <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Құқықтық сүйемелдеу және әдіснама департаменті
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Тәуекелдерді басқару және ішкі бақылау департаменті
											</div>
										</div>
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										<br><br>Қоғаммен және Үкіметпен байланыс жөніндегі басқарушы директор <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Қоғаммен байланыс департаменті
											</div>
										</div>
									<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Үкіметпен өзара іс-қимыл департаменті
											</div>
										</div>
									<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Сатыпалуды бақылау және мониторинг департаменті
											</div>
										</div>
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										<br><br><br> Экономика және қаржы жөніндегі басқарушы директор <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Экономика және жоспарлау департаменті
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Бухгалтерлік есеп және қаржылық есептілік департаменті
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Корпоративтік қаржы департаменті
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Қазынашылық департаменті
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
 <br>
		</div>
	</div>
</div>
 </section>
 <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p class="t-1 mb-5 mt-5 text-center">
				 <!--
	<b>*Қордың бекітілген штаттық кестесі 193 бірліктен тұрады.</b>
-->
			</p>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="block-line">
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p class="t-1 mb-3 mt-4 text-center">
				 «Самұрық-Қазына» АҚ-ның Корпоративтік басқару кодексі <br>
 <a href="/documents/CG Code_<?=LANGUAGE_ID?>.pdf" target="_blank" class="pdf mt-3"></a>
			</p>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12 mt-3">
			<div class="block-line">
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p class="t-1 mb-5 mt-5 text-center">
				 Корпоративтік басқарудың жаңа кодексі 2015 жылы кең көлемде<br>
				 және жан-жақты талқылаудан кейін қабылданды. <br>
				 2017 жылдың 1 қаңтарынан бастап Қор мен оның <br>
				 компаниялары жылдық есептерінде Кодекс<br>
				 талаптарының сақталғандығы туралы ақпаратты ашып көрсетуі тиіс.
			</p>
		</div>
	</div>
</div>
 </section>