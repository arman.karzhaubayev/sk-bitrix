<?
$APPLICATION->SetTitle("Корпоративное управление");
?><!-- Stock --> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"MODE" => "html",
		"PATH" => SITE_TEMPLATE_PATH."/inc/stock.php"
	)
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"MODE" => "html",
		"PATH" => SITE_TEMPLATE_PATH."/inc/top_menu.php"
	)
);?> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-7">
			<h2 class="h2-left pr-20 pb-30 page-title">
			<?=GetMessage('corporate-governance');?> </h2>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p class="t-1 mb-5 mt-3">
				 Система корпоративного управления АО «Самрук-Қазына» защищает права акционеров и предоставляет инвесторам оперативную и достоверную информацию о прибыли, принципах управления и положении дел в компании, минимизирует и устраняет потенциальные риски нарушения интересов и прав акционеров.
			</p>
		</div>
	</div>
</div>
 </section>
<section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left pr-20 pb-30 section-title text-center mt-3 t-up"> <span class="fw-4">В части корпоративного управления</span><br>
			 Фонд реализует 6 проектов: </h2>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			 <!-- Swiper -->
			<div class="swiper-container swiper-container-corp">
				<div class="swiper-wrapper">
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-001.png"><br>
								 <b>Новый Кодекс</b><br><br>
								Впервые в Казахстане Фонд при содействии Организации экономического сотрудничества и развития (ОЭСР) разработал новый кодекс корпоративного управления, который соответствует лучшим международным стандартам
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-002.png"><br>
								 <b>Новая методика</b><br><br> С внедрением нового Кодекса корпоративного управления перед портфельными компаниями Фонда появляются новые задачи
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-001.png"><br>
								 <b>Эффективность</b><br><br> В результате трансформации Фонд должен превратиться из операционного холдинга в стратегический холдинг
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-002.png"><br>
								 <b>Устойчивое развитие</b><br><br> <!--
АО «Самрук-Казына» — это Фонд национального благосостояния. 
Поэтому группа Фонда должна ответственно вести свой бизнес и преумножить ценность 
портфеля как для текущих, так и будущих поколений Казахстана. 
-->  Устойчивое развитие – это согласованность наших экономических, экологических и социальных целей, соблюдая баланс интересов заинтересованных сторон для устойчивого увеличения стоимости группы Фонда в долгосрочной перспективе
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-001.png"><br>
								 <b>Прозрачность</b><br><br> Прозрачность – важное направление для инвесторов и стейкхолдеров. Внедрение культуры ответственного отношения к информации – необходимый этап перед выходом на IPO
							</div>
						</div>
					</div>
					<div class="swiper-slide swiper-slide-corp">
						<div class="corp-slider">
							<div class="corp-slider-item text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-002.png"><br>
								 <b>Отчеты</b><br><br> Отчеты Качество годовых отчетов напрямую влияет на прозрачность, поэтому контент и подача информации в них были пересмотрены. Следующим шагом станет подготовка отчетов в соответствии со стандартом GRI
							</div>
						</div>
					</div>
				</div>
				 <!-- Add Arrows -->
				<div class="swiper-corp-button-next">
				</div>
				<div class="swiper-corp-button-prev">
				</div>
			</div>
			 <!-- Initialize Swiper --> <script>
    var swiper = new Swiper('.swiper-container-corp', {
      slidesPerView: 3,
      spaceBetween: 0,
      slidesPerGroup: 1,
      /*loop: true,
		loopFillGroupWithBlank: true,*/
      navigation: {
        nextEl: '.swiper-corp-button-next',
        prevEl: '.swiper-corp-button-prev',
      },
    });
  </script>
		</div>
	</div>
</div>
 </section> <section>
<div class="container mt-5">
	<div class="row">
		<div class="col-lg-12 corp-01 mt-5">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 text-left">
 <img src="/bitrix/templates/skfinance/img/img-corp-003.png"> <span class="corp-01-01">
						Органы управления АО «Самрук-Қазына» </span>
						Система корпоративного управления АО «Самрук-Қазына» включает в себя управление, контроль и ответственность органов управления в целом по Группе компаний Фонда от первого до последнего уровня:
					</div>
					<div class="col-lg-2">
					</div>
					<div class="col-lg-6">
						<div class="corp-01-02">
 <br>
 <br>
 <br>
 <br>
							 Единственным акционером АО «Самрук-Қазына» является Правительство Республики Казахстан, в полномочия которого входит утверждение Стратегии развития Фонда, назначение директоров и Председателя Правления, утверждение финансовых результатов и получение дивидендов;
							<div class="block-line-04">
							</div>
							 Общее руководство деятельностью компании осуществляет Совет директоров, который делегирует решение задач текущей деятельности компании Правлению во главе с его Председателем.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <section>
<div class="container mt-5">
	<div class="row">
		<div class="col-lg-12">
			<div class="corp-02">
				<h2 class="h2-left pr-10 pb-10 section-title text-center mt-3">
				Совет по управлению фондом </h2>
				<p class="t-1 mt-3 text-justify">
					 Совет по управлению АО «Самрук-Қазына» является консультативно-совещательным органом, возглавляемым Первым Президентом Республики Казахстан - Елбасы. Основной задачей Совета является выработка предложений по повышению конкурентоспособности и эффективности деятельности АО «Самрук-Қазына».
				</p>
				<div class="container corp-02-01 mt-5">
					<div class="row">
						<div class="col-lg-9">
 <img src="/bitrix/templates/skfinance/img/img-corp-005.png">
						</div>
						<div class="col-lg-3">
 <br>
							 Нурсултан Абишевич Назарбаев
							<p>
 <br>
								 Первый Президент Республики Казахстан - Елбасы, председатель Совета по управлению Фондом
							</p>
						</div>
					</div>
				</div>
				<h2 class="h2-left pr-10 pb-10 section-title text-center mt-3 fs-24">
				Члены Совета </h2>
				<div class="container corp-02-02 mt-5">
					<div class="row">
						<div class="col-lg-4">
 <img src="/bitrix/templates/skfinance/img/img-corp-006.png">
						</div>
						<div class="col-lg-2">
							 Аскар Узакпаевич Мамин
							<p>
 <br>
 <br>
								 Премьер-Министр Республики Казахстан
							</p>
						</div>
						<div class="col-lg-4">
 <img src="/bitrix/templates/skfinance/img/img-corp-008.png">
						</div>
						<div class="col-lg-2">
							 Ерлан Жаканович Кошанов
							<p>
 <br>
 <br>
								 Руководитель Администрации Президента Республики Казахстан
							</p>
						</div>
					</div>
				</div>
				<div class="container corp-02-02 mt-5">
					<div class="row">
						<div class="col-lg-3">
						</div>
						<div class="col-lg-4">
 <img alt="Satkaliyev.jpg" src="/upload/medialibrary/f21/f21c1900232808f4e4e982ceeb4c1017.jpg" title="Satkaliyev.jpg">
						</div>
						<div class="col-lg-3">
							 Саткалиев Алмасадам Маиданович
							<p style="text-align: left; padding-right: 18px;">
 <br>
 <br>
								 Председатель Правления АО «Самрук-Қазына» 
							</p>
						</div>
						<div class="col-lg-6">
							 <!--
                <div class="corp-02-03">
                  Два представителя отечественного бизнеса
                </div>

                <div class="corp-02-03">Представитель иностранного бизнеса</div>
-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left mt-5 pr-20 pb-30 section-title text-center mt-3">
			Совет директоров </h2>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="corp-02 corp-03">
				<div class="container">
					 <?/*$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/corp-1_".LANGUAGE_ID.".php",
        'MODE' => 'html'
    ),
    false
);*/?>
					<div class="row">
						<div class="col-lg-12">
							<div class="owl-carousel owl-theme" id="corp-1">
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/john.jpg">
										<p>
											 Джон Дудас <br>
 <br>
											 Председатель Совета директоров, Независимый директор АО «Самрук-Қазына»
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/mazhibaeyev.jpg">
										<p>
											 Мажибаев Кайрат Куанышбаевич <br>
 <br>
											 Член Совета директоров, Независимый директор АО «Самрук-Қазына»
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/irgaliev.jpg">
										<p>
											 Иргалиев Асет Арманович <br>
 <br>
											 Член Совета директоров АО «Самрук-Қазына»
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/ong.jpg">
										<p>
											 Онг Бун Хви <br>
 <br>
											 Член Совета директоров, Независимый директор АО «Самрук-Қазына»
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/bozumbayev.jpg">
										<p>
											 Бозумбаев Канат Алдабергенович <br>
 <br>
											 Член Совета директоров АО «Самрук-Қазына»<br>
											 Помощник Президента Республики Казахстан
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img alt="Satkaliyev.jpg" src="/upload/medialibrary/f21/f21c1900232808f4e4e982ceeb4c1017.jpg" title="Satkaliyev.jpg">
										<p>
											 Саткалиев Алмасадам<br>
											 Маиданович
										</p>
										<p>
											 Член Совета директоров, Председатель Правления АО «Самрук-Қазына»
										</p>
									</div>
								</div>
								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/luka.jpg">
										<p>
											 Лука Сутера <br>
 <br>
										</p>
										<p>
											 Независимый директор АО «Самрук-Қазына»,<br>
											 Член Совета директоров
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					 <script>
	$('#corp-1').owlCarousel({
		items:2,
    nav:false,
    dots:true,
    margin:10,
    loop:false

    
})


	 </script>
				</div>
				<h2 class="h2-left pr-10 pb-10 section-title text-center mt-5 fs-24">
				Правление </h2>
				<div class="container mt-5">
					 <?/*$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/corp-2_".LANGUAGE_ID.".php",
        'MODE' => 'html'
    ),
    false
);*/?>
					<div class="row">
						<div class="col-lg-12">
							<div class="owl-carousel owl-theme" id="corp-2">
								<div class="item">
									<div class="person">
 <img alt="Satkaliyev.jpg" src="/upload/medialibrary/f21/f21c1900232808f4e4e982ceeb4c1017.jpg" title="Satkaliyev.jpg">
										<p>
											 Саткалиев Алмасадам Маиданович<br>
 <br>
											 Председатель Правления АО «Самрук-Қазына»
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 										<img src="/bitrix/templates/skfinance/img/governance/nurbaeva.jpg">
										<p>
											 Нурбаева Назира Нуртулеуовна  <br>
 											<br>
											 Управляющий директор по экономике и финансам
										</p>
									</div>
								</div>


								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/zhanadil.jpg">
										<p>
											 Жанадил Ернар Бейсенулы <br>
 <br>
											 Управляющий директор по инвестициям, приватизации и международному сотрудничеству
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/kravchenko.jpg">
										<p>
											 Кравченко Андрей Николаевич <br>
 <br>
											 Управляющий директор по правовому сопровождению и рискам
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 									<img src="/upload/iblock/9f9/9f9b15834bca4d00a0cf7a53c0e3b173.jpg">
										<p>
											 Айтенов Марат Дуйсенбекович  <br>
 <br>
											 Управляющий директор по связям с общественностью и Правительством 
										</p>
									</div>
								</div>


							</div>
						</div>
					</div>
					 <script>
	$('#corp-2').owlCarousel({
		items:2,
    nav:false,
    dots:true,
    margin:10,
    loop:false

    
})


	 </script>
				</div>
			</div>
		</div>
	</div>
</div>
 </section>
<!-- Management --> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left mt-5 pr-20 pb-30 section-title text-center mt-3">
			Руководство </h2>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="corp-02 corp-03">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="owl-carousel owl-theme" id="corp-5">
								<div class="item">
									<div class="person">
 <img alt="Satkaliyev.jpg" src="/upload/medialibrary/f21/f21c1900232808f4e4e982ceeb4c1017.jpg" title="Satkaliyev.jpg">
										<p>
											 Саткалиев Алмасадам Маиданович<br>
 <br>
											 Председатель Правления АО «Самрук-Қазына»
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 <img alt="akuchalakov.jpg" src="/bitrix/templates/skfinance/img/governance/akuchalakov.jpg" >
										<p>
											 Акчулаков Болат Уралович <br>
 <br>
											 Управляющий директор по управлению активами 
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/zhanadil.jpg">
										<p>
											 Жанадил Ернар Бейсенулы <br>
 <br>
											 Управляющий директор по инвестициям, приватизации и международному сотрудничеству
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/durmagambetov.jpg">
										<p>
											 Дурмагамбетов Ерлан Дмитриевич <br>
 <br>
											 Управляющий директор по стратегии, устойчивому развитию и цифровой трансформации 
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/tajigaliev.jpg">
										<p>
											 Тажигалиев Мухтар Утепкалиевич  <br>
 <br>
											 Управляющий директор по корпоративному управлению и социально-трудовым отношениям
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 <img src="/bitrix/templates/skfinance/img/governance/kravchenko.jpg">
										<p>
											 Кравченко Андрей Николаевич <br>
 <br>
											 Управляющий директор по правовому сопровождению и рискам
										</p>
									</div>
								</div>


								<div class="item">
									<div class="person">
 									<img src="/upload/iblock/9f9/9f9b15834bca4d00a0cf7a53c0e3b173.jpg">
										<p>
											 Айтенов Марат Дуйсенбекович <br>
 <br>
											 Управляющий директор по связям с общественностью и Правительством
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 										<img src="/bitrix/templates/skfinance/img/governance/nurbaeva.jpg">
										<p>
											 Нурбаева Назира Нуртулеуовна  <br>
 											<br>
											 Управляющий директор по экономике и финансам
										</p>
									</div>
								</div>

								<div class="item">
									<div class="person">
 										<img src="/local/templates/default_template/assets/img/pages/management/sovet4/person-8.jpg">
										<p>
											 Бердигулов Ернат Кудайбергенович  <br>
 											<br>
											 Со-управляющий директор по стратегии, устойчивому развитию и цифровой трансформации
										</p>
									</div>
								</div>

							</div>
						</div>
					</div>
					 <script>
	$('#corp-5').owlCarousel({
		items:2,
    nav:false,
    dots:true,
    margin:10,
    loop:false

    
})
	 </script>
				</div>
			</div>
		</div>
	</div>
</div>
 </section>
<!-- /Management --> <section class="mt-5">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left section-title text-center">
			Комитеты Совета директоров </h2>
		</div>
	</div>
</div>
 </section> <section class="about-06 mt-5 mb-1">
<div class="container mb-5">
	<div class="row">
		<div class="col-lg-6">
			<div class="about-06-01">
				 Комитет по аудиту <a href="" class="block-down" data-id="0"></a>
			</div>
			<div class="block-down-box" data-id="0">
				 Председатель Комитета: <span class="corp-04">
				Сутера Лука<br>
				 независимый директор. </span>
				Члены Комитета: <span class="corp-04">
				Онг Бун Хви<br>
				 независимый директор. </span>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="about-06-01_1" style="width: 100%;
    height: 134px;
    border-radius: 5px;
    background-color: #08706a;
    font-size: 24px;
    color: #ffffff;
    font-weight: 700;
    font-family: 'Playfair Display';
    display: inline-block;
    line-height: 50px;
    padding: 10px;text-align: center;"><a href="" class="block-down" data-id="1"></a>
				 Комитет по назначениям и вознаграждениям 
			</div>
			<div class="block-down-box" data-id="1">
				 Председатель Комитета: <span class="corp-04">
				Дудас Джон<br>
				 независимый директор. </span>
				Члены Комитета: <span class="corp-04">
				Бозумбаев Канат Алдабергенович<br>
				 Помощник Президента Республики Казахстан;<br>
 </span> <span class="corp-04">
				Мажибаев Кайрат Куанышбаевич<br>
				 независимый директор;<br>
 </span> <span class="corp-04">
				Сутера Лука<br>
				 независимый директор.<br>
 </span>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-6">
			<div class="about-06-01">
				 Специализированный комитет <a href="" class="block-down" data-id="2"></a>
			</div>
			<div class="block-down-box" data-id="2">
				 Председатель Комитета: <span class="corp-04">
				Султанов Бахыт Турлыханович </span>
				Члены Комитета: <span class="corp-04">
				Бергенев Адылгазы Садвокасович<br>
				 Жанадил Ернар Бейсенулы<br>
				 Тулешов Габдулкамит Маденович<br>
				 Киякбаева Ардак Борановна<br>
				 Бушмухамбетова Айнур Сансизбаевна<br>
				 Карымсаков Бейбит Еркинбаевич<br>
				 Исаев Бахтияр Орынбасарович<br>
 </span>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="about-06-01_1" style="width: 100%;
    height: 134px;
    border-radius: 5px;
    background-color: #08706a;
    font-size: 24px;
    color: #ffffff;
    font-weight: 700;
    font-family: 'Playfair Display';
    display: inline-block;
    line-height: 50px;
    padding: 10px;text-align: center;"><a href="" class="block-down" data-id="3"></a>
				 Комитет по контролю за реализацией Программы трансформации 
			</div>
			<div class="block-down-box" data-id="3">
				 Председатель Комитета: <span class="corp-04">
				Дудас Джон<br>
				 независимый директор. </span>
				Члены Комитета: <span class="corp-04">
				Бозумбаев Канат Алдабергенович<br>
				 Помощник Президента Республики Казахстан; </span> <span class="corp-04">
				Онг Бун Хви<br>
				 независимый директор. </span>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-3 mt-5">
		</div>
		<div class="col-lg-6 mt-5">
			<div class="about-06-01">
				 Комитет по стратегии <a href="" class="block-down" data-id="5"></a>
			</div>
			<div class="block-down-box" data-id="5">
				 Председатель Комитета: <span class="corp-04">
				Иргалиев Асет Арманович<br>
				 Министр национальной экономики Республики Казахстан. </span>
				Члены Комитета: <span class="corp-04">
				Дудас Джон<br>
				 независимый директор; </span> <span class="corp-04">
				Мажибаев Кайрат Куанышбаевич<br>
				 независимый директор; </span> <span class="corp-04">
				Онг Бун Хви<br>
				 независимый директор. </span>
			</div>
		</div>
	</div>
</div>
 </section> <section class="mt-5">
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="h2-left section-title text-center">
			Оргструктура </h2>
		</div>
	</div>
</div>
 </section>
<section>
<div class="container">
	<div class="row">
		<div class="col-lg-12 mt-5">
			<div class="organization">
				<div class="organization-structure">
					<div class="organization-base organization-item">
						<div class="organization-base-content">
							<div class="organization-main-col">
								<div class="organization-item-main organization-item-directors">
 <span>Совет директоров<i class="organization-base-open-dir"></i></span> <i class="organization-item-open-base-dir"></i>
								</div>
								<div class="organization-item-main organization-item-seo" style="margin-bottom:20px;">
 <span>Председатель Правления<i class="organization-base-open-dir"></i></span> <i class="organization-item-open-dir arrow-yelow"></i>
								</div>
							</div><br><br><br>
							<div class="organization-main-col">
								<div class="organization-chain-directors">
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Служба корпоративного секретаря
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Служба внутреннего аудита
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Омбудсмен
										</div>
									</div>
								</div>
								<div class="organization-chain-seo">
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Советники Председателя Правления
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Дирекция по сводно-аналитической работе и контролю
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Служба комплаенс
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Департамент безопасности
										</div>
									</div>
									<div class="organization-chain-item">
										<div class="organization-chain-content">
											 Департамент по защите госсекретов и информационной безопасности
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="organization-chain">
							<div class="organization-items">
								<div class="organization-item">
									<div class="organization-item-main">
										<br><br><br>Управляющий директор по управлению активами <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Дирекция «Нефтегаз»
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Дирекция энергетических и горнорудных активов
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Дирекция «Транспорт и логистика»
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Дирекция «Коммуникации»
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												Департамент финансово-экономического управления активами
											</div>
										</div>  
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										 <br>Управляющий директор по инвестициям, приватизации и международному сотрудничеству <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент по приватизации и реструктуризации активов
											</div>
										</div>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент по инвестициям
											</div>
										</div>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент международного сотрудничества
											</div>
										</div>
									</div>
									
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										 <br>Управляющий директор по стратегии, устойчивому развитию и цифровой трансформации <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Со-управляющий директор
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент стратегического планирования и анализа рынков
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент инноваций и управления знаниями
											</div>
										</div>
									<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент цифровой трансформации
											</div>
									</div>
									<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Дирекция устойчивого развития
											</div>
									</div>
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										<br>Управляющий директор по корпоративному управлению и социально-трудовым отношениям <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент управления человеческими ресурсами
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент корпоративного управления
											</div>
										</div>
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										<br><br>Управляющий директор по правовому сопровождению и рискам <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент правового сопровождения и методологии
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент управления рисками и внутреннего контроля
											</div>
										</div>
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										<br><br>Управляющий директор по связям с общественностью и Правительством <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент по связям с общественностью
											</div>
										</div>
									<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент по взаимодействию с Правительством
											</div>
										</div>
									<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент контроля и мониторинга закупок
											</div>
										</div>
									</div>
								</div>
								<div class="organization-item">
									<div class="organization-item-main">
										<br><br><br> Управляющий директор по экономике и финансам <i class="organization-item-open"></i>
									</div>
									<div class="organization-chain">
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент экономики и планирования
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент бухгалтерского учета и финансовой отчетности
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент корпоративных финансов
											</div>
										</div>
										<div class="organization-chain-item">
											<div class="organization-chain-content">
												 Департамент казначейства
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
 <br>
		</div>
	</div>
</div>
 </section> <section>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p class="t-1 mb-5 mt-5 text-center">
				 <!--
	<b>*Утвержденное штатное расписание Фонда составляет 193 единицы.</b>
-->
			</p>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="block-line">
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p class="t-1 mb-3 mt-4 text-center">
				 Кодекс корпоративного управления<br>
				 АО «Самрук-Қазына» <br>
 <a href="/documents/CG Code_<?=LANGUAGE_ID?>.pdf" target="_blank" class="pdf mt-3"></a>
			</p>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12 mt-3">
			<div class="block-line">
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<p class="t-1 mb-5 mt-5 text-center">
 <b>Новый Кодекс корпоративного управления</b>
				был принят в 2015<br>
				 году после широкого и всестороннего обсуждения. <b>С 1 января<br>
				 2017</b> года Фонд и Компании должны раскрывать в годовых<br>
				 отчетах информацию о соблюдении Кодекса.
			</p>
		</div>
	</div>
</div>
 </section>