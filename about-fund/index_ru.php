<?$APPLICATION->SetTitle("О Фонде");?>	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('about-fund')?>
            </h2>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<p class="t-1 mb-3 mt-3">
Акционерное общество «Фонд национального благосостояния «Самрук-Қазына» основано в 2008 году Указом Президента Республики Казахстан. Единственным акционером Фонда является Правительство Республики Казахстан.
			  </p>
<p class="t-1 mb-5">
Фонд представляет собой коммерческую структуру — инвестиционный холдинг, миссия которого заключается в повышении национального благосостояния Республики Казахстан и обеспечении долгосрочной устойчивости для будущих поколений.
<!--	<?=GetMessage('main_1')?>-->
<!--<b>Акционерное общество «Фонд национального благосостояния «Самрук-Қазына»</b> — Фонд, единственным акционером которого является Правительство Республики Казахстан.-->
<!--<?=GetMessage('main_2')?>-->
	<!--<b>Фонд был основан в 2008 году Указом Президента Республики Казахстан</b> и представляет собой коммерческую структуру — инвестиционный холдинг, миссия которого заключается в повышении национального благосостояния Республики Казахстан и обеспечении долгосрочной устойчивости для будущих поколений.-->
</p>

    <!-- History -->
<? 
if(intval($_REQUEST['year'])==0)
	$_REQUEST['year'] = 2012;
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "aboutfund.history",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "N",
        "CHECK_DATES" => "Y",
        "COMPONENT_TEMPLATE" => "aboutfund.history",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "Y",
        "FIELD_CODE" => array(0 => "ID", 1 => "CODE", 2 => "XML_ID", 3 => "NAME", 4 => "SORT", 5 => "PREVIEW_TEXT", 6 => "PREVIEW_PICTURE", 7 => "",),
        "FILE_404" => "",
        "FILTER_NAME" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => constant("HISTORY_SLIDER_IBLOCK_" . LANGUAGE_ID),
        "IBLOCK_TYPE" => "sliders",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "100",
        "PAGER_BASE_LINK" => "",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "PAGER_DESC_NUMBERING" => "Y",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_PARAMS_NAME" => "arrPager",
        "PAGER_SHOW_ALL" => "Y",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(0 => "", 1 => "BUTTON", 2 => "BUTTON_LINK", 3 => "",),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "SORT",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER1" => "ASC",
        "SORT_ORDER2" => "DESC"
    )
); ?>

<!--
	<section class="about-01 mt-5 mb-5">
      <div class="container">
        <div class="row">

          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-5 mb-3">
				Цели создания Фонда
            </h2>
          </div>
        </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-4 about-01-block">
			  <span class="mr-3">1</span>
				<p>Содействие в модернизации
				и диверсификации
					национальной экономики</p>
          </div>
          <div class="col-lg-4 about-01-block">
			  <span class="mr-3">2</span>
				<p>Участие в стабилизации
					экономики страны</p>
          </div>
          <div class="col-lg-4 about-01-block">
			  <span class="mr-3">3</span>
				<p>Повышение<br>
эффективности
					деятельности компании</p>
          </div>
        </div>
		</div>
      </div>
    </section>
	<section class="about-02 mt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-3">
				Задачи Фонда
            </h2>
			<p class="text-center">Сразу после создания в 2008 году Фонд стал одним из катализаторов восстановления экономики
					<br>Казахстана после кризиса. В эти годы он решал несколько задач:
			</p>
          </div>
         </div>
        </div>
      <div class="container mt-3">
        <div class="row">
          <div class="col-lg-6">
				<div class="block-01">
					Стабилизация финансового сектора<br>
					через докапитализацию коммерческих банков
				</div>
				<div class="block-01">
					Решение проблем на рынке<br>
					долевого строительства
				</div>
          </div>
          <div class="col-lg-6">
				<div class="block-01">
					Рефинансирование дорогих<br>
ипотечных займов

				</div>
				<div class="block-01">
					Кредитование МСБ и реализация<br>
стратегических инвестиционных проектов
				</div>
          </div>
         </div>
        </div>

    </section>
-->
	<section class="about-03">
      <div class="container mt-3">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-3">
				Стратегия развития АО «ФНБ «Самрук-Қазына»
            </h2>
          </div>
         </div>
        </div>
      <div class="container block-02">
        <div class="row">
          <div class="col-lg-4">

<p class="block-04">
	<b>Миссия АО «Самрук-Қазына»</b> 
заключается в повышении национального благосостояния Республики Казахстан и обеспечении долгосрочной устойчивости для будущих поколений.
			  </p>

          </div>
          <div class="col-lg-8">

		      <div class="container">
        			<div class="row">
          				<div class="col-lg-12">

				<p class="block-05">
Стратегия развития «Самрук- Қазына» до 2028 года одобрена Советом по управлению Фондом и утверждена Единственным Акционером в 2018 году.

			  </p>
						</div>
<!--
          				<div class="col-lg-6">
<div class="block-yellow" style="    min-height: 200px;
    margin-top: 90px;
    font-size: 22px;
    line-height: 28px;">
Стратегия развития «Самрук- Қазына» до 2028 года одобрена Советом по управлению Фондом и утверждена Единственным Акционером в 2018 году.
							</div>
-->
<!-- Swiper -->
<!--
  <div class="swiper-container swiper-container-about">
    <div class="swiper-wrapper">
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-01">
						<span>Нефтегазового сектора</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-02">
						<span>Транспортно-логистического секторов</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-03">
						<span>Химической промышленности</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-04">
						<span>Атомной промышленности</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-05">
						<span>Горно-металлургического комплекса</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-06">
						<span>Энергетики</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-07">
						<span>Машиностроения </span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-08">
						<span>Недвижимости</span>
					</div>
          		</div>
		</div>
    </div>
    <div class="swiper-about-button-next"></div>
    <div class="swiper-about-button-prev"></div>
  </div>


  <script>
    var swiper = new Swiper('.swiper-container-about', {
      navigation: {
        nextEl: '.swiper-about-button-next',
        prevEl: '.swiper-about-button-prev',
      },
    });
  </script>

-->





						<!--</div>-->
					</div>
				</div>

          </div>
         </div>
        </div>

    </section>
<!--
	<section class="about-04 mb-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-3">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-3">
				Стратегические цели Фонда и видение 
            </h2>
          </div>
         </div>
        </div>
    </section>
	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<p class="t-1 text-center">
	<b>1. повышение эффективности через повышение рентабельности портфельных компаний;</b>
</p>
<p class="t-1 mt-3 text-center">
	<b>2. эффективное управление  портфелем через оптимизацию структуры портфеля и диверсификацию доходов;</b><br>
</p>
<p class="t-1 mt-3 text-center">
	<b>3. устойчивое развитие посредством социальной стабильности и эффективного корпоративного управления.</b><br>
</p>



          </div>
         </div>
        </div>
    </section>
-->
<!--
	<section class="about-05 mb-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
				<div class="about-05-01">
					<div class="img-01"></div>
					<span>Миссия</span>
Повышение национального благосостояния Республики
Казахстан и обеспечение
долгосрочной устойчивости
для будущих поколений
				</div>
          </div>
          <div class="col-lg-6">
				<div class="about-05-01">
					<div class="img-02"></div>
					<span>Видение</span>
Инвестиционный холдинг, обеспечивающий высокие финансовые показатели, доходы от инвестиций и развивающий портфель на уровне ведущих суверенных фондов благосостояния.
				</div>

          </div>
         </div>
        </div>
      <div class="container mt-4">
        <div class="row">
          <div class="col-lg-12">
				<div class="about-05-01">
					<div class="img-03"><span>Стратегическая Цель №1</span></div>

Одной из ключевых целей и мандатов Фонда является эффективное управление портфельными компаниями за счет выравнивания показателей эффективности с показателями ведущих мировых компаний аналогов в части операционной, производственной, финансовой эффективности, коммерческой деятельности и повышения доходности от вложенного капитала. Укрепление финансовой устойчивости, повышение операционной эффективности, повышение эффективности использования производственных активов, реализация 
Программы Трансформации станут первоочередными задачами для реализации данной стратегической цели.
				</div>
          </div>
          </div>
         </div>

    </section>
-->


	<section class="mb-1 mt-3">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-3">
				Стратегические цели Фонда
            </h2>
          </div>
         </div>
        </div>
    </section>

	<section class="about-06 mb-1">
      <div class="container mb-5">
        <div class="row">
          <div class="col-lg-6">
				<div class="about-06-01">
						<span>Повышение рентабельности
							компаний</span>
					<a href="" class="block-down" data-id="0"></a>
				</div>
				<div class="block-down-box" data-id="0">
Повышение рентабельности портфельных компаний является одной из основных задач Фонда. Планируется обеспечить ряд мер по укреплению финансовой устойчивости и повышению операционной эффективности компаний, оптимизации и реинжинирингу бизнес-процессов, а также обеспечению синергии между портфельными компаниями Фонда и повышению их инвестиционной привлекательности
				</div>
          </div>
          <div class="col-lg-6">
				<div class="about-06-01">
<span>Оптимизация структуры портфеля</span>
					<a href="" class="block-down" data-id="1"></a>
				</div>
				<div class="block-down-box" data-id="1">
Оптимизация структуры портфеля является важным шагом для перехода Фонда к активному управлению портфелем инвестиций. Реструктуризация текущего портфеля активов Фонда, пересмотр инвестиционных проектов, сокращение количества юридических лиц и уровней управления, а также выход Фонда из нестратегических активов позволят Фонду эффективно перераспределить свои ресурсы в целях повышения стоимости портфеля и увеличения его доходности.
				</div>

          </div>
         </div>
        </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
				<div class="about-06-01">
						<span>Диверсификация доходов
</span>
					<a href="" class="block-down" data-id="2"></a>
				</div>
				<div class="block-down-box" data-id="2">

				 Активное управление активами позволит Фонду повысить доходность портфеля и обеспечить его диверсификацию в различные отрасли, отдавая приоритет активам/проектам с потенциальным мультипликативным эффектом для экономики Казахстана и с большой доходностью.
				<p>
<br> <b>Инвестиции в Республике Казахстан</b>
				</p>
				 Фонд осуществляет инвестиции в пределах страны с целью развития различных секторов экономики Казахстана и проектов стратегического значения, (в соответствии с ключевыми государственными программами) принимая во внимание ограничения, установленные принципами "желтых страниц" и законодательными актами Республики Казахстан, и не препятствующих развитию частного сектора.
				<p>
 <br><b>Инвестиции за пределами Республики Казахстан</b>
				</p>
				В среднесрочной и долгосрочной перспективе Стратегия развития предусматривает распределение инвестиций за пределами Республики Казахстан, не связанных с национальной экономикой и промышленностью, с целью повышения доходности, сохранения благосостояния посредством диверсификации. Фонд будет создавать партнерские отношения с ведущими суверенными фондами, а также другими инвестиционными компаниями и фондами для осуществления совместных прямых инвестиций, которые в конечном итоге будут иметь стратегическую привязку к отраслям промышленности Казахстана.

				</div>

          </div>
          <div class="col-lg-6">
				<div class="about-06-01">
<span>Корпоративное управление и
устойчивое развитие
</span>
					<a href="" class="block-down" data-id="3"></a>
				</div>
				<div class="block-down-box" data-id="3">

				 Инициативы по устойчивому развитию, корпоративное управление и корпоративная культура являются основой устойчивого развития и критическими факторами, влияющими на стоимость портфеля.
				<p>
 <br><b>Корпоративное управление</b>
				</p>
				 В целях обеспечения эффективного управления активами, повышения инвестиционной привлекательности и успешного выполнения функции активного инвестора Фонду необходимо продолжить внедрение в Фонде и в портфельных компаниях передового опыта корпоративного управления в соответствии с рекомендациями Организации экономического сотрудничества и развития (ОЭСР) по корпоративному управлению.
				<p>
 <br><b>Развитие человеческого капитала</b>
				</p>
				 Использование лучших практик по развитию персонала в Фонде и портфельных компаниях за счет формирования эффективной корпоративной культуры,
внедрения основополагающих принципов меритократии, трансформации людей и внедрения новых моделей поведения для «коммерческого» мышления и готовности к изменениям.
				<p>
 <br><b>Спонсорская деятельность и социальная ответственность</b>
				</p>
				 Фонд продолжит свою деятельность в части оказания спонсорской и благотворительной деятельности. Распределение помощи должно быть пересмотрено и приоритезировано исходя из потребностей общества. Опираясь на лучшие практики, Фонд будет финансировать социальные проекты, направленные на здравоохранение, образование, профессиональное развитие молодежи, развитие сообщества, окружающей среды, искусства и наследия культуры и знаний, исследования, инновации, науку и технологии, на поддержку обездоленных лиц и семей.
				<p>
 <br><b>Инициативы устойчивого развития</b>
				</p>
				 В целях эффективного и успешного управления экономическими, экологическими и социальными аспектами деятельности, Фонд будет внедрять высокие этические стандарты и строить корпоративную культуру, основанную на доверии, внедрять передовые стандарты и совершенствовать корпоративное управление, инвестировать в человеческий капитал и предпринимать все меры по обеспечению экологической стабильности.
				</div>

          </div>
         </div>
        </div>
    </section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5 text-center desc ">
<p class="t-1 mb-3 mt-3">

Согласно Стратегическому плану развития Республики Казахстан до 2025 года Фонд намерен трансформироваться от роли исполнителя государственных инициатив и программ к роли инвестиционной компании. Данная цель подразумевает наряду с управлением деятельностью текущих портфельных компаний, активное управление портфелем инвестиций Фонда, обеспечивающего доходы от инвестиций и развивающий портфель на уровне ведущих суверенных фондов благосостояния. Фонд намерен продолжить активно управлять и улучшать качество своего портфеля посредством реализации программы приватизации, собственной программы дивестиции и реинвестиций средств и дивидендов, новых инвестиций в Казахстане и за ее пределами.

			  </p>
          </div>
         </div>
        </div>
    </section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-3 mb-3 text-center">
			<div class="block-line mt-5 mb-3"></div>
				<p class="t-1 mb-3 mt-4 text-center">Новая стратегия Фонда на 2018-2028 гг.
			  		<br><a target="_blank" href="/documents/новая стратегия Фонда на 2018-2028 гг..doc" class="pdf mt-3"></a>
				</p>
          </div>
         </div>
        </div>
    </section>
<!--
	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<p class="t-1 mb-5 mt-5 text-center">
	<b>Новый Кодекс</b> корпоративного управления, принятый в 2015 году,<br>
	будет способствовать становлению <b>Фонда стратегическим холдингом</b>,<br>
управляющим своими компаниями<br>
через эффективные <b>Советы директоров</b>
			  </p>
          </div>
         </div>
        </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
			  <div class="block-line"></div>
          </div>
         </div>
        </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
			  <p class="t-1 mb-3 mt-4 text-center">Кодекс корпоративного управления<br>
АО «Самрук-Қазына»
			  <br><a href="" class="pdf mt-3"></a>
</p>

          </div>
         </div>
        </div>
    </section>
-->
<!--
	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center mt-5 mb-5">
			  <a href="/about-fund/corporate-governance/" class="btn-read-more">Узнать больше</a>
          </div>
         </div>
        </div>
    </section>
-->
<!--
	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center mt-5 mb-1">
			  <div class="block-line-full"></div>
          </div>
         </div>
        </div>
    </section>
-->

			</div>
</div>
</div>
</section>