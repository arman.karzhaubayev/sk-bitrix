<?$APPLICATION->SetTitle("About SK");?>	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('about-fund')?>
            </h2>
          </div>
        </div>
      </div>
</section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<p class="t-1 mb-3 mt-3">
Sovereign Welfare Fund «Samruk-Kazyna» Joint Stock Company was established in 2008 by the Decree of the President of Republic of Kazakhstan. The sole shareholder of the Fund is the Government of the Republic of Kazakhstan.
			  </p>
<p class="t-1 mb-5">

The fund is a business corporation - an investment holding, whose mission is to improve the national welfare of the Republic of Kazakhstan and ensuring long-term sustainability for future generations.



<!--	<?=GetMessage('main_1')?>-->
<!--<b>Акционерное общество «Фонд национального благосостояния «Самрук-Қазына»</b> — Фонд, единственным акционером которого является Правительство Республики Казахстан.-->
<!--<?=GetMessage('main_2')?>-->
	<!--<b>Фонд был основан в 2008 году Указом Президента Республики Казахстан</b> и представляет собой коммерческую структуру — инвестиционный холдинг, миссия которого заключается в повышении национального благосостояния Республики Казахстан и обеспечении долгосрочной устойчивости для будущих поколений.-->
</p>
    <!-- History -->
<? 
if(intval($_REQUEST['year'])==0)
	$_REQUEST['year'] = 2012;
$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "aboutfund.history",
    Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPONENT_TEMPLATE" => "aboutfund.history",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "Y",
        "FIELD_CODE" => array(0 => "ID", 1 => "CODE", 2 => "XML_ID", 3 => "NAME", 4 => "SORT", 5 => "PREVIEW_TEXT", 6 => "PREVIEW_PICTURE", 7 => "",),
        "FILE_404" => "",
        "FILTER_NAME" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => constant("HISTORY_SLIDER_IBLOCK_" . LANGUAGE_ID),
        "IBLOCK_TYPE" => "sliders",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "100",
        "PAGER_BASE_LINK" => "",
        "PAGER_BASE_LINK_ENABLE" => "Y",
        "PAGER_DESC_NUMBERING" => "Y",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_PARAMS_NAME" => "arrPager",
        "PAGER_SHOW_ALL" => "Y",
        "PAGER_SHOW_ALWAYS" => "Y",
        "PAGER_TEMPLATE" => "",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(0 => "", 1 => "BUTTON", 2 => "BUTTON_LINK", 3 => "",),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "SORT",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER1" => "ASC",
        "SORT_ORDER2" => "DESC"
    )
); ?>

<!--
	<section class="about-01 mt-5 mb-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-5 mb-3">
				Purposes of establishing the Fund
            </h2>
          </div>
        </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-4 about-01-block">
			  <span class="mr-3">1</span>
				<p>Facilitate the modernization
and diversification of the
national economy</p>
          </div>
          <div class="col-lg-4 about-01-block">
			  <span class="mr-3">2</span>
				<p>Participate in a process
of stabilizing the
national economy</p>
          </div>
          <div class="col-lg-4 about-01-block">
			  <span class="mr-3">3</span>
				<p>Increase the Company’s<br>
business efficiency</p>
          </div>
        </div>
		</div>

      </div>
    </section>
	<section class="about-02 mt-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-3">
				Objectives of the Fund
            </h2>
			<p class="text-center">As soon as it was created in 2008, the Fund assumed<br> the role of one of the catalysts in recovering the national economy during post-crisis period. It successfully fulfilled the following tasks during that period:
			</p>
          </div>
         </div>
        </div>
      <div class="container mt-3">
        <div class="row">
          <div class="col-lg-6">
				<div class="block-01">
					Stabilization of the financial sector through<br>
additional capitalization of commercial banks 
				</div>
				<div class="block-01">
					Refinancing expensive<br> mortgage loans
				</div>
          </div>
          <div class="col-lg-6">
				<div class="block-01">
					Addressing challenges<br> in shared-equity construction market
				</div>
				<div class="block-01">
					Providing loans to SMB and<br>
implementation of strategic investment projects 
				</div>
          </div>
         </div>
        </div>

    </section>
-->
	<section class="about-03 mt-3">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-3">
				Development strategy of «Samruk-Kazyna» SWF» JSC
            </h2>
          </div>
         </div>
        </div>
      <div class="container block-02">
        <div class="row">
          <div class="col-lg-4">

<p class="block-04">
	<b>The mission of Samruk-Kazyna JSC</b> 
is to improve the sovereign welfare of the Republic of Kazakhstan and ensuring long-term sustainability for future generations.
			  </p>

          </div>
          <div class="col-lg-8">

		      <div class="container">
        			<div class="row">
          				<div class="col-lg-12">

				<p class="block-05">

The development strategy of Samruk-Kazyna until 2028 was approved by the Fund Management Board and approved by the Sole Shareholder in 2018.
			  </p>
						</div>
<!--
          				<div class="col-lg-6">

<div class="block-yellow" style="    min-height: 200px;
    margin-top: 90px;
    font-size: 22px;
    line-height: 28px;">
The development strategy of Samruk-Kazyna until 2028 was approved by the Fund Management Board and approved by the Sole Shareholder in 2018.
							</div>


<!-- 
  <div class="swiper-container swiper-container-about">
    <div class="swiper-wrapper">
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-01">
						<span>Oil</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-02">
						<span>Transport and logistics</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-03">
						<span>Chemical</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-04">
						<span>Nuclear</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-05">
						<span>Mining and smelting</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-06">
						<span>Energy</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-07">
						<span>Mechanical engineering</span>
					</div>
          		</div>
		</div>
		<div class="swiper-slide swiper-slide-about">
				<div class="about-slider">
					<div class="about-slider-item sector-08">
						<span>Real estate</span>
					</div>
          		</div>
		</div>
    </div>

    <div class="swiper-about-button-next"></div>
    <div class="swiper-about-button-prev"></div>
  </div>


  <script>
    var swiper = new Swiper('.swiper-container-about', {
      navigation: {
        nextEl: '.swiper-about-button-next',
        prevEl: '.swiper-about-button-prev',
      },
    });
  </script>
-->






						<!--</div>-->
					</div>
				</div>

          </div>
         </div>
        </div>

    </section>
<!--
	<section class="about-04 mb-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-3">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-3">
				Fund's strategic goals
            </h2>
          </div>
         </div>
        </div>
    </section>
	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<p class="t-1 text-center">
	<b>improving efficiency by increasing the profitability of portfolio companies;</b>
</p>
<p class="t-1 mt-3 text-center">
	<b>effective portfolio management by optimization of the portfolio structure and diversification of income;</b><br>
</p>
<p class="t-1 mt-3 text-center">
	<b>sustainable development by social stability and effective corporate governance</b><br>
</p>
          </div>
         </div>
        </div>
    </section>
-->
<!--
	<section class="about-05 mb-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
				<div class="about-05-01">
					<div class="img-01"></div>
					<span>Mission</span>
Enhancing national welfare of the Republic of Kazakhstan and providing long-term sustainability for future generations
				</div>
          </div>
          <div class="col-lg-6">
				<div class="about-05-01">
					<div class="img-02"></div>
					<span>Vision</span>
An investment holding that provides strong financial performance, investment returns and portfolio development on par with leading sovereign wealth funds.
				</div>

          </div>
         </div>
        </div>
      <div class="container mt-4">
        <div class="row">
          <div class="col-lg-12">
				<div class="about-05-01">
					<div class="img-03"><span>Strategic goal No. 1</span></div>

One of the key objectives and mandates of the Fund is effective management of portfolio companies through aligning performance indicators with those of the world's leading peer-companies in operation, production, financial performance, commercial activities and income increase from invested capital. Strengthening financial sustainability, increasing operational performance and impoving the efficiency of industrial assets use, as well as implementation of the Transformation Program will become top priorities for achieving this strategic goal.
				</div>
          </div>
          </div>
         </div>

    </section>
-->


	<section class="mb-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center mt-3">
				Fund's strategic goals
            </h2>
          </div>
         </div>
        </div>
    </section>

	<section class="about-06 mb-1">
      <div class="container mb-5">
        <div class="row">
          <div class="col-lg-6">
				<div class="about-06-01">
						<span>Improvement of companies` profitability</span>
					<a href="" class="block-down" data-id="0"></a>
				</div>
				<div class="block-down-box" data-id="0">
Improving the profitability of portfolio companies is one of the Fund's primary objectives. It is planned to provide a number of measures to strengthen financial stability and increase companies' operational efficiency, optimize and re-engineer business processes, and ensure synergy among portfolio companies of the Fund and enhance their investment attractiveness.
				</div>
          </div>
          <div class="col-lg-6">
				<div class="about-06-01">
<span>Optimization of portfolio structure</span>
					<a href="" class="block-down" data-id="1"></a>
				</div>
				<div class="block-down-box" data-id="1">
Optimization of portfolio structure is an important step in the transition process of the Fund to the active investment portfolio management. Restructure of the current portfolio of Fund's assets, revision of investment projects, reduction in the number of legal entities and management levels, and the Fund`s exiting from non-strategic assets, will enable the Fund to reallocate its resources efficiently in order to increase the value of the portfolio and its profitability.
				</div>

          </div>
         </div>
        </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
				<div class="about-06-01">
						<span>Income diversification
</span>
					<a href="" class="block-down" data-id="2"></a>
				</div>
				
				<div class="block-down-box" data-id="2">
				 Active asset management will allow the Fund to increase the portfolio return and ensure its diversification in various sectors giving the priority to assets and projects with a potential multiplier effect for the economy of Kazakhstan and high return rate.
				<p>
					<b>Investments within the Republic of Kazakhstan</b>
				</p>
				 The Fund makes investments within the country in order to develop various sectors of the economy of Kazakhstan and projects of strategic importance (in accordance with key state programs), taking into account restrictions established by "yellow pages" principles and legislative acts of the Republic of Kazakhstan, and non-hindering the development of the private sector.
				<p>
					<b>Investments outside of the Republic of Kazakhstan</b>
				</p>
				 In the medium and long term perspective the Development Strategy of the Fund foresees distribution of investments outside the Republic of Kazakhstan not related to the national economy and industry, with the purpose of increasing the  return and keeping wealth through diversification. The Fund will be create partnerships with leading sovereign funds and other investment companies and funds to make joint direct investments that will finally be strategically linked to the industries of Kazakhstan.
			</div>


          </div>
          <div class="col-lg-6">
				<div class="about-06-01">
<span>Corporate governance and
sustainable development
</span>
					<a href="" class="block-down" data-id="3"></a>
				</div>
				
				
				
				<div class="block-down-box" data-id="3">
				 Sustainable development initiatives, corporate governance and corporate culture are keystones of sustainable development and critical factors affecting the portfolio value.
				<p>
					<b>Corporate governance</b>
				</p>
				 In order to ensure efficient management of assets, increase investment attractiveness and successfully fulfill the function of an active investor, the Fund needs to continue introducing best practices of corporate governance in the Fund and portfolio companies in accordance with the recommendations of the Organization for Economic Cooperation and Development (OECD) on corporate governance.
				<p>
					<b>Human capital development</b>
				</p>
				 The use of the best HR development practices will be achieved through formation of an effective corporate culture at the Fund and portfolio companies,  introduction of key principles of meritocracy, transformation of people and introduction of new behaviors of employees for "commercial" thinking and readiness for change.
				<p>
					<b>Sponsorship and social responsiveness</b>
				</p>
				 The Fund will continue its activity in providing sponsorship and charity support. Allocation of aid should be reviewed and prioritized based on the needs of society. Relying on the best practices, the Fund will finance social projects in health, education, professional development of youth and community development, projects in the field of environmental protection, art and cultural heritage and knowledge, as well as research, innovation, science and technology, and projects aimed at supporting disadvantaged groups.
				<p>
					<b>Sustainable development initiatives</b>
				</p>
				 In order to manage the economic, environmental and social aspects of the activity efficiently and successfully, the Fund will be introducing high ethical standards and build a trust-based corporate culture, will be  introducing advanced standards and improve corporate governance, and will invest in human capital and take all measures to ensure environmental sustainability.
			</div>
			
			
			

          </div>
         </div>
        </div>
    </section>


	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5 text-center desc ">
<p class="t-1 mb-3 mt-3">

According to the Strategic Development Plan of the Republic of Kazakhstan until 2025, the Fund intends to transform from the role of an executor of government initiatives and programs to the role of an investment company. This goal implies, along with the management of the activities of current portfolio companies, active management of the Fund's investment portfolio, which provides investment income and develops a portfolio at the level of leading sovereign wealth funds. The Fund intends to continue to actively manage and improve the quality of its portfolio through the implementation of the privatization program, its own program of divestment and reinvestment of funds and dividends, new investments in Kazakhstan and beyond.

			  </p>
          </div>
         </div>
        </div>
    </section>



	<section>
<!--
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
<p class="t-1 mb-5 mt-5 text-center">
	<b>The New Corporate Governance Code</b> adopted in 2015<br>
that should contribute to the process of <b>Fund’s transformation<br>
to a strategic holding</b> that manages its Portfolio companies<br>
through efficient <b>Boards of Directors</b>. 
			  </p>
          </div>
         </div>
        </div>

      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5">
			  <div class="block-line"></div>
          </div>
         </div>
        </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
			  <p class="t-1 mb-3 mt-4 text-center">
Samruk-Kazyna JSC<br>
Corporate Governance Code
			  <br><a href="" class="pdf mt-3"></a>
</p>

          </div>
         </div>
        </div>
    </section>
-->
<!--
	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center mt-5 mb-5">
			  <a href="/about-fund/corporate-governance/" class="btn-read-more">Read more </a>
          </div>
         </div>
        </div>
    </section>
-->
<!--
	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center mt-5 mb-1">
			  <div class="block-line-full"></div>
          </div>
         </div>
        </div>
    </section>
-->
			</div>
</div>
</div>
</section>