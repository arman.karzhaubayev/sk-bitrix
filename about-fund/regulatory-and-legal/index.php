<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Нормативно-правовая база");
?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('regulatory-and-legal')?>
            </h2>
          </div>
        </div>
      </div>
</section>




<!--
	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5 mb-0">
			<div class="npb">
			<div class="container">
			  	<div class="row">
          			<div class="col-lg-6">
						<a href="" class="npb-all">Все документы</a>
						<a href="" class="npb-item">Корпоративные документы Фонда</a>
						<a href="" class="npb-item">Приватизация</a>
					</div>
          			<div class="col-lg-6">
						<span class="npb-empty">&nbsp;</span>
						<a href="" class="npb-item">Архив списков (неактивные)</a>
						<a href="" class="npb-item">Управление рисками</a>
					</div>
				</div>
			</div>
			</div>
			<div class="line-yellow"></div>
          </div>

        </div>
      </div>
    </section>

	<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mt-5 mb-5">
			<div class="npb-02 mb-5">


			<div class="container">
			  	<div class="row">
          			<div class="col-lg-6 pr-5">
						<a href="" class="npb-doc-item">Закон РК О «Фонде национального благосостояния»</a>
						<a href="" class="npb-doc-item">Устав АО «Самрук-Қазына» (с изменениями от 24.10.2018 г.)</a>
						<a href="" class="npb-doc-item">Корпоративный стандарт АО «Самрук-Қазына» по конфликтам интересов</a>
						<a href="" class="npb-doc-item">Выписка из Решения заочного заседания Совета директоров АО «Фонд национального благосостояния «Самрук-Қазына» от 07 апреля 2020 года № 171</a>
					</div>
          			<div class="col-lg-6 pr-3 pl-5">
						<a href="" class="npb-doc-item">Положение о Корпоративном секретаре</a>
						<a href="" class="npb-doc-item">Регламент проведения электронных торгов по продаже имущества на веб-портале реестра государственного имущества</a>
						<a href="" class="npb-doc-item">Кодекс корпоративного управления АО "Самрук-Қазына"</a>
						<a href="" class="npb-doc-item">Список страховых организаций на 1 февраля 2019</a>
					</div>
				</div>
			</div>
		  </div>

		  </div>
        </div>
      </div>
    </section>

-->
<?
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$id = (int)$request['id'];
$year = (int)$request['year'];

if (!empty($id))
{
    $activeFilter['SECTION_ID'] = $id;
}

if (!empty($year))
{
    $activeFilter[] = array(
        '>=DATE_ACTIVE_FROM' => '01.01.'.$year.' 00:00:01',
        '<=DATE_ACTIVE_FROM' => '31.12.'.$year.' 23:59:59'
    );
}
?>

<? $APPLICATION->IncludeComponent("bitrix:news", "ga.reports_and_plans", Array(
    "FILTER_NAME" => 'activeFilter', //имя переменной, в которой хранится фильтр
    "ADD_ELEMENT_CHAIN" => "Y",    // Включать название элемента в цепочку навигации
    "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
    "AJAX_MODE" => "Y",    // Включить режим AJAX
    "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
    "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
    "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
    "BROWSER_TITLE" => "NAME",    // Установить заголовок окна браузера из свойства
    "CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
    "CACHE_GROUPS" => "Y",    // Учитывать права доступа
    "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
    "CACHE_TYPE" => "N",    // Тип кеширования
    "CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
    "DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",    // Формат показа даты
    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
    "DETAIL_DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
    "DETAIL_FIELD_CODE" => array(    // Поля
        0 => "SHOW_COUNTER",
        1 => "",
    ),
    "DETAIL_PAGER_SHOW_ALL" => "Y",    // Показывать ссылку "Все"
    "DETAIL_PAGER_TEMPLATE" => "",    // Название шаблона
    "DETAIL_PAGER_TITLE" => "Страница",    // Название категорий
    "DETAIL_PROPERTY_CODE" => array(    // Свойства
        0 => "",
        1 => "",
    ),
    "DETAIL_SET_CANONICAL_URL" => "N",    // Устанавливать канонический URL
    "DISPLAY_BOTTOM_PAGER" => "Y",    // Выводить под списком
    "DISPLAY_DATE" => "Y",    // Выводить дату элемента
    "DISPLAY_NAME" => "Y",    // Выводить название элемента
    "DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
    "DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
    "DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
    "FILE_404" => "",
    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",    // Скрывать ссылку, если нет детального описания
    "IBLOCK_ID" => constant('NPB_IBLOCK_' . LANGUAGE_ID),    // Инфоблок
    "IBLOCK_TYPE" => "reports",    // Тип инфоблока
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
    "LIST_ACTIVE_DATE_FORMAT" => "j F Y",    // Формат показа даты
    "LIST_FIELD_CODE" => array(    // Поля
        0 => "",
        1 => "",
    ),
    "LIST_PROPERTY_CODE" => array(    // Свойства
        0 => "FILE",
        1 => "PROMOSITE",
    ),
    "MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
    "META_DESCRIPTION" => "-",    // Установить описание страницы из свойства
    "META_KEYWORDS" => "-",    // Установить ключевые слова страницы из свойства
    "NEWS_COUNT" => 999,    // Количество новостей на странице
    "PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
    "PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
    "PAGER_SHOW_ALL" => "Y",    // Показывать ссылку "Все"
    "PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
    "PAGER_TEMPLATE" => "",    // Шаблон постраничной навигации
    "PAGER_TITLE" => "Документы",    // Название категорий
    "PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
    "SEF_FOLDER" => "/about-fund/regulatory-and-legal/",    // Каталог ЧПУ (относительно корня сайта)
    "SEF_MODE" => "Y",    // Включить поддержку ЧПУ
    "SEF_URL_TEMPLATES" => array(
        "detail" => "#SECTION_CODE#/#ELEMENT_ID#/",
        "news" => "",
        "section" => "#SECTION_CODE#/",
    ),
    "SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
    "SET_STATUS_404" => "Y",    // Устанавливать статус 404
    "SET_TITLE" => "N",    // Устанавливать заголовок страницы
    "SHOW_404" => "N",    // Показ специальной страницы
    "SORT_BY1" => "SORT",
    "SORT_BY2" => "ACTIVE_FROM",
    "SORT_ORDER1" => "ASC",
    "SORT_ORDER2" => "DESC",
    "USE_CATEGORIES" => "N",    // Выводить материалы по теме
    "USE_FILTER" => "Y",    // Показывать фильтр
    "USE_PERMISSIONS" => "N",    // Использовать дополнительное ограничение доступа
    "USE_RATING" => "N",    // Разрешить голосование
    "USE_REVIEW" => "N",    // Разрешить отзывы
    "USE_RSS" => "N",    // Разрешить RSS
    "USE_SEARCH" => "N",    // Разрешить поиск
    "USE_SHARE" => "N",    // Отображать панель соц. закладок
    "TEMPLATE_LIST" => ".default", //шаблон списка новостей
),
    false
); ?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>