<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("Карта сайта");
?><? $APPLICATION->IncludeComponent("sk:main.map", "sk_sitemap", Array(
    "CACHE_TIME" => "3600",    // Время кеширования (сек.)
    "CACHE_TYPE" => "A",    // Тип кеширования
    "COL_NUM" => "3",    // Количество колонок
    "LEVEL" => "3",    // Максимальный уровень вложенности (0 - без вложенности)
    "SET_TITLE" => "Y",    // Устанавливать заголовок страницы
    "SHOW_DESCRIPTION" => "N",    // Показывать описания
    "HOW_MATCH_IN_COLUMN" => ['ONE' => 4, 'TWO' => 2], //добавлено - сколько выводить родителей в первых 2-х, остальные в 3-ю
),
    false
); ?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>