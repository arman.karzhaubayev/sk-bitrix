<?$APPLICATION->SetTitle("Байланыс деректері");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('contacts')?>
            </h2>
          </div>
        </div>
      </div>
</section>


<!--
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
				Байланыс деректері:
            </h2>
          </div>
         </div>
        </div>
</section>
-->
<section class="mb-5 mt-5 contacts">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
			  <b>«Самұрық-Қазына» АҚ</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-home"></i>Қазақстан Республикасы, 010000, Нур-Султан қаласы<br>

			  <b>Заңды:</b><br> Е 10 көшесі, 17/10 үйі<br>
			  <b>Нақты:</b><br> Е 10 көшесі, 17/10 үйі<br>
			  <b>Кеңсе:</b><br>
			  <i class="ico-phone"></i>+7 7172 554001, +7 7172 554097
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>Орын алған немесе жоспарланған алаяқтық және жемқорлық фактілері бойынша</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> 8 (800) 080 30 30<br>
			  <i class="ico-whatsapp"></i> 8 (702) 075 30 30<br>
			  <i class="ico-web"></i> nysana.cscc.kz<br>
			  <i class="ico-email"></i> nysana@cscc.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
БАҚ-пен өзара іс-қимыл жасау мәселелері бойынша
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i>  +7 (7172) 55-27-10, 55-26-94<br>
			  <i class="ico-email"></i>  press@sk.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Үкіметпен өзара іс-қимыл бойынша
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i>  +7 (7172) 55-26-24, +7 (7172) 55-41-64
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Сатып алулар порталының техникалық қолдауы
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-callcenter"></i>  +7 (7172) 55-22-66<br>
			  <i class="ico-email"></i>  support@skc.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Кадр мәселелері бойынша

</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i>  +7 (7172) 55-26-47<br>
+7 (7172) 55-40-49, 55-41-12 (қызметкерлерді іріктеу бойынша)<br>
			  <i class="ico-email"></i>  support@hrqyzmet.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Қор активтерін жекешелендіру мәселелері бойынша
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-callcenter"></i>  +7 (7172) 55-22-66<br>
			  <i class="ico-phone"></i>  +7 (7172) 55-92-81<br>
			  <i class="ico-email"></i>  DPRA@sk.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Инвесторлармен өзара іс-қимыл жасау мәселелері бойынша

</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 (7172) 55-40-71, 55-40-33<br>
			  <i class="ico-email"></i> ir@sk.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Халықаралық ынтымақтастық<br> департаментінің директоры

</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 7172 55 4036
          </div>
         </div>


        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Бастамаларды іздеу<br> секторының басшысы

</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 7172 55 4072
          </div>
         </div>



        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Қор сайтын контенттік және техникалық сүйемелдеу мәселелері бойынша
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-email"></i> sitesupport@sk.kz<br>
<!--
			  <b>Ескертпе:</b><br> 
			  <i class="ico-email"></i>tender.sk.kz және zakup.sk.kz
<br> сатып алу порталдарына қатысты<br>
электронды сатып алу порталының колдау кызметiне хабарласыңыз:<br>
			  <i class="ico-email"></i>support@skc.kz<br>
			  <i class="ico-phone"></i>+ 7-7172-552266
-->
          </div>
         </div>




        </div>

</section>
