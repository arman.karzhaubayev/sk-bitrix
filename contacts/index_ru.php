<?$APPLICATION->SetTitle("Контакты");?>
    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('contacts')?>
            </h2>
          </div>
        </div>
      </div>
</section>


<!--
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
				Контакты:
            </h2>
          </div>
         </div>
        </div>
</section>
-->
<section class="mb-5 mt-5 contacts">
      <div class="container">


        <div class="row">
          <div class="col-lg-6">
			  <b>АО
«Самрук-Қазына»</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-home"></i>Республика Казахстан, 010000, город Нур-Султан<br>
			  <b>Юридический:</b><br> ул. Е 10, 17/10<br>
<b>Фактический:</b><br> ул. Е 10, 17/10<br>
<br><b>Канцелярия:</b><br> 
<i class="ico-phone"></i>+7 7172 554001, +7 7172 554097
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>По поводу случившихся или<br> предполагаемых фактов<br> мошенничества и коррупции</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> 8 (800) 080 30 30<br>
			  <i class="ico-whatsapp"></i> 8 (702) 075 30 30<br>
			  <i class="ico-web"></i> nysana.cscc.kz<br>
			  <i class="ico-email"></i> nysana@cscc.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
По вопросам
взаимодействия со СМИ
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 (7172) 55-27-10, 55-26-94<br>
			  <i class="ico-email"></i> press@sk.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
По вопросам взаимодействия с Правительством
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 (7172) 55-26-24, +7 (7172) 55-41-64
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Техническая поддержка<br> портала закупок
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-callcenter"></i> +7 (7172) 55-22-66<br>
			  <i class="ico-email"></i> support@skc.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
По кадровым вопросам

</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 (7172) 55-26-47<br>
+7 (7172) 55-40-49, 55-41-12 (по подбору персонала)<br>
			  <i class="ico-email"></i> support@hrqyzmet.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
По вопросам<br> Программы приватизации
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-callcenter"></i> +7 (7172) 55-22-66<br>
			  <i class="ico-phone"></i> +7 (7172) 55-92-81<br>
			  <i class="ico-email"></i> DPRA@sk.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
По вопросам взаимодействия с инвесторами

</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 (7172) 55-40-71, 55-40-33<br>
			  <i class="ico-email"></i> ir@sk.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Директор департамента<br> международного сотрудничества

</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 7172 55 4036<br>
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Руководитель Сектора по поиску инициатив

</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 7172 55 4072<br>
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
По вопросам контентного и технического сопровождения сайта
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-email"></i> sitesupport@sk.kz<br>
          </div>
         </div>
<!--
        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Примечание: по вопросам порталов закупок
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-email"></i>tender.sk.kz и zakup.sk.kz<br>
          </div>
         </div>
-->

        </div>
</section>
