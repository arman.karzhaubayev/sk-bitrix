<?$APPLICATION->SetTitle("Contact information");?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <h2 class="h2-left pr-20 pb-30 page-title">
             <?=GetMessage('contacts')?>
            </h2>
          </div>
        </div>
      </div>
</section>

<!--
<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2 class="h2-left pr-20 pb-30 section-title text-center">
				Contact information:
            </h2>
          </div>
         </div>
        </div>
</section>
-->


<section class="mb-5 mt-5 contacts">
      <div class="container">


        <div class="row">
          <div class="col-lg-6">
			  <b> «Samruk-Kazyna» JSC</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-home"></i>010000, Nur-Sultan, Republic of Kazakhstan<br>

			  <b>Legal address:</b><br> E10 street, 17/10<br>
			  <b>The actual address:</b><br> E10 street, 17/10<br>
			  <b>Office:</b><br>
			  <i class="ico-phone"></i>+7 7172 554001, +7 7172 554097
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>On possible or happened fraud and corruption facts</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> 8 (800) 080 30 30<br>
			  <i class="ico-whatsapp"></i> 8 (702) 075 30 30<br>
			  <i class="ico-web"></i> nysana.cscc.kz<br>
			  <i class="ico-email"></i> nysana@cscc.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
For Media Enquiries
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 (7172) 55-27-10, 55-26-94<br>
			  <i class="ico-email"></i> press@sk.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
GR Department
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 (7172) 55-26-24, +7 (7172) 55-41-64
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Technical support of procurement website
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-callcenter"></i> +7 (7172) 55-22-66<br>
			  <i class="ico-email"></i> support@skc.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
On HR issues

</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 (7172) 55-26-47<br>
+7 (7172) 55-40-49, 55-41-12 (staff recruitment)<br>
			  <i class="ico-email"></i> support@hrqyzmet.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
For questions about the Privatization program
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-callcenter"></i>+7 (7172) 55-22-66<br>
			  <i class="ico-phone"></i> +7 (7172) 55-92-81<br>
			  <i class="ico-email"></i>DPRA@sk.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Investor Relations

</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 (7172) 55-40-71, 55-40-33<br>
			  <i class="ico-email"></i> ir@sk.kz
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Director of International<br> Cooperation Department

</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 7172 55 4036
          </div>
         </div>

        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
Head of Initiative<br> Search Sector

</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-phone"></i> +7 7172 55 4072
          </div>
         </div>



<!--
        <div class="row mt-3">
          <div class="col-lg-6">
			  <b>
For questions about the content and technical support of the Fund website
</b>
          </div>
          <div class="col-lg-6">
			  <i class="ico-email"></i>sitesupport@sk.kz<br>
			  <b>Note:</b><br> with inquiries regarding web-portal of informational system<br>
of electronic procurement
<br>
			  <i class="ico-email"></i>tender.sk.kz and zakup.sk.kz<br>
please contact technical support team at:<br>
			  <i class="ico-email"></i>support@skc.kz<br>
			  <i class="ico-phone"></i>+7-7172-552266
          </div>
         </div>
-->
        </div>
</section>
