<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/pages.php");
$pagetitle = Loc::getMessage('CONTACT_INFORMATION') ." — ". Loc::getMessage("NAME_SITE");
$description = Loc::getMessage("DESCRIPTION_SITE");
$keywords = Loc::getMessage("KEYWORDS_SITE");
$APPLICATION->SetTitle($pagetitle);
$APPLICATION->SetPageProperty("title", $pagetitle);
$APPLICATION->SetPageProperty("description",  $description);
$APPLICATION->SetPageProperty("keywords",  $keywords);
$APPLICATION->SetPageProperty("og:type",  "website");
$APPLICATION->SetPageProperty("og:title",  $pagetitle);
$APPLICATION->SetPageProperty("og:description",  $description);
$APPLICATION->SetPageProperty("og:url",  "https://sk.kz".$APPLICATION->GetCurPage(false));
$APPLICATION->SetPageProperty("og:image",  "https://sk.kz".SITE_TEMPLATE_PATH."/thumb.jpg");
?>
<div class="contacts">
  <div class="container container_xs_nopad">
    <div class="row">
      <div class="col-12">
        <div class="breadcrumb contacts__breadcumb">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="breadcrumb__content">
                  <a class="breadcrumb__item" href="/">
                    <p class="breadcrumb__item-text">
                      <?=Loc::getMessage('MAIN');?>
                    </p>
                  </a>
                  <span class="breadcrumb__item">
                    <p class="breadcrumb__item-text">
                      <?=Loc::getMessage('CONTACT_INFORMATION');?>
                    </p>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="title contacts_titlecomponent">
          <h2 class="title__text">
            <?=Loc::getMessage('CONTACT_INFORMATION');?>
          </h2>
        </div>
        <div class="contacts__info"> 
          <div class="contacts__info-content">
            <p class="contacts__info-title">
              <?=Loc::getMessage('NAME_SITE');?>
            </p>
            <div class="contacts__info-line"> 
            </div>
            <?if(LANGUAGE_ID == "ru"):?>
            <p class="contacts__info-text contacts__info-text_mb">Республика Казахстан, 010000, город Нур-Султан
            </p>
            <?elseif(LANGUAGE_ID == "kz"):?>
            <p class="contacts__info-text contacts__info-text_mb">Қазақстан Республикасы, 010000, Нур-Султан қаласы
            </p>
            <?elseif(LANGUAGE_ID == "en"):?>
            <p class="contacts__info-text contacts__info-text_mb">010000, Nur-Sultan, Republic of Kazakhstan
            </p>
            <?endif?>

            <div class="contacts__info-item">
                <p class="contacts__info-text contacts__info-text_extra"><?=Loc::getMessage('LEGAL_ADDRESS');?>:</p>

                <?if(LANGUAGE_ID == "ru"):?>
                <p class="contacts__info-text">ул. Е 10, 17/10
                </p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="contacts__info-text">Е 10 көшесі, 17/10 үйі
                </p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="contacts__info-text">E10 street, 17/10
                </p>
                <?endif?>
            </div>

            <div class="contacts__info-item">
                <p class="contacts__info-text contacts__info-text_extra"><?=Loc::getMessage('THE_ACTUAL_ADDRESS');?>:</p>

                <?if(LANGUAGE_ID == "ru"):?>
                <p class="contacts__info-text">ул. Е 10, 17/10
                </p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="contacts__info-text">Е 10 көшесі, 17/10 үйі
                </p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="contacts__info-text">E10 street, 17/10
                </p>
                <?endif?>
            </div>

            <div class="contacts__info-item">
                <p class="contacts__info-text contacts__info-text_extra"><?=Loc::getMessage('OFFICE');?>:</p>

                <p class="contacts__info-text">
                    <a href="tel:+7 7172 554001">+7 (7172) 55-40-01</a>
                    ,
                    <a href="tel:+7 7172 554097">+7 (7172) 55-40-97</a>
                </p>
            </div>

          </div>
          <div class="contacts__info-map">
            <iframe src="https://yandex.com/map-widget/v1/?um=constructor%3Aa8179201b56e86a0bbe0afd413f3d48ced9b4c0147fab79ba9bd2afe83c7cf71&amp;amp;source=constructor" width="100%" height="461" frameborder="0">
            </iframe>
          </div>
        </div>

        <div class="contacts__line">
          <div class="title__line">
            <img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
            <img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt="">
            <img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
          </div>
        </div>

      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12">

        <div class="contacts__cards">

          <div class="contacts-card">
                <div class="contacts-card__header">
                    <p class="contacts-card__title">
                        <?=Loc::getMessage('ON_POSSIBLE_OR_HAPPENED_FRAUD_AND_CORRUPTION_FACTS');?>
                    </p>
                </div>

                <div class="contacts-card__content">
                    <div class="contacts-card__line">
                        <div class="contacts-card__line-title">
                            <p class="contacts-card__line-title-text"> <?=Loc::getMessage('PHONE');?>:</p>
                        </div>

                        <div class="contacts-card__line-phones">
                            <p>
                                <a href="tel:8 (800) 080 30 30">8 (800) 080-30-30</a>
                            </p>
                        </div>
                    </div>

                    <div class="contacts-card__line">
                        <div class="contacts-card__line-title">
                            <p class="contacts-card__line-title-text">Whats app:</p>
                        </div>

                        <div class="contacts-card__line-phones">
                            <p>
                                <a href="https://wa.me/87020753030">8 (702) 075-30-30</a>
                            </p>
                        </div>
                    </div>

                    <div class="contacts-card__line">
                        <div class="contacts-card__line-title">
                            <p class="contacts-card__line-title-text"><?=Loc::getMessage('WEBSITE');?>: </p>
                        </div>

                        <div class="contacts-card__line-phones">
                            <p>
                                <a
                                    href="nysana.cscc.kz"
                                    target="_blank"
                                >nysana.cscc.kz</a>
                            </p>
                        </div>
                    </div>

                    <div class="contacts-card__line">
                        <div class="contacts-card__line-title">
                            <p class="contacts-card__line-title-text"><?=Loc::getMessage('MAIL');?>:</p>
                        </div>

                        <div class="contacts-card__line-phones">
                            <p>
                                <a href="mailto:nysana@cscc.kz">nysana@cscc.kz</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

          <div class="contacts-card">
            <div class="contacts-card__header">
                <p class="contacts-card__title">
                    <?=Loc::getMessage('FOR_MEDIA_ENQUIRIES');?>
                </p>
            </div>

            <div class="contacts-card__content">
                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"> <?=Loc::getMessage('PHONE');?>:</p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="tel:+7 (7172) 55-27-10">+7 (7172) 55-27-10,</a>

                            <a href="tel:+7 (7172) 55-26-94">+7 (7172) 55-26-94</a>
                        </p>
                    </div>
                </div>

                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"><?=Loc::getMessage('MAIL');?>:</p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="mailto:press@sk.kz">press@sk.kz</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="contacts-card">
            <div class="contacts-card__header">
                <p class="contacts-card__title">
                    <?=Loc::getMessage('GR_DEPARTMENT');?>
                </p>
            </div>

            <div class="contacts-card__content">
                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"><?=Loc::getMessage('PHONE');?>: </p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="tel:+7 (7172) 55-26-24">+7 (7172) 55-26-24,</a>

                            <a href="tel:+7 (7172) 55-41-64">+7 (7172) 55-41-64</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="contacts-card">
            <div class="contacts-card__header">
                <p class="contacts-card__title">
                <?=Loc::getMessage('TECHNICAL_SUPPORT_OF_PROCUREMENT_WEBSITE');?>
                </p>
            </div>

            <div class="contacts-card__content">
                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"><?=Loc::getMessage('PHONE');?>: </p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="tel:+7 (7172) 55-22-66">+7 (7172) 55-22-66</a>
                        </p>
                    </div>
                </div>

                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"><?=Loc::getMessage('MAIL');?>: </p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="mailto:support@skc.kz">support@skc.kz</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="contacts-card">
            <div class="contacts-card__header">
                <p class="contacts-card__title"><?=Loc::getMessage('ON_HR_ISSUES');?></p>
            </div>

            <div class="contacts-card__content">
                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"><?=Loc::getMessage('PHONE');?>: </p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="tel:+7 (7172) 55-26-47">+7 (7172) 55-26-47,</a>

                            <a href="tel:+7 (7172) 55-40-49">+7 (7172) 55-40-49,</a>

                            <a href="tel:+7 (7172) 55-41-12">+7 (7172) 55-41-12</a>

                            (<?=Loc::getMessage('ON_RECRUITMENT_OF_PERSONNEL');?>)
                        </p>
                    </div>
                </div>

                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"><?=Loc::getMessage('MAIL');?>: </p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="mailto:support@hrqyzmet.kz">support@hrqyzmet.kz</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="contacts-card">
            <div class="contacts-card__header">
                <p class="contacts-card__title">
                <?=Loc::getMessage('FOR_QUESTIONS_ABOUT_THE_PRIVATIZATION_PROGRAM');?>
                </p>
            </div>

            <div class="contacts-card__content">
                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"><?=Loc::getMessage('CALL_CENTRE');?>:</p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="tel:+7 (7172) 55-22-66">+7 (7172) 55-22-66</a>
                        </p>
                    </div>
                </div>

                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"><?=Loc::getMessage('PHONE');?>: </p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="tel:+7 7172 55 4036">+7 (7172) 55-92-81</a>
                        </p>
                    </div>
                </div>

                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"><?=Loc::getMessage('MAIL');?>: </p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="mailto:DPRA@sk.kz">DPRA@sk.kz</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="contacts-card">
            <div class="contacts-card__header">
                <p class="contacts-card__title">
                <?=Loc::getMessage('INVESTOR_RELATIONS');?>
                </p>
            </div>

            <div class="contacts-card__content">
                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"><?=Loc::getMessage('PHONE');?>: </p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="tel:+7 7172 55 4036">+7 (7172) 55-40-71,</a>

                            <a href="tel:+7 7172 55-40-33">+7 (7172) 55-40-33</a>
                        </p>
                    </div>
                </div>

                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"><?=Loc::getMessage('MAIL');?>: </p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="mailto:ir@sk.kz">ir@sk.kz</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="contacts-card">
            <div class="contacts-card__header">
                <p class="contacts-card__title">
                    <?=Loc::getMessage('DIRECTOR_OF_INTERNATIONAL_COOPERATION_DEPARTMENT');?>
                </p>
            </div>

            <div class="contacts-card__content">
                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"><?=Loc::getMessage('PHONE');?>:</p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="tel:+7 7172 55 4036">+7 (7172) 55-40-36</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="contacts-card">
            <div class="contacts-card__header">
                <p class="contacts-card__title">
                <?=Loc::getMessage('HEAD_OF_INITIATIVE_SEARCH_SECTOR');?>
                </p>
            </div>

            <div class="contacts-card__content">
                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"> <?=Loc::getMessage('PHONE');?>: </p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
                            <a href="tel:+7 7172 55 4072">+7 (7172) 55-40-72</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="contacts-card">
            <div class="contacts-card__header">
                <p class="contacts-card__title">
                <?=Loc::getMessage('ON_THE_ISSUES_OF_CONTENT_AND_TECHNICAL_SUPPORT_OF_THE_SITE');?>
                </p>
            </div>

            <div class="contacts-card__content">
                <div class="contacts-card__line">
                    <div class="contacts-card__line-title">
                        <p class="contacts-card__line-title-text"><?=Loc::getMessage('MAIL');?>: </p>
                    </div>

                    <div class="contacts-card__line-phones">
                        <p>
							<a href="mailto:sitesupport@sk.kz">sitesupport@sk.kz</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>