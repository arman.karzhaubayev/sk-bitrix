<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/pages.php");
$APPLICATION->SetTitle(Loc::getMessage("NAME_SITE"));
$APPLICATION->SetPageProperty("title", Loc::getMessage("NAME_SITE"));
$APPLICATION->SetPageProperty("description",  Loc::getMessage("DESCRIPTION_SITE"));
$APPLICATION->SetPageProperty("keywords",  Loc::getMessage("KEYWORDS_SITE"));
$APPLICATION->SetPageProperty("og:type",  "website");
$APPLICATION->SetPageProperty("og:title",  Loc::getMessage("NAME_SITE"));
$APPLICATION->SetPageProperty("og:description",  Loc::getMessage("DESCRIPTION_SITE"));
$APPLICATION->SetPageProperty("og:url",  "https://sk.kz/");
$APPLICATION->SetPageProperty("og:image",  "https://sk.kz".SITE_TEMPLATE_PATH."/thumb.jpg");

use Bitrix\Main\Loader;
Loader::includeModule("iblock");

function makeSort($blockId, $key = 'default', $lang = 'ru', $params){
	$result = Array();

	if($key == 'main'){
		$result = Array('SHOW_COUNTER' => 'DESC');
	} elseif($key == 'history'){
		$result = Array('SORT' => 'ASC');
	} else {
		$result = Array('ACTIVE_FROM' => 'DESC');
	}

	return $result;
}

function makeFilter($blockId, $key = 'default', $lang = 'ru', $params){
	$additions = Array();

	if($key == 'news') {
		$additions = Array(
			"!PROPERTY_SHOW_ON_MAIN_NEWS" => false,
			"<=DATE_ACTIVE_FROM" => date('d.m.Y H:i:s')
		);
	} else {
		$additions = Array(
			"<=DATE_ACTIVE_FROM" => date('d.m.Y H:i:s')
		);
	}

	return Array(
		"ACTIVE" => "Y"
		,"IBLOCK_ID" => $blockId
		,$additions
	);
}
function getNews($blockId, $newsCount, $key, $lang, $params){

	$siteUrl = (CMain::IsHTTPS() ? 'https:' : 'http:').'//'.SITE_SERVER_NAME;

	$arSort  	= Array('ACTIVE_FROM' => 'DESC');
	$arFilter 	= Array(
					"ACTIVE"    			=> "Y", 
					"IBLOCK_ID" 			=> $blockId, 
					"<=DATE_ACTIVE_FROM" 	=> date('d.m.Y H:i:s')
				);
	if (function_exists('makeSort')) {
		$arSort = makeSort($blockId, $key, $lang, $params);
	}
	if (function_exists('makeFilter')) {
		$arFilter = makeFilter($blockId, $key, $lang, $params);
	}

	$arNavStartParams["nTopCount"] = $newsCount;

	// print_r($arFilter);

	$res        = CIBlockElement::GetList(
		$arSort,
		$arFilter,
		false,
		$arNavStartParams,
		Array(
			"ID","IBLOCK_ID",
			"PROPERTY_*",
			"DETAIL_PAGE_URL",
			"NAME","ACTIVE_FROM",
			"CODE","IBLOCK_SECTION_ID",
			"PREVIEW_PICTURE","PREVIEW_TEXT",
			"DETAIL_PICTURE",
			"SHOW_COUNTER", "SORT"
		)
	);

	$counter = 0;
	$ndlCDBResult = $res;
	$arResult = [];
	while($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arProps  = $ob->GetProperties();

		//$result[$counterOffset]['ID']             = $arFields['ID'];
		//$result[$counter]['NAME']           = $arFields['NAME'];
		//$result[$counter]['SORT']           = $arFields['SORT'];
		//$result[$counter]['ACTIVE_FROM'] = $arFields['ACTIVE_FROM'];
		//$result[$counter]['SHOW_COUNTER'] = $arFields['SHOW_COUNTER'];
		$result[$counter]['FIELDS'] = $arFields;
		$result[$counter]['PROPS'] = $arProps;

		++$counter;
	}
	return $result;
}


$langNews = LANGUAGE_ID;

function EditData ($DATA,$langNews) {
	if($langNews == "ru") {
		$MES = array( 
			"01" => "Января", 
			"02" => "Февраля", 
			"03" => "Марта", 
			"04" => "Апреля", 
			"05" => "Мая", 
			"06" => "Июня", 
			"07" => "Июля", 
			"08" => "Августа", 
			"09" => "Сентября", 
			"10" => "Октября", 
			"11" => "Ноября", 
			"12" => "Декабря"
		);
	} elseif($langNews == "kz") {
		$MES = array( 
			'01' => 'Қаңтар',
			'02' => 'Ақпан',
			'03' => 'Наурыз',
			'04' => 'Сәуір',
			'05' => 'Мамыр',
			'06' => 'Маусым',
			'07' => 'Шілде',
			'08' => 'Тамыз',
			'09' => 'Қыркүйек',
			'10' => 'Қазан',
			'11' => 'Қараша',
			'12' => 'Желтоқсан',
		);
	} elseif($langNews == "en") {
		$MES = array( 
			'01' => 'January',
			'02' => 'February',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December',
		);
	}
	$arData = explode(".", $DATA); 
	$d = ($arData[0] < 10) ? substr($arData[0], 1) : $arData[0];
	
	$newData = $d." ".$MES[$arData[1]]; 
	return $newData;
}

switch ($langNews) {
  case "ru":
    $newsId = 177;
	$sliderId = 9;
    break;
  case "en":
    $newsId = 175;
	$sliderId = 39;
    break;
  case "kz":
    $newsId = 176;
	$sliderId = 40;
    break;
  default:
   	$newsId = 177;
	$sliderId = 9;
}
$slider = getNews($sliderId, 4, false, 'ru', $params);
$news = getNews($newsId, 4, 'news', 'ru', $params);
$history = getNews(189, 4, 'history', 'ru', $params);
global $USER;
if ($USER->IsAdmin()){
	//echo "<pre>";
	//print_r($slider);
	//echo "</pre>";
};
?>

<div id="appmain">
    <div class="mainpage">
        <div class="mainpage__slider">
            <div class="container container_visible">
                <div class="row">
                    <div class="col-12">
<div class="slider">
    <div class="slider__main">
        <div class="slider__arrow slider__arrow_left">
            <img
                class="slider__arrow-icon"
                src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-primary.png"
                alt=""
            >
        </div>

        <div class="slider__arrow slider__arrow_right">
            <img
                class="slider__arrow-icon"
                src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-primary.png"
                alt=""
            >
        </div>

        <div class="swiper-container main-swiper-container">
            <div class="swiper-wrapper">
<?foreach($slider as $arElements):?>
                <div class="swiper-slide">

<div class="slider__item">
	<div class="slider__content">
		<img
			class="slider__watermark"
			src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/watermark.svg"
			alt=""
		>

		<img
			class="slider__quote"
			src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/dots.svg"
			alt=""
		>

		<div class="slider__title">
			<p class="slider__title-text"><?=$arElements['PROPS']['SLIDE_TITLE']['VALUE'];?></p>
		</div>

		<hr>

		<div class="slider__short">
			<p class="slider__short-text"><?=TruncateText($arElements['PROPS']['SLIDE_SUBTITLE']['VALUE'],90);?></p>
		</div>

		<a
			class="slider__btn"
			href="<?=$arElements['PROPS']['SLIDE_LINK']['VALUE'];?>"
			target="blank"
		>
			<p class="slider__btn-text">{{ $t('messages.company6') }}</p>
			<?
			$preview = CFile::ResizeImageGet($arElements['FIELDS']['PREVIEW_PICTURE'], Array("width" => 590, "height" => 435),BX_RESIZE_IMAGE_EXACT, true);
			?>
			<img
				class="slider__btn-icon"
				src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
				alt=""
			>
		</a>
	</div>

	<div class="slider__image">
		<img
			class="slider__image-src"
			src="<?=$preview['src'];?>"
			alt="<?=$arElements['PROPS']['SLIDE_TITLE']['VALUE'];?>"
		>
	</div>

	<div class="slider__status">
		<div class="slider__status-arrow slider__status-arrow_left">
			<img
				class="slider__status-arrow-icon"
				src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-light.svg"
				alt=""
			>
		</div>

		<div class="slider__status-arrow slider__status-arrow_right">
			<img
				class="slider__status-arrow-icon"
				src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-light.svg"
				alt=""
			>
		</div>
		<?
		$smtp = MakeTimeStamp($arElements['FIELDS']['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS");
		$arElements['FIELDS']['ACTIVE_FROM'] = date("d M", $smtp);
		?>
		<p class="slider__status-text"><?=$arElements['FIELDS']['ACTIVE_FROM'];?></p>
	</div>
</div>

                </div>
<?endforeach?>

            </div>
        </div>
    </div>

    <div class="slider__nav">
    <?
$index = 0;
foreach($slider as $arElements):?>
	<?
		$smtp = MakeTimeStamp($arElements['FIELDS']['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS");
		$arElements['FIELDS']['ACTIVE_FROM'] = date("d.m", $smtp);
		?>
    <div class="slider__nav-item" data-index="<?=$index?>" :class=" mainNewsActiveIndex==<?=$index?>?'slider__nav-item_active':'' ">
            <p class="slider__nav-item-text"><?=EditData($arElements['FIELDS']['ACTIVE_FROM'],$langNews);?></p>
        </div>
<?
$index++;
endforeach?>
    </div>
</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="mainpage__news">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="title">
                            <div class="title__line">
                                <img
                                    class="title__line-border"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
                                    alt=""
                                >

                                <img
                                    class="title__line-center"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
                                    alt=""
                                >

                                <img
                                    class="title__line-border"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
                                    alt=""
                                >
                            </div>

                            <h2 class="title__text"><?=Loc::getMessage("NEWS_FEED");?></h2>
                        </div>

                        <div class="mainpage__news-list">
							<?foreach($news as $arElements):?>
								<?
								$smtp = MakeTimeStamp($arElements['FIELDS']['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS");
								$arElements['FIELDS']['ACTIVE_FROM'] = date("d.m.Y", $smtp);
								?>
								<?
								$preview = CFile::ResizeImageGet($arElements['PROPS']['INNDEX_PICTURES']['VALUE'][0], Array("width" => 280, "height" => 180),BX_RESIZE_IMAGE_EXACT, true);
//$arElements['FIELDS']['PREVIEW_PICTURE']['SRC'] = $preview['src'];
								?>

                            <a
                                href="https://samruk-akparat.kz<?=$arElements['FIELDS']['DETAIL_PAGE_URL'];?>"
                                target="blank"
                            >
                                <div class="newscard">
                                    <div class="newscard__image">
                                        <img
                                            class="newscard__image-src"
                                            src="<?=$preview['src'];?>"
                                            alt="<?=$arElements['FIELDS']['NAME'];?>"
                                        >
                                    </div>

                                    <div class="newscard__content">
                                        <div class="newscard__category">
                                            <p class="newscard__category-text">{{ $t('messages.news1') }}</p>
                                        </div>

                                        <div class="newscard__title">
                                            <p class="newscard__title-text">
                                                <?=TruncateText($arElements['FIELDS']['NAME'], 70);?>
                                            </p>
                                        </div>

                                        <div class="newscard__footer">
                                            <p class="newscard__date"><?=$arElements['FIELDS']['ACTIVE_FROM'];?></p>

                                            <div class="newscard__views">
                                                <img
                                                    class="newscard__views-icon"
                                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/views.png"
                                                    alt=""
                                                >

                                                <p class="newscard__views-text"><?=$arElements['FIELDS']['SHOW_COUNTER'];?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
							<?endforeach?>
                        </div>

                        <div class="mainpage__news-btn">
                            <a
                                class="btnwrap"
								:href=" $t('messages.allNewsLinks') "
                                target="_blank"
                            >
                                <div class="btn btn_w_auto">
                                    <p class="btn__text"><?=Loc::getMessage("ALL_NEWS");?></p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="mainpage__about">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="title">
                            <div class="title__line">
                                <img
                                    class="title__line-border"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
                                    alt=""
                                >

                                <img
                                    class="title__line-center"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
                                    alt=""
                                >

                                <img
                                    class="title__line-border"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
                                    alt=""
                                >
                            </div>

                            <h2 class="title__text"><?=Loc::getMessage("ABOUT_FUND");?></h2>
                        </div>

                        <div class="mainpage__about-content">
                            <img
                                class="mainpage__about-logo"
                                src="<?=SITE_TEMPLATE_PATH?>/assets/img/logo.svg"
                                alt=""
                            >

                            <div class="mainpage__about-content-des">
                                <p class="mainpage__about-text mainpage__about-text_bold">
                                    <?=Loc::getMessage("ABOUT_FUND_TEXT_TOP");?>
                                </p>

                                <p class="mainpage__about-text">
                                    <?=Loc::getMessage("ABOUT_FUND_TEXT_BOTTOM");?>
                                </p>

                                <div class="mainpage__about-btn">
                                    <a
                                        class="btnwrap"
										:href=" $t('messages.aboutLink') "
                                        target="_blank"
                                    >
                                        <div class="btn btn_hasicon">
                                            <p class="btn__text"><?=Loc::getMessage("LEARN_MORE");?></p>

                                            <div class="btn__icon">
                                                <img
                                                    class="btn__icon-src"
                                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.png"
                                                    alt=""
                                                >
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="mainpage__companies">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="title">
                            <div class="title__line">
                                <img
                                    class="title__line-border"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
                                    alt=""
                                >

                                <img
                                    class="title__line-center"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
                                    alt=""
                                >

                                <img
                                    class="title__line-border"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
                                    alt=""
                                >
                            </div>

                            <h2 class="title__text"><?=Loc::getMessage("PORTFOLIO");?></h2>
                        </div>
                    </div>

                    <div class="col-1 col-xs-0">
                        <div class="mainpage__companies-arrow mainpage__companies-arrow_left">
                            <img
                                class="mainpage__companies-arrow-icon"
                                src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow.png"
                                alt=""
                            >
                        </div>
                    </div>

                    <div class="col-10 col-xs-12">
                        <div class="swiper-container maincompany-swiper-container">
                            <div class="swiper-wrapper">
                                <div
									class="swiper-slide"
									v-for="(item,index) in mainCompanies[lang]"
									:key="index"
								>
									<div class="companycard">
										<a
											:href="item.link"
											target="_blank"
										>
											<div class="companycard__image">
												<img
													class="companycard__image-src"
													:src="item.image"
													alt=""
												>

												<div class="companycard__overlay">
													<img
														class="companycard__overlay-logo"
														:src="item.hoverImage"
														alt=""
													>
												</div>
											</div>
										</a>

										<div class="companycard__content">
											<div class="companycard__title">
												<p
													class="companycard__title-text"
													v-html="item.name"
												></p>
											</div>

											<div class="companycard__indicators">
												<p class="companycard__indicators-text">
													{{ $t('messages.company1') }}

													<span v-html="item.value1"></span>
												</p>

												<p class="companycard__indicators-text">
													{{ $t('messages.company2') }}

													<span v-html="item.value2"></span>
												</p>

												<p class="companycard__indicators-text">
													{{ $t('messages.company3') }}

													<span v-html="item.value3"></span>
												</p>

												<p class="companycard__indicators-text">
													{{ $t('messages.company4') }}

													<span v-html="item.value4"></span>
												</p>

												<p
													class="companycard__indicators-text"
													v-if="item.value5"
												>
													{{ $t('messages.company5') }}

													<span v-html="item.value5"></span>
												</p>
											</div>

											<a
												:href="item.link"
												target="_blank"
											>
												<div class="companycard__btn">
													<p class="companycard__btn-text">{{ $t('messages.company6') }}</p>

													<img
														class="companycard__btn-icon"
														src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
														alt=""
													>
												</div>
											</a>
										</div>
									</div>
								</div>


                            </div>
                        </div>
                    </div>

                    <div class="col-1 col-xs-0">
                        <div class="mainpage__companies-arrow mainpage__companies-arrow_right">
                            <img
                                class="mainpage__companies-arrow-icon"
                                src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow.png"
                                alt=""
                            >
                        </div>
                    </div>

                    <div class="col-10 col-start-2 col-xs-12 col-xs-start-1">
                        <div class="mainpage__companies-nav">
                            <div class="mainpage__companies-dots"></div>

                            <div class="mainpage__companies-btn">
                                <a
                                    class="btnwrap"
									:href=" $t('messages.allCompanyLink')"
                                    target="_blank"
                                >
                                    <div class="btn btn_hasicon">
                                        <p class="btn__text"><?=Loc::getMessage("ALL_PORTFOLIO_COMPANY");?></p>

                                        <div class="btn__icon">
                                            <img
                                                class="btn__icon-src"
                                                src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/briefcase.png"
                                                alt=""
                                            >
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<div class="mainpage__mobilehistory">
			<div class="mainhistory__slider">
				<div class="mainhistory__title">
					<p class="mainhistory__title-text">{{ $t('messages.history1') }}</p>
				</div>

				<div class="swiper-container mainhistory__slider-container">
					<div class="swiper-wrapper">
						<div
							class="swiper-slide"
							v-for="(year,index) in historyMobile"
							:key="index"
						>
							<div class="mainhistory__slider-item">
								<div class="mainhistory__list">
									<div
										class="mainhistory__list-item"
										v-for="(item,index) in year[1]"
										:key="index"
									>
										<p class="mainhistory__list-item-number">0{{index+1}}</p>

										<p
											class="mainhistory__list-item-text"
											v-html="item"
										></p>
									</div>
								</div>
							</div>

							<div class="mainhistory__year">
								<div class="mainhistory__year-arrow mainhistory__year-arrow_left">
									<img
										class="mainhistory__year-arrow-icon"
										src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-long.svg"
										alt=""
									>
								</div>

								<div class="mainhistory__year-arrow mainhistory__year-arrow_right">
									<img
										class="mainhistory__year-arrow-icon"
										src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-long.svg"
										alt=""
									>
								</div>

								<p
									class="mainhistory__year-text"
									v-html="year[0]"
								></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

        <div class="mainpage__history">
            <div class="mainpage__history-block">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="mainhistory">
                                <div class="mainhistory__control">
                                    <div class="mainhistory__control-item mainhistory__control-item_top">
                                        <img
                                            class="mainhistory__control-item-icon"
                                            src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
                                            alt=""
                                        >
                                    </div>

                                    <div class="mainhistory__control-item mainhistory__control-item_bottom">
                                        <img
                                            class="mainhistory__control-item-icon"
                                            src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
                                            alt=""
                                        >
                                    </div>
                                </div>

                                <div class="mainhistory__years">
									<div class="swiper-container mainhistory__yearsslider">
										<div class="swiper-wrapper">
											<div
												class="swiper-slide"
												v-for="(item, index) in historyMobile"
												:key="index"
											>
												<div
													class="mainhistory__years-item"
													:data-year="item[0]"
													:class="item[0]==historyActiveYear?'mainhistory__years-item_active':''"
													@click="()=&gt;historyActiveYear=item[0]"
												>
													<p
														class="mainhistory__years-item-text"
														v-html="item[0]"
													></p>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="mainhistory__content">
									<div class="mainhistory__title">
										<p class="mainhistory__title-text">{{ $t('messages.history1') }}</p>
									</div>

									<div class="mainhistory__list">
										<div
											class="mainhistory__list-item"
											v-for="(item,index) in history[lang][historyActiveYear]"
											:key="index"
										>
											<p class="mainhistory__list-item-number">0{{index+1}}</p>

											<p
												class="mainhistory__list-item-text"
												v-html="item"
											></p>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mainpage__history-partners">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="mpartners">
                                <div class="mpartners__arrow mpartners__arrow_left">
                                    <img
                                        class="mpartners__arrow-icon"
                                        src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
                                        alt=""
                                    >
                                </div>

                                <div class="swiper-container mpartners__list">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <a
                                                href="https://hrqyzmet.kz/"
                                                target="_blank"
                                            >
                                                <div class="mpartners__item">
                                                    <img
                                                        class="mpartners__item-icon"
                                                        src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/brands_qyzmet.png"
                                                        alt=""
                                                    >
                                                </div>
                                            </a>
                                        </div>

                                        <div class="swiper-slide">
                                            <a
                                                href="https://samruk-akparat.kz/"
                                                target="_blank"
                                            >
                                                <div class="mpartners__item">
                                                    <img
                                                        class="mpartners__item-icon"
                                                        src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aqparat.png"
                                                        alt=""
                                                    >
                                                </div>
                                            </a>
                                        </div>

                                        <div class="swiper-slide">
                                            <a
                                                href="https://www.skc.kz/"
                                                target="_blank"
                                            >
                                                <div class="mpartners__item">
                                                    <img
                                                        class="mpartners__item-icon"
                                                        src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/samruk.png"
                                                        alt=""
                                                    >
                                                </div>
                                            </a>
                                        </div>

                                        <div class="swiper-slide">
                                            <a
                                                href="https://aifc.kz/"
                                                target="_blank"
                                            >
                                                <div class="mpartners__item">
                                                    <img
                                                        class="mpartners__item-icon"
                                                        src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aifc_cmyk.png"
                                                        alt=""
                                                    >
                                                </div>
                                            </a>
                                        </div>

                                        <div class="swiper-slide">
                                            <a
                                                href="https://primeminister.kz/ru"
                                                target="_blank"
                                            >
                                                <div class="mpartners__item">
                                                    <img
                                                        class="mpartners__item-icon"
                                                        src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/min.png"
                                                        alt=""
                                                    >
                                                </div>
                                            </a>
                                        </div>

                                        <div class="swiper-slide">
                                            <a
                                                href="http://www.akorda.kz/ru"
                                                target="_blank"
                                            >
                                                <div class="mpartners__item">
                                                    <img
                                                        class="mpartners__item-icon"
                                                        src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-7.jpg"
                                                        alt=""
                                                    >
                                                </div>
                                            </a>
                                        </div>

                                        <div class="swiper-slide">
                                            <a
                                                href="https://elbasy.kz/ru"
                                                target="_blank"
                                            >
                                                <div class="mpartners__item">
                                                    <img
                                                        class="mpartners__item-icon"
                                                        src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-6.jpg"
                                                        alt=""
                                                    >
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="mpartners__arrow mpartners__arrow_right">
                                    <img
                                        class="mpartners__arrow-icon"
                                        src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
                                        alt=""
                                    >
                                </div>

								<div class="mpartners__mobile">
										<a
											href="https://hrqyzmet.kz/"
											target="_blank"
										>
											<div class="mpartners__item">
												<img
													class="mpartners__item-icon"
													src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/brands_qyzmet.png"
													alt=""
												>
											</div>
										</a>
	
										<a
											href="https://samruk-akparat.kz/"
											target="_blank"
										>
											<div class="mpartners__item">
												<img
													class="mpartners__item-icon"
													src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aqparat.png"
													alt=""
												>
											</div>
										</a>
	
										<a
											href="https://www.skc.kz/"
											target="_blank"
										>
											<div class="mpartners__item">
												<img
													class="mpartners__item-icon"
													src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/samruk.png"
													alt=""
												>
											</div>
										</a>
	
										<a
											href="https://aifc.kz/"
											target="_blank"
										>
											<div class="mpartners__item">
												<img
													class="mpartners__item-icon"
													src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/aifc_cmyk.png"
													alt=""
												>
											</div>
										</a>
	
										<a
											href="https://primeminister.kz/ru"
											target="_blank"
										>
											<div class="mpartners__item">
												<img
													class="mpartners__item-icon"
													src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/min.png"
													alt=""
												>
											</div>
										</a>
	
										<a
											href="http://www.akorda.kz/ru"
											target="_blank"
										>
											<div class="mpartners__item">
												<img
													class="mpartners__item-icon"
													src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-7.jpg"
													alt=""
												>
											</div>
										</a>
	
										<a
											href="https://elbasy.kz/ru"
											target="_blank"
										>
											<div class="mpartners__item">
												<img
													class="mpartners__item-icon"
													src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/mainpage/partners/partner-6.jpg"
													alt=""
												>
											</div>
										</a>
									</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>