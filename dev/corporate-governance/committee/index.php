<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/pages.php");
$pagetitle = Loc::getMessage('COMMITTEES_OF_THE_BOARD_OF_DIRECTORS') ." — ". Loc::getMessage('CORPORATE_GOVERNANCE') ." — ". Loc::getMessage('ABOUT_FUND') ." — ". Loc::getMessage("NAME_SITE");
$description = Loc::getMessage("DESCRIPTION_SITE");
$keywords = Loc::getMessage("KEYWORDS_SITE");
$APPLICATION->SetTitle($pagetitle);
$APPLICATION->SetPageProperty("title", $pagetitle);
$APPLICATION->SetPageProperty("description",  $description);
$APPLICATION->SetPageProperty("keywords",  $keywords);
$APPLICATION->SetPageProperty("og:type",  "website");
$APPLICATION->SetPageProperty("og:title",  $pagetitle);
$APPLICATION->SetPageProperty("og:description",  $description);
$APPLICATION->SetPageProperty("og:url",  "https://sk.kz".$APPLICATION->GetCurPage(false));
$APPLICATION->SetPageProperty("og:image",  "https://sk.kz".SITE_TEMPLATE_PATH."/thumb.jpg");
?>
<div class="committee">
  <div class="container">
	<div class="row">
	  <div class="col-12"> 
				<div class="breadcrumb corporate-governance__breadcrumb">
				  <div class="container">
					<div class="row">
					  <div class="col-12">
                        <div class="breadcrumb__content"><a class="breadcrumb__item" href="/">
                            <p class="breadcrumb__item-text"><?=Loc::getMessage('MAIN');?></p></a>
                            <a class="breadcrumb__item" href="/about-fund/">
                            <p class="breadcrumb__item-text"><?=Loc::getMessage('ABOUT_FUND');?></p></a>
                            <a class="breadcrumb__item" href="/corporate-governance/">
                            <p class="breadcrumb__item-text"><?=Loc::getMessage('CORPORATE_GOVERNANCE');?></p></a>
                            <span class="breadcrumb__item">
                            <p class="breadcrumb__item-text"><?=Loc::getMessage('COMMITTEES_OF_THE_BOARD_OF_DIRECTORS');?></p></span>
                        </div>
					  </div>
					</div>
				  </div>
				</div>
		<p class="corporate-governance__title"><?=Loc::getMessage('COMMITTEES_OF_THE_BOARD_OF_DIRECTORS');?></p>
	  </div>
	  <div class="col-12">
		<div class="committee__cards committee__cards_1">
		  <div class="infocard">
			<div class="infocard__header">
			  <p class="infocard__title"><?=Loc::getMessage('AUDIT_COMMITTEE');?></p>
			</div>
			<div class="infocard__content">
			  <div class="infocard__textblock">
				<p class="infocard__text"><?=Loc::getMessage('CHAIRMAN');?>:</p>
                <?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Сутера Лука</p>
				<p class="infocard__text infocard__text_bold">независимый директор</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Сутера Лука</p>
				<p class="infocard__text infocard__text_bold">тәуелсіз директор</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Sutera Luca</p>
				<p class="infocard__text infocard__text_bold">Independent Director</p>
                <?endif?>
			  </div>
			  <div class="infocard__textblock"> 
				<p class="infocard__text"><?=Loc::getMessage('MEMBER');?>:</p>
                <?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Онг Бун Хви</p>
				<p class="infocard__text infocard__text_bold">независимый директор</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Онг Бун Хви</p>
				<p class="infocard__text infocard__text_bold">тәуелсіз директор</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Ong Boon Hwee</p>
				<p class="infocard__text infocard__text_bold">Independent Director</p>
                <?endif?>
			  </div>
			</div>
		  </div>
		  <div class="infocard">
			<div class="infocard__header">
			  <p class="infocard__title"><?=Loc::getMessage('NOMINATION_AND_REMUNERATION_COMMITTEE');?></p>
			</div>
			<div class="infocard__content">
			  <div class="infocard__textblock">
				<p class="infocard__text"><?=Loc::getMessage('CHAIRMAN');?>:</p>
                <?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Дудас Джон</p>
				<p class="infocard__text infocard__text_bold">независимый директор</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Дудас Джон</p>
				<p class="infocard__text infocard__text_bold">тәуелсіз директор</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Dudas Jon</p>
				<p class="infocard__text infocard__text_bold">Independent Director</p>
                <?endif?>
			  </div>
			  <div class="infocard__textblock"> 
				<p class="infocard__text"><?=Loc::getMessage('MEMBER');?>:</p>
                <?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Бозумбаев Канат Алдабергенович,</p>
				<p class="infocard__text infocard__text_bold">Помощник Президента Республики Казахстан</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Бозымбаев Қанат Алдабергенұлы</p>
				<p class="infocard__text infocard__text_bold">Қазақстан Республикасы Президентінің көмекшісі</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Bozumbayev Kanat</p>
				<p class="infocard__text infocard__text_bold">Assistant to the President of the Republic of Kazakhstan</p>
                <?endif?>
			  </div>
			  <div class="infocard__textblock"> 
                <?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Мажибаев Кайрат Куанышбаевич,</p>
				<p class="infocard__text infocard__text_bold">независимый директор</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Мәжібаев Қайрат Қуанышбайұлы</p>
				<p class="infocard__text infocard__text_bold">тәуелсіз директор</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Mazhibayev Kairat</p>
				<p class="infocard__text infocard__text_bold">Independent Director</p>
                <?endif?>
			  </div>
			  <div class="infocard__textblock"> 
                <?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Сутера Лука</p>
				<p class="infocard__text infocard__text_bold">независимый директор</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Сутера Лука</p>
				<p class="infocard__text infocard__text_bold">тәуелсіз директор</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Sutera Luca</p>
				<p class="infocard__text infocard__text_bold">Independent Director</p>
                <?endif?>
			  </div>
			</div>
		  </div>
		  <div class="infocard">
			<div class="infocard__header">
			  <p class="infocard__title"><?=Loc::getMessage('SPECIAL_COMMITTEE');?></p>
			</div>
			<div class="infocard__content">
			  <div class="infocard__textblock">
				<p class="infocard__text"><?=Loc::getMessage('CHAIRMAN');?>:</p>
                <?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Султанов Бахыт Турлыханович</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Бақыт Тұрлыханұлы Сұлтанов</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Sultanov Bakhyt</p>
                <?endif?>
			  </div>
			  <div class="infocard__textblock"> 
				<p class="infocard__text"><?=Loc::getMessage('MEMBER');?>:</p>
                <?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Бергенев Адылгазы Садвокасович</p>
				<p class="infocard__text infocard__text_bold">Жанадил Ернар Бейсенулы</p>
				<p class="infocard__text infocard__text_bold">Тулешов Габдулкамит Маденович</p>
				<p class="infocard__text infocard__text_bold">Киякбаева Ардак Борановна</p>
				<p class="infocard__text infocard__text_bold">Бушмухамбетова Айнур Сансизбаевна</p>
				<p class="infocard__text infocard__text_bold">Карымсаков Бейбит Еркинбаевич</p>
				<p class="infocard__text infocard__text_bold">Исаев Бахтияр Орынбасарович</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Бергенев Адылгазы Садвоқасұлы</p>
                <p class="infocard__text infocard__text_bold">Жанәділ Ернар Бейсенұлы</p>
                <p class="infocard__text infocard__text_bold">Түлешов Ғабдұлхамит Мәденұлы</p>
                <p class="infocard__text infocard__text_bold">Қияқбаева Ардақ Боранқызы</p>
                <p class="infocard__text infocard__text_bold">Бұшмұхамбетова Айнұр Сансызбайқызы</p>
                <p class="infocard__text infocard__text_bold">Карымсаков Бейбит Еркинбаевич</p>
                <p class="infocard__text infocard__text_bold">Исаев Бахтияр Орынбасарович</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Bergenev Adylgazy</p>
                <p class="infocard__text infocard__text_bold">Zhanadil Yernar</p>
                <p class="infocard__text infocard__text_bold">Tuleshov Gabdulkumyt</p>
                <p class="infocard__text infocard__text_bold">Kiyakbaeva Ardak</p>
                <p class="infocard__text infocard__text_bold">Bushmuhambetova Ainur</p>
                <p class="infocard__text infocard__text_bold">Karymsakov Beıbıt</p>
                <p class="infocard__text infocard__text_bold">Issayev Bakhtiyar</p>
                <?endif?>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="col-8 col-start-3 col-sm-12 col-sm-start-1">
		<div class="committee__cards committee__cards_2">
		  <div class="infocard">
			<div class="infocard__header">
			  <p class="infocard__title"><?=Loc::getMessage('TRANSFORMATION_PROGRAM_OVERSIGHT_COMMITTEE');?></p>
			</div>
			<div class="infocard__content">
			  <div class="infocard__textblock">
				<p class="infocard__text"> <?=Loc::getMessage('CHAIRMAN');?>:</p>
				<?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Дудас Джон</p>
				<p class="infocard__text infocard__text_bold">независимый директор</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Дудас Джон</p>
				<p class="infocard__text infocard__text_bold">тәуелсіз директор</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Dudas Jon</p>
				<p class="infocard__text infocard__text_bold">Independent Director</p>
                <?endif?>
			  </div>
			  <div class="infocard__textblock"> 
				<p class="infocard__text"> <?=Loc::getMessage('MEMBER');?>:</p>
				<?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Бозумбаев Канат Алдабергенович,</p>
				<p class="infocard__text infocard__text_bold">Помощник Президента Республики Казахстан</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Бозымбаев Қанат Алдабергенұлы</p>
				<p class="infocard__text infocard__text_bold">Қазақстан Республикасы Президентінің көмекшісі</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Bozumbayev Kanat</p>
				<p class="infocard__text infocard__text_bold">Assistant to the President of the Republic of Kazakhstan</p>
                <?endif?>
			  </div>
			  <div class="infocard__textblock"> 
              <?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Онг Бун Хви</p>
				<p class="infocard__text infocard__text_bold">независимый директор</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Онг Бун Хви</p>
				<p class="infocard__text infocard__text_bold">тәуелсіз директор</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Ong Boon Hwee</p>
				<p class="infocard__text infocard__text_bold">Independent Director</p>
                <?endif?>
			  </div>
			</div>
		  </div>
		  <div class="infocard">
			<div class="infocard__header">
			  <p class="infocard__title"><?=Loc::getMessage('STRATEGY_COMMITTEE');?></p>
			</div>
			<div class="infocard__content">
			  <div class="infocard__textblock">
				<p class="infocard__text"><?=Loc::getMessage('CHAIRMAN');?>:</p>
                <?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Иргалиев Асет Арманович,</p>
				<p class="infocard__text infocard__text_bold">Министр национальной экономики Республики Казахстан</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Ерғалиев Әсет Арманұлы</p>
				<p class="infocard__text infocard__text_bold">Қазақстан Республикасының Ұлттық экономика министрі</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Irgaliyev Asset</p>
				<p class="infocard__text infocard__text_bold">Member of the Board of Directors</p>
                <?endif?>
			  </div>
			  <div class="infocard__textblock"> 
				<p class="infocard__text"><?=Loc::getMessage('MEMBER');?>:</p>
				<?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Дудас Джон</p>
				<p class="infocard__text infocard__text_bold">независимый директор</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Дудас Джон</p>
				<p class="infocard__text infocard__text_bold">тәуелсіз директор</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Dudas Jon</p>
				<p class="infocard__text infocard__text_bold">Independent Director</p>
                <?endif?>
			  </div>
			  <div class="infocard__textblock"> 
              <?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Мажибаев Кайрат Куанышбаевич,</p>
				<p class="infocard__text infocard__text_bold">независимый директор</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Мәжібаев Қайрат Қуанышбайұлы</p>
				<p class="infocard__text infocard__text_bold">тәуелсіз директор</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Mazhibayev Kairat</p>
				<p class="infocard__text infocard__text_bold">Independent Director</p>
                <?endif?>
			  </div>
			  <div class="infocard__textblock"> 
                <?if(LANGUAGE_ID == "ru"):?>
                <p class="infocard__text infocard__text_bold">Онг Бун Хви</p>
				<p class="infocard__text infocard__text_bold">независимый директор</p>
                <?elseif(LANGUAGE_ID == "kz"):?>
                <p class="infocard__text infocard__text_bold">Онг Бун Хви</p>
				<p class="infocard__text infocard__text_bold">тәуелсіз директор</p>
                <?elseif(LANGUAGE_ID == "en"):?>
                <p class="infocard__text infocard__text_bold">Ong Boon Hwee</p>
				<p class="infocard__text infocard__text_bold">Independent Director</p>
                <?endif?>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="col-12">
				<div class="title__line">
				<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
				<img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt="">
				<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt=""></div>
	  </div>
	  <div class="col-12">
		<p class="corporate-governance__title"><?=Loc::getMessage('ORGANIZATIONAL_STRUCTURE');?></p>
		<div class="committee__structure">
		  <div class="comstruture">
			<div class="comstruture__nav">
			  <div class="comstruture__nav-item" v-for=" (item, index) in orgstructure[$i18n.locale]" :key="index" @click=" ()=&gt;orgstructureActiveTab=index " :class=" (orgstructureActiveTab==index?'comstruture__nav-item_active':'') +' '+(item.block?'comstruture__nav-item_block':'') ">
				<p class="comstruture__nav-item-text" v-html="item.title"></p>
				<div class="comstruture__nav-item-indicator">
				  <svg width="31" height="43" viewBox="0 0 31 43" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path fill-rule="evenodd" clip-rule="evenodd" d="M30.5735 21.6332C29.2235 21.9032 27.8935 22.2332 26.5735 22.6332C23.8035 23.4332 21.2335 27.8032 18.5735 33.6332C16.7813 37.3092 14.014 40.4224 10.5735 42.6332C10.5735 42.6332 0.573485 40.6332 0.573485 37.5432L0.573486 7.00318C0.573486 3.17318 10.5735 0.633178 10.5735 0.633178C14.014 2.844 16.7813 5.95721 18.5735 9.63318C21.2135 15.4632 23.7835 19.8332 26.5735 20.6332C27.8835 21.0332 29.2135 21.3632 30.5735 21.6332Z" fill="#1B3351"></path>
					<rect x="15.5935" y="21.6332" width="9.72122" height="9.72122" transform="rotate(135 15.5935 21.6332)" fill="white"></rect>
				  </svg>
				</div>
			  </div>
					<div class="comstruture__nav-arrow comstruture__nav-arrow_left" @click="prevCorpStructureTab"><img class="comstruture__nav-arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow-light.svg" alt=""></div>
				  <div class="comstruture__nav-arrow comstruture__nav-arrow_right" @click="nextCorpStructureTab"><img class="comstruture__nav-arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow-light.svg" alt=""></div>
			</div>
			<div class="comstruture__content">
			  <div class="comstruture__line" v-for=" (listitem, index) in orgstructure[$i18n.locale][orgstructureActiveTab].list" :key="index">
				<div class="comstruture__number">
				  <p class="comstruture__number-text">0{{index+1}}</p>
				</div>
				<div class="comstruture__des">
				  <p class="comstruture__des-text" v-html="listitem"></p>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>