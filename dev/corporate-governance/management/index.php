<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/pages.php");
$pagetitle = Loc::getMessage('FUND_MANAGEMENT_BOARD') ." — ". Loc::getMessage('CORPORATE_GOVERNANCE') ." — ". Loc::getMessage('ABOUT_FUND') ." — ". Loc::getMessage("NAME_SITE");
$description = Loc::getMessage("DESCRIPTION_SITE");
$keywords = Loc::getMessage("KEYWORDS_SITE");
$APPLICATION->SetTitle($pagetitle);
$APPLICATION->SetPageProperty("title", $pagetitle);
$APPLICATION->SetPageProperty("description",  $description);
$APPLICATION->SetPageProperty("keywords",  $keywords);
$APPLICATION->SetPageProperty("og:type",  "website");
$APPLICATION->SetPageProperty("og:title",  $pagetitle);
$APPLICATION->SetPageProperty("og:description",  $description);
$APPLICATION->SetPageProperty("og:url",  "https://sk.kz".$APPLICATION->GetCurPage(false));
$APPLICATION->SetPageProperty("og:image",  "https://sk.kz".SITE_TEMPLATE_PATH."/thumb.jpg");
?>
<div class="management">
  <div class="container">
	<div class="row">
	  <div class="col-12"> 
				<div class="breadcrumb corporate-governance__breadcrumb">
				  <div class="container">
					<div class="row">
					  <div class="col-12">
						<div class="breadcrumb__content"><a class="breadcrumb__item" href="/">
							<p class="breadcrumb__item-text"><?=Loc::getMessage('MAIN');?></p></a>
                            <a class="breadcrumb__item" href="/about-fund/">
							<p class="breadcrumb__item-text"><?=Loc::getMessage('ABOUT_FUND');?></p></a>
                            <a class="breadcrumb__item" href="/corporate-governance/">
							<p class="breadcrumb__item-text"><?=Loc::getMessage('CORPORATE_GOVERNANCE');?></p></a>
                            <span class="breadcrumb__item">
							<p class="breadcrumb__item-text"><?=Loc::getMessage('FUND_MANAGEMENT_BOARD');?></p></span>
						</div>
					  </div>
					</div>
				  </div>
				</div>
		<p class="corporate-governance__title"><?=Loc::getMessage('FUND_MANAGEMENT_BOARD');?></p>
		<p class="corporate-governance__text"><?=Loc::getMessage('FUND_MANAGEMENT_BOARD_TEXT');?></p>
		<div class="management__main">
		  <div class="management__main-left">
				<img class="management__main-image" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/management/image.jpg" alt=""></div>
		  <div class="management__main-right">
            <?if(LANGUAGE_ID == "ru"):?>
            <p class="management__main-title">Нурсултан <br> Абишевич Назарбаев</p>
			<p class="management__main-text">Первый Президент Республики Казахстан - Елбасы, председатель Совета по управлению Фондом</p>
            <?elseif(LANGUAGE_ID == "kz"):?>
            <p class="management__main-title">Нұрсұлтан <br> Әбішұлы Назарбаев</p>
			<p class="management__main-text">Қазақстан Республикасының Тұңғыш Президенті - Елбасы, Кеңес Төрағасы</p>
            <?elseif(LANGUAGE_ID == "en"):?>
            <p class="management__main-title">Nursultan <br> Nazarbayev</p>
			<p class="management__main-text">First President of the Republic of Kazakhstan - Elbasy, Chairman of the Fund Management Board</p>
            <?endif?>
		  </div>
		</div>
				<div class="title__line">
					<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
					<img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt="">
					<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
				</div>
	  </div>
	  <div class="col-12">
		<p class="corporate-governance__title corporate-governance__title_mt"><?=Loc::getMessage('MEMBERS_OF_THE_FUND_MANAGEMENT_BOARD');?></p>
		<div class="management__list management__list_1">
		  <div class="personcard" v-for=" (item,index) in sovet1[$i18n.locale] " :key="index">
			<div class="personcard__image"><img class="personcard__image-src" :src="item.image" alt=""></div>
			<div class="personcard__content">
			  <p class="personcard__title" v-html="item.name"></p>
			  <div class="personcard__line"></div>
			  <p class="personcard__position" v-html="item.position"></p>
			</div>
		  </div>
		</div>
				<div class="title__line">
					<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
					<img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt="">
					<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
				</div>
	  </div>
	  <div class="col-12">
		<p class="corporate-governance__title corporate-governance__title_mt"><?=Loc::getMessage('BOARD_OF_DIRECTORS');?></p>
	  </div>
	  <div class="col-1">
		<div class="management__arrow management__arrow_left management__arrow_1">
				<img class="management__arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow.svg" alt="">
		</div>
	  </div>
	  <div class="col-10 col-xs-12">
		<div class="management__list management__list_2">
		  <div class="swiper-container management__slider_2">
			<div class="swiper-wrapper">
			  <div class="swiper-slide" v-for=" (item,index) in sovet2[$i18n.locale] " :key="index">
				<div class="personcard">
				  <div class="personcard__image"><img class="personcard__image-src" :src="item.image" alt=""></div>
				  <div class="personcard__content">
					<p class="personcard__title" v-html="item.name"></p>
					<div class="personcard__line"></div>
					<p class="personcard__position" v-html="item.position"></p>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="col-1">
		<div class="management__arrow management__arrow_right management__arrow_2">
			<img class="management__arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow.svg" alt="">
		</div>
	  </div>
	  <div class="col-12">
		<div class="management__slider-dots management__slider-dots_2"></div>
	  </div>
	  <div class="col-12">
				<div class="title__line">
					<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
					<img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt="">
					<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
				</div>
		<div class="col-12"></div>
		<p class="corporate-governance__title corporate-governance__title_mt"><?=Loc::getMessage('GOVERNANCE');?></p>
	  </div>
	  <div class="col-1">
		<div class="management__arrow management__arrow_left management__arrow_3">
			<img class="management__arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow.svg" alt="">
		</div>
	  </div>
	  <div class="col-10 col-xs-12">
		<div class="management__list management__list_2">
		  <div class="swiper-container management__slider_3">
			<div class="swiper-wrapper">
			  <div class="swiper-slide" v-for=" (item,index) in sovet3[$i18n.locale] " :key="index">
				<div class="personcard">
				  <div class="personcard__image"><img class="personcard__image-src" :src="item.image" alt=""></div>
				  <div class="personcard__content">
					<p class="personcard__title" v-html="item.name"></p>
					<div class="personcard__line"></div>
					<p class="personcard__position" v-html="item.position"></p>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="col-1">
		<div class="management__arrow management__arrow_right management__arrow_4">
			<img class="management__arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow.svg" alt="">
		</div>
	  </div>
	  <div class="col-12">
		<div class="management__slider-dots management__slider-dots_3"></div>
	  </div>
	  <div class="col-12">
				<div class="title__line">
					<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
					<img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt="">
					<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
				</div>
		<div class="col-12"></div>
		<p class="corporate-governance__title corporate-governance__title_mt"><?=Loc::getMessage('MANAGEMENT');?></p>
	  </div>
	  <div class="col-1">
		<div class="management__arrow management__arrow_left management__arrow_5"><img class="management__arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow.svg" alt=""></div>
	  </div>
	  <div class="col-10 col-xs-12">
		<div class="management__list management__list_2">
		  <div class="swiper-container management__slider_4">
			<div class="swiper-wrapper">
			  <div class="swiper-slide" v-for=" (item,index) in sovet4[$i18n.locale] " :key="index">
				<div class="personcard">
				  <div class="personcard__image"><img class="personcard__image-src" :src="item.image" alt=""></div>
				  <div class="personcard__content">
					<p class="personcard__title" v-html="item.name"></p>
					<div class="personcard__line"></div>
					<p class="personcard__position" v-html="item.position"></p>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="col-1">
		<div class="management__arrow management__arrow_right management__arrow_6"><img class="management__arrow-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow.svg" alt=""></div>
	  </div>
	  <div class="col-12">
		<div class="management__slider-dots management__slider-dots_4"></div>
	  </div>
	</div>
  </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>