<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/pages.php");
$pagetitle = Loc::getMessage('CORPORATE_GOVERNANCE') ." — ". Loc::getMessage('ABOUT_FUND') ." — ". Loc::getMessage("NAME_SITE");
$description = Loc::getMessage("DESCRIPTION_SITE");
$keywords = Loc::getMessage("KEYWORDS_SITE");
$APPLICATION->SetTitle($pagetitle);
$APPLICATION->SetPageProperty("title", $pagetitle);
$APPLICATION->SetPageProperty("description",  $description);
$APPLICATION->SetPageProperty("keywords",  $keywords);
$APPLICATION->SetPageProperty("og:type",  "website");
$APPLICATION->SetPageProperty("og:title",  $pagetitle);
$APPLICATION->SetPageProperty("og:description",  $description);
$APPLICATION->SetPageProperty("og:url",  "https://sk.kz".$APPLICATION->GetCurPage(false));
$APPLICATION->SetPageProperty("og:image",  "https://sk.kz".SITE_TEMPLATE_PATH."/thumb.jpg");
?>
<div class="corporate-governance">
  <div class="container">
	<div class="row">
	  <div class="col-12"> 
				<div class="breadcrumb corporate-governance__breadcrumb">
				  <div class="container">
					<div class="row">
					  <div class="col-12">
						<div class="breadcrumb__content"><a class="breadcrumb__item" href="/">
							<p class="breadcrumb__item-text"><?=Loc::getMessage('MAIN');?></p></a>
                            <a class="breadcrumb__item" href="/about-fund/">
							<p class="breadcrumb__item-text"><?=Loc::getMessage('ABOUT_FUND');?></p></a>
                            <span class="breadcrumb__item">
							<p class="breadcrumb__item-text"><?=Loc::getMessage('CORPORATE_GOVERNANCE');?></p></span>
						</div>
					  </div>
					</div>
				  </div>
				</div>
		<p class="corporate-governance__title"><?=Loc::getMessage('CORPORATE_GOVERNANCE');?></p>
		<p class="corporate-governance__text"><?=Loc::getMessage('CORPORATE_GOVERNANCE_TEXT');?></p>
				<div class="title__line">
					<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
					<img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt="">
					<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
				</div>
	  </div>
	</div>
  </div>
  <div class="corporate-governance__projects">
	<div class="container">
	  <div class="row">
		<div class="col-12"> 
		  <p class="corporate-governance__title corporate-governance__title_mb"><?=Loc::getMessage('IN_THE_PART_OF_CORPORATE_GOVERNANCE_THE_FUND_IS_OPERATING_6_PROJECTS');?></p>
		  <div class="corporate-governance__projects-list">
			<div class="corpcard" v-for=" (item,index) in corporateProjects[$i18n.locale] " :key="index">
			  <div class="corpcard__icon"><img class="corpcard__icon-src" :src="item.icon" alt=""></div>
			  <div class="corpcard__content">
				<p class="corpcard__title" v-html="item.title"></p>
				<p class="corpcard__text" v-html="item.text"></p>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
  <div class="container">
	<div class="row">
	  <div class="col-8 col-sm-12">
			<?if(LANGUAGE_ID == "ru"){
				$link = "/documents/CG Code_ru.pdf";
			} elseif(LANGUAGE_ID == "kz"){
				$link = "/documents/CG Code_kz.pdf";
			} elseif(LANGUAGE_ID == "en"){
				$link = "/documents/CG Code_en.pdf";
			};
			$file = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$link);
			$size = CFile::FormatSize($file['size']);
			?>
        <a href="<?=$link?>" target="_blank"> 
		  <div class="iconbtn corporate-governance__download">
			<div class="iconbtn__icon">
			<img class="iconbtn__icon-src" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/pdf.svg" alt="">
			  <div class="iconbtn__icon-info">
				<p class="iconbtn__icon-text"><?=Loc::getMessage('DOWNLOAD');?></p>
				<p class="iconbtn__icon-text"><?=$size?></p>
			  </div>
			</div>
			<div class="iconbtn__content">
			  <div class="iconbtn__title"><?=Loc::getMessage('CORPORATE_GOVERNANCE_CODE');?></div>
			  <div class="iconbtn__text"><?=Loc::getMessage('CORPORATE_GOVERNANCE_CODE_TEXT');?></div>
			</div>
		  </div>
        </a>
        </div>
	<div class="col-12">
        <div class="title__line">
            <img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
            <img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt="">
            <img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
        </div>
		<div class="corporate-governance__info">
		  <div class="corpinfo">
			<div class="corpinfo__left">
			  <div class="corpinfo__title">
				<p class="corpinfo__title-text"><?=Loc::getMessage('MANAGEMENT_BODIES');?></p>
			  </div>
			  <div class="corpinfo__subtitle">
				<p class="corpinfo__subtitle-text"><?=Loc::getMessage('MANAGEMENT_BODIES_BOTTOM_TEXT');?></p>
			  </div>
			</div>
			<div class="corpinfo__right">
            <?=Loc::getMessage('MANAGEMENT_BODIES_RIGHT_TEXT');?>
			</div>
		  </div>
		</div>
			<div class="title__line">
				<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
				<img class="title__line-center" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg" alt="">
				<img class="title__line-border" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg" alt="">
			</div>
	  </div>
	  <div class="col-6 col-start-4 col-sm-12 col-sm-start-1"><a href="/dev/corporate-governance/management/">
		  <div class="corporate-governance__link">
			<p class="corporate-governance__link-text"><?=Loc::getMessage('FUND_MANAGEMENT_BOARD');?></p>
		  </div></a></div>
	</div>
  </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>