<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/pages.php");
$pagetitle = Loc::getMessage('FINANCIAL_PERFORMANCE') ." — ". Loc::getMessage('ABOUT_FUND') ." — ". Loc::getMessage("NAME_SITE");
$description = Loc::getMessage("DESCRIPTION_SITE");
$keywords = Loc::getMessage("KEYWORDS_SITE");
$APPLICATION->SetTitle($pagetitle);
$APPLICATION->SetPageProperty("title", $pagetitle);
$APPLICATION->SetPageProperty("description",  $description);
$APPLICATION->SetPageProperty("keywords",  $keywords);
$APPLICATION->SetPageProperty("og:type",  "website");
$APPLICATION->SetPageProperty("og:title",  $pagetitle);
$APPLICATION->SetPageProperty("og:description",  $description);
$APPLICATION->SetPageProperty("og:url",  "https://sk.kz".$APPLICATION->GetCurPage(false));
$APPLICATION->SetPageProperty("og:image",  "https://sk.kz".SITE_TEMPLATE_PATH."/thumb.jpg");
?>
<div class="financial">
	<div class="breadcrumb about__financial">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="breadcrumb__content">
						<a class="breadcrumb__item" href="/">
							<p class="breadcrumb__item-text"><?=Loc::getMessage('MAIN');?></p>
						</a>
						<a class="breadcrumb__item" href="/about-fund/">
							<p class="breadcrumb__item-text"><?=Loc::getMessage('ABOUT_FUND');?></p></a>
						<span class="breadcrumb__item">
							<p class="breadcrumb__item-text"><?=Loc::getMessage('FINANCIAL_PERFORMANCE');?></p>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="financial__info">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="title">
						<h2 class="title__text"><?=Loc::getMessage('FINANCIAL_PERFORMANCE');?></h2>
					</div>

					<div class="mainpage__about-content">
						<img
							class="mainpage__about-logo"
							src="<?=SITE_TEMPLATE_PATH?>/assets/img/logo.svg"
							alt=""
						>

						<div class="mainpage__about-content-des">
							<?if(LANGUAGE_ID == "ru"):?>
							<p class="mainpage__about-text">
								<b>Акционерное общество «Самрук-Қазына»</b> создано в соответствии с Указом Президента Республики Казахстан от 13 октября 2008 года № 669 «О некоторых мерах по конкурентоспособности и устойчивости национальной экономики»
							</p>
							<p class="mainpage__about-text">
								<b>Постановление Правительства Республики Казахстан</b> от 17 октября 2008 года № 962 «О мерах по реализации Указа Президента Республики Казахстан от 13 октября 2008 года № 669» путем слияния акционерных обществ «Фонд устойчивого развития «Қазына» и «Казахстанский холдинг по управлению государственными активами «Самрук»
							</p>
							<?elseif(LANGUAGE_ID == "kz"):?>
							<p class="mainpage__about-text">
								<b>Самұрық-Қазына» Ұлттық әл-ауқат қоры» Акционерлік</b> қоғамы Қазақстан Республикасы Президентінің 13 қазан 2008 жылғы (№ 669) Жарлығына және
							</p>
							<p class="mainpage__about-text">
								<b>Қазақстан Республикасы Үкіметінің</b> 17 қазан 2008 жылғы (№ 962) қаулысына сәйкес құрылды. Қор мемлекеттік екі алпауыттың: «Қазына» орнықты даму қоры» акционерлік қоғамы мен «Самұрық» мемлекеттік активтерді басқару жөніндегі қазақстандық холдингін біріктіру жолымен құрылды.
							</p>
							<?elseif(LANGUAGE_ID == "en"):?>
							<p class="mainpage__about-text">
								<b>Sovereign Wealth Fund Samruk-Kazyna Joint Stock Company</b> was created in accordance with the Presidential Decree No. 668 of 13 October 2008 and Government Resolution No. 962 of 17 October 2008.
							</p>
							<p class="mainpage__about-text">
								<b>The Fund was created through a merger of two large state conglomerates:</b> Sustainable Development Fund Kazyna and Kazakhstan’s Holding for Management of State Assets Samruk.
							</p>
							<?endif?>
						</div>
					</div>
				</div>

				<div class="col-12">
					<div class="title__line">
						<img
							class="title__line-border"
							src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
							alt=""
						>

						<img
							class="title__line-center"
							src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
							alt=""
						>

						<img
							class="title__line-border"
							src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
							alt=""
						>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="financial__performance">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="title">
						<?if(LANGUAGE_ID == "ru"):?>
						<h2 class="title__text">Финансовые показатели за 3 месяца 2020 года</h2>
						<p class="title__description">(цифры указаны в млрд. тенге)</p>
						<?elseif(LANGUAGE_ID == "kz"):?>
						<h2 class="title__text">2020 жылдың 3 ай қорытындысы бойынша Қаржы көрсеткіштері</h2>
						<p class="title__description">(цифры указаны в млрд. тенге)</p>
						<?elseif(LANGUAGE_ID == "en"):?>
						<h2 class="title__text">Financial KPIs for 3 months of 2020</h2>
						<p class="title__description">(цифры указаны в млрд. тенге)</p>
						<?endif?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<div class="performance">
						<div class="performance__indicators">
							<div class="performance__indicator performance__indicator_proceeds"><?=Loc::getMessage('SALES_BLN');?></div>

							<div class="performance__indicator performance__indicator_net"><?=Loc::getMessage('NET_INCOME');?></div>
						</div>

						<div class="performance__list">
							<div
								class="performance__item"
								v-for="(item, index) in financialPerformance[$i18n.locale]"
								:key="index"
							>
								<div class="performance__chart">
									<svg
										viewBox="0 0 36 36"
										transform="rotate(-90)"
									>
										<circle
											cx="18"
											cy="18"
											r="15.92356687898"
											fill="transparent"
											:stroke="item.performance.p &gt; item.performance.n ? '#17335D' : '#009EE3'"
											stroke-width="2"
										></circle>

										<circle
											cx="18"
											cy="18"
											r="15.92356687898"
											fill="transparent"
											:stroke="item.performance.p &gt; item.performance.n ? '#009EE3' : '#17335D'"
											stroke-width="3"
											:stroke-dasharray="(item.performance.p &gt; item.performance.n ? (item.performance.n*100/item.performance.p &lt; 3 ? 3 : item.performance.n*100/item.performance.p ) : (item.performance.p*100/item.performance.n &lt; 3 ? 3 : item.performance.p*100/item.performance.n)) + ' 100'"
											stroke-dashoffset="0"
										></circle>
									</svg>

									<div class="performance__text">
										<span>
											{{item.performance.p.toString().replace('.', ',')}}
										</span>

										<span class="net">
											{{item.performance.n.toString().replace('.', ',')}}
										</span>
									</div>
								</div>

								<div
									class="performance__title"
									v-html="item.title"
								></div>
							</div>
						</div>

						<a
							class="btnwrap"
							target="_blank"
						>
							<div class="btn  undefined">
								<p class="btn__text">Все компании</p>
							</div>
						</a>

						<div class="title__line">
							<img
								class="title__line-border"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
								alt=""
							>

							<img
								class="title__line-center"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
								alt=""
							>

							<img
								class="title__line-border"
								src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
								alt=""
							>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="financial__reports-plans">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="reports-plans">
						<div class="reports-plans__tabs">
							<div class="reports-plans__tabs-actions">
								<div
									class="reports-plans__tabs-btn reports-plans__tabs-btn_left"
									@click="reportsPlansActiveTab &gt; 0 ? reportsPlansActiveTab-- : reportsPlansActiveTab = reportsPlans[$i18n.locale].length - 1"
								></div>

								<div
									class="reports-plans__tabs-btn reports-plans__tabs-btn_right"
									@click="reportsPlansActiveTab &lt; reportsPlans[$i18n.locale].length - 1 ? reportsPlansActiveTab++ : reportsPlansActiveTab = 0"
								></div>
							</div>

							<div
								class="reports-plans__tabs-item"
								v-for="(item, index) in reportsPlans[$i18n.locale]"
								:key="index"
								@click="()=&gt;reportsPlansActiveSet(index)"
								:class="reportsPlansActiveTab==index? 'active' : ''"
							>
								<span id="calculator">{{item.title}}</span>
							</div>
						</div>
						<div class="reports-plans__content">
							<h5 class="reports-plans__heading"><?=Loc::getMessage('REPORTS_AND_PLANS');?></h5>

							<div
								class="reports-plans__info-block"
								v-for="(item, index) in reportsPlans[$i18n.locale]"
								:key="index"
								v-if="reportsPlansActiveTab==index"
							>
								<div class="reports-plans__list">
									<a
										class="reports-plans__docs"
										v-for="(doc, index) in item.info"
										:key="index"
										:href="doc.link"
									>
										<div class="iconbtn iconbtn_right iconbtn_darktext">
											<div class="iconbtn__content">
												<div class="iconbtn__text">{{doc.title}}</div>
											</div>

											<div class="iconbtn__icon">
												<img
													class="iconbtn__icon-src"
													src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/doc.svg"
													alt=""
												>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>