<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();}
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
$curPage = $APPLICATION->GetCurPage(true);
$assets = \Bitrix\Main\Page\Asset::getInstance();

$langRequest = LANGUAGE_ID;
if($langRequest == "ru") {
	$langRequest = "?lang=ru";
}elseif($langRequest == "en") {
	$langRequest = "?lang=en";
}elseif($langRequest == "kz") {
	$langRequest = "?lang=kz";
}
$lang = LANGUAGE_ID;
if(LANGUAGE_ID=="kz") $lang="kk";
?>
<html lang="<?=$lang?>">
<head>
    <meta charset="UTF-8"></meta>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
    <title><?$APPLICATION->ShowTitle();?></title>
	<?
	$assets->addString('<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"></link>');
	$assets->addCss(SITE_TEMPLATE_PATH . '/assets/css/main.min.css');
	$assets->addString('<script src="https://unpkg.com/vue/dist/vue.js"></script>');
	$assets->addString('<script src="https://unpkg.com/vue-i18n/dist/vue-i18n.js"></script>');
	$assets->addString('<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>');
	$assets->addJs(SITE_TEMPLATE_PATH . '/assets/js/bundle.min.js');
	$assets->addString('<link rel="icon" href="https://sk.kz'.SITE_TEMPLATE_PATH.'/favicon.ico?v1" type="image/x-icon">');
	$assets->addString('<link rel="icon" href="https://sk.kz'.SITE_TEMPLATE_PATH.'/favicon.svg?v1" type="image/svg+xml">');

	$APPLICATION->ShowMeta("robots", false);
	$APPLICATION->ShowMeta("keywords", false);
	$APPLICATION->ShowMeta("description", false);
	$APPLICATION->ShowMeta("og:type", false);
	$APPLICATION->ShowMeta("og:title", false);
	$APPLICATION->ShowMeta("og:description", false);
	$APPLICATION->ShowMeta("og:url", false);
	$APPLICATION->ShowMeta("og:image", false);
	$APPLICATION->ShowLink("canonical", null);
	$APPLICATION->ShowCSS(true);
	$APPLICATION->ShowHeadStrings();
	$APPLICATION->ShowHeadScripts();
	?>
</head>

<body>
    <?$APPLICATION->ShowPanel();?>
	<div id="app">
		<div class="headerwrap">
			<div class="container container_visible container_header">
				<div class="row">
					<div class="col-12">
						<header class="header">
							<div class="header__mobile">
								<div class="hamburger  hamburger--collapse">
									<div class="hamburger-box">
										<div class="hamburger-inner"></div>
									</div>
								</div>
							</div>
							<a href="/">
								<img class="header__logo" src="<?=SITE_TEMPLATE_PATH?>/assets/img/logo.svg" alt="">
							</a>
							<nav class="header__menu">
								<div class="header__menu-item" href="/about-fund/">
									 <a class="header__menu-item-text" href="/about-fund/<?=$langRequest?>"><?=Loc::getMessage('MENU_TOP_ABOUT_FUND');?></a>
									<div class="header__menu-dropdown">
										<a href="/about-fund/corporate-governance/<?=$langRequest?>">
											<p class="header__menu-dropdown-item"><?=Loc::getMessage('MENU_TOP_ABOUT_FUND_CORPORATE_GOVERNANCE');?></p>
										</a>
										<a href="/about-fund/corporate-governance/management/<?=$langRequest?>">
											<p class="header__menu-dropdown-item"><?=Loc::getMessage('MENU_TOP_ABOUT_FUND_CORP_2');?></p>
										</a>
										<a href="/about-fund/corporate-governance/committee/<?=$langRequest?>">
											<p class="header__menu-dropdown-item"><?=Loc::getMessage('MENU_TOP_ABOUT_FUND_CORP_3');?></p>
										</a>
										<a href="/about-fund/regulatory-and-legal/<?=$langRequest?>">
											<p class="header__menu-dropdown-item"><?=Loc::getMessage('MENU_TOP_ABOUT_FUND_LEGAL_FRAMEWORK');?></p>
										</a>
										<a href="/investors/fund-ratings/<?=$langRequest?>">
											<p class="header__menu-dropdown-item"><?=Loc::getMessage('MENU_TOP_ABOUT_FUND_RATINGS');?></p>
										</a>
										<a href="/investors/financial-performance/<?=$langRequest?>">
											<p class="header__menu-dropdown-item"><?=Loc::getMessage('MENU_TOP_ABOUT_FUND_FINANCIAL_PERFORMANCE');?></p>
										</a>
										<a href="/investors/sustainable-development/<?=$langRequest?>">
											<p class="header__menu-dropdown-item"><?=Loc::getMessage('MENU_TOP_ABOUT_FUND_SUSTAINABLE_DEVELOPMENT');?></p>
										</a>
										<a href="/contacts/<?=$langRequest?>">
											<p class="header__menu-dropdown-item"><?=Loc::getMessage('MENU_TOP_ABOUT_FUND_CONTACTS');?></p>
										</a>
									</div>
								</div>
								<a href="/investors/<?=$langRequest?>">
									<p class="header__menu-item"><?=Loc::getMessage('MENU_TOP_INVESTORS');?></p>
								</a>
								<a href="/investors/portfolio-companies/<?=$langRequest?>">
									<p class="header__menu-item"><?=Loc::getMessage('MENU_TOP_PORTFOLIO');?></p>
								</a>
								<a href="https://qsamruk.kz/" target="blank">
									<p class="header__menu-item"><?=Loc::getMessage('MENU_TOP_CAREER');?></p>
								</a>
								<a href="https://zakup.sk.kz/" target="blank">
									<p class="header__menu-item"><?=Loc::getMessage('MENU_TOP_PURCHASES');?></p>
								</a>
							</nav>
							<div class="header__right">
		
								 <div class="header__lang">
									<p class="header__lang-item" v-html="currentAppLang"></p>
			
									<img
										class="header__lang-icon"
										src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/lang-arrow.svg"
										alt=""
									>
			
									<div class="header__lang-dropdown">
										<a
											class="header__lang-dropdown-item"
											href="?lang=ru"
											v-if="lang!='ru'"
										>Рус</a>
		
										<a
											class="header__lang-dropdown-item"
											href="?lang=kz"
											v-if="lang!='kk'"
										>Қаз</a>
		
										<a
											class="header__lang-dropdown-item"
											href="?lang=en"
											v-if="lang!='en'"
										>Eng</a>
									</div>
								</div>
								<div class="header__search header__search-main">
									<a :href="$t('messages.searchLink')" target="_blank">
										<img class="header__search-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/search.svg" alt="">
									</a>
								</div>
							</div>
						</header>
					</div>
				</div>
			</div>
		</div>