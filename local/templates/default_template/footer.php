<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {die();}
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);?>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="footer__content">
                        <p class="footer__text">
                            © 2009-<?= date('Y') ?> <?=Loc::getMessage("NAME_COMPANY");?>
                        </p>

                        <div class="footer__social">
                            <a class="footer__social-item" href="https://www.facebook.com/SamrukKazyna/" target="_blank">
                                <img class="footer__social-item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/facebook.png" alt="">
                            </a>
                            <a class="footer__social-item" href="https://www.youtube.com/user/SKazyna" target="_blank">
                                <img class="footer__social-item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/youtube.png" alt="">
                            </a>
                            <a class="footer__social-item" href="https://twitter.com/Samruk_Kazyna" target="_blank">
                                <img class="footer__social-item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/twitter.png" alt="">
                            </a>
                            <a class="footer__social-item" href="https://www.instagram.com/samrukkazyna_official/" target="_blank">
                                <img class="footer__social-item-icon" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/instagram.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

 	 <div class="mobilemenu">
		<div class="mobilemenu__content">
			<a
				class="mobilemenu__item mobilemenu__item_mb_none"
				:href="$t('messages.menu.link1')"
				v-html="$t('messages.menu.item1')"
			></a>

			<div class="mobilemenu__list">
				<a
					class="mobilemenu__list-item"
					:href="$t('messages.menu.link2')"
					v-html="$t('messages.menu.item2')"
				></a>

				<a
					class="mobilemenu__list-item"
					:href="$t('messages.menu.link22')"
					v-html="$t('messages.menu.item22')"
				></a>

				<a
					class="mobilemenu__list-item"
					:href="$t('messages.menu.link23')"
					v-html="$t('messages.menu.item23')"
				></a>

				<a
					class="mobilemenu__list-item"
					:href="$t('messages.menu.link3')"
					v-html="$t('messages.menu.item3')"
				></a>

				<a
					class="mobilemenu__list-item"
					:href="$t('messages.menu.link4')"
					v-html="$t('messages.menu.item4')"
				></a>

				<a
					class="mobilemenu__list-item"
					:href="$t('messages.menu.link5')"
					v-html="$t('messages.menu.item5')"
				></a>

				<a
					class="mobilemenu__list-item"
					:href="$t('messages.menu.link6')"
					v-html="$t('messages.menu.item6')"
				></a>

				<a
					class="mobilemenu__list-item"
					:href="$t('messages.menu.link7')"
					v-html="$t('messages.menu.item7')"
				></a>
			</div>

			<a
				class="mobilemenu__item"
				:href="$t('messages.menu.link8')"
				v-html="$t('messages.menu.item8')"
			></a>

			<a
				class="mobilemenu__item"
				:href="$t('messages.menu.link9')"
				v-html="$t('messages.menu.item9')"
			></a>

			<a
				class="mobilemenu__item"
				:href="$t('messages.menu.link10')"
				v-html="$t('messages.menu.item10')"
			></a>

			<a
				class="mobilemenu__item"
				:href="$t('messages.menu.link11')"
				v-if="lang!='en'"
				v-html="$t('messages.menu.item11')"
			></a>
		</div>
	</div>

    <div class="loader">
        <div class="loader__logo">
            <img class="loader__logo-src" src="<?=SITE_TEMPLATE_PATH?>/assets/img/logo.svg" alt="">
            <div class="loader__loading"></div>
        </div>
    </div>
	</div>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-18632649-1']);
        _gaq.push(['_setDomainName', 'samruk-kazyna.kz']);
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();

    </script>

    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-16714781-4']);
        _gaq.push(['_setDomainName', 'sk.kz']);
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!--Логин в Метрике-->
    <!-- Yandex.Metrika counter -->
    <div class="mynone">
        <script type="text/javascript">
            (function (w, c) {
                (w[c] = w[c] || []).push(function () {
                    try {
                        w.yaCounter12028825 = new Ya.Metrika({id: 12028825, enableAll: true, webvisor: true});
                    }
                    catch (e) {
                    }
                });
            })(window, "yandex_metrika_callbacks");
        </script>
    </div>
    <script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript" defer="defer"></script>
    <noscript>
        <div><img src="//mc.yandex.ru/watch/12028825" class="myyandex" alt="yandex"/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    </body>
</html>