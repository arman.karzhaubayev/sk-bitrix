<?
$MESS["MENU_TOP_ABOUT_FUND"] = "Қор туралы";
$MESS["MENU_TOP_ABOUT_FUND_CORPORATE_GOVERNANCE"] = "Корпоративтік басқару";
$MESS["MENU_TOP_ABOUT_FUND_LEGAL_FRAMEWORK"] = "Нормативтік-құқықтық база";
$MESS["MENU_TOP_ABOUT_FUND_RATINGS"] = "Қордың рейтингтері";
$MESS["MENU_TOP_ABOUT_FUND_FINANCIAL_PERFORMANCE"] = "Қаржылық көрсеткіштер";
$MESS["MENU_TOP_ABOUT_FUND_SUSTAINABLE_DEVELOPMENT"] = "Тұрақты даму";
$MESS["MENU_TOP_ABOUT_FUND_CORP_1"] = "Корпоративтік басқару";
$MESS["MENU_TOP_ABOUT_FUND_CORP_2"] = "Қорды басқару жөніндегі кеңес";
$MESS["MENU_TOP_ABOUT_FUND_CORP_3"] = "Директорлар кеңесінің комитеттері";
$MESS["MENU_TOP_ABOUT_FUND_CONTACTS"] = "Байланыс";
$MESS["MENU_TOP_INVESTORS"] = "Инвесторлар мен стейкхолдерлерге";
$MESS["MENU_TOP_PORTFOLIO"] = "Қоржын компаниялар";
$MESS["MENU_TOP_CAREER"] = "Қордағы қызмет";
$MESS["MENU_TOP_PURCHASES"] = "Сатып алулар";
?>