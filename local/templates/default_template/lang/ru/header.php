<?
$MESS["MENU_TOP_ABOUT_FUND"] = "О Фонде";
$MESS["MENU_TOP_ABOUT_FUND_CORPORATE_GOVERNANCE"] = "Корпоративное управление";
$MESS["MENU_TOP_ABOUT_FUND_LEGAL_FRAMEWORK"] = "Нормативно-правовая база";
$MESS["MENU_TOP_ABOUT_FUND_RATINGS"] = "Рейтинги Фонда";
$MESS["MENU_TOP_ABOUT_FUND_FINANCIAL_PERFORMANCE"] = "Финансовые показатели";
$MESS["MENU_TOP_ABOUT_FUND_SUSTAINABLE_DEVELOPMENT"] = "Устойчивое развитие";
$MESS["MENU_TOP_ABOUT_FUND_CORP_1"] = "Корпоративное управление";
$MESS["MENU_TOP_ABOUT_FUND_CORP_2"] = "Совет по управлению Фондом";
$MESS["MENU_TOP_ABOUT_FUND_CORP_3"] = "Комитеты совета директоров";
$MESS["MENU_TOP_ABOUT_FUND_CONTACTS"] = "Контакты";
$MESS["MENU_TOP_INVESTORS"] = "Инвесторам и стейкхолдерам";
$MESS["MENU_TOP_PORTFOLIO"] = "Портфельные компании";
$MESS["MENU_TOP_CAREER"] = "Карьера в фонде";
$MESS["MENU_TOP_PURCHASES"] = "Закупки";
?>