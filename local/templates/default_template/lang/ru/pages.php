<?
$MESS["NAME_SITE"] = "АО «Самрук-Қазына»";
$MESS["KEYWORDS_SITE"] = "АО «Самрук-Қазына», портал, новости, Фонд, инвестиционный холдинг";
$MESS["DESCRIPTION_SITE"] = "АО «Самрук-Қазына» представляет собой коммерческую структуру — инвестиционный холдинг, миссия которого заключается в повышении национального благосостояния Республики Казахстан и обеспечении долгосрочной устойчивости для будущих поколений";
$MESS["MAIN"] = "Главная";
$MESS["NEWS_FEED"] = "Лента событий";
$MESS["ALL_NEWS"] = "Все новости";
$MESS["ABOUT_FUND"] = "О Фонде";
$MESS["ABOUT_FUND_TEXT_TOP"] = "Акционерное общество  «Самрук-Қазына» основано в 2008 году Указом Президента Республики Казахстан. Единственным акционером Фонда является Правительство Республики Казахстан.";
$MESS["ABOUT_FUND_TEXT_BOTTOM"] = "Фонд представляет собой коммерческую структуру — инвестиционный холдинг, миссия которого заключается в повышении национального благосостояния Республики Казахстан и обеспечении долгосрочной устойчивости для будущих поколений.";
$MESS["LEARN_MORE"] = "Подробнее";

//ABOUT FOUND /about-found.php
$MESS["DEVELOPMENT_STRATEGY_OF_SAMRUK_KAZYNA_SWF_JSC"] = "Стратегия развития АО «Самрук-Қазына»";
$MESS["DEVELOPMENT_STRATEGY_OF_SAMRUK_KAZYNA_SWF_JSC_TEXT_LEFT_TOP"] = "Миссия АО «Самрук-Қазына»";
$MESS["DEVELOPMENT_STRATEGY_OF_SAMRUK_KAZYNA_SWF_JSC_TEXT_LEFT_BOTTOM"] = "заключается в повышении национального благосостояния Республики Казахстан и обеспечении долгосрочной устойчивости для будущих поколений.";
$MESS["DEVELOPMENT_STRATEGY_OF_SAMRUK_KAZYNA_SWF_JSC_TEXT_RIGHT"] = "Стратегия развития «Самрук- Қазына» до 2028 года одобрена Советом по управлению Фондом и утверждена Единственным Акционером в 2018 году.";

$MESS["FUNDS_STRATEGIC_GOALS"] = "Стратегические цели Фонда";
$MESS["IMPROVEMENT_OF_COMPANIES_PROFITABILITY"] = "Повышение рентабельности компаний";
$MESS["IMPROVEMENT_OF_COMPANIES_PROFITABILITY_TEXT"] = "Повышение рентабельности портфельных компаний является одной из основных задач Фонда. Планируется обеспечить ряд мер по укреплению финансовой устойчивости и повышению операционной эффективности компаний, оптимизации и реинжинирингу бизнес-процессов, а также обеспечению синергии между портфельными компаниями Фонда и повышению их инвестиционной привлекательности";
$MESS["OPTIMIZATION_OF_PORTFOLIO_STRUCTURE"] = "Оптимизация структуры портфеля";
$MESS["OPTIMIZATION_OF_PORTFOLIO_STRUCTURE_TEXT"] = "Оптимизация структуры портфеля является важным шагом для перехода Фонда к активному управлению портфелем инвестиций. Реструктуризация текущего портфеля активов Фонда, пересмотр инвестиционных проектов, сокращение количества юридических лиц и уровней управления, а также выход Фонда из нестратегических активов позволят Фонду эффективно перераспределить свои ресурсы в целях повышения стоимости портфеля и увеличения его доходности.";
$MESS["INCOME_DIVERSIFICATION"] = "Диверсификация доходов";
$MESS["INCOME_DIVERSIFICATION_TEXT"] = "
<p class=\"tabinfo__text\">Активное управление активами позволит Фонду повысить доходность портфеля и обеспечить его диверсификацию в различные отрасли, отдавая приоритет активам/проектам с потенциальным мультипликативным эффектом для экономики Казахстана и с большой доходностью.</p>
<p class=\"tabinfo__text\"><span>Инвестиции в Республике Казахстан</span><br>Фонд осуществляет инвестиции в пределах страны с целью развития различных секторов экономики Казахстана и проектов стратегического значения, (в соответствии с ключевыми государственными программами) принимая во внимание ограничения, установленные принципами \"желтых страниц\" и законодательными актами Республики Казахстан, и не препятствующих развитию частного сектора.</p>
<p class=\"tabinfo__text\"><span>Инвестиции за пределами Республики Казахстан</span><br>В среднесрочной и долгосрочной перспективе Стратегия развития предусматривает распределение инвестиций за пределами Республики Казахстан, не связанных с национальной экономикой и промышленностью, с целью повышения доходности, сохранения благосостояния посредством диверсификации. Фонд будет создавать партнерские отношения с ведущими суверенными фондами, а также другими инвестиционными компаниями и фондами для осуществления совместных прямых инвестиций, которые в конечном итоге будут иметь стратегическую привязку к отраслям промышленности Казахстана.</p>
";
$MESS["CORPORATE_GOVERNANCE_AND_SUSTAINABLE_DEVELOPMENT"] = "Корпоративное управление и устойчивое развитие";
$MESS["CORPORATE_GOVERNANCE_AND_SUSTAINABLE_DEVELOPMENT_TEXT"] = "
<p class=\"tabinfo__text\">Инициативы по устойчивому развитию, корпоративное управление и корпоративная культура являются основой устойчивого развития и критическими факторами, влияющими на стоимость портфеля.</p>
<p class=\"tabinfo__text\"><span>Корпоративное управление</span><br>В целях обеспечения эффективного управления активами, повышения инвестиционной привлекательности и успешного выполнения функции активного инвестора Фонду необходимо продолжить внедрение в Фонде и в портфельных компаниях передового опыта корпоративного управления в соответствии с рекомендациями Организации экономического сотрудничества и развития (ОЭСР) по корпоративному управлению.</p>
<p class=\"tabinfo__text\"><span>Развитие человеческого капитала</span><br>Использование лучших практик по развитию персонала в Фонде и портфельных компаниях за счет формирования эффективной корпоративной культуры, внедрения основополагающих принципов меритократии, трансформации людей и внедрения новых моделей поведения для «коммерческого» мышления и готовности к изменениям.</p>
<p class=\"tabinfo__text\"><span>Спонсорская деятельность и социальная ответственность</span><br>Фонд продолжит свою деятельность в части оказания спонсорской и благотворительной деятельности. Распределение помощи должно быть пересмотрено и приоритезировано исходя из потребностей общества. Опираясь на лучшие практики, Фонд будет финансировать социальные проекты, направленные на здравоохранение, образование, профессиональное развитие молодежи, развитие сообщества, окружающей среды, искусства и наследия культуры и знаний, исследования, инновации, науку и технологии, на поддержку обездоленных лиц и семей.</p>
<p class=\"tabinfo__text\"><span>Инициативы устойчивого развития</span><br>В целях эффективного и успешного управления экономическими, экологическими и социальными аспектами деятельности, Фонд будет внедрять высокие этические стандарты и строить корпоративную культуру, основанную на доверии, внедрять передовые стандарты и совершенствовать корпоративное управление, инвестировать в человеческий капитал и предпринимать все меры по обеспечению экологической стабильности.</p>
";
$MESS["FUNDS_STRATEGIC_GOALS_TEXT"] = "<p class=\"about__text\"><span>Согласно Стратегическому плану развития Республики Казахстан до 2025 года</span> Фонд намерен трансформироваться от роли исполнителя государственных инициатив и программ к роли инвестиционной компании. Данная цель подразумевает наряду с управлением деятельностью текущих портфельных компаний, активное управление портфелем инвестиций Фонда, обеспечивающего доходы от инвестиций и развивающий портфель на уровне ведущих суверенных фондов благосостояния.</p><p class=\"about__text\">Фонд намерен продолжить активно управлять и улучшать качество своего портфеля посредством реализации программы приватизации, собственной программы дивестиции и реинвестиций средств и дивидендов, новых инвестиций в Казахстане и за ее пределами.</p>";
$MESS["NEW_FUND_STRATEGY"] = "Новая стратегия Фонда на <br class=\"about__xsspan\">2018-2028 гг.";

$MESS["PORTFOLIO"] = "Портфельные компании";
$MESS["ALL_PORTFOLIO_COMPANY"] = "Все компании";
$MESS["PORTFOLIO_COMPANIES"] = "Портфельные компании";
$MESS["PORTFOLIO_COMPANIES_TEXT"] = "<p class=\"companies__text\"><span>В Группу компаний АО «Самрук-Қазына»</span> входят предприятия нефтегазового и транспортно-логистического секторов, химической и атомной промышленности, горно-металлургического комплекса, энергетики и недвижимости.</p><p class=\"companies__text\">Активы Фонда составляют порядка 69 млрд долларов США.</p>";
$MESS["INDICATORS_OF_THE_FUNDS_PORTFOLIO_COMPANIES"] = "Показатели портфельных компаний Фонда";
$MESS["NET_ASSET_VALUE"] = "Стоимость чистых активов за 2019 год (млрд $)";
$MESS["OIL_GAS"] = "Нефтегазового сектора";
$MESS["TRANSPORT_AND_LOGISTICS"] = "Транспорт и логистика";
$MESS["FINANCIAL_ASSETS"] = "Финансовые активы";
$MESS["COMMUNICATIONS"] = "Коммуникации";
$MESS["METALS_AND_MINING"] = "Горнорудная отрасль";
$MESS["ENERGY"] = "Энергетика";
$MESS["CHEMICAL_INDUSTRY"] = "Химическая отрасль";
$MESS["REAL_ESTATE"] = "Недвижимость";
$MESS["FINANCIAL_INDICATORS_OF_PORTFOLIO_COMPANIES"] = "Финансовые показатели портфельных компаний <br>АО «Самрук-Қазына» по итогам 2019 года";
$MESS["OUR_HISTORY"] = "История Фонда";

$MESS["INVESTORS_AND_STAKEHOLDERS"] = "Инвесторам и стейкхолдерам";
$MESS["SAMRUK_KAZYNA_JSC_NWF_INVESTS_IN_THREE_AREAS"] = "АО «Самрук-Қазына» осуществляет инвестиции по трем направлениям:";
$MESS["FINANCIAL_INVESTMENTS"] = "Финансовые инвестиции";
$MESS["PRIVATIZATION"] = "Приватизация";
$MESS["PRIVATIZATION_TEXT"] = "В Комплексный план приватизации на 2016-2020 годы включены 168 активов группы компаний АО «Самрук-Қазына»";
$MESS["INVESTORS_CARD_LEFT_TITLE"] = "141";
$MESS["INVESTORS_CARD_LEFT_ACTION"] = "Исполнено";
$MESS["INVESTORS_CARD_LEFT_TEXT_ONE"] = "актив - Казатомпром - размещены 25% акций на биржах AIX и LSE";
$MESS["INVESTORS_CARD_LEFT_TEXT_TWO"] = "активов реализованы";
$MESS["INVESTORS_CARD_LEFT_TEXT_THREE"] = "актива ликвидированы/ реорганизованы";
$MESS["INVESTORS_CARD_CENTRE_TITLE"] = "27";
$MESS["INVESTORS_CARD_CENTRE_ACTION"] = "В процессе";
$MESS["INVESTORS_CARD_CENTRE_TEXT_ONE"] = "в процессе реализации, в т.ч. 8 крупных активов подлежат выводу на IPO / SPO / продаже стратегическим инвесторам";
$MESS["INVESTORS_CARD_CENTRE_TEXT_TWO"] = "активов в процессе ликвидации или реорганизации";
$MESS["INVESTORS_CARD_RIGHT_TITLE"] = "8";
$MESS["INVESTORS_CARD_RIGHT_ACTION"] = "Крупных активов";
$MESS["INVESTORS_CARD_RIGHT_TEXT_ONE"] = "
<p class=\"investors-card__title\">в процессе реализации:</p>
<p class=\"investors-card__text\">АО НК «КазМунайГаз»</p>
<p class=\"investors-card__text\">АО «Эйр Астана»</p>
<p class=\"investors-card__text\">АО «Казахтелеком»</p>
<p class=\"investors-card__text\">АО «Самрук-Энерго»</p>
<p class=\"investors-card__text\">АО «Казпочта»</p>
<p class=\"investors-card__text\">АО «НГК «Тау-Кен Самрук»</p>
<p class=\"investors-card__text\">АО «Qazaq Air»</p>
<p class=\"investors-card__text\">АО «НК «Қазақстан темiр жолы»</p>
";
$MESS["INVESTORS_NEWS_CATEGORY"]="Новости фонда";
$MESS["PRIVATIZATION_TEXT_BOTTOM"] = "<span>В настоящее время портфельные компании осуществляют</span> внутреннюю подготовительную работу по повышению операционной и финансовой эффективности и определению оптимального периметра для возможного IPO.";
$MESS["YEAR"] = "год";
$MESS["DOWNLOAD"] = "скачать";
$MESS["KB"] = "кб";
$MESS["LIST_OF_SMALL_COMPANIES"] = "Список некрупных активов";
$MESS["ASSETS"] = "Активы";
$MESS["ASSETS_LIST_ONE"] = "<span>Активы,</span> подлежащие к выводу на IPO / SPO/ продаже стратегическому инвестору";
$MESS["ASSETS_LIST_TWO"] = "<span>Активы,</span> реализованные в рамках Комплексного плана приватизации";
$MESS["ASSETS_LIST_THREE"] = "<span>Активы</span> в процессе предпродажной подготовки";
$MESS["ASSETS_LIST_FOUR"] = "<span>Активы,</span> подлежащие реорганизации и ликвидации";
$MESS["PRIVATIZATION_UPDATES"] = "Новости приватизации";
$MESS["MORE_NEWS"] = "Больше новостей";
$MESS["CONTACTS"] = "Контакты";
$MESS["CONTACT_INFORMATION_ON_THE_SALE_AND_PRIVATIZATION_OF_ASSETS"] = "Контактные данные по реализации и приватизации активов";
$MESS["COMPANY_NAMES"] = "Названия компании";
$MESS["PHONE"] = "Телефон";
$MESS["CONTACTS_ON_PRIVATIZATION_OF_ASSETS_THE_GROUP_OF_THE_FUND_OF_COMPANIES_SAMRUK_KAZYNA_JSC"] = "Контакты по приватизации активов группы Фонда компаний АО «Самрук-Қазына»";
$MESS["CONTACTS_OF_COMMISSIONS_ON_ASSETS_DIVESTITURE_AND_FACILITIES_OF_SAMRUK_KAZYNA_JSC"] = "Контактные данные Комиссий по реализации активов дочерних компаний АО «Самрук-Қазына»";
$MESS["SAMRUK_KAZYNA_JSC"] = "АО «Самрук-Қазына»";
$MESS["SAMRUK_KAZYNA_CONTRACT_LLP"] = "ТОО «Самрук-Қазына Контракт»";
$MESS["CURATOR"] = "Куратор";
$MESS["SINGLE_OPERATOR"] = "Единый оператор";
$MESS["SAMRUK_ENERGY_JSC"] = "АО «Самрук-Энерго»";
$MESS["KAZMUNAYGAS_NATIONAL_COMPANY_JSC"] = "АО «НК «КазМунайГаз»";
$MESS["AIR_ASTANA_JSC"] = "АО «Air Astana»";
$MESS["KAZAKHSTAN_TEMIR_ZHOLY_NATIONAL_COMPANY_JSC"] = "АО «Қазақстан темір жолы»";
$MESS["KEGOC_JSC"] = "АО «KEGOC»";
$MESS["КАZPOST_JSC"] = "АО «Казпочта»";
$MESS["KAZAKHTELECOM_JSC"] = "АО «Казахтелеком»";
$MESS["UNITED_CHEMICAL_COMPANY_LLP"] = "ТОО «Объединенная химическая компания»";
$MESS["TAU_KEN_SAMRUK_JSC"] = "АО «Тау-Кен Самрук»";
$MESS["KAZATOMPROM_NATIONAL_COMPANY_JSC"] = "АО «НАК «Казатомпром»";

$MESS["STRATEGIC_INVESTMENT"] = "Стратегические инвестиции";
$MESS["PROJECTS"] = "Проекты";
$MESS["PROJECTS_TEXT"] = "
<p class=\"investors__text investors__text_mb investors__text_bold investors__text_l\">Проекты строительства и расширения солнечной электростанции <br> «Бурное» в Жамбылской области</p>
<p class=\"investors__text investors__text_mb\"><span>Данные проекты реализованы «Самрук-Қазына Инвест»</span> в партнерстве с компанией UG Energy Limited (Великобритания) на базе проектных компаний ТОО «Burnoye Solar-1» и ТОО «Burnoye Solar-2» с долей участия «Самрук-Қазына Инвест» 49%. Также, в рамках указанных проектов привлечено заемное финансирование ЕБРР.</p>
<p class=\"investors__text investors__text_mb\"><span>Первая и вторая очереди солнечной электростанции</span> введены в эксплуатацию в 2015 году и 2018 году соответственно.</p>
";
$MESS["PROJECTS_FACTS_LEFT_TITLE"] = "80,37";
$MESS["PROJECTS_FACTS_LEFT_VALUE"] = "млн кВт*ч";
$MESS["PROJECTS_FACTS_LEFT_TEXT"] = "За 2019 год станция «Бурное» выработала электроэнергии.";
$MESS["PROJECTS_FACTS_CENTRE_TITLE"] = "361,49";
$MESS["PROJECTS_FACTS_CENTRE_VALUE"] = "млн кВт*ч";
$MESS["PROJECTS_FACTS_CENTRE_TEXT"] = "С момента ввода в эксплуатацию в 2015 году по 31 декабря 2019 года станция «Бурное» выработала электроэнергии";
$MESS["PROJECTS_FACTS_RIGHT_TITLE"] = "78,7";
$MESS["PROJECTS_FACTS_RIGHT_VALUE"] = "млн кВт*ч";
$MESS["PROJECTS_FACTS_RIGHT_TEXT"] = "За 2019 год вторая очередь солнечной электростанции «Бурное» выработала электроэнергии.";
$MESS["PROJECTS_TEXT_BOTTOM"] = "Весь объем вырабатываемой электроэнергии выкупает <span>ТОО «Расчетно-финансовый центр по поддержке возобновляемых источников энергии»</span> на основании Закона РК «О поддержке использования возобновляемых источников энергии»";
$MESS["PROJECT_PARTNER"] = "Партнер по проекту:";
$MESS["PROJECT_PARTNER_LIST_ENERGY_LIMITED"] = "UG Energy Limited";
$MESS["PROJECT_PARTNER_LIST_ENERGY_LIMITED_TEXT"] = "
<p class=\"partner-block__text\"><span>UG Energy Limited</span> – дочерняя организация United Green (далее – UG), которая в свою очередь представляет собой международную диверсифицированную группу, осуществляющую прямые инвестиции в динамично развивающиеся отрасли, в том числе в проекты в сфере возобновляемых источников энергии.</p>
<p class=\"partner-block__text\"><span>UG участвует в реализации проектов как в качестве стратегического инвестора,</span> так и посредством предоставления услуг по реализации проектов «под ключ».</p>
<p class=\"partner-block__text\"><span>Суммарная пиковая мощность проектов, реализованных UG в сфере ВИЭ,</span> составляет более 400 МВт общей стоимостью 1,2 млрд. долл. США, в том числе более 300 млн. долл. США предоставлены в качестве инвестиций в капитал.</p>
<p class=\"partner-block__text\"><span>Офисы и представительства UG</span> расположены в Лондоне (Англия), Дубае (ОАЭ), Гамбурге (Германия), Эрбиле (Ирак).</p>
";
$MESS["PROJECT_PARTNER_LIST_HEVEL"] = "Группа компаний «Хевел»";
$MESS["PROJECT_PARTNER_LIST_HEVEL_TEXT"] = "
<p class=\"partner-block__text\"><span>В настоящее время «Самрук-Казына Инвест»</span> рассматривается возможность участия в проекте строительства в РК семи солнечных электростанций совокупной мощностью 238 МВт в партнерстве с группой компаний «Хевел» (Россия).</p>
";
$MESS["DIRECT_INVESTMENTS"] = "Прямые инвестиции";
$MESS["INVESTMENT_PROJECTS"] = "Инвестиционные проекты";
$MESS["INVESTMENT_PROJECTS_TEXT"] = "<span>В соответствии с новой Стратегией развития</span> АО «Самрук-Қазына» инвестирует в новые инициативы вместе со стратегическими инвесторами, имеющими необходимые опыт и технологии для совместного финансирования и реализации проекта. Создание интегрированного бизнеса в партнерстве с ведущими компаниями позволит АО «Самрук-Қазына» диверсифицировать портфель и создать новые компании, которые станут движущей силой структурных преобразований экономики страны";
$MESS["INVESTMENT_SECTORS"] = "Отрасли инвестирования";
$MESS["INVESTMENT_MECHANISMS"] = "Инвестиционные механизмы сотрудничества с АО «Самрук-Қазына»";
$MESS["INVESTMENT_MECHANISMS_TEXT"] = "АО «Самрук-Қазына» активно участвует в привлечении прямых иностранных инвестиций в Казахстан через взаимовыгодные инвестиционные структуры. В частности, существуют две схемы потенциального партнерства: совместное инвестирование в капитал и инвестирование через фонды прямых инвестиций с участием АО «Самрук-Қазына».";
$MESS["DECISION_MAKING_PROCESS"] = "Процесс принятия решения";
$MESS["DECISION_MAKING_PROCESS_TEXT"] = "При принятии решений АО «Самрук-Қазына» руководствуется ответственностью перед нынешним и будущим поколениями Казахстана. Мы придерживаемся строгого поэтапного подхода к каждой инвестиции в соответствии с Корпоративным Стандартом по инвестиционной деятельности.";
$MESS["RATINGS"] = "Рейтинги Фонда";
$MESS["S_P_GLOBAL_RATINGS"] = "S&P Global Ratings";
$MESS["S_P_GLOBAL_RATINGS_LIST_ONE"] = "Долгосрочный и краткосрочный кредитные рейтинги по обязательствам в иностранной валюте «BB+/B» прогноз «Стабильный»";
$MESS["S_P_GLOBAL_RATINGS_LIST_TWO"] = "Долгосрочный и краткосрочный кредитные рейтинги по обязательствам в национальной валюте «BB+/B» прогноз «Стабильный»";
$MESS["S_P_GLOBAL_RATINGS_LIST_THREE"] = "Долгосрочный рейтинг по национальной шкале «kzAA-»";
$MESS["S_P_GLOBAL_RATINGS_LIST_FOUR"] = "Приоритетные необеспеченные рейтинги внутренних облигаций «ВВ+»";
$MESS["FITCH_RATINGS"] = "Fitch Ratings";
$MESS["FITCH_RATINGS_LIST_ONE"] = "Долгосрочный РДЭ в национальной валюте «BBB» прогноз «Стабильный»";
$MESS["FITCH_RATINGS_LIST_TWO"] = "Долгосрочный РДЭ в иностранной валюте «BBB» прогноз «Стабильный»";
$MESS["FITCH_RATINGS_LIST_THREE"] = "Краткосрочный РДЭ в иностранной валюте на уровне «F2»";
$MESS["FITCH_RATINGS_LIST_FOUR"] = "Национальный долгосрочный рейтинг «AAA(kaz)» прогноз «Стабильный»";
$MESS["FITCH_RATINGS_LIST_FIVE"] = "Приоритетные необеспеченные рейтинги внутренних облигаций «AAA(kaz)»";
$MESS["RATINGS_MAP_PLACE"] = "Место";
$MESS["RATINGS_MAP_LIST_ONE"] = "Doing Business по легкости ведения бизнеса (2019)";
$MESS["RATINGS_MAP_LIST_TWO"] = "В рейтинге Doing Business по защите миноритарных инвесторов (2019)";
$MESS["RATINGS_MAP_LIST_THREE"] = "В рейтинге Doing Business по налогообложению (2019)";
$MESS["RATINGS_MAP_LIST_FOUR"] = "По глобальному индексу конкурентоспо- собности ВЭФ (2018-2019)";
$MESS["RATINGS_MAP_LIST_FIVE"] = "В рейтинге экономической свободы The Heritage Foundation (2019)";
$MESS["CORPORATE_GOVERNANCE"] = "Корпоративное управление";
$MESS["CORPORATE_GOVERNANCE_TEXT"] = "Система корпоративного управления АО «Самрук-Қазына» защищает права акционеров и предоставляет инвесторам оперативную и достоверную информацию о прибыли, принципах управления и положении дел в компании, минимизирует и устраняет потенциальные риски нарушения интересов и прав акционеров.";
$MESS["IN_THE_PART_OF_CORPORATE_GOVERNANCE_THE_FUND_IS_OPERATING_6_PROJECTS"] = "В части корпоративного управления <span>Фонд реализует 6 проектов:</span>";
$MESS["CORPORATE_GOVERNANCE_CODE"] = "Кодекс корпоративного управления АО «Самрук-Қазына»";
$MESS["CORPORATE_GOVERNANCE_CODE_TEXT"] = "Новый Кодекс корпоративного управления был принят в 2015 году после широкого и всестороннего обсуждения. С 1 января 2017 года Фонд и Компании должны раскрывать в годовых отчетах информацию о соблюдении Кодекса.";
$MESS["MANAGEMENT_BODIES"] = "Органы управления АО «Самрук-Қазына»";
$MESS["MANAGEMENT_BODIES_BOTTOM_TEXT"] = "Система корпоративного управления АО «Самрук-Қазына» включает в себя управление, контроль и ответственность органов управления в целом по Группе компаний Фонда от первого до последнего уровня:";
$MESS["MANAGEMENT_BODIES_RIGHT_TEXT"] = "<p class=\"corpinfo__text\">Единственным акционером АО «Самрук-Қазына» является Правительство Республики Казахстан, в полномочия которого входит утверждение Стратегии развития Фонда, назначение директоров и Председателя Правления, утверждение финансовых результатов и получение дивидендов;</p><p class=\"corpinfo__text\">Общее руководство деятельностью компании осуществляет Совет директоров, который делегирует решение задач текущей деятельности компании Правлению во главе с его Председателем.</p>";
$MESS["FUND_MANAGEMENT_BOARD"] = "Совет по управлению Фондом";
$MESS["FUND_MANAGEMENT_BOARD_TEXT"] = "Совет по управлению Акционерным обществом «Самрук-Қазына» является консультативно- совещательным органом, возглавляемым Первым Президентом Республики Казахстан - Елбасы. Основной задачей Совета является выработка предложений по повышению конкурентоспособности и эффективности деятельности АО «Самрук-Қазына».";
$MESS["MEMBERS_OF_THE_FUND_MANAGEMENT_BOARD"] = "Члены Совета";
$MESS["BOARD_OF_DIRECTORS"] = "Совет директоров";
$MESS["GOVERNANCE"] = "Правление";
$MESS["MANAGEMENT"] = "Руководство";
$MESS["COMMITTEES_OF_THE_BOARD_OF_DIRECTORS"] = "Комитеты совета директоров";
$MESS["AUDIT_COMMITTEE"] = "Комитет по аудиту";
$MESS["NOMINATION_AND_REMUNERATION_COMMITTEE"] = "Комитет по назначениям и вознаграждениям";
$MESS["SPECIAL_COMMITTEE"] = "Специализированный комитет";
$MESS["TRANSFORMATION_PROGRAM_OVERSIGHT_COMMITTEE"] = "Комитет по контролю за реализацией Программы трансформации";
$MESS["STRATEGY_COMMITTEE"] = "Комитет по стратегии";
$MESS["ORGANIZATIONAL_STRUCTURE"] = "Организационная структура";
$MESS["CHAIRMAN"] = "Председатель Комитета";
$MESS["MEMBER"] = "Члены Комитета";
$MESS["CONTACT_INFORMATION"] = "Контакты";
$MESS["LEGAL_ADDRESS"] = "Юридический";
$MESS["THE_ACTUAL_ADDRESS"] = "Фактический";
$MESS["OFFICE"] = "Канцелярия";
$MESS["ON_POSSIBLE_OR_HAPPENED_FRAUD_AND_CORRUPTION_FACTS"] = "По поводу случившихся или предполагаемых фактов мошенничества и коррупции";
$MESS["FOR_MEDIA_ENQUIRIES"] = "По вопросам взаимодействия со СМИ";
$MESS["GR_DEPARTMENT"] = "По вопросам взаимодействия с Правительством";
$MESS["TECHNICAL_SUPPORT_OF_PROCUREMENT_WEBSITE"] = "Техническая поддержка портала закупок";
$MESS["ON_HR_ISSUES"] = "По кадровым вопросам";
$MESS["FOR_QUESTIONS_ABOUT_THE_PRIVATIZATION_PROGRAM"] = "По вопросам Программы приватизации";
$MESS["INVESTOR_RELATIONS"] = "По вопросам взаимодействия с инвесторами";
$MESS["DIRECTOR_OF_INTERNATIONAL_COOPERATION_DEPARTMENT"] = "Директор департамента международного сотрудничества";
$MESS["HEAD_OF_INITIATIVE_SEARCH_SECTOR"] = "Руководитель Сектора по поиску инициатив";
$MESS["ON_THE_ISSUES_OF_CONTENT_AND_TECHNICAL_SUPPORT_OF_THE_SITE"] = "По вопросам контентного и технического сопровождения сайта";
$MESS["MAIL"] = "Почта";
$MESS["WEBSITE"] = "Сайт";
$MESS["CALL_CENTRE"] = "Call центр";
$MESS["ON_RECRUITMENT_OF_PERSONNEL"] = "по подбору персонала";
$MESS["FINANCIAL_PERFORMANCE"] = "Финансовые показатели";
$MESS["SALES_BLN"] = "Выручка млрд";
$MESS["NET_INCOME"] = "Чистый доход";
$MESS["REPORTS_AND_PLANS"] = "Отчеты и планы";
?>