<?
$MESS["MENU_TOP_ABOUT_FUND"] = "About SK";
$MESS["MENU_TOP_ABOUT_FUND_CORPORATE_GOVERNANCE"] = "Corporate governance";
$MESS["MENU_TOP_ABOUT_FUND_LEGAL_FRAMEWORK"] = "Legal framework";
$MESS["MENU_TOP_ABOUT_FUND_RATINGS"] = "Ratings";
$MESS["MENU_TOP_ABOUT_FUND_FINANCIAL_PERFORMANCE"] = "Financial performance";
$MESS["MENU_TOP_ABOUT_FUND_SUSTAINABLE_DEVELOPMENT"] = "Sustainable development";
$MESS["MENU_TOP_ABOUT_FUND_CORP_1"] = "Corporate governance";
$MESS["MENU_TOP_ABOUT_FUND_CORP_2"] = "Fund management board";
$MESS["MENU_TOP_ABOUT_FUND_CORP_3"] = "Committees of the board of directors";
$MESS["MENU_TOP_ABOUT_FUND_CONTACTS"] = "Contacts";
$MESS["MENU_TOP_INVESTORS"] = "Investors and stakeholders";
$MESS["MENU_TOP_PORTFOLIO"] = "Portfolio companies";
$MESS["MENU_TOP_CAREER"] = "Career opportunities";
$MESS["MENU_TOP_PURCHASES"] = "Procurements";
?>