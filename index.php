<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/pages.php");
$APPLICATION->SetTitle(Loc::getMessage("NAME_SITE"));
$APPLICATION->SetPageProperty("title", Loc::getMessage("NAME_SITE"));
$APPLICATION->SetPageProperty("description",  Loc::getMessage("DESCRIPTION_SITE"));
$APPLICATION->SetPageProperty("keywords",  Loc::getMessage("KEYWORDS_SITE"));
$APPLICATION->SetPageProperty("og:type",  "website");
$APPLICATION->SetPageProperty("og:title",  Loc::getMessage("NAME_SITE"));
$APPLICATION->SetPageProperty("og:description",  Loc::getMessage("DESCRIPTION_SITE"));
$APPLICATION->SetPageProperty("og:url",  "https://sk.kz/");
$APPLICATION->SetPageProperty("og:image",  "https://sk.kz".SITE_TEMPLATE_PATH."/thumb.jpg");
$APPLICATION->SetPageProperty("APEX_CHART", "Y");

use Bitrix\Main\Loader;
Loader::includeModule("iblock");

if(LANGUAGE_ID == "ru") {
	$newsId = 177;
	$sliderId = 9;
} elseif(LANGUAGE_ID == "en") {
	$newsId = 175;
	$sliderId = 39;
} elseif(LANGUAGE_ID == "kz") {
	$newsId = 176;
	$sliderId = 40;
} else {
	$newsId = 177;
	$sliderId = 9;
}


$slider = \Bitrix\Iblock\ElementTable::getList(array(
	'select' => array('ID', 'IBLOCK_ID', 'ACTIVE_FROM', 'NAME', 'PREVIEW_PICTURE', 'SHOW_COUNTER'),
	'filter' => array('ACTIVE' => 'Y','IBLOCK_ID' => $sliderId, '<=ACTIVE_FROM' => date('d.m.Y H:i:s')),
	'limit' => 4,
	'order' => array('ACTIVE_FROM' => 'DESC'),
	'count_total' => 4,
	'data_doubling' => false,
	'cache' => array( 
		'ttl' => 3600,
		'cache_joins' => true
	)
));
while ($arItem = $slider->fetch()) {
    $dbProperty = \CIBlockElement::getProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort", "asc"));
    while ($arProperty = $dbProperty->GetNext()) {
        if ($arProperty['VALUE']) {
            $arItem['PROPS'][] = $arProperty['VALUE'];
        }
    }
    $arItems[] = $arItem;
}

$news = \Bitrix\Iblock\ElementTable::getList(array(
	'select' => array('ID', 'IBLOCK_ID', 'ACTIVE_FROM', 'NAME', 'PREVIEW_PICTURE', 'SHOW_COUNTER'),
	'filter' => array('ACTIVE' => 'Y','IBLOCK_ID' => $newsId, '<=ACTIVE_FROM' => date('d.m.Y H:i:s')),
	'limit' => 4,
	'order' => array('ACTIVE_FROM' => 'DESC'),
	'count_total' => 4,
	'data_doubling' => false,
	'cache' => array( 
		'ttl' => 3600,
		'cache_joins' => true
	)
));
while ($arItemNews = $news->fetch()) {
    $dbProperty = \CIBlockElement::getProperty($arItemNews['IBLOCK_ID'], $arItemNews['ID'], array("sort", "asc"), array('CODE' => 'INNDEX_PICTURES'));
    while ($arProperty = $dbProperty->GetNext()) {
        if ($arProperty['VALUE']) {
            $arItemNews['INNDEX_PICTURES'][] = $arProperty['VALUE'];
        }
    }
    $arItemsNews[] = $arItemNews;
}

global $USER;
if ($USER->IsAdmin()){
	//echo "<pre>";
	//print_r($arItems);
	//echo "</pre>";
};
?>

<div id="appmain">
    <div class="mainpage">
        <div class="mainpage__slider">
            <div class="container container_visible">
                <div class="row">
                    <div class="col-12">
						<div class="slider">
							<div class="slider__main">
								<div class="slider__arrow slider__arrow_left">
									<img
										class="slider__arrow-icon"
										src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-primary.png"
										alt=""
									>
								</div>
						
								<div class="slider__arrow slider__arrow_right">
									<img
										class="slider__arrow-icon"
										src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-primary.png"
										alt=""
									>
								</div>
								<div class="swiper-container main-swiper-container">
									<div class="swiper-wrapper">
									<?foreach($arItems as $arElements):?>
										<div class="swiper-slide">
											<div class="slider__item">
												<div class="slider__content">
													<img
														class="slider__watermark"
														src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/watermark.svg"
														alt=""
													>
											
													<img
														class="slider__quote"
														src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/dots.svg"
														alt=""
													>
											
													<div class="slider__title">
														<p class="slider__title-text"><?=$arElements['PROPS'][1];?></p>
													</div>
											
													<hr>
											
													<div class="slider__short">
														<p class="slider__short-text"><?=TruncateText($arElements['PROPS'][2],90);?></p>
													</div>
											
													<a
														class="slider__btn"
														href="<?=$arElements['PROPS'][3].$langRequest;?>"
														target="blank"
													>
														<p class="slider__btn-text">{{ $t('messages.company6') }}</p>
														<?
														$preview = CFile::ResizeImageGet($arElements['PREVIEW_PICTURE'], Array("width" => 590, "height" => 435),BX_RESIZE_IMAGE_EXACT, true);
														?>
														<img
															class="slider__btn-icon"
															src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
															alt=""
														>
													</a>
												</div>
											
												<div class="slider__image">
													<img
														class="slider__image-src"
														src="<?=$preview['src'];?>"
														alt="<?=$arElements['PROPS'][1];?>"
													>
												</div>
											
												<div class="slider__status">
													<div class="slider__status-arrow slider__status-arrow_left">
														<img
															class="slider__status-arrow-icon"
															src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-light.svg"
															alt=""
														>
													</div>
											
													<div class="slider__status-arrow slider__status-arrow_right">
														<img
															class="slider__status-arrow-icon"
															src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-light.svg"
															alt=""
														>
													</div>
													<p class="slider__status-text"><?=FormatDate("d F", MakeTimeStamp($arElements['ACTIVE_FROM']->format("d.m.Y H:i:s")));?></p>
												</div>
											</div>
										</div>
										<?endforeach?>
									</div>
								</div>
							</div>
						
							<div class="slider__nav">
								<?
								$index = 0;
								foreach($arItems as $arElements):?>
								<div class="slider__nav-item" data-index="<?=$index?>" :class=" mainNewsActiveIndex==<?=$index?>?'slider__nav-item_active':'' ">
									<p class="slider__nav-item-text"><?=FormatDate("d F", MakeTimeStamp($arElements['ACTIVE_FROM']->format("d.m.Y H:i:s")));?></p>
								</div>
								<?
								$index++;
								endforeach?>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="mainpage__news">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="title">
                            <div class="title__line">
                                <img
                                    class="title__line-border"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
                                    alt=""
                                >

                                <img
                                    class="title__line-center"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
                                    alt=""
                                >

                                <img
                                    class="title__line-border"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
                                    alt=""
                                >
                            </div>

                            <h2 class="title__text"><?=Loc::getMessage("NEWS_FEED");?></h2>
                        </div>

                        <div class="mainpage__news-list">
							<?foreach($arItemsNews as $arElements):?>
							<?
							$preview = CFile::ResizeImageGet($arElements['INNDEX_PICTURES'][0], Array("width" => 280, "height" => 180),BX_RESIZE_IMAGE_EXACT, true);
							?>
                            <a
							href="/press-center/news/<?=$arElements['ID']."/".$langRequest;?>"
                                target="blank"
                            >
                                <div class="newscard">
                                    <div class="newscard__image">
                                        <img
                                            class="newscard__image-src"
                                            src="<?=$preview['src'];?>"
                                            alt="<?=htmlspecialcharsEx($arElements['NAME']);?>"
                                        >
                                    </div>

                                    <div class="newscard__content">
                                        <div class="newscard__category">
                                            <p class="newscard__category-text">{{ $t('messages.news1') }}</p>
                                        </div>

                                        <div class="newscard__title">
                                            <p class="newscard__title-text">
                                                <?=TruncateText($arElements['NAME'], 70);?>
                                            </p>
                                        </div>

                                        <div class="newscard__footer">
                                            <p class="newscard__date"><?=$arElements['ACTIVE_FROM']->format("d.m.Y");?></p>

                                            <div class="newscard__views">
                                                <img
                                                    class="newscard__views-icon"
                                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/views.png"
                                                    alt=""
                                                >
                                                <p class="newscard__views-text"><?=$arElements['SHOW_COUNTER'];?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
							<?endforeach?>
                        </div>

                        <div class="mainpage__news-btn">
                            <a
                                class="btnwrap"
								href="/press-center/news/<?=$langRequest?>"
                            >
                                <div class="btn btn_w_auto">
                                    <p class="btn__text"><?=Loc::getMessage("ALL_NEWS");?></p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<div class="mainpage__covid">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="title">
							<div class="title__line">
								<img
									class="title__line-border"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
									alt=""
								>

								<img
									class="title__line-center"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
									alt=""
								>

								<img
									class="title__line-border"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
									alt=""
								>
							</div>

							<h2 class="title__text">
								<?=Loc::getMessage("MAINPAGE_COVID_TITLE");?>
							</h2>
						</div>

						<div class="mainpage__covid-chart">
							<div class="info__graphic-diagram">
								<div id="chart0"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

        <div class="mainpage__about">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="title">
                            <div class="title__line">
                                <img
                                    class="title__line-border"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
                                    alt=""
                                >

                                <img
                                    class="title__line-center"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
                                    alt=""
                                >

                                <img
                                    class="title__line-border"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
                                    alt=""
                                >
                            </div>

                            <h2 class="title__text"><?=Loc::getMessage("ABOUT_FUND");?></h2>
                        </div>

                        <div class="mainpage__about-content">
                            <img
                                class="mainpage__about-logo"
                                src="<?=SITE_TEMPLATE_PATH?>/assets/img/logo.svg"
                                alt=""
                            >

                            <div class="mainpage__about-content-des">
                                <p class="mainpage__about-text mainpage__about-text_bold">
                                    <?=Loc::getMessage("ABOUT_FUND_TEXT_TOP");?>
                                </p>

                                <p class="mainpage__about-text">
                                    <?=Loc::getMessage("ABOUT_FUND_TEXT_BOTTOM");?>
                                </p>

                                <div class="mainpage__about-btn">
                                    <a
                                        class="btnwrap"
										:href=" $t('messages.aboutLink') "
                                        target="_blank"
                                    >
                                        <div class="btn btn_hasicon">
                                            <p class="btn__text"><?=Loc::getMessage("LEARN_MORE");?></p>

                                            <div class="btn__icon">
                                                <img
                                                    class="btn__icon-src"
                                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.png"
                                                    alt=""
                                                >
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="mainpage__companies">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="title">
                            <div class="title__line">
                                <img
                                    class="title__line-border"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
                                    alt=""
                                >

                                <img
                                    class="title__line-center"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
                                    alt=""
                                >

                                <img
                                    class="title__line-border"
                                    src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
                                    alt=""
                                >
                            </div>

                            <h2 class="title__text"><?=Loc::getMessage("PORTFOLIO");?></h2>
                        </div>
                    </div>

                    <div class="col-1 col-xs-0">
                        <div class="mainpage__companies-arrow mainpage__companies-arrow_left">
                            <img
                                class="mainpage__companies-arrow-icon"
                                src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow.png"
                                alt=""
                            >
                        </div>
                    </div>

                    <div class="col-10 col-xs-12">
                        <div class="swiper-container maincompany-swiper-container">
                            <div class="swiper-wrapper">
                                <div
									class="swiper-slide"
									v-for="(item,index) in mainCompanies[lang]"
									:key="index"
								>
									<div class="companycard">
										<a
											:href="item.link"
											target="_blank"
										>
											<div class="companycard__image">
												<img
													class="companycard__image-src"
													:src="item.image"
													alt=""
												>

												<div class="companycard__overlay">
													<img
														class="companycard__overlay-logo"
														:src="item.hoverImage"
														alt=""
													>
												</div>
											</div>
										</a>

										<div class="companycard__content">
											<div class="companycard__title">
												<p
													class="companycard__title-text"
													v-html="item.name"
												></p>
											</div>

											<div class="companycard__indicators">
												<p class="companycard__indicators-text">
													{{ $t('messages.company1') }}

													<span v-html="item.value1"></span>
												</p>

												<p class="companycard__indicators-text">
													{{ $t('messages.company2') }}

													<span v-html="item.value2"></span>
												</p>

												<p class="companycard__indicators-text">
													{{ $t('messages.company3') }}

													<span v-html="item.value3"></span>
												</p>

												<p class="companycard__indicators-text">
													{{ $t('messages.company4') }}

													<span v-html="item.value4"></span>
												</p>

												<p
													class="companycard__indicators-text"
													v-if="item.value5"
												>
													{{ $t('messages.company5') }}

													<span v-html="item.value5"></span>
												</p>
											</div>

											<a
												:href="item.link"
												target="_blank"
											>
												<div class="companycard__btn">
													<p class="companycard__btn-text">{{ $t('messages.company6') }}</p>

													<img
														class="companycard__btn-icon"
														src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
														alt=""
													>
												</div>
											</a>
										</div>
									</div>
								</div>


                            </div>
                        </div>
                    </div>

                    <div class="col-1 col-xs-0">
                        <div class="mainpage__companies-arrow mainpage__companies-arrow_right">
                            <img
                                class="mainpage__companies-arrow-icon"
                                src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow.png"
                                alt=""
                            >
                        </div>
                    </div>

                    <div class="col-10 col-start-2 col-xs-12 col-xs-start-1">
                        <div class="mainpage__companies-nav">
                            <div class="mainpage__companies-dots"></div>

                            <div class="mainpage__companies-btn">
                                <a
                                    class="btnwrap"
									:href=" $t('messages.allCompanyLink')"
                                    target="_blank"
                                >
                                    <div class="btn btn_hasicon">
                                        <p class="btn__text"><?=Loc::getMessage("ALL_PORTFOLIO_COMPANY");?></p>

                                        <div class="btn__icon">
                                            <img
                                                class="btn__icon-src"
                                                src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/briefcase.png"
                                                alt=""
                                            >
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<!-- <div class="about__mobilehistory">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="title">
							<div class="title__line">
								<img
									class="title__line-border"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
									alt=""
								>

								<img
									class="title__line-center"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
									alt=""
								>

								<img
									class="title__line-border"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
									alt=""
								>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="mainhistory__slider">
				<div class="mainhistory__title">
					<p class="mainhistory__title-text"><?=Loc::getMessage('OUR_HISTORY');?></p>
				</div>

				<div class="swiper-container mainhistory__slider-container">
					<div class="swiper-wrapper">
						<div
							class="swiper-slide"
							v-for="(year,index) in historyMobile"
							:key="index"
						>
							<div class="mainhistory__slider-item">
								<div class="mainhistory__list">
									<div
										class="mainhistory__list-item"
										v-for="(item,index) in year[1]"
										:key="index"
									>
										<p class="mainhistory__list-item-number">0{{index+1}}</p>

										<p
											class="mainhistory__list-item-text"
											v-html="item"
										></p>
									</div>
								</div>
							</div>

							<div class="mainhistory__year">
								<div class="mainhistory__year-arrow mainhistory__year-arrow_left">
									<img
										class="mainhistory__year-arrow-icon"
										src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-long.svg"
										alt=""
									>
								</div>

								<div class="mainhistory__year-arrow mainhistory__year-arrow_right">
									<img
										class="mainhistory__year-arrow-icon"
										src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-long.svg"
										alt=""
									>
								</div>

								<p
									class="mainhistory__year-text"
									v-html="year[0]"
								></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> -->

		<!-- <div class="about__history">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="mainhistory">
							<div class="mainhistory__control">
								<div class="mainhistory__control-item mainhistory__control-item_top">
									<img
										class="mainhistory__control-item-icon"
										src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
										alt=""
									>
								</div>

								<div class="mainhistory__control-item mainhistory__control-item_bottom">
									<img
										class="mainhistory__control-item-icon"
										src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
										alt=""
									>
								</div>
							</div>

							<div class="mainhistory__years">
								<div class="swiper-container mainhistory__yearsslider">
									<div class="swiper-wrapper">
										<div
											class="swiper-slide"
											v-for="(item, index) in historyMobile"
											:key="index"
										>
											<div
												class="mainhistory__years-item"
												:data-year="item[0]"
												:class="item[0]==historyActiveYear?'mainhistory__years-item_active':''"
												@click="()=&gt;historyActiveYear=item[0]"
											>
												<p
													class="mainhistory__years-item-text"
													v-html="item[0]"
												></p>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="mainhistory__content">
								<div class="mainhistory__title">
									<p class="mainhistory__title-text">{{ $t('messages.history1') }}</p>
								</div>

								<div class="mainhistory__list">
									<div
										class="mainhistory__list-item"
										v-for="(item,index) in history[lang][historyActiveYear]"
										:key="index"
									>
										<p class="mainhistory__list-item-number">0{{index+1}}</p>

										<p
											class="mainhistory__list-item-text"
											v-html="item"
										></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> -->

		<div class="mainpage__callback">
			<div class="container container_xs_nopad">
				<div class="row">

					<div class="col-12">
						<div class="title">
							<div class="title__line">
								<img
									class="title__line-border"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
									alt=""
								>

								<img
									class="title__line-center"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bird.svg"
									alt=""
								>

								<img
									class="title__line-border"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/border.svg"
									alt=""
								>
							</div>

						</div>
					</div>

					<div class="col-12">
						 <div class="mcallback">
							<div class="mcallback__card">
								<div class="mcallback__texts">
									<p class="mcallback__text_bold mcallback__text"><?=Loc::getMessage("KKDS_MAINPAGE_CALLBACK_1"); ?> </p>
									<p class="mcallback__text"><?=Loc::getMessage("KKDS_MAINPAGE_CALLBACK_1_1"); ?></p>
								</div><img class="mcallback__img" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/compliance-details/hotline.jpg">
							</div>
							<div class="mcallback__card">
								<div class="mcallback__texts">
									<p class="mcallback__text_bold mcallback__text"><?=Loc::getMessage("KKDS_MAINPAGE_CALLBACK_2"); ?></p>
									<p class="mcallback__text"> <?=Loc::getMessage("KKDS_MAINPAGE_CALLBACK_2_1"); ?></p>
									<p class="mcallback__text"> <?=Loc::getMessage("KKDS_MAINPAGE_CALLBACK_2_2"); ?></p>
								</div><img class="mcallback__img" src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/compliance-details/garanty.jpg">
							</div>
							<div class="mcallback-contacts">
								<div class="mcallback-contacts__block"> <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/compliance-details/phone-call.svg"> 
									<div class="div"><p class="mcallback-contacts__text" > <?= Loc::getMessage("KKDS_MAINPAGE_CALLBACK_3"); ?></p> <a  href="tel:88000804747" class="mcallback-contacts__text">8 800 080 4747</a></div>
								</div>
								<div class="mcallback-contacts__block"> <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/compliance-details/whatsapp.svg">
									<div class="div"> <p class="mcallback-contacts__text" ><?=Loc::getMessage("KKDS_MAINPAGE_CALLBACK_3_1"); ?> </p><a href="https://wa.me/+77711918816" class="mcallback-contacts__text">+7 771 191 8816 </a></div>
								</div>
								<div class="mcallback-contacts__block"> <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/compliance-details/internet.svg">
									<div class="div"> <p class="mcallback-contacts__text" ><?=Loc::getMessage("KKDS_MAINPAGE_CALLBACK_3_2"); ?> </p><a href="https://sk-hotline.kz" class="mcallback-contacts__text">www.sk-hotline.kz</a></div>
								</div>
								<div class="mcallback-contacts__block"> <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/pages/compliance-details/email.svg">
									<div class="div"> <p class="mcallback-contacts__text" ><?=Loc::getMessage("KKDS_MAINPAGE_CALLBACK_3_3"); ?> </p><a href="mailto:mail@sk-hotline.kz" class="mcallback-contacts__text">mail@sk-hotline.kz</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="mainpage__history-partners">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="mpartners">
							<div class="mpartners__arrow mpartners__arrow_left">
								<img
									class="mpartners__arrow-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
									alt=""
								>
							</div>

							<div class="swiper-container mpartners__list">
								<div class="swiper-wrapper">
									<div
										class="swiper-slide"
										v-for="(item, index) in partners[$i18n.locale]"
										:key="index"
									>
										<a
											:href="item.link"
											target="_blank"
										>
											<div class="mpartners__item">
												<img
													class="mpartners__item-icon"
													:src="item.image"
													alt=""
												>
											</div>
										</a>
									</div>
								</div>
							</div>

							<div class="mpartners__arrow mpartners__arrow_right">
								<img
									class="mpartners__arrow-icon"
									src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/slider-arrow.svg"
									alt=""
								>
							</div>

							<div class="mpartners__mobile">
								<a
									:href="item.link"
									target="_blank"
									v-for="(item, index) in partners[$i18n.locale]"
									:key="index"
								>
									<div class="mpartners__item">
										<img
											class="mpartners__item-icon"
											:src="item.image"
											alt=""
										>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
