<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Результаты поиска");
?>
	    <!-- Stock -->
    <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/stock.php",
        'MODE' => 'html'
    ),
    false
	);?>

 <?$APPLICATION->IncludeComponent(
    "bitrix:main.include", "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_TEMPLATE_PATH . "/inc/top_menu.php",
        'MODE' => 'html'
    ),
    false
	);?>


<section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
    <div class="faq search-result">
        <div class="faq-wrap">
            <div class="faq-content" style="width: 100%;">

                <? $APPLICATION->IncludeComponent(
	"bitrix:search.page", 
	"sk_search", 
	array(
		"RESTART" => "N",
		"CHECK_DATES" => "Y",
		"USE_TITLE_RANK" => "N",
		"DEFAULT_SORT" => "date",
		"FILTER_NAME" => "",
		"arrFILTER" => array(
			0 => "main",
			1 => "iblock_npb",
			2 => "iblock_reports",
			3 => "iblock_purchases",
		),
		"arrFILTER_main" => array(
		),
		"arrFILTER_forum" => array(
			0 => "all",
		),
		"arrFILTER_iblock_photos" => array(
			0 => "all",
		),
		"arrFILTER_iblock_news" => array(
			0 => "all",
		),
		"arrFILTER_iblock_services" => array(
			0 => "all",
		),
		"arrFILTER_iblock_job" => array(
			0 => "all",
		),
		"arrFILTER_blog" => array(
			0 => "all",
		),
		"SHOW_WHERE" => "Y",
		"arrWHERE" => array(
			0 => "iblock_npb",
			1 => "iblock_reports",
			2 => "iblock_purchases",
		),
		"SHOW_WHEN" => "N",
		"PAGE_RESULT_COUNT" => "100",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_SHADOW" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Результаты поиска",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"USE_SUGGEST" => "N",
		"SHOW_ITEM_TAGS" => "Y",
		"TAGS_INHERIT" => "Y",
		"SHOW_ITEM_DATE_CHANGE" => "Y",
		"SHOW_ORDER_BY" => "Y",
		"SHOW_TAGS_CLOUD" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SHOW_RATING" => "N",
		"PATH_TO_USER_PROFILE" => "/forum/user/#USER_ID#/",
		"NO_WORD_LOGIC" => "N",
		"USE_LANGUAGE_GUESS" => "Y",
		"RATING_TYPE" => "",
		"COMPONENT_TEMPLATE" => "sk_search",
		"arrFILTER_iblock_npb" => array(
			0 => "all",
		),
		"arrFILTER_iblock_jobs" => array(
			0 => "all",
		),
		"arrFILTER_iblock_objects" => array(
			0 => "all",
		),
		"arrFILTER_iblock_reports" => array(
			0 => "all",
		),
		"arrFILTER_iblock_research" => array(
			0 => "all",
		),
		"arrFILTER_iblock_purchases" => array(
			0 => "all",
		),
		"arrFILTER_iblock_newsandevents" => array(
			0 => "all",
		),
		"arrFILTER_iblock_transformation" => array(
			0 => "all",
		),
		"arrFILTER_iblock_portfolio_companies" => array(
			0 => "all",
		)
	),
	false
); ?>
            </div>


            <aside class="faq-aside aside">
                <div class="aside-block">
					<?/*
                    $APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        array(
                            "AREA_FILE_SHOW" => "index",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "COMPONENT_TEMPLATE" => ".default",
                            "AREA_FILE_RECURSIVE" => "Y"
                        ),
                        false
);*/
                    ?>
                </div>
                <div class="aside-block">
                    <?
                    //Последние обновления
/*
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "list.last_updates.news.vertical.noimg",
                        Array(
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "ADD_SECTIONS_CHAIN" => "Y",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => array("SHOW_COUNTER"),
                            "FILTER_NAME" => "",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => constant('EVENTS_IBLOCK_' . SITE_ID),
                            "IBLOCK_TYPE" => "-",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "4",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "Y",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Новости",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "PROPERTY_CODE" => array("AUTHOR_LINK", ""),
                            "SET_BROWSER_TITLE" => "Y",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_STATUS_404" => "Y",
                            "SET_TITLE" => "N",
                            "SHOW_404" => "N",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER1" => "DESC",
                            "SORT_ORDER2" => "ASC",
                            "TITLE" => 'Последние обновления', // текст заголовка списка
                        )
);*/ ?>
                </div>
                <!--<div class="aside-block interview">

					<? /*$APPLICATION->IncludeComponent(
                        "bitrix:voting.form",
                        "sk_vote",
                        Array(
                            "CACHE_TIME" => "3600",
                            "CACHE_TYPE" => "A",
                            "VOTE_ID" => "2",
                            "VOTE_RESULT_TEMPLATE" => ""//"vote_result.php?VOTE_ID=#VOTE_ID#"
                        )
); */?>
                </div>-->
            </aside>
        </div>
    </div>
          </div>
        </div>
      </div>
</section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>